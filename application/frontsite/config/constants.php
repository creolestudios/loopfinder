<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
  |--------------------------------------------------------------------------
  | Constants Defined In Site
  |--------------------------------------------------------------------------
  |
 */

define('_PATH', substr(dirname(__FILE__), 0, -25));

define('_URL', substr($_SERVER['PHP_SELF'], 0, - (strlen($_SERVER['SCRIPT_FILENAME']) - strlen(_PATH))));

define('SITE_PATH', _PATH . "/");
define('SITE_URL', _URL . "/");

define('WEBSITE_URL', SITE_URL . '/');

define('DOMAIN_URL', 'http://'.$_SERVER['SERVER_NAME'].'/otojoy/');
define('DOC_ROOT',$_SERVER['DOCUMENT_ROOT'].'/otojoy/');

define('BASEURL', DOMAIN_URL);
define('gmapkey', '');

define('MAINTITLE', 'LoopFinder &#45; Hearing Loop Design and Installation');
define('ADMIN_EMAIL', 'info@otojoy.com');

define('ADMIN_URL', SITE_URL);
define('ADMIN_PATH', SITE_PATH);

//	 URL AND PATH FOR IMAGES 
define('IMAGE_PATH', DOMAIN_URL . "assets/images/");
define('IMAGE_URL', DOMAIN_URL . "assets/images/");
define('CSS_IMAGE_PATH', ADMIN_PATH . "assets/css/");
define('CSS_IMAGE_URL', DOMAIN_URL . "assets/css/");

//	 URL FOR CSS AND JS 
define('CSS_URL', DOMAIN_URL . "assets/css/");
define('PLUGIN_URL', DOMAIN_URL . "assets/lib/");

define('JS_URL', DOMAIN_URL . "assets/js/");
define("CUSTOM_JS", JS_URL . 'custom/');
define('DESIGN_PLUGIN_URL', DOMAIN_URL . "assets/lib/");

// URLS FOR FILE UPLOADED
define('UPLOADS', DOC_ROOT . "uploads/");
define('PROFILER_PATH', DOC_ROOT . "logs/");
define('UPLOADS_URL', DOMAIN_URL . "uploads/");
define('ADMIN_ASSETS_ROOT', DOC_ROOT . "admin/assets");
define('UPLOADS_FILES', UPLOADS_URL . "files/");
define('INDEX_CONSTANT', DOC_ROOT . "uploads/index.php");
define('PROJECT_LOGO', UPLOADS_URL . 'logo/logo.png');
define('LIMIT','8');
define('VENUE_LIMIT', '9');
//define('CAT_IMG_PATH', UPLOADS . 'category_image' . DS);
//define('CAT_IMG_URL', UPLOADS_URL . 'category_image/');
/*
  ############# SET DEFAULT IMAGES #################
 */


/*
  |--------------------------------------------------------------------------
  | CONSTANT MEASSAGES
  |--------------------------------------------------------------------------
  |
 */




/* * **************************************************
 * *				ERROR MEASSAGE
 * ************************************************* */
define('LOGIN_ERROR_PASSWORD', 'The <strong>password</strong> you have entered is incorrect.');
define('LOGIN_ERROR_EMAIL', 'Invalid <strong>Username/Email</strong>');
define('ACTIVATED_ERROR', 'Your account is already activated.');
define('ADMIN_ERROR', 'The <strong>email</strong> you have entered is incorrect.');
define('ACTIVE_ERROR_ADMIN', 'Account is disabled.');
define('ACTIVE_ERROR', 'You have not activated your account yet. Please go to your inbox and activate your account first.');
define('ACCESS_ERROR', 'Access not authorized for this account.');
define('SETTING_NOT_FOUND', 'Settings not found.');
define('SETTING_SUCCESS_UPDATE', 'Account settings has been updated.');
define('SETTINGS_NOT_CHANGED', 'Problem changing Settings, Please try again.');
define('OLD_PASSWORD_NOT_OK', 'Old Password you have entered is not correct.');
define('PASSWORD_SENT', 'New temporary password is sent to your email,<br /> Please change it after login.');
define('MAIL_SENT_PWD_LINK', 'Please check your inbox for email.');
define('MAIL_SENT_FAIL', 'Mail has not sent! Please try again.');
define('PASSWORD_NOT_CHANGED', 'Problem changing password, Please try again.');
define('ACTIVE_ACCOUNT', 'Your WannA IT account activated successfully.');
define('PASSWORD_CONFIRM_NOT_MATCH', 'New Password and Re-type New Password do not match.');
define('OLD_PASS_CORRECT', 'Old Password has matched!');
define("NO_RECORD_FOUNDS", "No Records found! Please Enter ");
define("ADMIN_TOKEN_EXPIRED", "Link has been expired!! ");
define("PASSWORD_CHANGED", "Password has been changed successfully!! ");
define("INVALID_MERCHANT_ID", "Invalid admin id!! ");
define("EMPTY_MERCHANT_ID", "Admin id should not be empty!! ");
define("COMMON_SUCCESS", "Query successfull");

//PASSWORD_CHANGED

/* End of file constants.php */
/* Location: ./application/config/constants.php */