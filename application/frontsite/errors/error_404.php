<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.0
Version: 1.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />
   <title>LoopFinder-Hearing Loop Design and Installation</title>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="width=device-width, initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <meta name="MobileOptimized" content="320">
   <!-- BEGIN GLOBAL MANDATORY STYLES -->          
   <link href="<?=PLUGIN_URL?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
   <link href="<?=PLUGIN_URL?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
   <link href="<?=PLUGIN_URL?>uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
   <!-- END GLOBAL MANDATORY STYLES -->
   <!-- BEGIN THEME STYLES --> 
   <link href="<?=CSS_URL?>style-metronic.css" rel="stylesheet" type="text/css"/>
   <link href="<?=CSS_URL?>style.css" rel="stylesheet" type="text/css"/>
   <link href="<?=CSS_URL?>style-responsive.css" rel="stylesheet" type="text/css"/>
   <link href="<?=CSS_URL?>plugins.css" rel="stylesheet" type="text/css"/>
   <link href="<?=CSS_URL?>themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
   <link href="<?=CSS_URL?>pages/error.css" rel="stylesheet" type="text/css"/>
   <link href="<?=CSS_URL?>custom.css" rel="stylesheet" type="text/css"/>
   <!-- END THEME STYLES -->
   <link rel="shortcut icon" href="<?=IMAGE_URL?>favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-404-3">
   <div class="page-inner">
      <img src="<?=IMAGE_URL?>pages/earth.jpg" class="img-responsive" alt="">
   </div>
   <div class="container error-404">
      <h1>404</h1>
      <h2>Houston, we have a problem.</h2>
      <p>
         Actually, the page you are looking for does not exist. 
      </p>
      <p>
         <a href="<?=BASEURL?>">Return home</a>
         <br>
      </p>
   </div>
   <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
   <!-- BEGIN CORE PLUGINS -->   
   <!--[if lt IE 9]>
   <script src="<?=PLUGIN_URL?>respond.min.js"></script>
   <script src="<?=PLUGIN_URL?>excanvas.min.js"></script> 
   <![endif]-->   
   <script src="<?=PLUGIN_URL?>jquery-1.10.2.min.js" type="text/javascript"></script>
   <script src="<?=PLUGIN_URL?>jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
   <script src="<?=PLUGIN_URL?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
   <script src="<?=PLUGIN_URL?>bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js" type="text/javascript" ></script>
   <script src="<?=PLUGIN_URL?>jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
   <script src="<?=PLUGIN_URL?>jquery.blockui.min.js" type="text/javascript"></script>  
   <script src="<?=PLUGIN_URL?>jquery.cookie.min.js" type="text/javascript"></script>
   <script src="<?=PLUGIN_URL?>uniform/jquery.uniform.min.js" type="text/javascript" ></script>
   <!-- END CORE PLUGINS -->
   <script src="<?=JS_URL?>app.js"></script>  
   <script>
      jQuery(document).ready(function() {    
         App.init();
      });
   </script>
   <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>