<?php
/*
 * @category   Categories Model
 * @package    Database activity for category
 * @author     Jignesh Virani <jignesh@creolestudios.com>
 * @author     Another Author <another@example.com>
 * @copyright  2014 Creole Studios
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    Release: 1.0 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class category_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->helper('cookie');
        $this->category = 'category';
        $this->load->database();
    }

    //#################################################################
    // Name : GetCategoryDetails
    // Purpose : To get category details
    // In Params : category id
    // Out params : category details
    //#################################################################
    public function GetCategories($offset) {

        $data = array();
        $Limit = 8;
        $temp = UPLOADS_URL . 'category_image/';
        $this->db->select("c.id,c.name,c.image");
        $this->db->from("categories c");
        $this->db->where("status","1");
       ## pagination settings
        if (isset($offset) && $offset == '-1') {
            $offset = 0;
        }
       $this->db->order_by('priority', 'asc');
       $this->db->limit($Limit,$offset);
       $query = $this->db->get();
      // echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $result = $query->result_array(); 
            $ReturnData['status'] = "1";
            $ReturnData['categories_data'] = $result ;
        } else {
            $ReturnData['status'] = "0";
        }
        return $ReturnData;
    }
      //#################################################################
    // Name : GetCategoryDetails
    // Purpose : To get category details
    // In Params : category id
    // Out params : category details
    //#################################################################
    public function GetCategoriesIndex($offset) {

        $data = array();
        $this->db->select('id,name,image,pin,priority');
        $this->db->from('categories');
        if (!empty($offset)) {
            $this->db->where('priority >', $offset);
        }
        $this->db->where("status","1");
        $this->db->order_by('priority', 'asc');
        $this->db->limit(8);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {

            //fetch the data
            $result = $query->result_array();           
        } else {
            $result = -1;
        }
        
        if ($result < LIMIT) {
            $result = -1;
        }
       
        //   var_dump($result);
        return $result;
    }

    

}

/* End of file category_model.php */
/* Location: ./application/otojoy/models/category_model.php */



