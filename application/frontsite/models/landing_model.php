<?php

/*
 * @category   Landing Model
 * @package    Database activity for venue
 * @author     Jignesh Virani <jignesh@creolestudios.com>
 * @author     Another Author <another@example.com>
 * @copyright  2014 Creole Studios
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    Release: 1.0 
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class landing_model extends CI_Model {

    function __construct() {
        parent::__construct();

        $this->load->helper('cookie');
        $this->load->database();
        $this->donation = 'donation';
    }

    //#################################################################
    // Name : GetVenuesLocation
    // Purpose : To get venues address,loaction details
    // In Params : venues name
    // Out params : venues location, address
    //#################################################################


    public function GetVenuesLocation($location) {

        $this->db->select('CONCAT(city,",",state) as city', false);
        $this->db->from("venues");
        $this->db->where("REPLACE(city,'''','') like  '{$location}%'");
        // $this->db->like('v.city', $location, 'after');
        $this->db->group_by("city");
        $query = $this->db->get();
      
        if ($query->num_rows > 0) {
            foreach ($query->result() as $row) {
                $datas[] = $row->city;
            }
            echo json_encode($datas);
        } else {
            $datas[] = 'Oops! No suggestions found. Try a different search.';
            echo json_encode($datas);
        }
    }

    //#################################################################
    // Name : getVenuesName
    // Purpose : To get venues address,loaction details
    // In Params : venues name
    // Out params : venues location, address
    //#################################################################
    public function getVenuesName($VenueName, $City) {


        $this->db->select('CONCAT(name,",",city) as name', false);
        $this->db->from('venues');
        if (isset($VenueName) && $VenueName != '') {
            //$VenueName = mysql_real_escape_string($VenueName);
            $this->db->where("REPLACE(venues.name,'''','') like  '%{$VenueName}%'");
            //$this->db->like('name', $VenueName, 'both');
        }

        if (isset($City) && $City != '') {
            $this->db->like('city', $City, 'after');
        }
        $query = $this->db->get();
        //mprd($this->db->last_query());
        if ($query->num_rows > 0) {
            foreach ($query->result() as $row) {
                $datap[] = $row->name;
            }
            echo json_encode($datap);
        } else {
            $datas[] = 'Oops! No suggestions found. Try a different search.';
            echo json_encode($datas);
        }
    }

    //#################################################################
    // Name : getLatitudeLongitude
    // Purpose : To get venues address,loaction details
    // In Params : city, venue name
    // Out params : venues latitude, longitude
    //#################################################################
    public function getLatitudeLongitude($data) {
        $city = $data['city'];
        $name = $data['location'];
        $this->db->select('latitude , longitude');
        $this->db->from('venues');
        $this->db->where("(city = '" . $city . "' or name= '" . $name . "')", null, FALSE);
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            foreach ($query->result() as $row) {
                $datar['latitude'] = $row->latitude;
                $datar['longitude'] = $row->longitude;
            }
            return $datar;
        }
    }

    //#################################################################
    // Name : GetMapData
    // Purpose : To get venues address,loaction details
    // In Params : venues categories
    // Out params : venues latitude, longitude
    //#################################################################
    public function GetMapData($Categories = '', $VenueDataArray = array(), $VenueLatLong = array(), $VenueLooped = '') {

        //serror_reporting(0);
        ##Initialize data
        $ReturnData = array();
        //$VenueLatLong
        $city = "";
        $name = "";
        $latDistance = '';
        $distance = 25;
        $venue_lat = '';
        $venue_long = '';
       
        ## process array
        if (isset($VenueDataArray) && !empty($VenueDataArray)) {
            $city = $VenueDataArray['city']; // intialize city
            $name = $VenueDataArray['venue_name']; //initialize location of venue(name)

        }
        ## process lat and long.
        if (isset($VenueLatLong) && !empty($VenueLatLong)) {

            $venue_lat = $VenueLatLong['lat'];
            $venue_long = $VenueLatLong['long'];

            $latDistance = "TRUNCATE(( 3959 * acos( cos ( radians('" . $venue_lat . "') ) *  cos( radians( venues.latitude ) ) *  cos( radians( venues.longitude ) - radians('" . $venue_long . "') ) + sin ( radians('" . $venue_lat . "') ) * sin( radians( venues.latitude ) ) ) ),2) AS `distance`";
        }
        $venueImagePath = UPLOADS_URL . 'venues/';
        $DefaultVenueImage = UPLOADS_URL . 'default_venue.png';
        $CategoriesImage = UPLOADS_URL . 'category_image/';
        // select data from venues
        $this->db->select("venues.latitude as lat,
                               venues.id,
                               CONCAT('" . $CategoriesImage . "',c.image) as categories_image,
                               venues.longitude lng,
                               venues.name as name,
                               venues.venue_type as type,
                               venues.city,
                               venues.meetupdate,
                               venues.phone_number,
                               venues.website,
                               venues.email,
                               venues.state,
                               if(venues.zipcode != '',LPAD(venues.zipcode, 5, '0'),venues.zipcode)as zipcode,
                               venues.address as address,
                               IF(venues.venue_image = '', '',CONCAT('" . $venueImagePath . "',venues.id,'/thumb/', venues.venue_image)) as venue_image,
                               venues.is_certified,
                               $latDistance,
                               GROUP_CONCAT(c.`name` SEPARATOR ', ') AS ctype,
                               GROUP_CONCAT(c.id  SEPARATOR ', ') AS cid,
                               c.pin as categories_pin,
                               c.id as categories_id,
                               c.not_looped_pin as categories_pin2", FALSE);

        $this->db->from('venues');
        $this->db->join('venue_categories', 'venues.id = venue_categories.venue_id', 'inner');
        $this->db->join('categories c', 'c.id = venue_categories.category_id', 'inner');

        // if categories is posted filter
        if (isset($Categories) && $Categories != '') {
            //if categories is checked
            $temp = explode(',', $Categories);
            $this->db->where_in('c.id', $temp);
        }

        //if venues are looped filter
        if (isset($VenueLooped) && $VenueLooped != "") {
            $venueLoopedArray = explode(',', $VenueLooped);
            $this->db->where_in('venues.venue_type', $venueLoopedArray);
        }

        if (isset($city) && !empty($city)) {
            $this->db->like('venues.city', $city);
        }
        if (isset($name) && !empty($name)) {
            $this->db->like('venues.name', $name);
        }
        // collect data by  venues id
        $this->db->group_by('venues.id');
        if ($venue_lat != '' && $venue_long != '') {

            $this->db->having("distance <= ", $distance);
            ## orderby condition
            $this->db->order_by("distance", "asc");
        }
        
        $PlaceQuery = $this->db->get();
        //echo $this->db->last_query();exit;

        if ($PlaceQuery->num_rows() > 0) {
            $result = $PlaceQuery->result_array();

            $ReturnData['status'] = "1";
            $ReturnData['map_data'] = $result;
        } else {
            $ReturnData['status'] = "0";
        }
        return $ReturnData;
    }
    //#################################################################
    // Name : GetChapterData
    // Purpose : To get chapter address,loaction details
    // In Params : chapter categories
    // Out params : chapter latitude, longitude
    //#################################################################
    
    public function GetChapterData()
    {
        ##Initialize data
        $ReturnData = array();
        $latDistance = '';
        $chapterImagePath = UPLOADS_URL . 'hlaa_chapter/';
        $DefaultVenueImage = UPLOADS_URL . 'default_venue.png';
        $CategoriesImage = UPLOADS_URL . 'category_image/';
        
        // select data from venues
        $this->db->select("h.latitude as lat,
                               h.id,
                               CONCAT('" . $CategoriesImage . "',c.image) as categories_image,
                               h.longitude lng,
                               h.name as name,
                               h.city,
                               h.meetupdate,
                               h.category_id,
                               h.phone,
                               h.website,
                               h.email,
                               h.state,
                               if(h.zipcode != '',LPAD(h.zipcode, 5, '0'),h.zipcode)as zipcode,
                               h.address as address,
                               IF(h.hlaa_image = '', '',CONCAT('" . $chapterImagePath . "',h.id,'/thumb/', h.hlaa_image)) as hlaa_image,
                               $latDistance,
                               GROUP_CONCAT(c.`name` SEPARATOR ', ') AS ctype,
                               GROUP_CONCAT(c.id  SEPARATOR ', ') AS cid,
                               c.pin as categories_pin,
                               c.id as categories_id", FALSE);

        $this->db->from('hlaa as h');
        $this->db->join("categories c", "c.id = h.category_id", 'inner');
        
         if (isset($city) && !empty($city)) {
            $this->db->like('h.city', $city);
        }
        if (isset($name) && !empty($name)) {
            $this->db->like('h.name', $name);
        }
        // collect data by  venues id
        $this->db->group_by('h.id');
        // collect data by  venues id
        $this->db->group_by('h.id');
        $PlaceQuery = $this->db->get();
        
        if ($PlaceQuery->num_rows() > 0) {
            $result = $PlaceQuery->result_array();

            $ReturnData['status'] = "1";
            $ReturnData['chapter_map_data'] = $result;
        } else {
            $ReturnData['status'] = "0";
        }
        return $ReturnData;
        
    }
    //#################################################################
    // Name : GetVenueData
    // Purpose : To get venues address,loaction details
    // In Params : venues categories
    // Out params : venues latitude, longitude
    //#################################################################
    public function GetVenueData($Categories = '', $VenueDataArray = array(), $VenueLatLong = array(), $VenueLooped = '', $offset) {

        //serror_reporting(0);
        ##Initialize data
        $ReturnData = array();
        //$VenueLatLong
        $city = "";
        $name = "";
        $latDistance = '';
        $distance = 25;
        $venue_lat = '';
        $venue_long = '';


        //set the limit
        //$Limit = VENUE_LIMIT;
        $Limit =39;

        ## process array
        if (isset($VenueDataArray) && !empty($VenueDataArray)) {
            $city = $VenueDataArray['city']; // intialize city
            $name = $VenueDataArray['venue_name']; //initialize location of venue(name)



            if (isset($city) && !empty($city)) {
                
            }
        }
        ##get venue nearby
        if (isset($VenueLatLong) && !empty($VenueLatLong)) {

            $venue_lat = $VenueLatLong['lat'];
            $venue_long = $VenueLatLong['long'];

            $latDistance = "TRUNCATE(( 3959 * acos( cos ( radians('" . $venue_lat . "') ) *  cos( radians( venues.latitude ) ) *  cos( radians( venues.longitude ) - radians('" . $venue_long . "') ) + sin ( radians('" . $venue_lat . "') ) * sin( radians( venues.latitude ) ) ) ),2) AS `distance`";
        }
        $venueImagePath = UPLOADS_URL . 'venues/';
        $DefaultVenueImage = UPLOADS_URL . 'default_venue.png';
        $CategoriesImage = UPLOADS_URL . 'category_image/';
        // select data from venues
        $this->db->select("  SQL_CALC_FOUND_ROWS venues.id as id,
                               venues.latitude as lat,
                               venues.longitude lng,
                               venues.name as name,
                                CONCAT('" . $CategoriesImage . "',c.image) as categories_image,
                               venues.venue_type as type,
                               venues.city,
                               venues.state,
                               venues.meetupdate,
                               if(venues.zipcode != '',LPAD(venues.zipcode, 5, '0'),venues.zipcode)as zipcode,
                               venues.address as address,
                               IF(venues.venue_image = '','',CONCAT('" . $venueImagePath . "',venues.id,'/thumb/', venues.venue_image)) as venue_image,
                               venues.is_certified,
                               $latDistance,
                               GROUP_CONCAT(c.`name` SEPARATOR ', ') AS ctype,
                                GROUP_CONCAT(c.id  SEPARATOR ', ') AS cid,
                               c.pin as categories_pin,
                               c.not_looped_pin as categories_pin2", FALSE);

        $this->db->from('venues');
        $this->db->join('venue_categories', 'venues.id = venue_categories.venue_id', 'inner');
        $this->db->join('categories c', 'c.id = venue_categories.category_id', 'inner');

        // if categories is posted filter
        if (isset($Categories) && $Categories != '') {
            //if categories is checked
            $temp = explode(',', $Categories);
            $this->db->where_in('c.id', $temp);
        }

        //if venues are looped filter
        if (isset($VenueLooped) && $VenueLooped != "") {
            $venueLoopedArray = explode(',', $VenueLooped);
            $this->db->where_in('venues.venue_type', $venueLoopedArray);
        }
        ##set filter by name and city.
       // if (isset($city) && !empty($city)) {
           // $this->db->like('venues.city', $city);
        //}
        if (isset($name) && !empty($name)) {
            $this->db->like('venues.name', $name);
        }
        ## pagination settings
        if (isset($offset) && $offset == '-1') {

            $offset = 0;
        }
        ## pagination settings
        ## collect data by  venues id
        $this->db->group_by('venues.id');

        ## if search by nearby
        if ($venue_lat != '' && $venue_long != '') {

            $this->db->having("distance <= ", $distance);
        }

        // $this->db->limit(9);
        $this->db->limit($Limit, $offset);
        
        ##set orderby
        ##set filter by name and city.
        if (isset($city) && !empty($city)) {
            $this->db->order_by("city IN ('$city')", "desc");
        }
        
        $PlaceQuery = $this->db->get();
         //echo $this->db->last_query();exit;
        ## calculate found rows.
        $TotalRows = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

        if ($PlaceQuery->num_rows() > 0) {
            $result = $PlaceQuery->result_array();
            $ReturnData['status'] = "1";
            $ReturnData ['total_venues'] = $TotalRows;
            $ReturnData['map_data'] = $result;
        } else {
            $ReturnData['status'] = "0";
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : getLocationsByCategories
    // Purpose : To get venues address,loaction details
    // In Params : venues categories
    // Out params : venues latitude, longitude
    //#################################################################

    public function getLocationsByCategories($Categories, $data = array(), $VenueLooped, $NearbyData = array()) {

        $lat = "";
        $lng = "";
        $distance = 25;
        if ($data != "") {
            $city = $data['city']; // intialize city
            $name = $data['venue_name']; //initialize location of venue(name)
        }

        if (isset($datalat_lng) && !empty($datalat_lng)) {
            $lat = $datalat_lng['latitude'];
            $lng = $datalat_lng['longitude'];
            // $lat = '34.4474';
            // $lng = '-119.7238';
        }
        $venueImagePath = UPLOADS_URL . 'venues/';
        $DefaultVenueImage = UPLOADS_URL . 'default_venue.png';
        // select data from venues
        $this->db->select("venues.latitude as lat,
                               venues.longitude lng,
                               venues.`name` as title,
                               venues.name as name,
                               venues.id,
                               venues.venue_type as type,
                               venues.public_comment,
                               venues.city,
                               venues.state,
                                if(venues.zipcode != '',LPAD(venues.zipcode, 5, '0'),venues.zipcode)as zipcode,
                               venues.meetupdate,
                               venues.address as address,
                               venues.phone_number as phone,
                               venues.email as email,
                               venues.website as website,
                               IF(venues.venue_image = '','" . $DefaultVenueImage . "',CONCAT('" . $venueImagePath . "',venues.id,'/thumb/', venues.venue_image)) as venue_image,
                               venues.is_certified,
                               GROUP_CONCAT(c.`name` SEPARATOR ', ') AS ctype,
                               c.pin as categories_pin,
                               c.not_looped_pin as categories_pin2,
                               TRUNCATE(( 3959 * acos( cos ( radians('" . $lat . "') ) *  cos( radians( venues.latitude ) ) *  cos( radians( venues.longitude ) - radians('" . $lng . "') ) + sin ( radians('" . $lat . "') ) * sin( radians( venues.latitude ) ) ) ),2) AS `distance`,
                               (select GROUP_CONCAT(vr.name SEPARATOR '^') FROM venue_rooms vr WHERE  vr.venue_id = venues.id) as rooms,
                               (select GROUP_CONCAT(vr.last_verified SEPARATOR '^')  last_verified  FROM venue_rooms vr WHERE  vr.venue_id = venues.id) as last_verified,
                               (select GROUP_CONCAT(vi.image SEPARATOR '^') FROM venue_images vi WHERE  vi.venue_id = venues.id) as images", FALSE);

        $this->db->from('venues');
        $this->db->join('venue_categories', 'venues.id = venue_categories.venue_id', 'inner');
        $this->db->join('categories c', 'c.id = venue_categories.category_id', 'inner');

        // if categories is posted filter
        if (isset($Categories) && !empty($Categories)) {
            //if categories is checked
            $temp = explode(',', $Categories);
            $this->db->where_in('c.id', $temp);
        }
        //if venues are looped filter
        if (isset($VenueLooped) && $VenueLooped != "") {
            $venueLoopedArray = explode(',', $VenueLooped);
            $this->db->where_in('venues.venue_type', $venueLoopedArray);
        }
        if (isset($city) && !empty($city)) {
            $this->db->like('venues.city', $city);
        }
        if (isset($name) && !empty($name)) {
            $this->db->like('venues.name', $name);
        }
        // collect data by  venues id
        $this->db->group_by('venues.id');
        if ($lat != '' && $lng != '') {

            $this->db->having("distance <= ", $distance);
        }
        $query = $this->db->get();
        // print_r($all_data);
        //   echo $this->db->last_query();exit;
        $result = $query->result();
        return $result;
    }

    //#################################################################
    // Name : getVenueDetails 
    // Purpose : To get venues address,loaction details
    // In Params : venue id
    // Out params : venues name, address, phone, email, website.    
    //#################################################################

    public function getVenueDetails($id, $cat_id_in) {

        // global declaration.
        $ReturnData = array();
        $VenuesImages = array();
        $VenueDetails = array();
        $LoopedRooms = array();
        $CategoryId = array();

        $venueImagePath = UPLOADS_URL . 'venues/';
        $VenueImage = UPLOADS_URL . 'default_venue.png';
        $CategoriesImage = UPLOADS_URL . 'category_image/';
        
        ## Get Venue data %d %b %Y %h:%i %p
        $this->db->select("v.id as venue_id,DATE_FORMAT(v.meetupdate,'%d %b %Y %h:%i %p') as meetupdate, v.venue_type as type,CONCAT('" . $CategoriesImage . "',c.image) as categories_image,v.name as venue_name,GROUP_CONCAT(distinct c.name SEPARATOR ', ') as category_name,v.venue_type,CONCAT('" . $venueImagePath . "','" . $id . "','/', v.venue_image) as venue_image,v.address,v.city,v.state, if(v.zipcode != '',LPAD(v.zipcode, 5, '0'),v.zipcode)as zipcode,,v.latitude,v.longitude,v.phone_number,v.email,v.phone_number,v.website,v.is_certified as is_certified,IF(v.certified_date = '0000-00-00' ,'',v.certified_date)as certified_date,v.public_comment as additional_information", false);
        $this->db->from('venues v');
        $this->db->join("venue_categories vc", " v.id = vc.venue_id and vc.venue_id = '" . $id . "'", "left");
        $this->db->join("categories  c", "c.id = vc.category_id", "left");
        $this->db->where("v.id", $id);
        $this->db->group_by("v.id");
        $getVenuesDetails = $this->db->get();
        // echo $this->db->last_query();exit;
        ## venue details is found then fetch looped rooms and venue images
        if ($getVenuesDetails->num_rows > 0) {

            ##Fetch vennue data
            $VenueDetails = $getVenuesDetails->result_array();

            ##Fetch venue images
            $this->db->select("IF(ISNULL(image),'" . $VenueImage . "',(CONCAT('" . $venueImagePath . "',venue_images.venue_id,'/thumb/', image))) as images", false);
            $this->db->from('venue_images');
            $this->db->where_in('venue_images.venue_id', $id);
            $getVenuesImages = $this->db->get();

            //process image data
            if ($getVenuesImages->num_rows > 0) {
                //fetch the data
                $VenuesImages = $getVenuesImages->result_array();
            }

            ## fetch looped room data
            $this->db->select("id as looped_room_id,name,IF(ISNULL(venue_rooms.last_verified),'',venue_rooms.last_verified)as last_verified", false);
            $this->db->from("venue_rooms");
            $this->db->where_in('venue_rooms.venue_id', $id);
            $VenuesLoopedRooms = $this->db->get();
            //process looped room data
            if ($VenuesLoopedRooms->num_rows > 0) {
                $LoopedRooms = $VenuesLoopedRooms->result_array();
            }
            if (isset($cat_id_in) && !empty($cat_id_in)) {
                $this->db->select("c.name");
                $this->db->from("categories c");
                $this->db->where_in("c.id", $cat_id_in);
                $categoryFromVenue = $this->db->get();
                if ($categoryFromVenue->num_rows > 0) {

                    // $CategoryId = $categoryFromVenue->result();
                    $CategoryId = $categoryFromVenue->row();
                    $CategoryId = $CategoryId->name;
                }
            }

            ## generate output
            $ReturnData['status'] = '1';
            $ReturnData['venue_details'] = $VenueDetails[0];
            ##Image data
            $ReturnData['venue_images'] = $VenuesImages;
            ## looped room data
            $ReturnData['looped_rooms'] = $LoopedRooms;
            ## category data.
            $ReturnData['category_id'] = $CategoryId;
        } else {
            $ReturnData['status'] = '0';
        }
        //mprd($ReturnData);
        return $ReturnData;
    }

    //#################################################################
    // Name : getVenueInGrid     
    // Purpose : To get venues address,loaction details
    // In Params : venue id
    // Out params : venues name, address, phone, email, website.
    //#################################################################

    public function getVenueInGrid($getDataId, $CityName) {
        $City = $CityName['city'];
        $Name = $CityName['VenueName'];
        $this->db->select('v.id,
                                        `address`,
                                        v.`name`,
                                        `zipcode`,
                                        `venue_image`,
                                        `phone_number`,
                                        `email`,
                                        `website`,
                                        `is_certified`,
                                        `city`,
                                        `certified_date`,
                                        GROUP_CONCAT(c.`name`) AS cname
                                ', false);
        $this->db->from('venues v');
        $this->db->join('venue_categories', 'v.id = venue_categories.venue_id', 'inner');
        $this->db->join('categories c', 'c.id = venue_categories.category_id', 'inner');
        if (isset($collectString) && !empty($collectString)) {
            $this->db->where_in('c.id', $collectString);
        }
        if (isset($venueIDForGridView) && !empty($venueIDForGridView)) {

            $this->db->where_in('v.id', $venueIDForGridView);
        }
        if (isset($City) && !empty($City) || isset($Name) && !empty($Name)) {
            $this->db->where("(v.city = '" . $City . "' or v.name = '" . $Name . "')", null, FALSE);
        }
        $this->db->group_by('v.id');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    //###################################  ##############################
    // Name : getVenuesCategoriesName     
    // Purpose : To get categories id and name
    // In Params : void
    // Out params : categories id, name.
    //#################################################################

    public function getVenuesCategories() {
        $this->db->select('id as categoriesId, name as categoriesName');
        $this->db->from('categories');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    //#################################################################
    // Name : getVenueFeedback
    // Purpose : To get categories id and name
    // In Params : void
    // Out params : categories id, name.
    //#################################################################

    public function getVenueFeedback($id) {
        $this->db->select("venue_rooms.name, venue_rooms.id");
        $this->db->from("venue_rooms");
        $this->db->where("venue_rooms.venue_id", $id);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    //#################################################################
    // Name : getFeedbackSave
    // Purpose : To insert data into feedback table
    // In Params : looped room id, venue id, comment, reasons, working or not
    // Out params : retrun message
    //#################################################################
    public function getFeedbackSave($SavedData) {
        // collect data from form
        $first_name = $SavedData['name'];
        $email = $SavedData['email'];
        $comment = $SavedData['comment'];
        $loop_not_working_reason = $SavedData['loop_not_working_reason'];
        $loop_room_id = $SavedData['loop_room_id'];
        $loop_working = $SavedData['loop_working'];
        // select venue id from loop room id
        $this->db->select("venue_rooms.venue_id");
        $this->db->from("venue_rooms");
        $this->db->where("venue_rooms.id", $loop_room_id);
        $query = $this->db->get();
        // echo $this->db->last_query();exit;
        $result = $query->result_array();
        if (count($result) > 0) {
            foreach ($result as $val) {
                $VenueId = $val['venue_id'];
            }
        }
        $this->db->select("customers.id");
        $this->db->from("customers");
        $this->db->where("customers.email", $email);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        $result = $query->result();
        if (count($result) > 0) {

            $CustomerId = $result[0]->id;
            // insert data of existing customer
            if ($CustomerId != "" && !empty($CustomerId)) {
                // insert table feedback table.
                $FeedbackData = array('venue_id' => $VenueId,
                    'customer_id' => $CustomerId,
                    'loop_room_id' => $loop_room_id,
                    'loop_working' => $loop_working,
                    'loop_not_working_reason' => $loop_not_working_reason,
                    'comment' => $comment,
                    'feedback_date' => date("Y-m-d H:i:s"));
                // insert in to feedback table data.
                $feedSuccess = $this->db->insert('feedback', $FeedbackData);
                if ($feedSuccess) {
                    $data['message'] = "Your feedback is send";
                    $data['success'] = "1";
                } else {
                    $data['message'] = "Sorry somthing wend wrong";
                    $data['success'] = "0";
                }
                echo json_encode($data);
            }
        } else {
            //insert data of new customer
            $CustomerNew = array('first_name' => $first_name, 'email' => $email, 'password' => '12345', 'user_type' => '2');
            // insert into customers table if customer does't exist.
            $this->db->insert('customers', $CustomerNew);
            $insert_id = $this->db->insert_id();

            // insert data if user is succesfully inserted in table.
            if ($insert_id != '' && !empty($insert_id)) {
                // insert data into feedback table with new created user.
                $FeedbackData = array('venue_id' => $VenueId,
                    'customer_id' => $insert_id,
                    'loop_room_id' => $loop_room_id,
                    'loop_working' => $loop_working,
                    'loop_not_working_reason' => $loop_not_working_reason,
                    'comment' => $comment,
                    'feedback_date' => date("Y-m-d H:i:s"));
                // insert into feedback table data.
                $feedSuccess = $this->db->insert('feedback', $FeedbackData);
                if ($feedSuccess) {
                    $data['message'] = "You feedabck has been submitted";
                    $data['success'] = "1";
                } else {
                    $data['message'] = "Sorry somthing went wrong";
                    $data['success'] = "0";
                }
                echo json_encode($data);
            }
        }
    }

    //#################################################################
    // Name : getRequestSave
    // Purpose : To insert data into votes table
    // In Params : venue_id, customer_id
    // Out params : retrun message
    //#################################################################

    public function getRequestSave($dataRequest) {


        $email = $dataRequest['email'];
        $venue_id = $dataRequest['id'];
        $name = $dataRequest['name'];
        $password = md5('12345');
        if (isset($email) && isset($venue_id)) {

            // check if customer is exist
            $this->db->select("customers.id");
            $this->db->from("customers");
            $this->db->where("customers.email", $email);
            $query = $this->db->get();

            $result = $query->result();
            if (count($result) > 0) {

                $CustomerId = $result[0]->id;
                $this->db->select('votes.id');
                $this->db->from("votes");
                //$where = "venue_id = $venue_id AND customer_id = $CustomerId";

                $this->db->where('venue_id', $venue_id);
                $this->db->where('customer_id', $CustomerId);
                $query = $this->db->get();
                $result1 = $query->result();

                if (count($result1) > 0) {
                    $data['message'] = "You have already vote this venue.";
                    $data['success'] = "1";
                } else {
                    $vote_data = array('venue_id' => $venue_id, 'customer_id' => $CustomerId, 'vote_type' => '1', 'status' => '0');
                    $insert_vote = $this->db->insert('votes', $vote_data);
                    if ($insert_vote) {
                        $data['message'] = "You have successfully vote this venue.";
                        $data['success'] = "2";
                    }
                }
                echo json_encode($data);
            } else {
                //create new user and insert vote
                //insert data of new customer
                $CustomerNew = array('email' => $email, 'password' => $password, 'user_type' => '2', 'first_name' => $name);
                // insert into customers table if customer does't exist.
                $this->db->insert('customers', $CustomerNew);
                $insert_id = $this->db->insert_id();
                //insert a vote.
                $vote_data = array('venue_id' => $venue_id, 'customer_id' => $insert_id, 'vote_type' => '1', 'status' => '0');
                $insert_vote = $this->db->insert('votes', $vote_data);
                if ($insert_vote) {

                    $data['message'] = "You have successfully vote this venue.";
                    $data['success'] = "2";
                }
                echo json_encode($data);
            }
        }
    }

    //#################################################################
    // Name : getcity
    // Purpose : To fetch data from lat long
    // In Params : venue_id, customer_id
    // Out params : retrun message
    //#################################################################

    public function getcity($datalat_lng) {

        $lat = $datalat_lng['Latitude'];
        $lan = $datalat_lng['Longitude'];

        $this->db->select('id , id, ( 3959 * acos( cos( radians() ) * cos( radians( lat ) ) * cos( radians( lng ) - radians(-122) ) + sin(             radians(37) *sin( radians( lat ) ) ) )');
        $this->db->from('venues v');
    }
    
    public function insertDonationData($Params)
    {
        //extract params
        extract($Params);
       
        $date = date('Y-m-d H:i:s');
        //prepare array for insertion
        $InsertArray = array(
            'transaction_id' => $tx,
            'amount' => $amt,
            'created_date' => $date,
            'status' => $st
            );
       
         //insert customer data
        $AddDonations = $this->db->insert($this->donation, $InsertArray);
    }

}

/* End of file landing_model.php */
/* Location: ./application/otojoy/models/landing_model.php */
?>