<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Request_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->helper('cookie');
        $this->request = 'request';
        $this->load->database();
    }

    //#################################################################
    // Name : saveRequestLoop
    // Purpose : To save requested loop data
    // In Params : name, city, state, message, user name, user email.
    // Out params : void
    //#################################################################

    public function saveRequestLoop($requestData) {

        if (isset($requestData) && !empty($requestData)) {
            $email = $requestData['email'];
            $password= md5('12345');
            $userName = $requestData['name'];
            if(isset($requestData['hasloop']) && $requestData['hasloop'] != ''){
               $hasLoop = $requestData['hasloop'];
            }
            else{  $hasLoop = '1'; }
            if(isset( $requestData['headset']) &&  $requestData['headset'] !=''){
            $headset = $requestData['headset']; }else { $headset = '3';  }
            // collect data from request loop
            $venueName = $requestData['venue_name_request'];
            $state = $requestData['state'];
            $city = $requestData['city'];
            $comment = $requestData['comment'];

            $timezone = "UTC";
            date_default_timezone_set($timezone);
            $requestDate = gmdate("Y-m-d h:i:s");
            
            $status = '0';

            if (isset($email) && !empty($email)) {
                //check user exist or not

                $this->db->select("customers.id");
                $this->db->from("customers");
                $this->db->where("customers.email", $email);
                $query = $this->db->get();

                $result = $query->result();

                if (count($result) > 0) {

                    $customerId = $result[0]->id;
                    if ($customerId != "" && !empty($customerId)) {

                        // request table data.
                        $insertData = array('customer_id' => $customerId,
                            'venue_name' => $venueName,
                            'request_type' => $hasLoop,
                            'venue_provide_headset' => $headset,
                            'state' => $state,
                            'city' => $city,
                            'comment' => $comment,
                            'request_date' => $requestDate,
                            'user_type' => '1',
                            'status' => $status,
                        );
                        // insert into request table.
                        $this->db->insert('request', $insertData);
                        //$this->session->set_flashdata('message_request', 'your request sucessfully send.');
                    }
                } else {
                    //insert data of new customer
                    $customerNew = array('first_name' => $userName, 'email' => $email, 'password' => $password);
                    // insert into customers table if customer does't exist.
                    $this->db->insert('customers', $customerNew);
                    // get last inserted id.
                    $insert_id = $this->db->insert_id();
                    // insert data if user is succesfully inserted in table.
                    if ($insert_id != '' && !empty($insert_id)) {
                        // request table data.
                        $insertData = array('customer_id' => $insert_id,
                            'venue_name' => $venueName,
                            'request_type' => $hasLoop,
                            'venue_provide_headset' => $headset,
                            'state' => $state,
                            'city' => $city,
                            'comment' => $comment,
                            'request_date' => $requestDate,
                            'user_type' => '2',
                            'status' => $status,
                        );
                        // insert into request table.
                        $this->db->insert('request', $insertData);
                       
                    }
                }
            }
        }

        return true;
    }

}

/* End of file request_model.php */
/* Location: ./application/otojoy/models/request_model.php */
?>
