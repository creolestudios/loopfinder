<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class General_model extends CI_Model {
    /*     * ****************************************************
     * Function : 
     * Purpose  : 
     * Input    : 
     * Outpur   : 
     * Author   : 
     * **************************************************** */

    public function __construct() {
        parent::__construct();
        $this->load->helper('csv');
        $this->role_tbl = 'admin';
        $this->table = 'merchants';
        $this->venue = 'venues';
    }

    /*     * ****************************************************
     * Function : getMessages
     * Purpose  : Error Or success Msg View
     * Input    : 
     * Outpur   : 
     * Author   : 
     * **************************************************** */

    function getMessages() {
        if ($this->session->flashdata('ERROR') && array_count_values($this->session->flashdata('ERROR')) > 0)
            return $this->load->view('messages/error_view');
        else if ($this->session->flashdata('SUCCESS') && array_count_values($this->session->flashdata('SUCCESS')) > 0)
            return $this->load->view('messages/success_view');
    }

    /*     * ****************************************************
     * Function : isSuperAdmin
     * Purpose  : 
     * Input    : 
     * Outpur   : 
     * Author   : 
     * **************************************************** */

    function isSuperAdmin() {
        if ($this->session->userdata('ADMINTYPE') && $this->session->userdata('ADMINTYPE') == 'super')
            return 1;
        return 0;
    }

    /*     * ****************************************************
     * Function : generateRandomString
     * Purpose  : 
     * Input    : length
     * Outpur   : 
     * Author   : 
     * **************************************************** */

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

    /*     * ****************************************************
     * Function : noRecordsHere
     * Purpose  : If not find any record
     * Input    : 
     * Outpur   : 
     * Author   : 
     * **************************************************** */

    function noRecordsHere() {
        echo '<div class="alert i_magnifying_glass yellow"><strong>Opps!!&nbsp;&nbsp;:</strong>&nbsp;&nbsp;No Records available here.</div>';
    }

    /*     * ****************************************************
     * Function : myTruncate
     * Purpose  : 
     * Input    : string,limit,break,pad
     * Outpur   : 
     * Author   : 
     * **************************************************** */

    function myTruncate($string, $limit, $break = ".", $pad = ".") { // return with no change if string is shorter than $limit
        if (strlen($string) <= $limit)
            return $string; // is $break present between $limit and the end of the string?
        if (false !== ($breakpoint = strpos($string, $break, $limit))) {
            if ($breakpoint < strlen($string) - 1) {
                $string = substr($string, 0, $breakpoint) . $pad;
            }
        }
        return $string;
    }

    /*     * ****************************************************
     * Function : getAdminBreadCrumb
     * Purpose  : 
     * Input    : arr
     * Outpur   : 
     * Author   : 
     * **************************************************** */

    function getAdminBreadCrumb($arr) {
        $seg = $this->uri->segment(1);
        $str = '';
        foreach ($arr as $k => $v) {
            if (next($arr))
                $str .= "<li><a href='../$seg/'>" . $v . "</a></li>";
            else
                $str .="<li><a class='active'>" . $v . "</a></li>";
        }
        return $str;
    }

    /*     * ****************************************************
     * Function : getAdminBreadCrumbL2
     * Purpose  : 
     * Input    : arr
     * Outpur   : 
     * Author   : 
     * **************************************************** */

    function getAdminBreadCrumbL2($arr) {
        $seg = $this->uri->segment(1);
        $str = '';
        foreach ($arr as $k => $v) {
            if (next($arr))
                $str .= "<li><a href='../../../$seg'>" . $v . "</a></li>";
            else
                $str .="<li><a class='active'>" . $v . "</a></li>";
        }
        return $str;
    }

    /*     * ****************************************************
     * Function : getLang
     * Purpose  : 
     * Input    : 
     * Outpur   : 
     * Author   : 
     * **************************************************** */

    function getLang() {
        $lang = $this->session->userdata('userLang');

        if ($this->session->userdata('userLang') && in_array($this->session->userdata('userLang'), $this->config->item('avail_languages')))
            return $this->session->userdata('userLang');

        return $this->config->item('language');
    }

    /*     * ****************************************************
     * Function : chk_admin_session
     * Purpose  : 
     * Input    : 
     * Outpur   : 
     * Author   : 
     * **************************************************** */

    function chk_admin_session() {
        $session_login = $this->session->userdata("ADMINLOGIN");
        if ($session_login == '' && $this->uri->segment(1) != 'login' && $this->uri->segment(1) != 'forgot_pass' && $this->uri->segment(1) != 'confirmation') {
            redirect('login');
            exit;
        }

        /* if($session_login == 1 && $session_login == TRUE && ($this->uri->segment(1) == '' || $this->uri->segment(1) == 'login' || $this->uri->segment(1) == 'forgot_pass'))
          redirect('dashboard'); */
        // if(!empty($session_login) && $session_login!='' && $session_login == 1 && $session_login == TRUE && ($this->uri->segment(1) == '' || $this->uri->segment(1) == 'welcome' || $this->uri->segment(1) == 'forgot_pass' || $this->uri->segment(1) == 'confirmation'))
        // 	redirect('dashboard');
    }

    /*     * ****************************************************
     * Function : chk_admin_cookie
     * Purpose  : 
     * Input    : 
     * Outpur   : 
     * Author   : 
     * **************************************************** */

    function chk_admin_cookie() {
        $loggedin = $this->chk_cookie();

        if ($loggedin === TRUE) {
            redirect('event');
        }
    }

    /*     * ****************************************************
     * Function : getVenueNameByMerchantID
     * Purpose  : 
     * Input    : 
     * Outpur   : 
     * Author   : 
     * **************************************************** */

    function getVenueNameByMerchantID() {
        $MerchantID = $this->session->userdata('M_ADMINID');
        $this->db->select('venue_name');
        $query = $this->db->get_where('venues', array('merchant_id' => $MerchantID));
        if ($query->num_rows() > 0) {
            $row = $query->row();
            return $row->venue_name;
        } else {
            return '';
        }
    }

    /*     * ****************************************************
     * Function : getEventsForCalender
     * Purpose  : EVENT CALENDAR VIEW 
     * Input    : 
     * Outpur   : 
     * Author   : 
     * **************************************************** */

    /**
     * [getEventsForCalender : EVENT CALENDAR VIEW]
     * @return [type] [description]
     */
    function getEventsForCalender() {
        $venue_id = $this->session->userdata('M_ADMINVENUEID');
        $query = $this->db->query("SELECT events.event_name as title,
    		DATE_FORMAT(events.event_start_time,'%a %b %d %Y %k:%i:%s') as start,
    		DATE_FORMAT(events.event_end_time,'%a %b %d %Y %k:%i:%s') as end,
                IF(events.event_book = '1','fc-event','fc-event-unbook') as className
    		FROM events 
			JOIN venues ON venues.id=events.venue_id
    		WHERE event_status=1 AND venue_id=$venue_id AND venue_status=1");
        if ($query->num_rows() > 0) {
            $res = $query->result_array();
            return $res;
        } else {
            return array();
        }
    }

    /*     * ****************************************************
     * Function : getVenueNameByVenueID
     * Purpose  : 
     * Input    : id
     * Outpur   : 
     * Author   : 
     * **************************************************** */

    function getVenueNameByVenueID($id) {
        $this->db->select('venue_name');
        $query = $this->db->get_where('venues', array('id' => $id));
        if ($query->num_rows() == 1) {
            $row = $query->row();
            $venue_name = $row->venue_name;
            $string = preg_replace("/[^a-zA-Z0-9_\s-]/", "", $venue_name);
            //Clean multiple dashes or whitespaces
            $string = preg_replace("/[\s-]+/", " ", $string);
            //Convert whitespaces and underscore to dash
            $cleanTitle = preg_replace("/[\s_]/", "_", $string);
            return $cleanTitle;
        } else {
            return "";
        }
    }

    /*     * ****************************************************
     * Function : GetLogoImage
     * Purpose  : 
     * Input    : VenueId
     * Outpur   : 
     * Author   : 
     * **************************************************** */
    /*
      | -------------------------------------------------------------------
      |  Function : GetLogoImage
     * Fetch logo image for venue from quote table
     * IN Params : Venue Id
     * Out Params : All modifiers data
     *
      | -------------------------------------------------------------------
     */

    function GetLogoImage($VenueId) {
        $this->db->select('quote_logo_img');
        $query = $this->db->get_where('quote_details', array('venue_id' => $VenueId));
        if ($query->num_rows() == 1) {

            $row = $query->row();
            $venue_log = $row->quote_logo_img;
            return $venue_log;
        } else {
            return "";
        }
    }

    /*     * ****************************************************
     * Function : GetAllVenueOfMerchant
     * Purpose  : 
     * Input    : MerchantId
     * Outpur   : 
     * Author   : 
     * **************************************************** */

    function GetAllVenueOfMerchant($MerchantId) {
        //define return data
        $ReturnData = array();
        $this->db->select('id as venue_id,venue_name');

        $VenueQuery = $this->db->get_where($this->venue, array('merchant_id' => $MerchantId));
        //die($this->db->last_query());
        if ($VenueQuery->num_rows() > 0) {
            $ReturnData = $VenueQuery->result_array();
            return $ReturnData;
        } else {
            return $ReturnData;
        }
    }

    //#################################################################
    // Name : getContentInfo
    // Purpose : To get privacy policy content
    // In Params : void
    // Out params : page data.
    //#################################################################

    public function getContentInfo() {

        $this->db->select("c.title,c.text");
        $this->db->from("content c");
        $Getcontent = $this->db->get();

        if ($Getcontent->num_rows() > 0) {

            //fetch the data
            $VenueData = $Getcontent->result_array();
            $ReturnData['content_data'] = $VenueData[0];
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

}

/* End of file general_model.php */
/* Location: ./application/admin/models/general_model.php */