<?php
$headerData = $this->headerlib->data();
$JsArr = array("SCRIPT" => $headerData['javascript'], 'PLUGINS' => $headerData['javascript_plugins']);
?>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <title><?= MAINTITLE; ?></title>

        <!------- BEGIN STYLESHEET   ---->
        <?php echo $headerData['stylesheets']; ?>
        <link href="<?= CSS_IMAGE_URL . 'frontsite/font-awesome.min.css' ?>" rel="stylesheet"> 
        <link href="<?= PLUGIN_URL ?>icheck/skins/all.css" rel="stylesheet"/>

        <!-- END THEME STYLESHEET -->

        <!-- BEGIN THEME STYLES --> 
        <?php //echo $headerData['themestyles']; ?>
        <!-- END THEME STYLES -->
        <!---------- favicon icon -------------->

        <link rel="shortcut icon" href="<?= IMAGE_URL ?>favicon.ico"/>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <!-- load header menu  -->
        <?php $this->load->view('include/landing_menu'); ?>
        <!-- searchbar  -->
        <?php $this->load->view('include/search'); ?>
        <!-----------------------------main section of request view --------------------------->
        <section class="contact-container">
            <div class="contact-section">
                <div class="container-fluid">
                    <div class="col-md-4 col-md-push-4 col-sm-6 col-sm-push-3">
                        <div class="contact-head">Request / Add a Loop</div>
                        <div class="contact-text">Use the form below to add a new location to the loop database.</div>

                        <?php if ($this->session->flashdata('success') != '') { ?>
                            <div id="request_message" class="alert alert-success" style="" tabindex="-1"><?php echo $this->session->flashdata('success'); ?></div>
                        <?php } ?>

                        <?php if ($this->session->flashdata('error') != '') { ?>
                            <div id="request_message" class="alert alert-error" style="" tabindex="-1"><?php echo $this->session->flashdata('error'); ?></div>
                        <?php } ?>

                        <form action="<?= BASEURL . 'requestloop/RequestLoopSave'; ?>" method="post" id="register-form" onsubmit="">
                            <div class="inner-addon left-addon">

                                <i class="fa  fa-university input-icon"></i>
                                <input type="text" name="venue_name_request" id="venue_name_request" class="form-control input" placeholder="Name of the Venue"/>
                            </div>
                            <div class="inner-addon left-addon input">
<!--                                <i class="glyphicon" style="margin-left: -2px"><img class="input-icon" src="<?php echo IMAGE_URL . 'headphone-icon.png'; ?>"/></i>--><i class="fa  fa-headphones input-icon" style="top:-3px !important; left: -3px;"></i>
                                <span class="headset-text-fix" style="left: -20px">Does the venue provide any headsets?</span>

                                <div class="radio row">
                                    <label class="radio-label1"><input type="radio" name="headset" class="radio-1" value="1"><span class="radio-img-yes"></span> Yes</label>
                                    <label class="radio-label1"><input type="radio" name="headset" class="radio-1" value="0"><span class="radio-img-no"></span> No</label>
                                    <label class="radio-label1"><input type="radio" name="headset" class="radio-1" value="3"><span class="radio-img-unknown"></span>Don't Know</label>
                                </div>

                            </div>
                            <div class="inner-addon left-addon input clearfix">
                                <div class="radio">
                                    <label class="radio-label1"><input type="radio" name="hasloop" class="radio-round" value="1"><span class="radio-img-round"></span>Has a Loop</label>
                                    <label class="radio-label1"><input type="radio" name="hasloop" class="radio-round" value="2"><span class="radio-img-round"></span>Needs a Loop</label>
                                </div>
                            </div>

                            <div class="inner-addon left-addon">

                                <i class="fa  fa-map-marker input-icon"></i>
                                <input type="text" name="city" class="form-control  input" id="city" placeholder="City" />
                            </div>
                            <div class="inner-addon left-addon">

                                <i class="fa  fa-map-marker input-icon"></i>
                                <input type="text" name="state" class="form-control input" id="state" placeholder="State" />
                            </div>
                            <div class="inner-addon left-addon">

                                <i class="fa  fa-comment input-icon"></i>
                                <textarea  class="form-control input" placeholder="Comment" rows="8" id="comment"  name="comment" ></textarea>
                            </div>
                            <div class="inner-addon left-addon">
<!--                                <i class="glyphicon"><img class="input-icon" src="<?php echo IMAGE_URL . 'user-icon.png'; ?>" /></i>-->
                                <i class="fa  fa-user input-icon"></i>
                                <input type="text" name="name"  id="name" class="form-control input" placeholder="Name" />
                            </div>
                            <div class="inner-addon left-addon">
                                <i class="fa  fa-envelope input-icon"></i>
                                <input type="text" name="email" id="email" class="form-control input" placeholder="E-Mail" />
                            </div>
                            <button type="submit" class="btn btn-general btn-contact-submit" style="width:126px !important; height:42px !important;">Submit</button>
                        </form>

                    </div>
                    <div class="col-sm-4">&nbsp;</div>
                </div>
            </div>
        </section>

        <?php $this->load->view('include/footer', $JsArr); ?>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>jquery-validation/js/jquery.validate.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#donate_link").click(function () {
                    //$("#donation_form").submit();
                    alert("Donation Coming Soon!!");
                    return false;
                });

                $('#request_message').fadeOut(3000);

                var availableTags = '<?= BASEURL . 'landing/getVenuesLocation' ?>'
                var NoResultsLabel = "Oops! No suggestions found. Try a different search.";
                $("#venue_location_serch").autocomplete({
                    source: availableTags,
                    minLength: 2,
                    select: function (event, ui) {
                        if (ui.item.value === NoResultsLabel) {
                            event.preventDefault();
                        }
                    },
                    focus: function (event, ui) {
                        if (ui.item.value === NoResultsLabel) {
                            event.preventDefault();
                        }
                    }

                });
                var availableTags1 = '<?= BASEURL . 'landing/getVenuesName'; ?>';
                $("#venue_name_search").autocomplete({
                    //source: availableTags1
                    source: function (request, response) {
                        $.getJSON(availableTags1, {location: $('#venue_location_serch').val(), term: request.term},
                        response);
                    },
                    minLength: 2,
                    select: function (event, ui) {
                        if (ui.item.value === NoResultsLabel) {
                            event.preventDefault();
                        }
                    },
                    focus: function (event, ui) {
                        if (ui.item.value === NoResultsLabel) {
                            event.preventDefault();
                        }
                    }
                });

                $('#register-form').validate({
                    rules: {
                        venue_name_request: {required: true},
                        city: {required: true},
                        state: {required: true},
                        name: {required: true, minlength: 4},
                        email: {required: true, email: true}

                    },
                    messages: {
                        venue_name_request: {
                            required: "Please enter the Venue Name."
                        },
                        city: {
                            required: "Please enter the City."
                        },
                        state: {
                            required: "Please enter the State."
                        },
                        name: {
                            required: "Please enter your name."
                        },
                        email: {required: "Please enter your e-mail address.",
                            email: "Invalid email address."}
                    },
                    submitHandler: function (form) {
                        form.submit();
                    }
                });
                $(document).on('click', '#common_submit', function (event) {
                    event.preventDefault();
                    var venue_city = $('#venue_location_serch').val();
                    var venue_name = $('#venue_name_search').val();
                    var search;
                    if (venue_city == '' && venue_name == '') {
                        window.location.href = '<?= BASEURL . 'landing/getPlace/' ?>';
                    } else if (venue_city != '' && venue_name == '')
                    {
                        //split string by comma
                        window.location.href = '<?= BASEURL . 'landing/getPlace/city/' ?>' + venue_city;
                    } else if (venue_city == '' && venue_name != '')
                    {
                        window.location.href = '<?= BASEURL . 'landing/getPlace/venue/' ?>' + venue_name;
                    } else if (venue_city != '' && venue_name != '') {

                        window.location.href = '<?= BASEURL . 'landing/getPlace/city/' ?>' + venue_city + '/venue/' + venue_name;

                    } else {
                        window.location.href = '<?= BASEURL . 'landing/getPlace/' ?>';
                    }
                });
            })

//            function getLocation_lat() {
//
//                if (navigator.geolocation) {
//                    navigator.geolocation.getCurrentPosition(function (position) {
//                        $("#latitude").val(position.coords.latitude);
//                        $("#longitude").val(position.coords.longitude);
//                        //submit form with latitude and longitude
//                        $("#form_landing").submit();
//                        $("#form_search").submit();
//                        $("#map_grid_form").submit();
//                    });
//                } else {
//                    x.innerHTML = "Geolocation is not supported by this browser.";
//                }
//            }

            $(document).on('click', '#venue_lat_log_com', function () {

                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        $("#latitude").val(position.coords.latitude);
                        $("#longitude").val(position.coords.longitude);

                        window.location.href = '<?= BASEURL . 'landing/getPlace/lat/' ?>' + position.coords.latitude + '/long/' + position.coords.longitude;
                    });
                } else {
                    x.innerHTML = "Geolocation is not supported by this browser.";
                }
            });
        </script>

    </body>
</html> 

