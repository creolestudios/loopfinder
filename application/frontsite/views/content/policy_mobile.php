<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<?php
//if (isset($content_data) && !empty($content_data)) {
//    //extract($content_data);
//}
?>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            .policy_text{
                font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
                font-size: 14px;
                line-height: 1.42857143;
                color: #333;
            }
            .policy_text h3{
                font-size: 14px !important;
            }
            
        </style>    
    </head>
    <body>
        <div class="policy_text">
            <p>Otojoy LLC (&ldquo;Otojoy&rdquo;) knows that you care how information about you is used and shared. We appreciate your trust and are committed to safeguarding your privacy and the privacy of all visitors to www.otojoy.com (&ldquo;Website&rdquo;). By visiting our Website, you are accepting the practices described in this Policy. We do reserve the right to modify our Policy from time to time, so we encourage you to check back frequently.</p>

            <h3><strong>Use of Your Information</strong></h3>

            <p>Otojoy collects and uses your personal information to operate and improve our sites and services. These uses may include providing you with more effective customer service; making our sites or services easier to use by eliminating the need for you to repeatedly enter the same information; and performing research and analysis aimed at improving our products, services and technologies.</p>

            <p>We also use your personal information to communicate with you. We may also occasionally send you product surveys or promotional mailings to inform you of other products or services available from Otojoy and its affiliates. You may notify us at any time if you do not wish to receive these offers by contacting us at info@otojoy.com.</p>

            <p>Our Website uses small files called &quot;cookies&quot; which may be attached to your web browser. Use of this technology helps us recognize you when you visit our Website and thus allows us to personalize your online experience.</p>

            <h3><strong>Children&#39;s Personal Information</strong></h3>

            <p>We do not knowingly collect personally identifiable information from users under the age of 13. If we are made aware that we have such personal information, we will take reasonable steps to delete that information.</p>

            <h3><strong>Disclosure of Information</strong></h3>

            <p>Otojoy keeps your personally identifiable information confidential. We do not disclose it to third parties or others that do not have a need to know such information except under certain circumstances. Set forth below are additional circumstances where Otojoy may share your personal information:</p>

            <ul>
                <li>Certain of our employees, independent contractors and service providers may have access to your personally identifiable information necessary for the sole purpose of helping us run our business. These employees and contractors have confidentiality and security obligations.</li>
                <li>Our affiliates, subsidiaries and third party service providers as we deem appropriate.</li>
                <li>We may disclose your personally identifiable information to another business entity should we plan to merge with or be acquired by that business entity; or to any organization involved in a merger, transfer, or sale of our assets or business.</li>
                <li>Before we disclose your personally identifiable data for other reasons, we will first obtain your consent, for example via e-mail, click-through, an online opportunity to opt-out, or otherwise.</li>
                <li>If you post any information in public areas of our Web site, such as chat rooms and message boards, it will be available to the public and we may reuse and republish such information at our discretion and in accordance with our Terms of Use.</li>
                <li>To protect and defend the rights or property of Otojoy, including its Web sites.</li>
                <li>We may also share your personally identifiable information to respond to law enforcement requests, court orders or other legal processes or if we believe that such disclosure is necessary to investigate, prevent or respond to illegal activities, fraud, physical threats to you or others, national security threats or as otherwise required by any applicable law or regulation.</li>
                <li>In the event that we disclose your personally identifiable information as set out above, we will require third party organizations that have access to your personally identifiable information to protect and maintain your personally identifiable information in compliance with the terms and conditions of this Statement.</li>
            </ul>

            <h3><strong>Prohibited Disclosures</strong></h3>

            <p>Except as set forth above, Otojoy will not disclose your personally identifiable information to any third party, including advertisers, without your explicit consent. In addition, Otojoy will not share, trade, rent, or otherwise disclose or distribute your personal information, Otojoy&rsquo;s e-mail list or address list to third parties for their promotional purposes. We use your personal information and mailing list only to send out information about products, educational events, or other relevant topics.</p>

            <h3><strong>Safe Harbor</strong></h3>

            <p>Personal information collected on this site may be stored and processed in the United States or any other country in which Otojoy or its affiliates, subsidiaries or agents maintain facilities. By using this site, you consent to any such transfer of information outside of your country. Otojoy complies with the EU Safe Harbor framework as set forth by the Department of Commerce regarding the collection, use, and retention of data from the European Union. For more information about Safe Harbor, visit the U.S. Department of Commerce&rsquo;s Safe Harbor Web site regarding the collection, use, and retention of data from the European Union.</p>

            <h3><strong>Protection of Information</strong></h3>

            <p>Otojoy is committed to protecting the security of your personal information. We use a variety of security technologies and procedures to help protect your personal information from unauthorized access, use, or disclosure. For example, we store personal information you provide on computer systems with limited access. These systems are located in controlled facilities. When we transmit highly confidential information (such as a credit card number) over the Internet, we protect it through the use of encryption, such as the Secure Socket Layer (SSL) protocol.</p>

            <h3><strong>Changes to This Privacy Statement</strong></h3>

            <p>We will occasionally update this privacy statement to reflect changes in our services and customer feedback. When we post changes to this Statement, we will revise the &quot;last updated&quot; date at the top of this statement.</p>

            <h3><strong>Inquiries</strong></h3>

            <p>Please direct any questions or concerns you may have regarding our Website to our site manager.</p>

        </div>
        <?php //echo $text; ?>
    </body>
</html>
