<?php
$headerData = $this->headerlib->data();
$JsArr = array("SCRIPT" => $headerData['javascript'], 'PLUGINS' => $headerData['javascript_plugins']);
if (isset($donation_data) && !empty($donation_data)) {
    extract($donation_data);
}
?>
<html  lang="en-us">
    <head>
        <?php $headerData['meta_tags']; ?>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <title><?= MAINTITLE; ?></title>
        <!------- BEGIN STYLESHEET   ---->
        <?php echo $headerData['stylesheets']; ?>
        <link href="<?= CSS_IMAGE_URL . 'frontsite/font-awesome.min.css' ?>" rel="stylesheet"> 
        <link href="<?= PLUGIN_URL ?>icheck/skins/all.css" rel="stylesheet"/>
        <!-- END THEME STYLESHEET -->
        <!-- BEGIN THEME STYLES --> 
        <?php //echo $headerData['themestyles']; ?>
        <!-- END THEME STYLES -->
        <!---------- favicon icon -------------->
        <link rel="shortcut icon" href="<?= IMAGE_URL ?>favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="<?php echo CSS_IMAGE_URL . 'frontsite/style.css'; ?>" />
        <!--------- LOAD EXTERNAL JS FILE ---------------------->
        <!------------------- END EXTERNAL JS FILE ------------>
    </head>
    <body>
        <!-- load header menu  -->
        <?php //$this->load->view('include/landing_menu'); ?>
        <!-- searchbar  -->
        <?php // $this->load->view('include/search'); ?>
        <section class="list-container" >
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12" style="top:5px;  text-align: center;">
                        <h3 class="text-center list-heading">Thank you for your donation.</h3>
                       <a href="LoopFinderpaypal://" class="btn btn-primary" id="back_btn">Back to application</a>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>