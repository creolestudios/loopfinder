<?php

    $headerData = $this->headerlib->data();
    $JsArr = array("SCRIPT" => $headerData['javascript'], 'PLUGINS' => $headerData['javascript_plugins']);
    if (isset($donation_data) && !empty($donation_data)){
        extract($donation_data);
    }
?>
<html  lang="en-us" >
    <head>
        <?php $headerData['meta_tags']; ?>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" http-equiv="refresh" />
        <title><?= MAINTITLE; ?></title>
        <!------- BEGIN STYLESHEET   ---->
        <?php echo $headerData['stylesheets']; ?>
        <link href="<?= CSS_IMAGE_URL . 'frontsite/font-awesome.min.css' ?>" rel="stylesheet"> 
        <link href="<?= PLUGIN_URL ?>icheck/skins/all.css" rel="stylesheet"/>
        <!-- END THEME STYLESHEET -->
        <!-- BEGIN THEME STYLES --> 
        <?php //echo $headerData['themestyles']; ?>
        <!-- END THEME STYLES -->
        <!---------- favicon icon -------------->
        <link rel="shortcut icon" href="<?= IMAGE_URL ?>favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="<?php echo CSS_IMAGE_URL . 'frontsite/style.css'; ?>" />
        <!--------- LOAD EXTERNAL JS FILE ---------------------->
        <!------------------- END EXTERNAL JS FILE ------------>
    </head>
    <body onload="myFunction()" style="display: none;" >
        <?php $this->load->view('include/footer', $JsArr); ?>
    <script>
        function myFunction(){
        //   $("#back_btn").click();
           // $('#back_btn').trigger('click');
            window.location.href = "LoopFinderpaypal://";
        }
    </script>
    </body>
    
</html>