<?php
$headerData = $this->headerlib->data();
$JsArr = array("SCRIPT" => $headerData['javascript'], 'PLUGINS' => $headerData['javascript_plugins']);
if (isset($donation_data) && !empty($donation_data)) {
    extract($donation_data);
}
?>
<html  lang="en-us">
    <head>
        <?php $headerData['meta_tags']; ?>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <title><?= MAINTITLE; ?></title>
        <!------- BEGIN STYLESHEET   ---->
        <?php echo $headerData['stylesheets']; ?>
        <link href="<?= CSS_IMAGE_URL . 'frontsite/font-awesome.min.css' ?>" rel="stylesheet"> 
        <link href="<?= PLUGIN_URL ?>icheck/skins/all.css" rel="stylesheet"/>
        <!-- END THEME STYLESHEET -->
        <!-- BEGIN THEME STYLES --> 
        <?php //echo $headerData['themestyles']; ?>
        <!-- END THEME STYLES -->
        <!---------- favicon icon -------------->
        <link rel="shortcut icon" href="<?= IMAGE_URL ?>favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="<?php echo CSS_IMAGE_URL . 'frontsite/style.css'; ?>" />
        <!--------- LOAD EXTERNAL JS FILE ---------------------->
        <!------------------- END EXTERNAL JS FILE ------------>
    </head>
    <body onload="myFunction()" style="display:none ;">
        <!-- load header menu  -->
        <?php //$this->load->view('include/landing_menu'); ?>
        <!-- searchbar  -->
        <?php // $this->load->view('include/search'); ?>


        <section class="list-container" >
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12" style="top:5px;  text-align: center;">
                        <img src="<?= IMAGE_URL . 'ajax_loader_blue_32.gif' ?>"/>
                    </div>
                    <div class="col-md-12" style="top:5px; text-align: center;">
                        <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
                            <input type="hidden" name="cmd" value="_s-xclick">
                            <input type="hidden" name="hosted_button_id" value="GUPAPU3FG3AWU">
                            <input type="hidden" name="tx" value="TransactionID"> 
                <!--            <input type="image" src="Donate-Button.png" sy border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">-->
                            <input type="submit" class="btn btn-general btn-contact-submit donation_btn" style="width: 136px;   margin-top: 5px;"value="Donate"/>
                            <img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
                        </form>
<!--                        <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
                            <input type="hidden" name="cmd" value="_s-xclick">
                            <input type="hidden" name="hosted_button_id" value="GUPAPU3FG3AWU">
                            <input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                            <img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
                        </form>-->

                    </div>
                </div>
            </div>
        </section>
        <?php $this->load->view('include/footer', $JsArr); ?>
        <script>
            function myFunction() {

                setTimeout(function () {
                    $('.donation_btn').trigger('click');
                }, 1000);
            }
        </script>
    </body>
</html>