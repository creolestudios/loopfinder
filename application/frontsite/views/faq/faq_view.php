<?php
$headerData = $this->headerlib->data();
$JsArr = array("SCRIPT" => $headerData['javascript'], 'PLUGINS' => $headerData['javascript_plugins']);
?>
<html  lang="en-us">
    <head>
        <?php $headerData['meta_tags']; ?>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <title><?= MAINTITLE; ?></title>

        <!------- BEGIN STYLESHEET   ---->
        <?php echo $headerData['stylesheets']; ?>
        <link href="<?= CSS_IMAGE_URL . 'frontsite/font-awesome.min.css' ?>" rel="stylesheet"> 
        <link href="<?= PLUGIN_URL ?>icheck/skins/all.css" rel="stylesheet"/>
        <!-- END THEME STYLESHEET -->

        <!-- BEGIN THEME STYLES --> 
        <?php //echo $headerData['themestyles']; ?>
        <!-- END THEME STYLES -->
        <!---------- favicon icon -------------->
        <link rel="shortcut icon" href="<?= IMAGE_URL ?>favicon.ico"/>

        <link rel="stylesheet" type="text/css" href="<?php echo CSS_IMAGE_URL . 'frontsite/style.css'; ?>" />
<!--        <link rel="stylesheet" type="text/css" href="<?php echo CSS_IMAGE_URL . 'frontsite/faq-general.css'; ?>" />-->
        <!--------- LOAD EXTERNAL JS FILE ---------------------->

        <!------------------- END EXTERNAL JS FILE ------------>
        <style>
            .body{ text-align: left;}
            .h4{ text-align:  left; color: #0095DE !important;font-weight: 600;}
            .u5734_clip {  width: 566; height: 733;}
            #u5733 {
                float: right;
                margin-bottom: 4px;
                margin-left: 19px;}
            </style>     
        </head>
        <body>
            <!-- load header menu  -->
            <?php $this->load->view('include/landing_menu'); ?>
            <!-- searchbar  -->
            <?php $this->load->view('include/search'); ?>
            <section class="contact-container" style="min-height: 1400px !important;">
                <div class="contact-section">
                    <h1 class="contact-head">Frequently Asked Questions</h1>
                    <br>
                    <div class="col-sm-10 col-sm-offset-1">
                        <h4 class="h4"><span><span class="actAsDiv normal_text" id="u5733" style="border: 2px solid #0095DE !important; border-radius: 4px;"><!-- content --><span class="actAsDiv clip_frame clearfix"><!-- image --><span class="actAsDiv"><img class="position_content img-responsive" id="u5734_img" src="<?= IMAGE_URL . 'hearing-loop-figure.jpg'; ?>" alt="" title="Illustration to show how a hearing loop works"></span></span></span></span></h4>
                        <h4 class="h4">Why are assistive listening systems needed?</h4>
                        <p class="Body">Approximately 20% (48 million) of American adults have some form of hearing loss in at least one ear, making it the most prevalent form of disability in the United States. Despite legislation and available technology to provide access, many individuals find their communication needs to be misunderstood and unaddressed.</p>
                        <p class="Body">Consequently, they seriously cut back on attendance, reducing audience size at performances, events, meetings and religious services throughout our community.</p>

                        <h4 class="h4">Are assistive listening systems required by law?</h4>

                        <p class="Body">Most places of worship, schools, and businesses have made themselves accessible to the visible minority of people in wheelchairs. For less money, they can also make themselves optimally accessible to the large, but largely invisible number of people with hearing loss, about 90,000 people just in Santa Barbara county, and thus comply with the Americans with Disabilities Act.</p>

                        <h4 class="h4">What is a hearing loop?</h4>

                        <p class="Body">Hearing loop systems take sound straight from the source and deliver it right into the listener’s hearing aid without extraneous noise or blurring. To them, it sounds like the speaker is right in their head. It turns their&nbsp; hearing aids into wireless earphones that broadcast sound customized for their hearing loss. Hearing Loops are the only assistive listening system to send clear, pure sound directly to hearing aids. They are the international standard for universal hearing access. Hearing loops are also known as audio loop, induction loop, t-loop, teleloop and audio frequency induction loop.</p>

                        <h4 class="h4">How do you use the hearing loop?</h4>

                        <p class="Body">All the user needs to do is switch his or her hearing aid or cochlear implant to the “T”, telephone, or hearing loop program. All but the very smallest modern hearing aids tend to be supplied with an internal t-coil or telecoil. However, it may not have been enabled when the device was initially programmed. If the user is not sure whether his or her hearing aid has a t-coil and/or does not know how to access the T program, his or her audiologist or hearing aid specialist will be able to advise.</p>

                        <h4 class="h4">Can a hearing loop be used by someone without a hearing aid?</h4>

                        <p class="Body">Yes, there are portable receivers available. They are not as convenient to use as a hearing aid with a built-in t-coil and are not fine-tuned to suit individual patterns of hearing, but can still be helpful to people with hearing loss who do not wish to wear hearing aids on a regular basis.</p>

                        <h4 class="h4">Where can hearing loops be used?</h4>

                        <p class="Body">Hearing loops have almost infinite applications. They can be used in theaters, auditoriums, churches, class rooms, at ticket counters, drive-thru windows, banks, doctor’s offices, pharmacies, and even with home TVs.</p>

                        <h4 class="h4">When were hearing loops invented?</h4>

                        <p class="Body">The first hearing aid ever equipped with a telecoil was developed in 1936 to help with hearing on the telephone. The first patent for an induction hearing loop was filed in 1937, but the benefits with the technology at the time were limited. In the 1960s and 1970s, experimentation with these systems was very prevalent in Scandinavia. Univox, one of the leading hearing loop manufacturers, was founded in 1965 and developed the first true hearing loop amplifier in 1969. During this time, hearing loops were installed in places of worship and schools. In 1987, Ampetronic opened their doors in the United Kingdom and developed the first phased array hearing loop system in 1991. In the mid-2000s, further advancements were made with computer modeling of the magnetic fields sent out by hearing loops as well as in audio processing to optimize the sound quality and help with compensating metal loss. In 2006, the new IEC 60118-4 standard was released, which dictates the performance standard for hearing loops to date.</p>

                        <h4 class="h4">Are hearing loops going to be replaced soon?</h4>

                        <p class="Body">In October 2013, the 3rd International Hearing Loop Conference in the United Kingdom was attended by more than 200 industry experts. The concluding statement of the conference was that currently, no new technology is on the horizon that is as cost-efficient and universal and creates equal access for individuals with hearing loss with such simplicity as hearing loop technology. Per Kokholm Sorensen, Director of Research &amp; Development for Widex, German hearing loop engineer Hannes Seidler, psychology professor and hearing loop advocate David Myers, as well as Cynthia Compton-Conley, former audiology professor and one of the leading authorities on assistive listening technology in the United States, all concluded that it will be a long time before Bluetooth or any other alternative wireless connectivity can match the virtues of hearing loops.</p>
                    </div></div>
            </section>
            <!------------- load footer view   --->
            <?php $this->load->view('include/footer', $JsArr); ?>
            <!-------- page level js  --------->
            <script type="text/javascript" src="<?= PLUGIN_URL ?>jquery-validation/js/jquery.validate.min.js"></script>
            <script src="<?= JS_URL ?>/custom/frontsite/validateform.js"></script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $("#donate_link").click(function () {
                        //$("#donation_form").submit();
                        alert("Donation Coming Soon!!");
                        return false;
                    });

                    $('.contact-container').height($(window).height());
                    
                    var availableTags = '<?= BASEURL . 'landing/getVenuesLocation' ?>'
                    var NoResultsLabel = "Oops! No suggestions found. Try a different search.";
                    $("#venue_location_serch").autocomplete({
                        source: availableTags,
                        minLength: 2,
                        select: function (event, ui) {
                            if (ui.item.value === NoResultsLabel) {
                                event.preventDefault();
                            }
                        },
                        focus: function (event, ui) {
                            if (ui.item.value === NoResultsLabel) {
                                event.preventDefault();
                            }
                        }
                    });

                    var availableTags1 = '<?= BASEURL . 'landing/getVenuesName'; ?>';

                    $("#venue_name_search").autocomplete({
                        //source: availableTags1
                        source: function (request, response) {
                            $.getJSON(availableTags1, {location: $('#venue_location_serch').val(), term: request.term},
                            response);
                        },
                        minLength: 2,
                        select: function (event, ui) {
                            if (ui.item.value === NoResultsLabel) {
                                event.preventDefault();
                            }
                        },
                        focus: function (event, ui) {
                            if (ui.item.value === NoResultsLabel) {
                                event.preventDefault();
                            }
                        }
                    });

                    jQuery('.accordion .panel-heading a[data-toggle="collapse"]').on('click', function () {
                        jQuery('.accordion .panel-heading a[data-toggle="collapse"]').removeClass('active');
                        $(this).addClass('active');
                    });

    //                $('#accordion1').on('shown.bs.collapse', function (e) {
    //                    $(e.currentTarget).parent().find('.panel-heading').addClass('active')                     
    //                });
    //
    //                $('#accordion1').on('hidden.bs.collapse', function (e) {
    //                    $(e.currentTarget).parent().find('.panel-heading').removeClass('active')                    
    //                });
    //
                    //form submit
                    $(document).on('click', '#common_submit', function (event) {
                        event.preventDefault();
                        var venue_city = $('#venue_location_serch').val();
                        var venue_name = $('#venue_name_search').val();
                        var search;
                        if (venue_city == '' && venue_name == '') {
                            window.location.href = '<?= BASEURL . 'landing/getPlace/' ?>';
                        } else if (venue_city != '' && venue_name == '')
                        {
                            //split string by comma
                            window.location.href = '<?= BASEURL . 'landing/getPlace/city/' ?>' + venue_city;
                        } else if (venue_city == '' && venue_name != '')
                        {
                            window.location.href = '<?= BASEURL . 'landing/getPlace/venue/' ?>' + venue_name;
                        } else if (venue_city != '' && venue_name != '') {

                            window.location.href = '<?= BASEURL . 'landing/getPlace/city/' ?>' + venue_city + '/venue/' + venue_name;

                        } else {
                            window.location.href = '<?= BASEURL . 'landing/getPlace/' ?>';
                        }
                    });

                });

                $(document).on('click', '#venue_lat_log_com', function () {

                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function (position) {
                            $("#latitude").val(position.coords.latitude);
                            $("#longitude").val(position.coords.longitude);

                            window.location.href = '<?= BASEURL . 'landing/getPlace/lat/' ?>' + position.coords.latitude + '/long/' + position.coords.longitude;
                        });
                    } else {
                        x.innerHTML = "Geolocation is not supported by this browser.";
                    }
                });



            </script>           

        </body>
    </html>