<?php
$headerData = $this->headerlib->data();
$JsArr = array("SCRIPT" => $headerData['javascript'], 'PLUGINS' => $headerData['javascript_plugins']);
?>
<html  lang="en-us">
    <head>
        <?php $headerData['meta_tags']; ?>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <title><?= MAINTITLE; ?></title>

        <!------- BEGIN STYLESHEET   ---->
        <?php echo $headerData['stylesheets']; ?>
        <link href="<?= CSS_IMAGE_URL . 'frontsite/font-awesome.min.css' ?>" rel="stylesheet"> 
        <link href="<?= PLUGIN_URL ?>icheck/skins/all.css" rel="stylesheet"/>
        <!-- END THEME STYLESHEET -->

        <!-- BEGIN THEME STYLES --> 
        <?php //echo $headerData['themestyles']; ?>
        <!-- END THEME STYLES -->
        <!---------- favicon icon -------------->
        <link rel="shortcut icon" href="<?= IMAGE_URL ?>favicon.ico"/>

        <link rel="stylesheet" type="text/css" href="<?php echo CSS_IMAGE_URL . 'frontsite/style.css'; ?>" />
        <!--------- LOAD EXTERNAL JS FILE ---------------------->

        <!------------------- END EXTERNAL JS FILE ------------>
        <style>

        </style>     
    </head>
    <body>
        <!-- load header menu  -->
        <?php $this->load->view('include/landing_menu'); ?>
        <!-- searchbar  -->
        <?php $this->load->view('include/search'); ?>
        <section class="contact-container">
            <div class="contact-section">
                <h1 class="contact-head">Frequently Asked Questions</h1>
                <br>
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="panel-group accordion" id="accordion1">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle active" data-toggle="collapse" data-parent="#accordion1" href="#collapse_1">
                                        1.How many Americans live with hearing loss? </a>
                                </h4>
                            </div>
                            <div id="collapse_1" class="panel-collapse in">
                                <div class="panel-body">
                                    <p>
                                        According to the National Institute on Deafness and Other Communication Disorders  “approximately 17 percent (36 million) of American adults report some degree of hearing loss.”    According to a 2011 report based on audiometric testing of Americans 12 and older in the National Health and Nutritional Examination Surveys (NHANES), 30 million Americans have at least a 25 db hearing loss in both ears and 48 million in one or both ears.  Unlike those challenged by mobility or vision loss, people challenged by hearing loss are often an invisible and forgotten minority.  About 1 in 4—some 8.4 million —have hearing aids, a number that would surely increase if hearing aids could double as wireless, customized loudspeakers.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_2">
                                        2.Why are hearing loops needed? Don’t hearing aids enable hearing?</a>
                                </h4>
                            </div>
                            <div id="collapse_2" class="panel-collapse collapse">
                                <div class="panel-body" style="height:200px; overflow-y:auto;">
                                    <p>
                                        Today’s digital hearing aids enhance hearing in conversational settings.  Yet for many people with hearing loss the sound becomes unclear when auditorium or TV loudspeakers are at a distance, when the context is noisy, or when room acoustics reverberate sound.  A hearing loop magnetically transfers the microphone or TV sound signal to hearing aids and cochlear implants that have a tiny, inexpensive “telecoil” receiver.  This transforms the instruments into in-the-ear loudspeakers that deliver sound customized for one’s own hearing loss.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_3">
                                        3.How many hearing aids have the telecoil (t-coil) receptor for receiving hearing loop input?  </a>
                                </h4>
                            </div>
                            <div id="collapse_3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>In surveys of hearing professionals, the Hearing Journal (April, 2009) reported that 58% of hearing aid fittings included a telecoil, an increase from 37% in 2001.  In its 2009/2010 reviews of hearing aid models, the Hearing Review Products reported that 126 (69%) of 183 hearing aid models—including all 38 in-the-ear models and 29 of 30 conventional behind-the-ear models—come with telecoils.  In 2014, the Consumer’s Guide to Hearing Aids reported that 323 of 415 hearing aid models (71.5%) were now coming with telecoils, as were 81% of models larger than the miniaturized completely-in-the-canal aid.  Moreover, the greater people’s need for hearing assistance, the more likely they are to have hearing aids with telecoils—as did 84 percent of Hearing Loss Association of America members in one survey.  New model cochlear implants also offer telecoils.</p>  
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_4">
                                        4.Can hearing loops serve those without telecoils or without hearing aids? </a>
                                </h4>
                            </div>
                            <div id="collapse_4" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>
                                        Yes, all forms of assistive listening, including hearing loops, come with portable receivers and headsets (though most such units sit in closets unused.)
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



        </section>
        <!------------- load footer view   --->
        <?php $this->load->view('include/footer', $JsArr); ?>
        <!-------- page level js  --------->
        <script type="text/javascript" src="<?= PLUGIN_URL ?>jquery-validation/js/jquery.validate.min.js"></script>
        <script src="<?= JS_URL ?>/custom/frontsite/validateform.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#donate_link").click(function () {
                    //$("#donation_form").submit();
                    alert("Donation Coming Soon!!");
                    return false;
                });
                $('.contact-container').height($(window).height());
                var availableTags = '<?= BASEURL . 'landing/getVenuesLocation' ?>'
                var NoResultsLabel = "Oops! No suggestions found. Try a different search.";
                $("#venue_location_serch").autocomplete({
                    source: availableTags,
                    minLength: 2,
                    select: function (event, ui) {
                        if (ui.item.value === NoResultsLabel) {
                            event.preventDefault();
                        }
                    },
                    focus: function (event, ui) {
                        if (ui.item.value === NoResultsLabel) {
                            event.preventDefault();
                        }
                    }
                });

                var availableTags1 = '<?= BASEURL . 'landing/getVenuesName'; ?>';

                $("#venue_name_search").autocomplete({
                    //source: availableTags1
                    source: function (request, response) {
                        $.getJSON(availableTags1, {location: $('#venue_location_serch').val(), term: request.term},
                        response);
                    },
                    minLength: 2,
                    select: function (event, ui) {
                        if (ui.item.value === NoResultsLabel) {
                            event.preventDefault();
                        }
                    },
                    focus: function (event, ui) {
                        if (ui.item.value === NoResultsLabel) {
                            event.preventDefault();
                        }
                    }
                });

                jQuery('.accordion .panel-heading a[data-toggle="collapse"]').on('click', function () {
                    jQuery('.accordion .panel-heading a[data-toggle="collapse"]').removeClass('active');
                    $(this).addClass('active');
                });

//                $('#accordion1').on('shown.bs.collapse', function (e) {
//                    $(e.currentTarget).parent().find('.panel-heading').addClass('active')                     
//                });
//
//                $('#accordion1').on('hidden.bs.collapse', function (e) {
//                    $(e.currentTarget).parent().find('.panel-heading').removeClass('active')                    
//                });
//
                //form submit
                $(document).on('click', '#common_submit', function (event) {
                    event.preventDefault();
                    var venue_city = $('#venue_location_serch').val();
                    var venue_name = $('#venue_name_search').val();
                    var search;
                    if (venue_city == '' && venue_name == '') {
                        window.location.href = '<?= BASEURL . 'landing/getPlace/' ?>';
                    } else if (venue_city != '' && venue_name == '')
                    {
                        //split string by comma
                        window.location.href = '<?= BASEURL . 'landing/getPlace/city/' ?>' + venue_city;
                    } else if (venue_city == '' && venue_name != '')
                    {
                        window.location.href = '<?= BASEURL . 'landing/getPlace/venue/' ?>' + venue_name;
                    } else if (venue_city != '' && venue_name != '') {

                        window.location.href = '<?= BASEURL . 'landing/getPlace/city/' ?>' + venue_city + '/venue/' + venue_name;

                    } else {
                        window.location.href = '<?= BASEURL . 'landing/getPlace/' ?>';
                    }
                });

            });

            $(document).on('click', '#venue_lat_log_com', function () {

                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        $("#latitude").val(position.coords.latitude);
                        $("#longitude").val(position.coords.longitude);

                        window.location.href = '<?= BASEURL . 'landing/getPlace/lat/' ?>' + position.coords.latitude + '/long/' + position.coords.longitude;
                    });
                } else {
                    x.innerHTML = "Geolocation is not supported by this browser.";
                }
            });



        </script>           

    </body>
</html>