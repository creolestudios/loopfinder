<?php
$headerData = $this->headerlib->data();
$JsArr = array("SCRIPT" => $headerData['javascript'], 'PLUGINS' => $headerData['javascript_plugins']);
?>
<html  lang="en-us">
    <head>
        <?php $headerData['meta_tags']; ?>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <title><?= MAINTITLE; ?></title>

        <!------- BEGIN STYLESHEET   ---->
        <?php echo $headerData['stylesheets']; ?>
        <link href="<?= CSS_IMAGE_URL . 'frontsite/font-awesome.min.css' ?>" rel="stylesheet"> 
        <link href="<?= PLUGIN_URL ?>icheck/skins/all.css" rel="stylesheet"/>
        <!-- END THEME STYLESHEET -->

        <!-- BEGIN THEME STYLES --> 
        <?php //echo $headerData['themestyles']; ?>
        <!-- END THEME STYLES -->
        <!---------- favicon icon -------------->
        <link rel="shortcut icon" href="<?= IMAGE_URL ?>favicon.ico"/>

        <link rel="stylesheet" type="text/css" href="<?php echo CSS_IMAGE_URL . 'frontsite/style.css'; ?>" />
        <!--------- LOAD EXTERNAL JS FILE ---------------------->

        <!------------------- END EXTERNAL JS FILE ------------>
        <style>
            .contact-section ul{
                list-style: inherit !important;
            }
            .policy_text{
                font-family: sans-serif;
                font-size: inherit;
                line-height: 1.42857143;
                text-align: left;                
            }
            .policy_text h3 {
                font-size: 16px;
                font-family: 'Helvetica Neue', sans-serif !important;
                color: #0095D7 !important;
            }
            .policy_text p {
                font-size: 14px;                
            }
            .policy_text ul li {
                margin-left: 15px;
            }

        </style>
    </head>
    <body>
        <!-- load header menu  -->
        <?php $this->load->view('include/landing_menu'); ?>
        <!-- searchbar  -->
        <?php $this->load->view('include/search'); ?>
        <section class="contact-container" style="">
            <div class="contact-section">
                <h1 class="contact-head">Privacy Policy</h1>
                <br>
                <div class="col-sm-8 col-sm-offset-2 policy_text">
                    <?php echo html_entity_decode($contentdata['text'], ENT_HTML5); ?>
                </div>
            </div>
        </section>
        <!------------- load footer view   --->
        <?php $this->load->view('include/footer', $JsArr); ?>
        <!-------- page level js  --------->
        <script type="text/javascript" src="<?= PLUGIN_URL ?>jquery-validation/js/jquery.validate.min.js"></script>
        <script src="<?= JS_URL ?>/custom/frontsite/validateform.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {

                $('.contact-container').height($(window).height());
                $("#donate_link").click(function () {
                    //$("#donation_form").submit();
                    alert("Donation Coming Soon!!");
                    return false;
                });
                var availableTags = '<?= BASEURL . 'landing/getVenuesLocation' ?>'
                var NoResultsLabel = "Oops! No suggestions found. Try a different search.";
                $("#venue_location_serch").autocomplete({
                    source: availableTags,
                    minLength: 2,
                    select: function (event, ui) {
                        if (ui.item.value === NoResultsLabel) {
                            event.preventDefault();
                        }
                    },
                    focus: function (event, ui) {
                        if (ui.item.value === NoResultsLabel) {
                            event.preventDefault();
                        }
                    }
                });

                var availableTags1 = '<?= BASEURL . 'landing/getVenuesName'; ?>';

                $("#venue_name_search").autocomplete({
                    //source: availableTags1
                    source: function (request, response) {
                        $.getJSON(availableTags1, {location: $('#venue_location_serch').val(), term: request.term},
                        response);
                    },
                    minLength: 2,
                    select: function (event, ui) {
                        if (ui.item.value === NoResultsLabel) {
                            event.preventDefault();
                        }
                    },
                    focus: function (event, ui) {
                        if (ui.item.value === NoResultsLabel) {
                            event.preventDefault();
                        }
                    }
                });

                jQuery('.accordion .panel-heading a[data-toggle="collapse"]').on('click', function () {
                    jQuery('.accordion .panel-heading a[data-toggle="collapse"]').removeClass('active');
                    $(this).addClass('active');
                });
                //form submit
                $(document).on('click', '#common_submit', function (event) {
                    event.preventDefault();
                    var venue_city = $('#venue_location_serch').val();
                    var venue_name = $('#venue_name_search').val();
                    var search;
                    if (venue_city == '' && venue_name == '') {
                        window.location.href = '<?= BASEURL . 'landing/getPlace/' ?>';
                    } else if (venue_city != '' && venue_name == '')
                    {
                        //split string by comma
                        window.location.href = '<?= BASEURL . 'landing/getPlace/city/' ?>' + venue_city;
                    } else if (venue_city == '' && venue_name != '')
                    {
                        window.location.href = '<?= BASEURL . 'landing/getPlace/venue/' ?>' + venue_name;
                    } else if (venue_city != '' && venue_name != '') {

                        window.location.href = '<?= BASEURL . 'landing/getPlace/city/' ?>' + venue_city + '/venue/' + venue_name;

                    } else {
                        window.location.href = '<?= BASEURL . 'landing/getPlace/' ?>';
                    }
                });

            });

            $(document).on('click', '#venue_lat_log_com', function () {

                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        $("#latitude").val(position.coords.latitude);
                        $("#longitude").val(position.coords.longitude);

                        window.location.href = '<?= BASEURL . 'landing/getPlace/lat/' ?>' + position.coords.latitude + '/long/' + position.coords.longitude;
                    });
                } else {
                    x.innerHTML = "Geolocation is not supported by this browser.";
                }
            });
        </script>           

    </body>
</html>