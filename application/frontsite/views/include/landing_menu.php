<?php 
$crntollerName = $this->uri->segment(1);
?>
<header class="header-other">
    <div class="container-fluid">
    </div>
    <div class="row menu_header_row">
            <div class="col-sm-3 hidden-xs hidden-sm">
                    <a href="<?= BASEURL.'landing/index';?>"><img class="logo img-responsive" src="<?= IMAGE_URL.'new_logo269x49.png'; ?>"></a>
                </div>
            <div class="col-sm-7 hidden-xs hidden-sm">
                <div class="menu-container">
                        <ul class="main-menu">
                            <li class="left_li_padding "><a href="<?= BASEURL.'landing/index'?>" class="">Home</a></li>
                            <li class="left_li_padding"><a href="<?= BASEURL.'requestloop'?>" class="<?= $crntollerName =='requestloop' ? 'menu_active_li': ' '; ?>">Request / Add a Loop</a></li>
                            <li class="left_li_padding"><a href="<?= BASEURL.'contactus'?>" class="<?=$crntollerName =='contactus' ? 'menu_active_li': ' '; ?>">Contact Us</a></li>
                            <li class="left_li_padding"><a href="<?= BASEURL.'faq'?>" class="<?=$crntollerName =='faq' ? 'menu_active_li': ' '; ?>">FAQ</a></li>
                            <!--                                <li class="left_li_padding donation_li" id=""><a href="javascript:void(0)" id="donate_link">Donate</a></li>-->
                            <li class="left_li_padding donation_li" id=""><img src="<?= IMAGE_URL . 'Donate-Button.png'; ?>" style="margin-top: -7px; max-width: 74px;" alt="donation link" id="donate_link"></li>
                        </ul>
                </div>
            </div>
            <div class="col-sm-2 hidden-xs hidden-sm">
                    <a href="http://www.hearingloss.org/" target="_blank"><img class="logo img-responsive donation_image" style="width: 130px;" src="<?= IMAGE_URL.'HLAA-logo.png'?>"></a>
            </div>
            <!-- mobile menu -->
            <div class="col-xs-4 visible-xs visible-sm">
                <a href="<?= BASEURL . 'landing/index'; ?>" ><img class="logo img-responsive" src="<?php echo IMAGE_URL . 'new_logo269x49.png'; ?>" /></a>
            </div>
            <div class="col-xs-4 visible-xs visible-sm">
                <a href="http://www.hearingloss.org/" target="_blank" style="float: right;"><img class="logo img-responsive" style="width: 100px !important;" src="<?php echo IMAGE_URL . 'For-Web.png'; ?>" /></a>
            </div>
            <div class="col-xs-4 visible-xs visible-sm text-right">
                <div class="clearfix inline-block">
                    <a class="touchbtn dropdown-toggle blue" href="javascript:void(0);" style="padding-right: 10px;" type="button" data-toggle="dropdown"><span class="fa fa-bars" style="line-height: 2 !important;"></span></a>
                    <ul class="dropdown-menu dropdown-position" style="font-size: 14px; top: 46px !important;">
                        <li><a href="<?= BASEURL . 'landing/index'; ?>">Home</a></li>
                        <li><a href="<?= BASEURL . 'requestloop/'; ?>">Request / Add a Loop</a></li>
                        <li><a href="<?= BASEURL . 'contactus/'; ?>">Contact Us</a></li>
                        <li><a href="<?= BASEURL . 'faq/'; ?>">FAQ</a></li>
                        <li><a href="javascript:void(0)" id="donate_link">Donate</a></li>
                    </ul>                                
                </div> 	<!-- mobile menu end -->
            </div>
        </div>
</header>
