<footer class="footer hidden-xs" style="background-color: #005fa3;color: #fff;">
    <section>
        <div class="container-fluid">
            <div class="col-sm-6 col-xs-9">
                <p>Copyright 2015, LoopFinder. All rights reserved.</p>
            </div>

            <div class="col-sm-6 padding5 contactlink col-xs-3">
                <a href="<?php echo BASEURL . 'privacy_policy/index' ?>"><i class="fa fa-lock"></i> Privacy Policy</a>&nbsp;|&nbsp;
                <a href="<?php echo BASEURL . 'contactus' ?>"><i class="fa fa-phone"></i> Contact Us</a>
            </div>

        </div>
    </section>
    <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" id="donation_form" method="post" target="_top" style="display:none;">
        <input type="hidden" name="cmd" value="_s-xclick">
        <input type="hidden" name="hosted_button_id" value="8PHF7YVBNYS8E">
        <input type="hidden" name="tx" value="TransactionID"> 
<!--            <input type="image" src="Donate-Button.png" sy border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">-->
        <input type="submit" class="btn btn-general btn-contact-submit" style="width: 136px;   margin-top: 5px;"value="Donate"/>
        <img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
    </form>
</footer>
<footer  class="footer visible-xs" style="background-color: #005fa3;color: #fff;">
    <section>
        <div class="container-fluid">
            <div class="col-xs-12">
                <p>Copyright 2015, LoopFinder. All rights reserved.</p>
            </div>
            <div class="col-xs-12" style="text-align: right;">
                <a href="<?php echo BASEURL . 'privacy_policy/index' ?>"><i class="fa fa-lock"></i> Privacy Policy</a>&nbsp;|&nbsp;
                <a href="<?php echo BASEURL . 'contactus' ?>"><i class="fa fa-phone"></i> Contact Us</a>
            </div>
        </div>
    </section>
    <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" id="donation_form" method="post" target="_top" style="display:none;">
        <input type="hidden" name="cmd" value="_s-xclick">
        <input type="hidden" name="hosted_button_id" value="8PHF7YVBNYS8E">
        <input type="hidden" name="tx" value="TransactionID"> 
<!--            <input type="image" src="Donate-Button.png" sy border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">-->
        <input type="submit" class="btn btn-general btn-contact-submit" style="width: 136px;   margin-top: 5px;"value="Donate"/>
        <img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
    </form>
</footer>
<!-- Button trigger modal -->
<!-- Modal -->
<div class="modal fade" id="modal-feedback" tabindex="-1" role="dialog" aria-labelledby="modal-feedback-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><img src="<?= IMAGE_URL . 'close-icon.png'; ?>" /></span></button>
                <h4 class="modal-title center" id="myModalLabel"><strong>Feedback</strong></h4>
                <p class="text-center">Please tell us about your experience at this venue</p>
            </div>
            <div class="modal-body" id="send-detail">

            </div>
        </div>
    </div>
</div>
<?php echo $SCRIPT; ?>

<!--<script>window.jQuery || document.write('<script src="<?= PLUGIN_URL; ?>jquery-2.0.3.min.js"><\/script>')</script>  -->

