

<!-------------------------------- END MAP TO GRID VIEW MENU --------------->

<header class="header-other">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-3 hidden-xs">
                <a href="<?= BASEURL . 'landing/index'; ?>"><img class="logo img-responsive" src="<?php echo IMAGE_URL . 'new_logo269x49.png'; ?>" /></a>
            </div>
            <div class="col-sm-7 hidden-xs">
                <div class="menu-container">
                    <ul class="main-menu">
                        <li left_li_padding><a href="<?= BASEURL . 'landing/index'; ?>">Home</a></li>
                        <li left_li_padding><a href="<?= BASEURL . 'requestloop/'; ?>">Request / Add a Loop</a></li>
                        <li left_li_padding><a href="<?= BASEURL . 'contactus/'; ?>">Contact Us</a></li>
                        <li left_li_padding><a href="<?= BASEURL . 'faq/'; ?>">FAQ</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-2 hidden-xs">
                <a href="http://www.hearingloss.org/"><img class="logo img-responsive" style="width: 130px;" src="<?php echo IMAGE_URL . 'For-Web.png'; ?>" /></a>
            </div>

            <!-- mobile menu -->
            <div class="col-xs-4 visible-xs">
                <a href="<?= BASEURL . 'landing/index'; ?>"><img class="logo img-responsive" src="<?php echo IMAGE_URL . 'new_logo269x49.png'; ?>" /></a>
            </div>
            <div class="col-xs-4 visible-xs">
                    <a href="http://www.hearingloss.org/" target="_blank"><img class="logo img-responsive" style="width: 100px !important;" src="<?php echo IMAGE_URL . 'For-Web.png'; ?>" /></a>
            </div>
            <div class="col-xs-4 visible-xs">
                <div class="clearfix inline-block right">
                    <a class="touchbtn dropdown-toggle blue" href="javascript:void(0);" style="padding-right: 10px;" type="button" data-toggle="dropdown"><span class="fa fa-bars" style="line-height: 2 !important;"></span></a>
                    <ul class="dropdown-menu dropdown-position" style="font-size: 14px;  top: 46px !important;">
                        <li><a href="<?= BASEURL . 'landing/index'; ?>">Home</a></li>
                        <li><a href="<?= BASEURL . 'requestloop/'; ?>">Request / Add a Loop</a></li>
                        <li><a href="<?= BASEURL . 'contactus/'; ?>">Contact Us</a></li>
                        <li><a href="<?= BASEURL . 'faq/'; ?>">FAQ</a></li>
                        <li class="left_li_padding donation_li" id="donate_link">Donate</li>
                    </ul>                                
                </div> 	<!-- mobile menu end -->
            </div>
        </div>
    </div>

</header>
<!--<img class="img_location2" src="<?php echo IMAGE_URL . 'location-icon.png' ?>" onClick="getLocation_lat()"/>-->
<section class="container-fluid clearfix" style="padding-top: 87px;">
    <div class="row">
        <div class="col-md-6 col-md-push-3" style="">
            <div class="filter-input-container find">
                <form action="" method="post" id="map_grid_form">
                    <input type="text" class="input1" name="LocationForVenue" onkeypress="return runScript(event)"  placeholder="City" id="venue_location"><input type="text" id="venue_name_search" onkeypress="return runScript(event)"  name="VenueName" class="input2" placeholder="Name">                            <input type="hidden" id="venue_lat" />
                     <input type="hidden" id="venue_long"/>
                    <button class="filter-search" type="button" id="landing_search_button"></button>
                    <!--                    <button type="button" class="btn btn-general get-direction-btn" id="btn-select" style="width: 50%;margin-top: 5px;">Apply</button>-->

                </form>
            </div>
        </div>

        <div class="col-md-3 col-md-pull-6 col-sm-6 col-xs-8" style="">
            <div class="filter-input-container ok-left">
                <a id="filter-opener" href=""><span class="btn btn-filter"><i class="fa fa-filter" id="filter_apply_btn" style="line-height: 1.42857143"></i></span> Filter Results</a>
                <button type="button" class="btn btn-general get-direction-btn" id="btn-select" style="width: 106px;margin-top: 5px; display:none;">Apply</button>
                <div class="main-filter-container">
                    <form id="filter-box" class="sub-filter-container" action="<?= BASEURL . 'landing/getPlace'; ?>" method="post">
                        <div class="filter-container">
                            <div class="filter-section">
                                <p class="section-head left"><img class="v-middle" src="<?= IMAGE_URL . 'venue-icon.png'; ?>" />&nbsp; Venue Status</p>
                                <!--                                <ul class="filter-items">
                                                                    <li> <label class="inputcheck" for="filter-all"><input name="filter-type[]" type="checkbox" class="looped_chk hidden checkloop" id="filter-all" value="3" />All </label> </li>
                                                                    <li> <label class="inputcheck" for="filter-looped"><input name="filter-type[]" type="checkbox" class="looped_chk hidden checkloop" id="filter-looped" value="1" />Looped </label> </li>
                                                                    <li> <label class="inputcheck" for="filter-unlooped"><input name="filter-type[]" type="checkbox" class="looped_chk hidden checkloop" id="filter-unlooped" value="2" />Not Looped </label> </li>
                                                                     
                                                                </ul>-->
                                <div class="form-group" style="margin-top: 15px">

                                    <div class="btn-toolbar" style="margin-left: 5px">
                                        <div class="btn-group btn-group btn-group-solid">
                                            <button type="button" class="btn filter-btn filter-btn-active" id="0">All</button>
                                            <button type="button" class="btn filter-btn" id="1">Looped</button>
                                            <button type="button" class="btn filter-btn" id="2">Not Looped</button>
                                            <input type="hidden" value="" id="hidden_looped">
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div>

                        <div class="filter-container">
                            <div class="filter-section">
                                <p class="section-head left"><img class="v-middle" src="<?= IMAGE_URL . 'categories-icon.png'; ?>" />&nbsp; Venue Categories</p>
                                <ul class="filter-items">
                                    <?php
                                    foreach ($Categories as $key => $row) {
                                        if($row['categoriesName']!= 'Hearing Professional' && $row['categoriesId']!= '9' && $row['categoriesId']!= '21'){
                                        ?>
                                        <li> 
                                            <label class="inputcheck  " for="venue-category-<?php echo $row['categoriesId']; ?>">
                                                <input type="checkbox" name="categories[]" class="hidden a checkcat cat_checkers" id="venue-category-<?php echo $row['categoriesId']; ?>"  value="<?php echo $row['categoriesId']; ?>" /><?php echo $row['categoriesName']; ?>
                                            </label> 
                                        </li>
                                    <?php } }?>  

                                </ul>

<!--                                <button type="button" class="btn btn-general get-direction-btn" id="btn-select" style="width: 50%;margin-top: 5px;">Apply</button>-->
                            </div>  
                        </div>
                  </form>
                </div>


            </div>
        </div>
        <div class="col-sm-1 nopaddingleft col-xs-4 ipad_place" style="top: 8px; text-align: center;">
            <img class="tooltips" src="<?php echo IMAGE_URL . 'icon-nearby.png' ?>" id="venue_lat_log_com"  style="cursor: pointer;max-height: 48px;width: auto;vertical-align: baseline;" title="Show all venues near me"/>
        </div>
        <div class="col-md-2 col-sm-6 col-xs-12 text-center  ipade_view" style="float: right">
            <div class="filter-input-container ok-right">                
                <a class="btn btn-switch-left btn-active views" href="<?= BASEURL . 'landing/getPlace/'; ?>" id="Map-view"><i class="icon-map"></i> Map</a><a class="btn btn-switch-right views" href="" id="Grid-view" ><i class="fa fa-th-large"></i> Grid</a><input type="hidden" id="viewselector" value=""/>
            </div>
        </div>
    </div>
    <!--    <div class="row">
            <div class="col-md-6 col-sm-6 col-md-offset-5 col-sm-offset-3" style="top:5px;">
                <button type="submit" class="btn btn-general btn-contact-submit" id="search_venue" value="" style="padding:10px;" onClick="getLocation_lat()">Show all venues near me..</button>
            </div>
        </div>-->
</section>
<script>
                function runScript(e) {
                   
                    if (e.keyCode == 13)
                    {
                       
                        $('#landing_search_button').click();
                    }
                }
</script>
<!-------------------------------- END MAP TO GRID VIEW MENU --------------->


