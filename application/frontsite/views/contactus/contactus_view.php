<?php
$headerData = $this->headerlib->data();
$JsArr = array("SCRIPT" => $headerData['javascript'], 'PLUGINS' => $headerData['javascript_plugins']);
?>
<html  lang="en-us">
    <head>
        <?php $headerData['meta_tags']; ?>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <title><?= MAINTITLE; ?></title>

        <!------- BEGIN STYLESHEET   ---->
        <?php echo $headerData['stylesheets']; ?>
        <link href="<?= CSS_IMAGE_URL . 'frontsite/font-awesome.min.css' ?>" rel="stylesheet"> 
        <link href="<?= PLUGIN_URL ?>icheck/skins/all.css" rel="stylesheet"/>

        <!-- END THEME STYLESHEET -->

        <!-- BEGIN THEME STYLES --> 
        <?php //echo $headerData['themestyles']; ?>
        <!-- END THEME STYLES -->
        <!---------- favicon icon -------------->
        <link rel="shortcut icon" href="<?= IMAGE_URL ?>favicon.ico"/>

        <link rel="stylesheet" type="text/css" href="<?php echo CSS_IMAGE_URL . 'frontsite/style.css'; ?>" />
        <!--------- LOAD EXTERNAL JS FILE ---------------------->

        <!------------------- END EXTERNAL JS FILE ------------>
        
    </head>
    <body>
        <!-- load header menu  -->
        <?php $this->load->view('include/landing_menu'); ?>
        <!-- searchbar  -->
        <?php $this->load->view('include/search'); ?>
        <section class="contact-container">
            <div class="contact-section">
                <div class="container-fluid">
                    <div class="col-sm-4">&nbsp;</div>
                    <div class="col-sm-4">
                        <div class="contact-head">Contact Us</div>
                        <!--                        <div class="contact-text">Feel free to express your views</div>-->
                        <br>
                        <?php if ($this->session->flashdata('success') != '') { ?>

                            <div id="contact_message" class="alert alert-success" style="" tabindex="-1"><?php echo $this->session->flashdata('success'); ?></div>
                        <?php } ?>

                        <?php if ($this->session->flashdata('error') != '') { ?>

                            <div id="contact_message" class="alert alert-error" style="" tabindex="-1"><?php echo $this->session->flashdata('error'); ?></div>
                        <?php } ?>
                        <?php
                        if (isset($data)) {
                            echo $data;
                        }
                        ?>

                        <form action="<?php echo BASEURL . 'contactus/getContactDetail'; ?>" autocomplete="off" method="post" class="contactus-from" id="contact-form">
                            <div class="alert hide" id="login_msg"></div>
                            <div class="form-group">

                                <div class="inner-addon left-addon">
<!--                                    <i class="glyphicon"><img class="input-icon" src="<?php echo IMAGE_URL . 'user-icon.png'; ?>" /></i>-->
                                    <i class="fa fa-user input-icon"></i>
                                    <input type="text" name="name" id="name" class="form-control input" placeholder="Name"  />
                                </div>
                            </div>
                            <div class="form-group">

                                <div class="inner-addon left-addon">
<!--                                    <i class="glyphicon"><img class="input-icon" src="<?php echo IMAGE_URL . 'email-icon.png'; ?>" /></i>-->
                                    <i class="fa fa-envelope input-icon"></i>
                                    <input type="email" name="email" id="email" class="form-control input" placeholder="E-Mail" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="inner-addon left-addon">
<!--                                    <i class="glyphicon"><img class="input-icon" src="<?php echo IMAGE_URL . 'chat-icon.png'; ?>" /></i>-->
                                    <i class="fa fa-comment input-icon"></i>
                                    <textarea  name="message" class="form-control input" placeholder="Comment" rows="8"  id="message"></textarea>
                                </div>
                            </div>

                            <div class="form-actions">
                                <div class="submit-btn">
                                    <button type="submit" class="btn btn-general btn-contact-submit" name="submit" id="contact-submit" style="width:126px;height:42px">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-sm-4">&nbsp;</div>
                </div>
            </div>
        </section>
        <!------------- load footer view   --->
        <?php $this->load->view('include/footer', $JsArr); ?>
        <!-------- page level js  --------->
        <script type="text/javascript" src="<?= PLUGIN_URL ?>jquery-validation/js/jquery.validate.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#donate_link").click(function () {
                    alert("Donation Coming Soon!!");
                    return false;
                    //$("#donation_form").submit(); 
                });
                $('#contact_message').fadeOut(3000);

                var availableTags = '<?= BASEURL . 'landing/getVenuesLocation' ?>'
                var NoResultsLabel = "Oops! No suggestions found. Try a different search.";
                $("#venue_location_serch").autocomplete({
                    source: availableTags,
                    minLength: 2,
                    select: function (event, ui) {
                        if (ui.item.value === NoResultsLabel) {
                            event.preventDefault();
                        }
                    },
                    focus: function (event, ui) {
                        if (ui.item.value === NoResultsLabel) {
                            event.preventDefault();
                        }
                    }
                });

                var availableTags1 = '<?= BASEURL . 'landing/getVenuesName'; ?>';

                $("#venue_name_search").autocomplete({
                    //source: availableTags1
                    source: function (request, response) {
                        $.getJSON(availableTags1, {location: $('#venue_location_serch').val(), term: request.term},
                        response);
                    },
                    minLength: 2,
                    select: function (event, ui) {
                        if (ui.item.value === NoResultsLabel) {
                            event.preventDefault();
                        }
                    },
                    focus: function (event, ui) {
                        if (ui.item.value === NoResultsLabel) {
                            event.preventDefault();
                        }
                    }
                });
                $('#contact-form').validate({
                    rules: {
                        name: {
                            minlength: 4,
                            required: true

                        },
                        email: {
                            required: true,
                            email: true
                        },
                        message: {
                            required: true
                        }
                    },
                    messages: {
                        name: {required: "Please enter your name.",
                            minlength: "Please enter minimum 4 words."},
                        email: {
                            required: "Please enter your e-mail address.",
                            email: "Invalid email address."
                        },
                        message: {required: "Please enter a message."
                        }
                    },
                    submitHandler: function (form) {
                        form.submit();
                    }
                });
                $(document).on('click', '#common_submit', function (event) {
                    event.preventDefault();
                    var venue_city = $('#venue_location_serch').val();
                    var venue_name = $('#venue_name_search').val();
                    var search;
                    if (venue_city == '' && venue_name == '') {
                        window.location.href = '<?= BASEURL . 'landing/getPlace/' ?>';
                    } else if (venue_city != '' && venue_name == '')
                    {
                        //split string by comma
                        window.location.href = '<?= BASEURL . 'landing/getPlace/city/' ?>' + venue_city;
                    } else if (venue_city == '' && venue_name != '')
                    {
                        window.location.href = '<?= BASEURL . 'landing/getPlace/venue/' ?>' + venue_name;
                    } else if (venue_city != '' && venue_name != '') {

                        window.location.href = '<?= BASEURL . 'landing/getPlace/city/' ?>' + venue_city + '/venue/' + venue_name;

                    } else {
                        window.location.href = '<?= BASEURL . 'landing/getPlace/' ?>';
                    }
                });

            });

            $(document).on('click', '#venue_lat_log_com', function () {

                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        $("#latitude").val(position.coords.latitude);
                        $("#longitude").val(position.coords.longitude);

                        window.location.href = '<?= BASEURL . 'landing/getPlace/lat/' ?>' + position.coords.latitude + '/long/' + position.coords.longitude;
                    });
                } else {
                    x.innerHTML = "Geolocation is not supported by this browser.";
                }
            });
        </script>        
    </body>
</html>













