<html>
    <head>

     
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places,geometry"></script>
        <script src="<?=CUSTOM_JS.'/frontsite/locationpicker.jquery.js'?>"></script> 
        
        <script>


            function getLocation1() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(showPosition1);
                } else {
                    x.innerHTML = "Geolocation is not supported by this browser.";
                }
            }
            // get direction script
            var directionsDisplay2;
            // var directionsService1 = new google.maps.DirectionsService();
            var map;
            function initialize2()
            {

                // the var for our initial point

                var mapOptions = {
                    zoom: 10,
                    center: new google.maps.LatLng(34.6466, -120.4498),
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    scrollwheel: false
                };

                map = new google.maps.Map(document.getElementById('map-canvas1'), mapOptions);
                directionsDisplay2 = new google.maps.DirectionsRenderer();
                directionsDisplay2.setMap(map);
                getLocation1();
                showPosition1();
                
            }
            var gridlat = $('.grid_direction_lat', window.parent.document).val();
            var gridlng = $('.grid_direction_lng', window.parent.document).val();
            var grid_latlong = gridlat + ','+ gridlng;
           
            function showPosition1(position)
            {

                if(typeof(coords) == 'undefined')
                {
                 var desti_latlong = "0,0";
                }else{
                 var desti_latlong = position.coords.latitude + ','  + position.coords.longitude;
               }
               //console.log(desti_latlong);
                //var desti_latlong = "34.6466,-120.4498";
                var request = {
                    origin: grid_latlong,
                    destination: desti_latlong,
                    travelMode: google.maps.TravelMode.DRIVING
                };
                var directionsService = new google.maps.DirectionsService();
                directionsService.route(request, function (response, status)
                {
                    if (status == google.maps.DirectionsStatus.OK)
                    {
                        directionsDisplay2.setDirections(response);
                    }
                });
            }
            google.maps.event.addDomListener(window, 'load', initialize2);
        </script>
    </head>
    <body>

<!--        <input id="pac-input" class="controls" type="text" placeholder="Search Box">-->
        <div id="map-canvas1" style="width:540px; height:370px"></div>
    </body>
</html>