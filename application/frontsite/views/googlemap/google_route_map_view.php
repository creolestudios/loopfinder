<html>
    <head>

       <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places,geometry"></script>
        <script src="<?=CUSTOM_JS.'/frontsite/locationpicker.jquery.js'?>"></script> 
        
        <script>


            function getLocation() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(showPosition);
                } else {
                    x.innerHTML = "Geolocation is not supported by this browser.";
                }
            }
            // get direction script
            var directionsDisplay1;
            // var directionsService1 = new google.maps.DirectionsService();
            var map;
            function initialize1(data)
            {

               // the var for our initial point
               
                var mapOptions = {
                    zoom: 10,
                    center: new google.maps.LatLng(34.6466, -120.4498),
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    scrollwheel: false
                };

                map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
                directionsDisplay1 = new google.maps.DirectionsRenderer();
                directionsDisplay1.setMap(map);
                getLocation();
                showPosition();
            }
               
                var lat = $('.lat_distance', window.parent.document).val();
                var long = $('.lon_distance', window.parent.document).val();
                var start_latlong = lat + ',' + long;
               function showPosition(position)
              {
               
                  if(typeof(coords) == 'undefined')
                {
                
                 var desti_latlong = "0,0";
                }else{
                    
                 var desti_latlong = position.coords.latitude + ','  + position.coords.longitude;
               }
                var request = {
                    origin: start_latlong ,
                    destination: desti_latlong,
                    travelMode: google.maps.TravelMode.DRIVING
                };
                var directionsService = new google.maps.DirectionsService();
                directionsService.route(request, function (response, status)
                {
                    if (status == google.maps.DirectionsStatus.OK)
                    {
                        directionsDisplay1.setDirections(response);
                    }
                });
            }
            google.maps.event.addDomListener(window, 'load', initialize1);
        </script>
    </head>
    <body>

<!--        <input id="pac-input" class="controls" type="text" placeholder="Search Box">-->
        <div>
        <div id="map-canvas" style="width:540px; height:370px"></div>
        </div>
    </body>
</html>