<?php

/**
 * Author : Jignesh Virani
 * Description : Demo of google clusturing using MySql.
 */
                
               
                define('UPLOADS_URL',"http://localhost/otojoy/uploads/venues/");
                define('DB_NAME', 'otojoy');
                define('DB_USER', 'root');
                define('DB_PASSWORD', '');
                define('DB_HOST', '192.168.1.204');
                $result = json_decode(json_encode($data), true);
                $LocationOnMap = json_encode($data);
                
                ?>
<html>
    
    <head>
        
<!--        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
        <script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places,geometry'></script>
        <script src="http://localhost/otojoy/assets/js/custom/frontsite/locationpicker.jquery.js"></script> 
        <script src="https://maps.googleapis.com/maps/api/js?v=3&sensor=false"></script>
       <script src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/src/markerclusterer_compiled.js" type="text/javascript"></script>

        <style>
            .photo-map {
                background-color : #222222;
                height : 100%;
                width : 100%;
            }

        </style>-->
        <script>
             /*
             * searching by categories
             */
                             $(document).on('click', '#btn-select', function (event) { 
                             var selected = $(".a:checked").map(function() {return this.id;}).get();
                             
                              
                                $.ajax({
                                type:'post',
                                url:'http://localhost/otojoy/frontsite/landing/getPlaceByCategories',
                                data: "categories=" +selected,
                                success: function(html)
                                {                                    
                                        if(html)
                                        { 
                                            
                                             $("#cancel").replaceWith(html);
                                        }
                                }
                          });
                          });
                          /*
                          * End of serching by categories.
                           */
            
            var marker;
            var gm_map;
            var markerArray = [];
            var address = 'CA';
            //var infowindow;
            var geocoder = new google.maps.Geocoder();
            var locations_window ='<?php  echo $LocationOnMap ; ?>';
            
           //pass the latitude and longitude.
            var latlng = new google.maps.LatLng(34.4471, -119.7307);
            geocoder.geocode({location: latlng}, function (results, status) {
                 var bounds1 = new google.maps.LatLngBounds();
                if (status == google.maps.GeocoderStatus.OK) {
                    gm_map.setCenter(results[0].geometry.location);
                   // gm_map.fitBounds(results[0].geometry.bounds);
                    bounds1.extend(results[0].geometry.location);
                    //gm_map.fitBounds(bounds1);
                    gm_map.setZoom(15);
                    
                } else {
                    alert("Unable to complete the geological setting due to the following error:\n\n" + status);
                }
            });
            
            function getCluserMarkers(locations) {               
                
                var i = "";
                var lat = "";
                var long = "";
                var clusterMarkers = [];
                var marker = "";
                var ico_me;
                var name;
                var address;
                var category;
                var cpin;
                var id;
                var image;
                var content;
                var infowindow;
                var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
                var imageIcon= '<?php echo UPLOADS_URL.'venues/' ;?>'
                for (i in locations) {
                    //console.log(locations[i]);
                    
                    if(locations[i].cnam =='Theater')             // icon 1
                    {ico_me = iconBase + 'parking_lot_maps.png' ;}
                    else if(locations[i].cnam=='Library')          // icon 2
                    {ico_me  = iconBase + 'library_maps.png';}
                    else if(locations[i].cnam=='Goverment')           // icon 3
                    {ico_me  = iconBase + 'schools_maps.png';}
                    else if(locations[i].cnam=='House Worship')        // icon 4
                    { ico_me= iconBase + 'info-i_maps.png';}
                    else if(locations[i].cnam == 'Church')               // icon 5
                    {ico_me=iconBase +'factory_maps.png';}
                    else
                    {ico_me="";}
                    
                    //add name in marker
                     name= locations[i].name;
                     address= locations[i].address;
                     category= locations[i].cname;
                     image= locations[i].venue_image;
                     cpin=locations[i].cpin;
                     id=locations[i].id;
                   
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(locations[i].latitude, locations[i].longitude),
                        map: gm_map,
                        icon: ico_me,
                        zoom: 5
                       // content: content
                    });
                    //display click event
                    content = "<img width='100' height='100' src=" +imageIcon+id + '/' + image + '><br>'+ 
                              "<a href=''>" + name +  '</a><br>' + 
                              "<b>address</b>: " + address + '<br>' +
                              "<b>category</b>:" + category +'<br>'+
                              "<a href='' id='"+ id +"' class='selector'><button>View More</button></a>";
                    var infowindow= new google.maps.InfoWindow({content: content, maxWidth:1000, maxHeight:800})
                    google.maps.event.addListener(marker, 'mouseover', (function (marker,content,infowindow) {
                            return function () {
                              infowindow.setContent(content);
                              infowindow.open(gm_map,marker);
                            }
                        })(marker,content,infowindow));
                        //click event end
                    clusterMarkers.push(marker);
                }
                return clusterMarkers;
            }
            
               // intialize google map
                function initialize() {
                
                var marker, i;
                var clusterMarkers = "";
               
                if (locations_window != 0) {
                   locations_window = JSON.parse(locations_window);
                } else {
                   locations_window = false;
                }
                if (locations_window) {
                    clusterMarkers = getCluserMarkers(locations_window);
                }
                
                var options_googlemaps = {
                    minZoom: 0,
                    //  maxZoom: 200,
                    zoom: 0,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    streetViewControl: false
                }
                
                gm_map = new google.maps.Map(document.getElementById('google-maps'), options_googlemaps);
                console.log(gm_map);
                 if (clusterMarkers.length > 0) {

                        var options_markerclusterer = {
                        gridSize: 20,
                        maxZoom: 80,
                        zoomOnClick: true,// using style
                
                    };
                    var markerCluster = new MarkerClusterer(gm_map, clusterMarkers, options_markerclusterer);
                    google.maps.event.addListener(markerCluster, 'clusterclick', function (cluster) {
                    });
                }
            }
             $(document).ready(function () {
                initialize();
            });
            
                                        // collect selected checkbox id's
                            
                    //auto complete for venues name.
                    
                
                
</script>
</head>
    <body>
        
        
         
<!--        <input id="pac-input" class="controls" type="text" placeholder="Search Box">-->
       
            <div class="photo-map" id="google-maps">
            </div>  
       
        
    </body>
</html>