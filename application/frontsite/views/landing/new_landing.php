        <?php
        
        $TempImage = UPLOADS_URL . 'venues/';
         //count the total data.
        $TotalResult = count($data);
        ?>
        <section class="list-container">
            <h3 class="text-center list-heading"><?= $TotalResult; ?>&nbsp;Venues found..</h3>
          
            
            <div class="container-fluid">
                <?php 
                          $array = json_decode(json_encode($data), true);
                          mprd($array); exit;
                          foreach($array as $key => $Value){
                              
                              if((strpos($Value['images'], '^')) == TRUE)
                              { 
                                  $Value['images']= strstr( $Value['images'], '^' ,TRUE);
                              }
                    ?>        

                <div class="search-venue-item col-md-4 col-sm-6 div_venue_details " id="<?= $Value['id']; ?>" >
                        <div class="<?php echo ($Value['is_certified'] == 1) ? 'looped' : 'non-looped'; ?>">

                            <?php if ($Value['images'] != "") { ?>
                            <div class="cus-bg" style="background:url(<?= $TempImage . $Value['id'] . '/' . $Value['images']; ?>) no-repeat center; background-size:cover; height:250px;"></div>
                            <?php } ?>
                            <?php if ($Value['images'] == "") { ?>
                                <div class="cus-bg" style="background:url(<?= UPLOADS_URL . 'default_venue.png'; ?>) no-repeat center; background-size:cover; height:250px;"></div>
                            <?php } ?>
                            <?php if ($Value['is_certified'] == 0) { ?><div class="non-looped-header">Not looped</div> <?php } ?>       
                            <div class="overlay-container">

                                <div class="list-overlay">
                                    <p><strong><?= $Value['name']; ?></strong></p>
                                    <p></p>
                                </div>

                                <div class="list-desc">
                                    <div class="row">
                                        <div class="col-xs-9">
                                            <div class="col-xs-12"><img class="g-icon" src="<?= IMAGE_URL . 'marker-icon.png'; ?>" /> 2.7   miles</div>
                                            <div class="clearfix"></div>
                                            <div class="col-xs-12"><img class="g-icon" src="<?= IMAGE_URL    . 'address-icon.png'; ?>" /> <?= $Value['address']; ?>, <?= $Value['city']; ?> <?= $Value['zipcode']; ?></div>
                                        </div>
                                        <div class="col-xs-3">
                                            <?php if ($Value['is_certified'] == 1) { ?>
                                                <img class="img-responsive loop-img" src="<?= IMAGE_URL . 'loop-certified.png'; ?>" />
                                            <?php }if ($Value['is_certified'] == 0) { ?>
                                                <div class="loop-btn-container"><a class="loop-btn" href="javascript:void(0);">Request</a></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div id="venue-detail">
                <div class="container-fluid" id="ven-det">
                    <div class="list-detail-container">
                        <div class="notch notch-1-3"></div>
                        <div class="clearfix">
                            <div class="col-sm-4">
                                <div class="slider-container">
                                    <div class="slider-wrapper theme-default">
                                        <div id="slider" class="nivoSlider">
                                            
                                            <img class="img-responsive" src="<?= IMAGE_URL . 'slide1.png'; ?>" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h1>St. Andrews</h1>
                                        <h4><i class="fa fa-th-large"></i> Theatre </h4>
                                        <p>&nbsp;</p>
                                        <p>&nbsp;</p>
                                        <img class="img-responsive loop-img" src="<?= IMAGE_URL . 'loop-certified.png'; ?>" />

                                        <div class="detail-list-adds"><img class="icon-mid" src="<?= IMAGE_URL . 'marker-icon.png'; ?>" /> 140 W, 46th St, New York NY 100036 </div>

                                    </div>

                                    <div class="col-sm-6">
                                        <p class="section-head"><img class="v-middle" src="<?= IMAGE_URL . 'headphone-icon.png'; ?>" />&nbsp; Looped Room Detaills</p>
                                        <p class="clearfix"><span class="pull-left blue">Room</span><span class="pull-right blue">Last Verified</span></p>
                                        <p class="clearfix"><span class="pull-left">Audi 1</span><span class="pull-right">5 Days Ago</span></p>
                                        <p class="clearfix"><span class="pull-left">Wagen Hall</span><span class="pull-right">4 Weeks Ago</span></p>
                                        <p class="clearfix"><span class="pull-left">Business hall</span><span class="pull-right">2 Months Ago</span></p>

                                        <p class="section-head"><img class="v-middle" src="<?= IMAGE_URL . 'phone-icon.png'; ?>" />&nbsp; &nbsp; Contact Info</p>
                                        <p class="clearfix"><span class="pull-left">Phone: +12-12-362-1389</span><span class="pull-right"><img src="<?= IMAGE_URL . 'circle-phone-icon.png'; ?>" /></span></p>
                                        <p class="clearfix"><span class="pull-left">Email: events@standrews.com</span><span class="pull-right"><img src="<?= IMAGE_URL . 'circle-email-icon.png' ?>" /></span></p>
                                        <p class="clearfix"><span class="pull-left">Website: www.standrews.com</span><span class="pull-right"><img src="<?= IMAGE_URL . 'circle-web-icon.png'; ?>" /></span></p>

                                        <p class="section-head"><img class="v-middle" src="<?= IMAGE_URL . 'info-icon.png'; ?>" />&nbsp; Additional Information</p>
                                        <p class="clearfix"><span class="pull-left">Please collect the headset from the front desk at this venue.</span></p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <a class="btn btn-general" href="">Get Directions</a>
                                    </div>

                                    <div class="col-sm-6">
                                        <a id="feedback-btn" class="btn btn-general" href="" data-toggle="modal" data-target="#modal-feedback">Feedback</a>
                                    </div>
                                </div>

                            </div>

                        </div>  
                    </div> <!-- END list-detail-container -->
                </div>  
            </div>
        </section>