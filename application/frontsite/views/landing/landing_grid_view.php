<?php if($data > 0) {?>
<section class="list-container">
                <?php
               
                $TempImage = UPLOADS_URL . 'venues/';
                $TotalResult = count($data);
                 ?>
                <?php if ($TotalResult > 0) { ?>
                    <h3 class="text-center list-heading"><?= $TotalResult; ?>&nbsp;Venues found.</h3>
                    <div class="container-fluid">
                    <?php } else { ?>
                        <h3 class="text-center list-heading">There are no venues matching your search criteria.</h3>
                        <div class="container-fluid">

                        <?php } ?>
                        <?php
                       // $array = json_decode(json_encode($data), true);
                        foreach ($data as $key => $Value) {

                            ?>        

                            <div class="search-venue-item col-md-4 col-sm-6 div_venue_details " id="<?= $Value['id']; ?>" >
                                <div class="<?php echo ($Value['type'] == 1) ? 'looped' : 'non-looped'; ?> <?php echo ($Value['is_certified'] == '1' && $Value['is_certified'] != '') ? 'loopedborderblue' : ''; ?>">

                                    <?php if ($Value['venue_image'] != "") { ?>
                                        <div class="cus-bg1" style="background:url(<?= $Value['venue_image']; ?>) no-repeat center; background-size:cover;"></div>
                                    <?php } ?>
                                    <?php if ($Value['venue_image'] == "") { ?>
                                        <div class="cus-bg1" style="background:url(<?= UPLOADS_URL . 'default_venue.png'; ?>) no-repeat center; background-size:cover;"></div>
                                    <?php } ?>
                                    <?php if ($Value['type'] == 2) { ?><div class="non-looped-header">Not looped</div> <?php } ?>       
                                    <div class="overlay-container">

                                        <div class="list-overlay">

                                            <p><h1 id="fd-name"><?= $Value['name']; ?></h1></p>
                                            <p><h4 style=""><i class="fa fa-th-large"></i><span id="fd-type"> <?= $Value['ctype']; ?></span> </h4></p>
                                        </div>

                                        <div class="list-desc">
                                            <div class="row">
                                                <div class="col-xs-9">
    <!--                                                    <div class="col-xs-12"><img class="g-icon" src=" //IMAGE_URL . 'circle-phone-icon.png'; ?>" /> //$Value['phone']; ?></div>-->
                                                    <div class="clearfix"></div>
                                                    <div class="col-xs-1 top10 paddingleft6" style=""><img class="g-icon" src="<?= IMAGE_URL . 'address-icon.png'; ?>" /></div> <div class="col-xs-10" style=""><?= $Value['address']; ?><br><?= $Value['city']; ?>, <?= $Value['state']; ?> <?= $Value['zipcode']; ?></div>
                                                </div>
                                                <div class="col-xs-3" style="left: 1.333333%">
                                                    <?php if ($Value['is_certified'] == 1) { ?>
                                                        <img class="img-responsive loop-img" src="<?= IMAGE_URL . 'loop-certified.png'; ?>" />
                                                    <?php } ?>
                                                    <?php if ($Value['type'] == 2) { ?>
                                                        <div class="loop-btn-container">   <a class="loop-btn orange_request" href="#" id="request" data-grid="9">Request</a></div>
                                                        <input type="hidden" id="venue_req_id1" value="<?= $Value['id']; ?>">
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                        
                    <div id="venue-detail">
                        <div class="container-fluid" id="ven-det">

                        </div>  
                    </div>

                      
<?php 
}else{ ?>
  <section class="list-container">
                            <h3 class="text-center list-heading">There are no venues matching your search criteria.</h3>
                        </section>
<?php } ?>

