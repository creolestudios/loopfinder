 
<div id="ajax-content">
        <section class="map-container">
           
            <div id="full-detail" class="list-detail-horizontal-container container-fluid map-right-box no-display">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row relative">
                            <a class="sidebar-close" href="#"><img src="<?= IMAGE_URL . 'icon-close.png'; ?>" alt="" /></a>
                            <div class="slider-container">
                                <div class="slider-wrapper theme-default"><div id="fd-slider"></div></div>
                            </div>
                            <div class="sidebar-overlay clearfix">
                                <div id="fd-loop" class="col-xs-3">
                                    <img class="img-responsive loop-img" src="<?= IMAGE_URL . 'loop-certified.png'; ?>" />
                                </div>
                                <div class="col-xs-9">
                                    <h1 id="fd-name">-</h1>
                                    <h4><i class="fa fa-th-large"></i> <span id="fd-type">-</span> </h4>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="clearfix">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-12 sidebar-section">
                                <div class="detail-list-adds"><img class="icon-mid" src="<?= IMAGE_URL . 'marker-icon.png'; ?>" /> <span id="fd-address">-</span> </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12 sidebar-section">
                                <p class="section-head"><img class="v-middle" src="<?= IMAGE_URL . 'headphone-icon.png'; ?>" />&nbsp; Looped Room Detaills</p>
                                <p class="clearfix"><span class="pull-left blue">Room</span><span class="pull-right blue">Last Verified</span></p>
                                <div id="fd-rooms"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 sidebar-section">
                                <p class="section-head"><img class="v-middle" src="<?= IMAGE_URL . 'phone-icon.png'; ?>" />&nbsp; &nbsp; Contact Info</p>
                                <p class="clearfix"><span class="pull-left">Phone: <span id="fd-phone">-</span></span><span class="pull-right"><img class="xs-icon" src="<?= IMAGE_URL . 'circle-phone-icon.png'; ?>" /></span></p>
                                <p class="clearfix"><span class="pull-left">Email: <span id="fd-email">-</span></span><span class="pull-right"><img class="xs-icon" src="<?= IMAGE_URL . 'circle-email-icon.png'; ?>" /></span></p>
                                <p class="clearfix"><span class="pull-left">Website: <span id="fd-web">-</span></span><span class="pull-right"><img class="xs-icon" src="<?= IMAGE_URL . 'circle-web-icon.png'; ?>" /></span></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 sidebar-section">
                                <p class="section-head"><img class="v-middle" src="<?= IMAGE_URL . 'info-icon.png'; ?>" />&nbsp; Additional Information</p>
                                <p class="clearfix"><span class="pull-left" id="fd-info">-</span></p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <a class="btn btn-general" href="#">Get Directions</a>
                            </div>

                            <div class="col-sm-6">
                                <a class="btn btn-general" href="#" data-toggle="modal" data-target="#modal-feedback" id="feed-button">Feedback</a><i id="feed-btn" class="feed-btn1"></i> 
                            </div>
                        </div>

                    </div>  
                </div>
            </div>
            <div id="map-can">
                
        </div>

    </section>
            </div>

      <?php
    // echo $this->session->userdata("view");
    $result = json_decode(json_encode($data), true);
//mprd($result);
    $returnData = array();
    foreach ($result as $key => $value) {
        
          if ($key = 'rooms'){
                   $rooms['rooms']= explode('^',$value['rooms']);
                   $retunData = array_merge($value,$rooms);
                   //var_dump($retunData);exit;
        }
        if($key = 'last_verified')
        {
            $roomvarified['last_verified']= explode('^', $value['last_verified']);
            $returnData1 =  array_merge($retunData,$roomvarified);
        }
        if ($key = 'images') {
            $images['images'] = explode('^', $value['images']);
            $VenuesOnMap1 = array_merge($returnData1, $images);
        }
        $VenuesOnMap[]['data'] = $VenuesOnMap1;
    }

   
    $LocationOnMap = addslashes(json_encode($VenuesOnMap));
   // print_r($LocationOnMap);exit;
    ?>
     <script>
     var markers_data = '<?= $LocationOnMap ?>';</script>

<script>
    // GOOGLE MAPS ----
        function initialize() {


            if (typeof (document.getElementById('map-canvas')) == 'undefined')
                return;

            // Do not progress is <markers_data> is not found.
            // <markers_data> should only be available in pages with map.
            if (typeof (markers_data) == 'undefined')
                return;

            // Init Bounds ---
            var bound = new google.maps.LatLngBounds();
//            var json_obj = JSON.parse(markers_data)
//            for (var i in json_obj)
//            {
//                console.log(json_obj[i].data.lat);
//            }

            //return false;
            // Init LatLng's array and extend bounding region ---
            var json_obj = JSON.parse(markers_data)
             //console.log(json_objjson_obj);
            for (i in json_obj) {
                //json_obj[ i ].latlng = new google.maps.LatLng(json_obj[ i ].data.lat, json_obj[ i ].data.lng);
                bound.extend(json_obj[i]['latlng']= new google.maps.LatLng(json_obj[ i ].data.lat, json_obj[ i ].data.lng));
            }

            // Map Options ---
            var mapOptions = {
                scrollwheel: false,
                center: bound.getCenter()
            };

            // Init Map and autofit area by bounding region ---
            var map = new google.maps.Map(document.getElementById('map-can'), mapOptions);
            map.fitBounds(bound);

            // Map Styles ---
            map.set('styles', [{"featureType": "landscape", "stylers": [{"saturation": -100}, {"lightness": 65}, {"visibility": "on"}]}, {"featureType": "poi", "stylers": [{"saturation": -100}, {"lightness": 51}, {"visibility": "simplified"}]}, {"featureType": "road.highway", "stylers": [{"saturation": -100}, {"visibility": "simplified"}]}, {"featureType": "road.arterial", "stylers": [{"saturation": -100}, {"lightness": 30}, {"visibility": "on"}]}, {"featureType": "road.local", "stylers": [{"saturation": -100}, {"lightness": 40}, {"visibility": "on"}]}, {"featureType": "transit", "stylers": [{"saturation": -100}, {"visibility": "simplified"}]}, {"featureType": "administrative.province", "stylers": [{"visibility": "off"}]}, {"featureType": "water", "elementType": "labels", "stylers": [{"visibility": "on"}, {"lightness": -25}, {"saturation": -100}]}, {"featureType": "water", "elementType": "geometry", "stylers": [{"hue": "#ffff00"}, {"lightness": -25}, {"saturation": -97}]}]);


            // Place each marker & their info windows
            for (i in json_obj) {
               
                /* -- INFOBOX -- */
                json_obj[i]['infoBox'] = new InfoBox({
                    content : gen_infoBox_content(json_obj[i]['data']),
                    disableAutoPan: false,
                    maxWidth: 0,
                    pixelOffset: new google.maps.Size(-175, -50),
                    zIndex: null,
                    boxStyle: {
                        opacity: 1,
                    },
                    closeBoxMargin: "30px 20px 2px 2px",
                    closeBoxURL: "<?= IMAGE_URL . 'icon-close.png'; ?>",
                    infoBoxClearance: new google.maps.Size(1, 1),
                    alignBottom: true,
                });
               // console.log(json_obj[i]['data']);
                json_obj[i]['marker'] = new google.maps.Marker({
                    position: json_obj[i]['latlng'], 
                    map: map,
                    title:  json_obj[ i ].data.name,
                    icon: '<?= UPLOADS_URL . 'category_icons/'; ?>' + json_obj[i].data.categories_pin
                    
                     
                });
               
                
                (function (markers_data) {
                    google.maps.event.addListener(markers_data['marker'], 'click', function () {
                        //markers_data['marker'].setIcon("<?= IMAGE_URL . 'custom-map-marker2.png'; ?>");
                        markers_data[ 'infoBox' ].open(map, markers_data['marker']);
                        display_venue_detail_pane(markers_data['data']);

                    });
                     //console.log(json_obj);
                })(json_obj[i]);
            }
            google.maps.event.addListenerOnce(map, 'idle', function () {
                map.setZoom(map.getZoom() - 1);
            });

        }

        // Init Google Map Now only if Google Map is loaded in Current Page ---
        if (typeof (google) != 'undefined' && typeof (google.maps) != 'undefined')
            google.maps.event.addDomListener(window, 'load', initialize);
          
           function gen_infoBox_content(data) {
           
           if(data['is_certified'] == 1 && data['is_certified'] != "")
           {
               var certified_image = "<img class='img-responsive loop-img' src='<?= IMAGE_URL .'loop-certified.png'; ?>' />";
           }
           else
           {
               var certified_image = "";
           }
           
           var html = '<div class="ufxd-infoBox">' +
                    '<div class="looped clearfix">' +
                    '<div class="cus-bg" style="background:url('+ data['venue_image']+') no-repeat center; background-size:cover; height:250px;"></div>' +
                    '<div class="overlay-container">' +
                    '<div class="list-overlay">' +
                    '<p><strong>' + data['name'] + '</strong></p>' +
                    '<p>' + data['ctype'] + '</p>' +
                    '</div>' +
                    '<div class="list-desc">' +
                    '<div class="row">' +
                    '<div class="col-xs-9">' +
                    '<div class="col-xs-2"><img class="img-responsive icon-medium" src="<?= IMAGE_URL . 'marker-icon.png'; ?>" /></div>' +
                    '<div class="col-xs-10">' + data['distance'] + '</div>' +
                    '<div class="col-xs-2"><img class="img-responsive icon-medium" src="<?= IMAGE_URL . 'address-icon.png'; ?>" /></div>' +
                    '<div class="col-xs-10">' + data['address'] + data['city'] + data['state'] + data['zipcode'] +'</div>' +
                    '</div>' +
                    '<div class="col-xs-3">' + certified_image  +
                    
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';

            return html;
        }

        function display_venue_detail_pane(venue) {
            
          
             $('.ufxd-infoBox').remove();
            var image_icon = "<?= UPLOADS_URL. 'venues/';?>"; 
            $('#full-detail').fadeIn();
            if (venue.is_certified == 1)
                $('#fd-loop').show();
            else
                $('#fd-loop').hide();
            var html = '<div id="fd-slider" class="nivoSlider">';
            for (i in venue.images)
                html += '<img src="'+ image_icon  + venue.id + '/' + venue.images[ i ] + '" style=heigth: 296px">';
            html += '</div>';
            $('#fd-slider').replaceWith(html);
            setTimeout(function () {
                $('#fd-slider').nivoSlider({effect: 'fade'});
            }, 10);

            $('#fd-name').html(venue.name);
            $('#fd-type').html(venue.ctype);
            $('#fd-address').html(venue.address);
            $('#fd-phone').html(venue.phone);
            $('#fd-email').html(venue.email);
            $('#fd-web').html(venue.website);
            $('#fd-info').html(venue.public_comment);
            $('.feed-btn1').attr('id',venue.id);
            html = '';
            for (i in venue.rooms) {
                html += '<p class="clearfix"><span class="pull-left">' + venue.rooms[i] + '</span><span class="pull-right">' + 
                        '<abbr class="timeago" title="' + venue.last_verified[i] + '"></abbr></span></p>'
            }
            $('#fd-rooms').html(html);
           
            $("abbr.timeago").timeago();

        }
	
        </script>
