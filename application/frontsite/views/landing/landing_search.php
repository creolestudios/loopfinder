<?php
$headerData = $this->headerlib->data();
$JsArr = array("SCRIPT" => $headerData['javascript'], 'PLUGINS' => $headerData['javascript_plugins']);

if (isset($marker_data) && !empty($marker_data)) {
    $MarkerData = addslashes(json_encode($marker_data));
} else {
    $MarkerData = '';
}
if (isset($all_marker_data) && !empty($all_marker_data)) {

    $all_marker_data = addslashes(json_encode($all_marker_data));
} else {
    $all_marker_data = '';
}
?>
<html  lang="en-us">
    <head>
        <?php $headerData['meta_tags']; ?>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <title><?= MAINTITLE; ?></title>
        <!------- BEGIN STYLESHEET   ---->
        <?php echo $headerData['stylesheets']; ?>
        <link href="<?= CSS_IMAGE_URL . 'frontsite/font-awesome.min.css' ?>" rel="stylesheet"> 
        <link href="<?= PLUGIN_URL ?>simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
        <link href="<?= PLUGIN_URL ?>icheck/skins/all.css" rel="stylesheet"/>
        <!-- END THEME STYLESHEET -->

        <!-- BEGIN THEME STYLES --> 

        <?php //echo $headerData['themestyles'];      ?>
        <!-- END THEME STYLES -->
        <!---------- favicon icon -------------->
        <link rel="shortcut icon" href="<?= IMAGE_URL ?>favicon.ico"/>

        <!--------- LOAD EXTERNAL JS FILE ---------------------->
        <style>
            .list-container{
                display: none;
            }
            .alerter1 {
                width: 390px;
                height: 50px;
                background-color: #323a4a;
                display:none;
                position: absolute;
                top:0;
                bottom: 0;
                left: 0;
                right: 0;
                opacity:0.9;
                margin: auto;
                text-align:center;
                border: 4px #323a4a solid;
                border-radius: 7px;
                z-index: 9990;
                color: #fff;                
            }
            .alerter{
                width: 390px;
                height: 50px;
                background-color: #323a4a;
                position: absolute;
                top:0;
                bottom: 0;
                left: 0;
                right: 0;
                opacity:0.9;
                margin: auto;
                text-align:center;
                border: 4px #323a4a solid;
                border-radius: 7px;
                z-index: 9990;
                color: #fff;                
            }
            .clip-svg {
                clip-path: url(#myClip);
                width: auto;
                height: auto;
                position: relative;
                left: 50%;
                right: 50%;
                top: 6px;
                margin-bottom:9px;
            }
            .infoBox img{ z-index: 99999 !important;}
            #media-top-margin{margin-top: 85px !important;}
            .modal-dialog{margin-top: 85px !important;}
        </style>
    </head>
    <body>
        <?php $this->load->view('include/map_grid_view_menu'); ?>

        <!---------------    BEGIN MAP SECTION ------------------------>
        <a class="back-to-top btn btn-general btn-viewpane" style="display: none;" href="#" id="onclick_pdf">Print</a>
        <div id="ajax-content">
            <section class="map-container">
                <div id="full-detail" class="list-detail-horizontal-container container-fluid map-right-box no-display">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row relative">
                                <a class="sidebar-close" href="#"><img src="<?= IMAGE_URL . 'icon-close.png'; ?>" alt="" /></a>
                                <div class="slider-container">
                                    <div id="non_looped"></div>
                                    <div class="slider-wrapper theme-default"><div id="fd-slider"></div></div>
                                </div>
                                <span class=" sidebar-overlay clearfix" id="slider_message" style="bottom: 68px;font-size:17px; text-align: center;font-weight:bold;"></span>
                                <div class="sidebar-overlay clearfix list-overlay">
                                    <div class="col-xs-12">
                                        <h1 id="fd-name">-</h1>
                                        <h4><i class="fa fa-th-large"></i> <span id="fd-type">-</span> </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-12 sidebar-section address_in_iphone">
                                  <div class="detail-list-adds" style="margin-top: 15px;float:left;"><!--  <img class="icon-mid" src="<?= IMAGE_URL . 'address-icon.png' ?>"--><i class="fa  fa-map-marker" style="font-size: 18px;"></i></div>
                                    <div class="detail-list-adds" style="margin-left: 9px;float:left;"><span id="fd-address"></span></div>
                                </div>
                            </div>
                            <div class="row" id="looped_room_div">
                                <div class="col-sm-12 sidebar-section">
                                    <p class="section-head">
<!--                                        <img class="v-middle" src="<?= IMAGE_URL . 'headphone-icon.png'; ?>" style="width:17.5px;"/>-->
                                        <i class="fa  fa-headphones" style="font-size: 18px;"></i>&nbsp;Looped Room Details</p>
                                    <hr class="sidebar-section3">
                                    <p class="clearfix" style="margin:5px auto 12px"><span class="pull-left blue">Room</span><span class="pull-right blue">Last Verified</span></p>
                                    <div id="fd-rooms"></div>
                                </div>
                            </div>
                            <!--                            <div class="row" id="chapter_div">
                                                            <div class="col-sm-12 sidebar-section">
                                                                <p class="clearfix" style="margin:9px auto 4px"><span class="pull-left" style="font-weight: 600; font-size: 15px;"><i class="fa fa-calendar" style="font-size: 18px;"></i>&nbsp;Meetup Date </span><span class="pull-right chapter_text"></span></p>
                                                            </div>
                                                        </div>-->
                            <div class="row">
                                <div class="col-sm-12 sidebar-section">
                                    <p class="section-head">
<!--                                        <img class="v-middle" src="<?= IMAGE_URL . 'phone-icon.png'; ?>" />-->
                                        <i class="fa   fa-phone" style="font-size: 18px;"></i>&nbsp;Contact Info</p>
                                    <hr class="sidebar-section3">
                                    <p class="clearfix phone_num_b" style="margin: 8px auto 5px"><span class="pull-left">
<!--                                            <img class="v-middle phone_image" src="<?= IMAGE_URL . 'circle-phone-icon.png'; ?>" style="height: 19px;"/>-->
                                            <i class="fa fa-phone-square" style="font-size: 18px;"></i></span>&nbsp;<span id="fd-phone"></span></p>
                                    <p class="clearfix email_b" style="margin: 8px auto 5px"><span class="pull-left">
<!--                                            <img class="v-middle mail_image" src="<?= IMAGE_URL . 'circle-email-icon.png'; ?>" />-->
                                            <i class="fa  fa-envelope-square" style="font-size: 18px;"></i>&nbsp;<a href="mailto:" id="email_link" style="color:#21a5e8 !important; text-decoration: underline;"><span id="fd-email"></span></a></span></p>
                                    <p class="clearfix link_b" style="margin: 8px auto 5px"><span class="pull-left">
<!--                                            <img class="v-middle web_image" src="<?= IMAGE_URL . 'circle-web-icon.png'; ?>" />-->
                                            <i class="fa fa-globe" style="font-size: 18px;"></i>&nbsp;<a href<a id="myLink" href="" target="_blank" style="color:#21a5e8 !important;  text-decoration: underline;"><span id="fd-web"></span></a></span></p>
                                </div>
                            </div>                           
                            <br>
                            <div class="col-sm-12">
                                <a class="btn btn-general map_sidebar_btn" href="#" data-id=""></a>
                                <!--<a class="btn btn-general orange_request map_sidebar_btn" href="#"  id="request" data-id="">Request</a> --->
                                <i data-id="" class="feed-btn1"></i> 
                            </div>
                        </div>
                    </div>  
                </div>
                <div id="map-canvas">
                </div>
                <div id="pdf_class" style="display:none;">
                </div>
                <div class="alerter1">
                    <div style="padding: 1px;">
                        <img src="<?= IMAGE_URL ?>sad_face.png" alt="Which Wich" style="width: 30px;margin-top:5px">
                        <span style="float: right;margin: 10px 2px;">There are no venues matching your search criteria.</span>
                    </div>
                </div>
            </section>
            <section class="list-container" id="list_height_id">
<!--                <span class="append-count"></span>-->
                <!--                <h3 class="text-center list-heading"></h3> -->
                <div class="container-fluid">
                    <div class="alerter_grid" style="display: none;">
                        <div style="padding: 1px;">
                            <img src="<?= IMAGE_URL ?>sad_face.png" alt="Which Wich" style="width: 30px;margin-top:5px">
                            <span style="float: right;margin: 10px 2px;">There are no venues matching your search criteria.</span>
                        </div>
                    </div>
                </div>
                <div id="venue-detail">
                    <div class="" id="ven-det">
                        <img src="<?= IMAGE_URL . 'ajax_loader_blue_32.gif' ?>" alt="" class="clip-svg">
                    </div>
                </div>
            </section>
        </div>
        <section class="venue-more" style="display: none;">
            <div class="container-fluid">
                <div class="col-sm-4 col-sm-offset-4" id="cat_btn">
                    <div class="more-button-container">
                        <a class="btn btn-more" type="button" href="javascript:void(0)" id="venue_show_more">More Venues</a> 
                    </div>
                </div>
                <div class="col-sm-4 col-sm-offset-4" id="cat_img"  style="text-align:center;display: none;">
                    <div class="">
                        <img src="<?= IMAGE_URL . 'categories_loader.gif'; ?>"/>
                    </div>
                </div>
            </div>
        </section>
        <?php $this->load->view('include/footer', $JsArr); ?>

        <!-- Button trigger modal -->
        <!-- Modal -->
        <div class="modal" id="modal-feedback" tabindex="-1" >
            <div class="modal-dialog" id="media-top-margin" style="margin-top: 85px !important;">
                <div class="modal-content corsola">
                    <div class="modal-header"style="padding-bottom: 0px !important;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><img src="<?= IMAGE_URL . 'close-icon.png'; ?>" /></span></button>
                        <h4 class="modal-title center" id="myModalLabel"><strong>Feedback</strong></h4>
                        <p class="text-center" styel="margin-bottom:0px">Please tell us about your experience at this venue</p>
                    </div>
                    <div class="modal-body model_set" id="send-detail" style="top:-38px important;">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="model-request" tabindex="-1" role="dialog" aria-labelledby="modal-feedback-label" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><img src="<?= IMAGE_URL . 'close-icon.png'; ?>" /></span></button>
                        <h4 class="modal-title center" id="myModalLabel"><strong>Request</strong></h4>                        
                    </div>
                    <div class="modal-body">
                        <div id="form_message_req" class="alert" style="display: none" tabindex="-1">hello</div>
                        <form  id="request-form" method="post" name="request-form">
                            <div class="inner-addon left-addon feedback-margin">
        <!--                                <i class="glyphicon"><img class="input-icon" src="<?= IMAGE_URL . 'user-icon.png'; ?>" /></i>-->
                                <i class="fa  fa-user input-icon"></i>
                                <input type="text" name="feed_name1" class="form-control input" placeholder="Name" id="feed_name1" />
                            </div>
                            <div class="inner-addon left-addon feedback-margin">
        <!--                                <i class="glyphicon"><img class="input-icon" src="<?= IMAGE_URL . 'email-icon.png'; ?>" /></i>-->
                                <i class="fa  fa-envelope input-icon"></i>
                                <input type="text" name="feed_email_request" class="form-control input" placeholder="E-Mail" id="feed_email_request"/>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-general btn-request-submit" id="request-submit" style="width:126px;">Thank You</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!---------------------LOAD FOOTER FILE ----------------->
        <?php //$this->load->view('include/footer');     ?>
        <!--------------------- END FOOOTER FILE ---------------->


        <!----------------- END MAP SECTION --------------------------->

        <script src="<?= JS_URL . 'frontsite/jquery.nivo.slider.js'; ?>"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places,geometry"></script>
        <script src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/src/markerclusterer_compiled.js" type="text/javascript"></script>
        <script src="<?= CUSTOM_JS . 'frontsite/google.map.infobox.js' ?>"></script>
<!--                <script src="<?= CUSTOM_JS . 'frontsite/venue_script.js' ?>"></script>-->
        <script src="<?= JS_URL . 'frontsite/jquery.timeago.js'; ?>"></script>
        <script src="<?= JS_URL . 'frontsite/blazy.min.js'; ?>"></script>
        <script src="<?= JS_URL . 'frontsite/jquery.lazyload.js'; ?>"></script>
        <script src="<?= JS_URL . 'frontsite/moment.js'; ?>"></script>

        <script src="<?= JS_URL . 'frontsite/jstz-1.0.4.min.js'; ?>"></script>
        <script src="<?= JS_URL . 'frontsite/moment-timezone-with-data.min.js'; ?>"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>jquery-validation/js/jquery.validate.min.js"></script>
        <script src="<?php echo PLUGIN_URL ?>icheck/icheck.min.js"></script>
        <script src="<?php echo PLUGIN_URL ?>jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>custom/locationpicker.jquery.js" type="text/javascript"></script>
<!--        <script src="<?= JS_URL . 'frontsite/jquery.jscroll.js'; ?>"></script>
        <script src="<?= JS_URL . 'frontsite/jquery.jscroll.min.js'; ?>"></script>-->

        <!------ PDF generator script start here.   ----->
<!--        <script src="<?= JS_URL . 'jspdf/jspdf.js'; ?>"></script>
        <script src="<?= JS_URL . 'jspdf/jspdf.plugin.addimage.js'; ?>"></script>
        <script src="<?= JS_URL . 'jspdf/jspdf.plugin.cell.js'; ?>"></script>
        <script src="<?= JS_URL . 'jspdf/jspdf.plugin.from_html.js'; ?>"></script>
        <script src="<?= JS_URL . 'jspdf/jspdf.plugin.javascript.js'; ?>"></script>
        <script src="<?= JS_URL . 'jspdf/jspdf.plugin.sillysvgrenderer.js'; ?>"></script>
        <script src="<?= JS_URL . 'jspdf/jspdf.plugin.split_text_to_size.js'; ?>"></script>
        <script src="<?= JS_URL . 'jspdf/jspdf.plugin.standard_fonts_metrics.js'; ?>"></script>
        <script src="<?= JS_URL . 'jspdf/jspdf.PLUGINTEMPLATE.js'; ?>"></script>
        <script src="<?= JS_URL . 'jspdf/libs/png_support/zlib.js'; ?>"></script>
        <script src="<?= JS_URL . 'jspdf/jspdf.plugin.addimage.js'; ?>"></script>
        <script src="<?= JS_URL . 'jspdf/libs/png_support/png.js'; ?>"></script>
        <script src="<?= JS_URL . 'jspdf/jspdf.plugin.png_support.js'; ?>"></script>
        <script src="<?= JS_URL . 'jspdf/FileSaver.js'; ?>"></script>
        <script src="<?= JS_URL . 'jspdf/html2canvas.js'; ?>"></script>-->
        <!------- End here ---->


        <script>
            var markers_data = '<?php echo $MarkerData; ?>';
<?php if (!empty($all_chapter_data)) { ?>
                markers_data = '<?php echo $all_chapter_data; ?>';
<?php } ?>
        </script>
        <script>
            $("#donate_link").click(function () {
                $("#donation_form").submit();
            });
            //  $('.map-canvas').height($(window).height());    
            var flag_categories = false;
            $(document).on('click', '.true_radio', function (event) {
                $('.iradio_square-blue').removeClass('checked');
                $("input:radio[name=reason_radio]:checked").val('0');
            });
            $(document).on('click', '.radio-1', function (event) {

                var val = $(this).val();
                if (val == "0")
                {
                    $("#reason_slide").slideDown("slow");
                    $(".icheck").removeClass('disabled');
                }
                if (val == "1")
                {
                    //$("#reason_slide").addClass('hide');
                    $("#reason_slide").slideUp("slow");
                    $(".icheck").addClass('disabled');
                }

            });
            /*
             * detail of venue
             */
            $(document).on('click', '#feed-button', function (event) {

                $('#modal-feedback').modal('show');
                var selected = $('.feed-btn2').data('id');
                var data_string = 'id=' + selected;
                $.ajax({
                    type: 'post',
                    url: '<?= BASEURL . 'landing/getFeedback' ?>',
                    data: data_string,
                    success: function (html)
                    {
                        if (html)
                        {
                            $("#send-detail").html("");
                            $("#send-detail").html(html);
                            $('#feedback-from').validate({
                                rules: {
                                    feed_name: {
                                        required: true,
                                        minlength: 4},
                                    feed_email: {
                                        required: true,
                                        email: true
                                    }
                                },
                                messages:
                                        {
                                            feed_name: {
                                                required: "Please enter your name.",
                                                minlength: "Please enter atleast 4 words.",
                                            },
                                            feed_email: {
                                                required: "Please enter your e-mail address.",
                                                email: "Invalid email"
                                            }
                                        },
                                submitHandler: function (form)
                                {
                                    var Selected = $("input:radio[name=reason_radio]:checked").val();
                                    if (typeof (Selected) == 'undefined' || Selected == '')
                                    {
                                        var Selected = '0';
                                    }
                                    var Name = $('#feed_name').val();
                                    var Email = $('#feed_email').val();
                                    var VenueNameList = $('#VenueNameList').val();
                                    var WorkWell = $("input:radio[name=work_radio]:checked").attr('id');
                                    var comment = $('#feed_comment').val();
                                    var dataString = 'Name=' + Name + '&Email=' + Email + '&Reasons=' + Selected + '&VenueName=' + VenueNameList + '&WorkWell=' + WorkWell + '&comment=' + comment;
                                    $.ajax({
                                        url: '<?= BASEURL . 'landing/getFeedbackSave'; ?>',
                                        type: 'POST',
                                        data: dataString,
                                        success: function (data) {

                                            var data = jQuery.parseJSON(data);
                                            $('#form_message').html(data.message);
                                            $('#form_message').show();
                                            $('#form_message').focus();
                                            $('#feedback-from')[0].reset();
                                            $('#feedback-from').css('opacity', '0.3');
                                            setTimeout(function () {
                                                $('#modal-feedback').modal('hide');
                                            }, 3000);
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            });
            $(document).on('click', '#request', function (event) {

                $("label.error").hide();
                $(".error").removeClass("error");
                $('#model-request').modal('show');
            });
            $(document).on('click', '#feed-button-map', function (event) {

                $('#modal-feedback').modal('show');
                var selected = $('.feed-btn1').data('id');
                var data_string = 'id=' + selected;
                $.ajax({
                    type: 'post',
                    url: '<?= BASEURL . 'landing/getFeedback' ?>',
                    data: data_string,
                    success: function (html)
                    {
                        if (html)
                        {
                            $("#send-detail").html("");
                            $("#send-detail").html(html);
                            $('#feedback-from').validate({
                                rules: {
                                    feed_name: {
                                        required: true,
                                        minlength: 4},
                                    feed_email: {
                                        required: true,
                                        email: true
                                    }
                                },
                                messages:
                                        {
                                            feed_name: {
                                                required: "Please enter your name.",
                                                minlength: "Please enter atleast 4 words.",
                                            },
                                            feed_email: {
                                                required: "Please enter your e-mail address.",
                                                email: "Invalid email"
                                            }
                                        },
                                submitHandler: function (form)
                                {

                                    var Selected = $("input:radio[name=reason_radio]:checked").val();
                                    if (typeof (Selected) == 'undefined' || Selected == '')
                                    {
                                        var Selected = '0';
                                    }
                                    var Name = $('#feed_name').val();
                                    var Email = $('#feed_email').val();
                                    var VenueNameList = $('#VenueNameList').val();
                                    var WorkWell = $("input:radio[name=work_radio]:checked").attr('id');
                                    var comment = $('#feed_comment').val();
                                    var dataString = 'Name=' + Name + '&Email=' + Email + '&Reasons=' + Selected + '&VenueName=' + VenueNameList + '&WorkWell=' + WorkWell + '&comment=' + comment;
                                    $.ajax({
                                        url: '<?= BASEURL . 'landing/getFeedbackSave'; ?>',
                                        type: 'POST',
                                        data: dataString,
                                        success: function (data) {

                                            var data = jQuery.parseJSON(data);
                                            $('#form_message').html(data.message);
                                            $('#form_message').show();
                                            $('#form_message').focus();
                                            $('#feedback-from')[0].reset();
                                            $('#feedback-from').css('opacity', '0.3');
                                            setTimeout(function () {
                                                $('#modal-feedback').modal('hide');
                                            }, 3000);
                                        }
                                    });
                                }
                            });
                        }
                    }
                });
            });</script>

        <script>
            var code_address_flag = false;
            $(document).ready(function () {

                //Bootstrap code for model fadeout.
                //scrollLocation = $(window).scrollTop();
               
                if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {

//                    $('.modal').on('show.bs.modal', function () {
//                        setTimeout(function () {
//                           
//                            $('.modal')
//                                    .addClass('modal-ios')
//                                    .height($(window).height())
//                                    .css({'margin-top': scrollLocation + 'px'});
//                        }, 0);
//                    });

                    $('input').on('blur', function () {
                       // alert('in iphone');
                        setTimeout(function () {
                            // This causes iOS to refresh, fixes problems when virtual keyboard closes
                            $(window).scrollLeft(0);
                            var $focused = $(':focus');
                            // Needed in case user clicks directly from one input to another
                            if (!$focused.is('input')) {
                                // Otherwise reset the scoll to the top of the modal
                                $(window).scrollTop($(window).scrollTop());
                            }
                        }, 0);
                    })

                }
                // Bootstrap model for iphone end


                $(document).on('click', '#filter_apply_btn', function () {
                    //  $('#filter-opener').trigger('click');
                });
                $(document).on('click', '#onclick_pdf', function () {
                    var doc = new jsPDF('p', 'mm', [297, 210]);
                    var specialElementHandlers = {
                        '#editor': function (element, renderer) {
                            return true;
                        }
                    };
                    var source = $('#pdf_class').html();
                    var logo_img = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQ0AAAAxCAYAAADA3fxqAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RTlBRkFEQkRCOEVFMTFFNEFEQjJCNzRBM0I0MzRBMDYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RTlBRkFEQkVCOEVFMTFFNEFEQjJCNzRBM0I0MzRBMDYiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpCQjI1Mjg2MEI4RUQxMUU0QURCMkI3NEEzQjQzNEEwNiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpFOUFGQURCQ0I4RUUxMUU0QURCMkI3NEEzQjQzNEEwNiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PjjUvoEAAA/9SURBVHja7F1bbBTXGf4NthOMEhvIpQnELCQFKYFkXUtQNVDWUl8iU2E3b6CyXql9cKQW/BTyZPsp9MmmleChlXxRYvWpGCmUhz6wCOchSNQmzkMSSFjIrYpMsiblFnPp+c5l9+x4LmeGvbrnk8br3Zk5Mzt7zjfffztT9+DBAyo26urqitNQcjzG/ibYgtdX2NListUZtmTZkqax/TPFOGwpromFxVJBXdWRRnI8jr9s6ZJkEQYgj0m2nGAEMmlJw8JiKZNGcryH/T3Alrj6aMdPn6CdLzzBXtdQ84oG2rq2uWCXq9/d5Mvsl/N09tI1mro4R/O3FtTqDFvG2DLMCCRrScPCYqmQRnIc5ke/NEM4Uezb9hx1vvwMJ4qwOPnhN/Te7H9o4oOruvroY8QxaknDwqLWSSM5PsT+HlRk8dZrm2kHUxbFABTI26c+0ckjzZYUI4+MJQ0Li1ojjeQ4HJqnYYpATRzb18aVRSkw+9U89b4zzV+l6uhmxJG2pGFhUSukIRydIIwWqIuJ322LZIaExaF/fETH0p+ptyk/c8WShoVFtZCGRhh7t7dyhWFiZhxNf86dnFItEP3iV/yldWUd7XhqOe3bUM9fgwBTpffd6UDisKRhYVENpCFMksumhIEoCNSB5pPIQ5KGjq2rltHhtsZA8nAQR7dbaNaShoVFdZAGRmrchDCgKDr//D4nDpgu8Hfs3vqTRX6P2ex9mvr2Hh39ZIGu3hDf4/DPGql3U4MpccDH0eFMCjO5JkVLYLOIBhF1S8h3oyYO7v9XFHuMl4c0kuMD7G8/8iym3kwYEwZIQhGMMk3c8jUOn/qE3p65SfTcRqL6enprSyMd2uJPHHyfUx/j3xnW4dosadQUYfSwvyPaJ67kX6FzW9x5xvbXLSXSqC/DRYQfox+D/eQfXw00SRRhHP7NFupNPK8P7hxaVzfx9XCkQjEgN4Pj+vdEL7XT2x8Jc6Vzrbepcui1zXT20hx8JXFOamP7B8J8rfb2dnTcpN41zp8/P2pHtG9fQIg9Hnn/sf0d8r8hxxqYvv3c3LQoOerLcAz+A2OQB0VJ4MMAYYAssKiIB5RFc1N+36vXbtLev53jSiRHGMCNH+it1v/S0blVdOjfdxhpNPkeDypmx5/SOOYB1qHDStyYJo+BM7Y7BSLuuGZR0WL4mUUJsKwcdicUAXwZfkCUBL4GEAQIBu9BGNgXpgkIZ6dM/AKxcFPn4lzOVMF6mD6HfrmWmScN3Mcxcfmu7zGhWN5g5KTdqSxqA1nDzyxqjjTkQESmZxAQVtW3nfjgCz6ooSqgOpDPAZPi5B9epa3rmnPkAQWifCWKQOAIRTj26KcLgcftTWxUCqhHRngsqh99LoTRZy9LrZOGLGvHQDZJDVdqQo+QtK5p4opj3/bnCrZ9Y9dGvj1UCNqHrwQEo6Nz1S2a/f4+zS/4O4FwTKk2BHFYVD9Efs0GSRTd/H8bPVkSPo0DfICzO7kJYGrohLGXEUXr6hXcJHFGS7RKVmphg97NV7J7/aN07EvixBGUu4FjSWcrHJvDlfxB2tvbW6jQWZg9f/78TCXaddknw/bJyHXKpxPT7vbpCOc6GNYJLaMnMc1PAmd2mpcHiJuVk/wznGiEksS6Pdo6+KLM/Flif0zZsEs7fka2MRm2mlq7ueKc9Pli0M4F3/MqDDkrpHMlEuIa7SqFCislafAvFKam5GWNHKAc4AeBooBDFH6O/CBv5e22vvlPrjpcFcSyu8ZfD8cCMbFjxeWPmKkAWSQk0Xa5rMP58DJ/NiizIdvtIceUA9o6tHXEp12VwZsf4EQDbL9coaHHufax9iZLeLmS5O5QTcvB3L/o8+Q4XodoscM0wbdPjmMKhT6fwY3fZYTcHa49vO3keCokWYyQt2O4S54XvpNboWWC3Pxw4nuOaKSWrg3zRDBynEc9HqKuBI5RhGDhEEXoVcex9Od8sHuREjdXbvxgfKzd+XYS5VYWbBmRg7PLYzM1EC5LcjFpN86WadmBvMKcLVq7XYbtTnsRhnaux+V3qhYkfAa8wkEZEvZSNscD9m+R25iMD5zPtGFfE9sK0grCHtmPYrXo0+CdFD6HqIC6QA6GMkVgPqjw6tSlOf4e6qPzL+8XLNhP+Uio3pywMNGPxCtl7tCnQ/hSeHVw0ABn6w/KThkP0e5xqUqCTE7TNnuqjDhMcFAOaDeFUawbaq7+KsReLfwcxL6B465WfRoJYW48brwDVAmSrXpvbSxM2HKYJCCRvX89xyMqeD/71XWVpMW3UcdE1IUeWUOtK5cZH7+cF14ObrdkJ/hUkCg2I30KXS6DdYStm1H+BUebI642vTBvJrV2lTmkD5Ih2e6MT+dV7Q3qyWyScJzyH8RxIsBU2SUzhoOQDprSwAAZaY5l5F25x4MY05pi9iIMNbVklsJNTemlRnDNT2iKIe5y7XF9OyrNrCVN7kL0w5g01jXnzJFcNauEyg5VZglMHuXjQBiWTglHKqIsKlJz9ps71NxYx0OvJtDMqFiZCCPmkPlZ2SHQOQ7I9RjAsLNHHWSgHHoDBoQxLAd4VppCiiTQdgd7P6DZxiYdEwOuzekDAYGwtiblXVTv8P1ycPndYExNwochDTgquwveJ8ezLqZWl+P/Fo8B3qE5PvtYWyOBirHQgavDWXE9oEovFl0rqI3gdPlJEg5msd3ob2vCPNmV8ysYYucLa/JmhTaQkZ+hCAMq42j6s8CIDLabuHibdjwV7utJc6ospCHvaDq6NccjOmpKEoYalClt0MyQI8rjQRi8DUUYkhCS8u4JP8YQWzfgGNQJ+EN8zjvl5YyVnzudgXFJgJWGm5PziI9PTt3x3UizY1GkZGx/KoAcvdobdJ2iQUSU3Mihy4gcS1iHU1qlEYI0uEMzX7LO9534/TZuNkCBfMjMEEUoQdmlvX+fJXrmedq9tp6qGF2OO0NGEsaoJAivjo8B36EPXOnD6HGqFt3McA5ouQ/MkQuy3S7H3d+t00Gd+N7tpfkz6WgvThWISBUoA7fQJT4T0QY330DaQwEd8QmtDgYMarf21vuYZ27HaY5AjrVDGmGgEruUL0NP2MI6lfwFInFGZJBnAUemIpiT2RWMNOqpc93yaiYN/e57Qr7P+hCGGpDdDsJQvoSCjhOUL8HWD0v1kZSmxYxmVng5g08YfrcLLqQxWcFrHTXF3Ms0cQfu7u4k5NdeT8hziocmx1oiDTUfhikwZ4YiDTg7QRyKTPzyPUAuWBCaPTT1HdELL3LCaG6omfL1jKY4KGCwZzTCiLs46lKm1bYwTVgbl10GVjnNCZzrWIhrVC2/VTWiLFMD1Jfw5BM81TvE7OIwOzCDOH+WCdt368C/uMPTpNgNEZep+UZOGMAbm8Lnh+D5KVSZwqcYhXTyScI47aIwRkMe+0iIgdFcgu9+pQhRkXL/VpkAX0ilcKWWSYOPvvmbC6F33MeJ4+OcUgEZgEigNBBO1SMyUxev0XtMmczO/Ui0YRMjjCeF8pBzh0ZRRuVia9nx1B19jzQRjoQkDL2TwhcyrG3DE7d0Z6qPmRJz2NtXQtjkbnA6/GqJFJzn7fzOSZ/vE+SknHExL1Ih1UvFq3lLqTR4DkXYRxOg6hQREr2+RJXJ5+93q8Rr02Psf9bf1z9Z0AZm7goLJIyVWX7CFFHhvi6EQoOcjJIM0ImPuxCG0xcyIts9Y5DSPeRybq72NBLL/NqT5xevhGwulWJe5INIjo8tUkdCZfRHII09jlCws80etn64mi7KslKSxtn8QDSGo+rUHS+1iwXqYnUhYUBl7N0QngulaQJcKNO1d6qK436hTpljMeShMFKObYe0u96IX+q5jKLod8igorMRr/OUnzuTl0bD1stUEbyU32ke8RD1IyprdNrAFzTmqk6S46cLMlFBFiKnY5pETctQNV2U0igNEcrKTF2ci4V1hnqpDVNEURkAHuX4kFI6yQbNLhNClbkTGbb9IBUmVk2zz3IZodqdW2UvtrgMyJSXeai1i9TzUdlu2icjlPtFAs5fP88TmpmVpMWRAJDFINUqRD8eJvdam34SBWVh2kPh3KSLGZMgkbjltSfS2y+EebRoLSqNnMR1poObqg29qpXjUUY8a1cLU+TpRrGsLPRbwI8RRWWAnJBRSqKEOqqUjlE+u9FviWv+hAES0YPCDiIG5QMsUlkcdCGMQa/wrEe7PZI80Ob3UhE4CSMVorT9oDy3y+RdP9PnlupeY8TRV2TzKhWxvZGcslnCpMGl2Lvnvoi0MyImvB4EZPHyeqK2DYwkHtXOvI5oHXu/eSXR4/VSZUSrqD0mZw0js9BfUSEHfhibFR2uTRJDsdrNklmodtiFjDwHxxKaaLnDUIGOGpCQKhcIk7cC4m2rlomGSkca4o49gzv4VATfBnD4daY2li9HGIboU6ZY7t5jMqRJKIxGRhrf/kj05W3+/17Dp6y54d38A5kq0sllhCOoI03KgdhmqgYM2s3K79xmOMDnJRn5efxxrA1LamZ2DHQxE3qfx/cWE92IVHLT9uD87A4go4w8ZltVPJ5BorTPPZHPp0BNB+b2jAKEXCcyzETf+DTR10xVZ28QbdvJiILx3SqmMO49oOZv7tDsr1dESubiCWGinH5U/eiVfO6Jy2xZZBJVMWxbN0dMZu5KkGMSHl3hSMdni8Nfs/Qn+BUl6i0FvoqHa2/Rby5N5aIoi9p7WFJyHDZvDNmdYRK9dH8DkrzmG6VPY81jRC9uJrp9n1nmC0TX79HE9kbfZ5wEti0crrl5Ju3DksxIw6I2UOwxvqwM58zv3r3vTEeKhsApyp+ydpeRxLUfiD68QvT5LaY67hD9+IA6n6qLRBhKxchzGrYT01pYVAtpCOk2qlK9o4DXnjwvE7o2PUv07CNETzRQ8+17dGz7I5Ha5IVtIrIDshi0XcHConqUBsDDVhikzrk+jX0PTG00X4dD9GuiOaYOrtzmhBHFj6EmK5bojjSLtIWFJY2Sqg01l0MWdSUT+WhFKDMFZfF0Z4E7P3s3N0QyS/QHTJOYMWnGdgMLi+pTGioEi7BVFmZKQS2JIeBI7f15K21dfj9SToaDMIarJcOuiqF+M7XY62VRhuiJE9pszEjgMnkwtBN4alpYswTqRvOp+D7johTXxMLCkkZU0sgTB9KYY/yBz69viRSONSIYWV6vpbOnghSGJQ0Li2ojDUEcanp4XrwD1YGHP4eZVzSILJAerhW+ZUg4PQN9GJY0LCyqkTTy5AHSQOlvTJGH/iiCsEBoF0+cd1TJDkqTxChKYknDwqKaSSOvOlA1mVTkAcWB9HM82gDPRHE+BFonCcyFcfbSNV6p6nhmyiiJKeIzYU7HkoaFRbWTRiGB9JCYP8J16jT+fNimBlXK7gaYH2Mkakki5V9Y0rCwqCXSKCSQBIk5HzClfotUITG5Nkv5eQnOyP+LMoW7JQ0LizKThoWFxdLF/wQYAH+WI8A5QfKqAAAAAElFTkSuQmCC';
                    doc.addImage(logo_img, 'PNG', 130, 12);
                    doc.setTextColor(255, 0, 0);
                    doc.fromHTML(source, 15, 15, {
                        'width': 170,
                        'elementHandlers': specialElementHandlers
                    });
                    doc.save('sample-file.pdf');
                });
                $('.footer').css({'margin-top': '1px'});
                var flag;
                $('.dropdown-toggle').dropdown();
                $('#ajax-content').block({message: 'Loading Map ...', css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#0099e6',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: 0.5,
                        color: '#fff',
                    }});
                var data = 'view = grid-view';
                //cat_checkers

                $('#Map-view').click(function (e) {

                    e.preventDefault();
                    flag = true;
                    $('#viewselector').val('1');
                    $('#onclick_pdf').hide();
                    $('#Map-view').addClass('btn-active');
                    $('#Grid-view').removeClass('btn-active');
                    $('#filter-box').slideUp();
                    $('.list-container').hide();
                    $('.map-container').show();
                    $('.venue-more').css({'display': 'none'});
                    $('#btn-select').trigger('click');
                    initialize();
                });
                // filter process for venues
                $(document).on('click', '#btn-select', function (event) {

                    $('#filter-box').hide();
                    $('#btn-select').hide();
                    $('.venue-more').css('display', 'none');
                    var offset = 9;
                    $('#venue-detail').css('display', 'none');
                    $('#venue_lat').val('');
                    $('#venue_long').val('');
                    $('.list-container').css({'min-height': '535px'});
                    flag = true;
                    var view = $('#viewselector').val();
                    var venue_name_search = '';
                    var venue_location = '';
                    state_opened = false;
                    var venue_Looped2 = '';
                    var venue_name_search = $.trim($('#venue_name_search').val());
                    // var venue_location = $.trim($('#venue_location').val());

                    var venue_Looped = $('.filter-btn-active').attr('id');
                    if (venue_Looped > 0) {
                        venue_Looped2 = venue_Looped;
                    }
                    else
                    {
                        venue_Looped2 = '';
                    }
                    var data_string = 'category_id=' + $(".checkcat:checked").map(function () {
                        return this.id.match(/\d+/);
                    }).get();
                    var CheckedBox = $(".filter-items input[name='categories[]']:checked").length;
                    if (CheckedBox > 0 || venue_Looped > 0)
                    {
                        $('.btn-filter').css('background-color', '#005FA3');
                        $('.btn-filter').css('color', '#FFFFF');
                    }
                    else {
                        $('.btn-filter').css('background-color', '');
                        $('.btn-filter').css('color', '#5c5c5c')
                    }
                    data_string = data_string + '&venue_name_search=' + venue_name_search + '&venue_location=' + venue_location + '&venue_looped=' + venue_Looped2;
                    if (view == '1' || view == '')
                    {

                        $.ajax({
                            url: '<?= BASEURL . 'landing/getPlace'; ?>',
                            type: 'POST',
                            data: data_string,
                            dataType: 'json',
                            beforeSend: function (xhr) {
                                $('#ajax-content').block({message: 'Please Wait...', css: {
                                        border: 'none',
                                        padding: '15px',
                                        backgroundColor: '#0099e6',
                                        '-webkit-border-radius': '10px',
                                        '-moz-border-radius': '10px',
                                        opacity: 0.8,
                                        color: '#fff',
                                        //z-index: 99999
                                    }});
                            },
                            complete: function (xhr) {
                                $('#ajax-content').unblock();
                            },
                            success: function (data) {

                                $('.list-container .container-fluid').html('');
                                $('.list-container').hide();
                                $('#venue-detail').hide();
                                $('#venue-detail').css('top', '0px');
                                $('.search-venue-item').css('margin-top', '0px');
                                if ($.trim(data) != '-1')
                                {
                                    $('.alerter1').css('display', 'none');
                                    $('.search-venue-item').remove();
                                    if (data.length > 0)
                                    {
                                        markers_data = data;
                                    } else {
                                        markers_data = '';
                                    }
                                    $('#map-canvas').remove();
                                    $('.map-container').append('<div id="map-canvas"></div>');
                                    initialize(1);
                                } else {
                                    $('.alerter1').css('display', 'block');
                                    markers_data = '';
                                    $('#map-canvas').remove();
                                    $('.map-container').append('<div id="map-canvas"></div>');
                                    var options = {
                                        draggable: false,
                                        scrollwheel: false,
                                        panControl: false,
                                    };
                                    initialize(1);
                                    var boxHtml = '';
                                    $('.list-container .container-fluid').append(boxHtml);
                                    $('.list-heading').html('There are no venues matching your search criteria.');
                                }
                            }
                        });
                    }
                    else if (view == '2' && view != '')
                    {
                        $('.list-container .container-fluid').html('');
                        $('#Grid-view').unbind('click');
                        $('#Grid-view').bind('click', function () {
                            $('#ajax-content').block({message: 'Please Wait...', css: {
                                    border: 'none',
                                    padding: '15px',
                                    backgroundColor: '#0099e6',
                                    '-webkit-border-radius': '10px',
                                    '-moz-border-radius': '10px',
                                    opacity: 0.8,
                                    color: '#fff',
                                    //z-index: 99999
                                }});
                            $('#ajax-content').unblock();
                        });
                        $("#Grid-view").trigger('click');
                    }
                    event.preventDefault();
                });
<?php if (isset($category_id)) {
    ?>
                    $('.cat_checkers').each(function () {
                        flag_categories = true;
                       
                        if ($(this).val() === '<?= $category_id ?>')
                        {
                            $(this).parent().addClass('checked');
                            $(this).prop('checked', true);
                            $(this).attr('checked', 'checked');
                            $('.btn-filter').css('background-color', '#005FA3');
                            $('.btn-filter').css('color', '#FFFFF');
                        }
                    });
<?php }
?>
<?php
if ($LocationForVenue) {
    ?>
                    $('#venue_location').val('<?= $LocationForVenue ?>');
                    codeAddress();
                    code_address_flag = true;
<?php }
?>

<?php
if ($VenueName) {
    ?>
                    $('#venue_name_search').val("<?= $VenueName ?>");
<?php }
?>
                //venue latr long value.
<?php
if ($lat) {
    ?>
                    $('#venue_lat').val('<?= $lat ?>');
<?php }
?>
<?php
if ($long) {
    ?>
                    $('#venue_long').val('<?= $long ?>');
<?php }
?>

                $('#request-form').validate({
                    rules: {
                        feed_name1: {
                            required: true,
                            minlength: 4},
                        feed_email_request: {
                            required: true,
                            email: true
                        }
                    },
                    messages: {
                        feed_name1: {
                            required: "Please enter your name.",
                            minlength: "Please enter atleast 4 words.",
                        },
                        feed_email_request: {
                            required: "Please enter your e-mail address.",
                            email: "Invalid email"
                        }
                    },
                    submitHandler: function (form)
                    {

                        var selected = $('.feed-btn1').data('id');
                        var name = $('#feed_name1').val();
                        var grid_Id = $('#venue_req_id').val();
                        var email = $('#feed_email_request').val();
                        $.ajax({
                            type: 'post',
                            url: '<?= BASEURL . 'landing/getVote' ?>',
                            data: {venue: selected, email: email, grid_venue: grid_Id, name: name},
                            success: function (data)
                            {
                                var Responsedata = jQuery.parseJSON(data);
                                if (Responsedata.success == '1')
                                {
                                    $("#form_message_req").addClass("alert-danger");
                                }
                                if (Responsedata.success == '2')
                                {
                                    $("#form_message_req").addClass("alert-success");
                                }

                                $('#form_message_req').html(Responsedata.message);
                                $("#form_message_req").fadeIn();
                                $('#request-form')[0].reset();
                                $("#form_message_req").fadeOut(5000)
                                //$('#form_message_req').show();
                                $('#request-form').css('opacity', '0.3');
                                setTimeout(function () {
                                    $('#model-request').modal('hide');
                                }, 5000);
                                //$('#form_message_req').html('');
                                $('#request-form').css('opacity', '1');
                            }
                        });
                        // ajax call end
                    }
                });
                setup_filter_box();
                setup_checkbox();
                setup_selects();
                $venue_detail = $('#venue-detail');
                $venue_detail_notch = $('#venue-detail.notch');
                $('.sidebar-close').click(function (e) {
                    e.preventDefault();
                    var left = $('#full-detail').offset().left;
                    $('#full-detail').fadeOut();
                });
                var availableTags = '<?= BASEURL . 'landing/getVenuesLocation' ?>'
                var NoResultsLabel = "Oops! No suggestions found. Try a different search.";
                $("#venue_location").autocomplete({
                    source: availableTags,
                    minLength: 2,
                    select: function (event, ui) {
                        if (ui.item.value === NoResultsLabel) {
                            event.preventDefault();
                        }
                    },
                    focus: function (event, ui) {
                        if (ui.item.value === NoResultsLabel) {
                            event.preventDefault();
                        }
                    }

                });
                var availableTags1 = '<?= BASEURL . 'landing/getVenuesName'; ?>';
                $("#venue_name_search").autocomplete({
                    source: function (request, response) {
                        $.getJSON(availableTags1, {location: $('#venue_location').val(), term: request.term},
                        response);
                    },
                    minLength: 2,
                    select: function (event, ui) {
                        if (ui.item.value === NoResultsLabel) {
                            event.preventDefault();
                        }
                    },
                    focus: function (event, ui) {
                        if (ui.item.value === NoResultsLabel) {
                            event.preventDefault();
                        }
                    }

                });
                var view_manager = $('#viewselector').val();
                var mess_address = $('#venue_location').val();
                $(document).on('click', '#landing_search_button', function () {


                    if ($('.btn-switch-right').hasClass('btn-active') || $('#venue_location').val() == "")
                    {

                        $("#btn-select").trigger('click');
                    }
                    if ($('#venue_name_search').val() == '') {

                        codeAddress();
                    }
                    if ($('#venue_name_search').val() != '' && $('#venue_location').val() != "") {
                        $("#btn-select").trigger('click');
                    }
                    if ($('#venue_name_search').val() == '' && $('#venue_location').val() == "") {
                        $("#btn-select").trigger('click');
                    }

                });
                flag = true;
                var venue_stack = [];
                $(document).on('click', '#Grid-view', function (event) {
                    event.preventDefault();
                    $('#onclick_pdf').hide();
                    $('#viewselector').val('2');
                    $('.sidebar-close').click();
                    $('.venue_class').show();
                    //$('.list-container').css('height', 'auto');
                    $('#Grid-view').addClass('btn-active');
                    $('#Map-view').removeClass('btn-active');
                    $('#filter-box').slideUp();
                    var venue_Looped2 = '';
                    var lat = '';
                    var long = '';
                    lat = $('#venue_lat').val();
                    long = $('#venue_long').val();
                    var venue_name_search = $.trim($('#venue_name_search').val());
                    var venue_location = $.trim($('#venue_location').val());
                    var venue_Looped = $('.filter-btn-active').attr('id');
                    if (venue_Looped > 0) {
                        venue_Looped2 = venue_Looped;
                    }
                    else
                    {
                        venue_Looped2 = '';
                    }
                    var data_string = 'category_id=' + $(".checkcat:checked").map(function () {
                        return this.id.match(/\d+/);
                    }).get();
                    data_string = data_string + '&venue_name_search=' + venue_name_search + '&venue_location=' + venue_location + '&venue_looped=' + venue_Looped2 + '&venue_lat=' + lat + '&venue_long=' + long;
                    if (flag == true)
                    {
                        $.ajax({
                            type: 'post',
                            url: '<?= BASEURL . 'landing/VenueGridView' ?>',
                            //dataType: 'json',
                            data: data_string,
                            beforeSend: function (xhr) {

                                $('#ajax-content').block({message: 'Please Wait...', css: {
                                        border: 'none',
                                        padding: '15px',
                                        backgroundColor: '#0099e6',
                                        '-webkit-border-radius': '10px',
                                        '-moz-border-radius': '10px',
                                        opacity: 0.8,
                                        color: '#fff',
                                        //z-index: 99999
                                    }});
                            },
                            complete: function (xhr) {
                                $('#ajax-content').unblock();
                            },
                            success: function (data)
                            {

                                $('.map-container').hide();
                                $('.list-container').show();
                                $('.venue-more').removeAttr('style');
                                var inform = $.parseJSON(data);
                                var mapdata = inform.map_data;
                                var venue = $.parseJSON(data);
                                var boxHtml = "";
                                var image_path = '<?= UPLOADS_URL . 'venues/' ?>';
                                var marker_image = '<?= IMAGE_URL ?>';
                                var y = [];
                                var i = 0;
                                var venue_stack = [];
                                if ($.parseJSON(data) != '-1') {

                                    for (var key in mapdata)
                                    {

                                        y = i + 1;
                                        var full_image_path;
                                        var venue_class;
                                        var loopedborderblue = '';
                                        var loopheader = '';
                                        var certified_image = '';
                                        var venue_image_default = '';
                                        var ImageMessage;
                                        venue_stack.push(mapdata[key].id);
                                        if (mapdata[key].type == 1 && mapdata[key].type != 3) {
                                            venue_class = 'looped';
                                            loopheader = '';
                                        }
                                        if (mapdata[key].type == 2 && mapdata[key].type != 3) {
                                            venue_class = 'non-looped';
                                            loopheader = '<div class="non-looped-header">Not looped</div>';
                                        }
                                        if (mapdata[key].type == 3) {
                                            venue_class = 'looped';
                                            loopheader = '';
                                        }
                                        //if image is blank
                                        if (mapdata[key].venue_image == "")
                                        {
                                            venue_image_default = mapdata[key].categories_image;
                                            // $('.cus-bg .b-lazy').css({ opacity: 0.8 });
                                            ImageMessage = '<span class="image_slogan"><p class="image_name_slogan list-overlay"><img class="no_img_avail" src="<?= IMAGE_URL . 'categoryPlace@2x3.png'; ?>"/></p></span>';
                                        }
                                        else {
                                            venue_image_default = mapdata[key].venue_image;
                                            ImageMessage = '';
                                        }

                                        //is certified condition
                                        if (mapdata[key].is_certified == 1 && mapdata[key].is_certified != '')
                                        {
                                            certified_image = "<img class='img-responsive loop-img' src='<?= IMAGE_URL . 'loop-certified.png'; ?>'style='padding-bottom:1px;' />";
                                            loopedborderblue = 'loopedborderblue';
                                        }
                                        else if (mapdata[key].type == '2' && mapdata[key].type != '')
                                        {
                                            certified_image = ' <div class="loop-btn-container"><a class="loop-btn orange_request" href="#" id="request" data-grid="9">Request</a></div>';
                                            certified_image += '<input type="hidden" id="venue_req_id1" value="' + mapdata[key].id + '">';
                                            loopedborderblue = '';
                                        }
                                        // full_image_path = image_path + mapdata[key].id + '/' + ;
                                        boxHtml += '<div class="search-venue-item col-md-4 col-sm-6 div_venue_details " data-id="' + mapdata[key].cid + '" id="' + mapdata[key].id + '"><div class="' + venue_class + ' ' + loopedborderblue + '"><div class="cus-bg1 lazy" style="background:url(' + venue_image_default + ') no-repeat center; background-size:cover;">' + ImageMessage + '</div> ' + loopheader + ' <div class="overlay-container"> <div class="list-overlay"> <p><h1>' + mapdata[key].name + '</h1></p><p><i class="fa fa-th-large"></i>&nbsp;' + mapdata[key].ctype + '</p> </div> <div class="list-desc"> <div class="row"> <div class="col-xs-9"><div class="clearfix"></div> <div class="col-xs-1" style="padding-left:12px; top:12px;"><i class="fa  fa-map-marker" style="font-size: 18px;"></i></div><div class="col-xs-10 " style="float:left;left:-12px;">' + mapdata[key].address + '<br>' + mapdata[key].city + ', ' + mapdata[key].state + ' ' + mapdata[key].zipcode + '</div> </div> <div class="col-xs-3">' + certified_image + ' </div> </div> </div> </div> </div> </div>'
                                        //mapdata[i].data.

                                        venue_class = '';
                                        loopheader = '';
                                        i++;
                                    }
                                    $('.list-container .container-fluid').append(boxHtml);
                                    $('.list-heading').html(inform.total_venue + ' Venues found.');
                                    $("img.lazy").lazy({
                                        bind: "event",
                                        effect: "fadeIn",
                                        effectTime: 1000,
                                        placeholder: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAEk0lEQVR42qVWfUyVVRx+3nvfexmgNE1LxiBmNAvMEoQk20okRHLRRonJR6uIxWq4EZklkgi2AmnOLWHUH00zvHw43Mhl0AeNSq4Ea0a6xI8LAhGGgXDve9+Pc/qd92Y1k68622/nveec93l+9znv+T1Hwg0t2dHPPW4VmqqDURiqBqbp0DUDhtf3LMag6+YzUw0wpvnWGwYhMAw6sqXreNKNBAmH+/j2dcG44mUwdEBlDFzn0MChGgyq5us1xuHVmTnvoXkvjWuSBScONGP46LNTEyTW9vGU5QvhGidIykhV6UUBSM+6Sj2FwsS4AU0ThIYZmiCzWDHa7sRQ/TQESY7LPbLHvWCJvyXY5i+DkSyGTjLogoBkor9lkDSakMhLY0IyAndPTmLwV/fQNY96ddiRHTUlgWhtTa0vLg4LrrpnZSRu3khrUzraGyJkTMeZH11Qrrnz4h9dU/3PlTclaKpvjbP7+3VseOyhv8aYCE5B2GIvVcpaN/+VSigGOr/rps3XH0h9MtE5I4Fohw5+yh9/aj2CvHvACZUbHJxzc8MZ7QmnELIxQ4VbCULL6QeRkbn+X3hTEhz+8Lgj7LYvNq1YWwFpoAIB6jZTGcmQIOmit5q/xaf2zVAhXJ6EuoxnUtJnTSDa+9XH+YZVRZgf9b1JEujZRRoJAkmoQuAqBseW45P+MuS+kHJTrGkJPqhyhHq1gL7YOw/i7mWx5ljgRAksRACSrNO1Ec7hTPjJ7rCcvPT+OROIVrX/ULhm+DXeGngpekt8Me2u1QSv7dpNhzGsyyZ50/Lysy5N9f6MBNdbRflHTWvvqk2NueNLdF9IwOcXNh97dVvmEzO9N2uCPaU1TUlLG1NXLW1DZ9/DaPk57dgbO3P/P8FbJZXhuq43+geFRBeufh6wWeG2hqCyuRASH+uSZTnt9eKC/yZRSdHboYpH6UvemIT7V8dj3ikJFnsgvAEROO/XirOnT6Lb+QPsdltYcdlrc9/kV/J38OzntiB8WaT5mc4f3g5u9weXDZyTP8MkC4Xr/E/oaO/AO+/unttnWpi/wxG54t5N6ZnpsPWsgTzxLST7PHAbTVrpBFs9+AVZ6B0vwsm2ExgdGakr31c6+4OWl1PAy/aWYsGZCEjeK5Q5IcsWAucmCbcwWO1ejChx+Kq3HO0tzdhfVT67UpGfWxC7ODTCWbT5LNB/ALD7kSy0lI4At1HFsxCJTAtlDquswDn4JhoaLNCU8bh91XtPzUjwckZ20X0xMaU5K7cSGCH5icxpqZCHwM3eKggo7FT4cDt2vbcVk7/17qysqSmbliC5bqDHO3r1lkXWiZCFlotkLIbpvRrVf2ZovvqvqeQBwlLJ9TgVcsmDi64ojE96BjRJHhs6kjW14SR+3Mefjl6EMfJklUqzTmXBMHy99qcHmBZqzjEzAZWsk1KgkNBe/zWGG6axzHVk+i89sgTnftfNLMklCZRM3RC+K5lGI8xeIWBFEz0zfVshX/ZIEi4fbcXQdATi2qLQtUV4LVOE3wpJNN+1ha4mXFxPdNV3ZTHD8Emn+fxaJDV45O9ryx+UubI3TsTNbgAAAABJRU5ErkJggg=="
                                    });
                                    setup_grid_venues();
                                    if (venue_stack.length < 9) {
                                        $('.venue-more').css({'display': 'none'});
                                    }
                                    flag = false;
                                }
                                else {
                                    var listheight = $(".list-container").height();
                                    $('.list-container').css('min-height', '535px');
                                    var text_grid = '<div class="alerter"><div style = "padding: 1px;" ><img src = "<?= IMAGE_URL ?>sad_face.png" alt = "Which Wich" style = "width: 30px;margin-top:5px" ><span style = "float: right;margin: 10px 2px;" > There are no venues matching your search criteria. </span></div></div>';
                                    $('.list-container .container-fluid').html(text_grid);
                                    $('.list-container .container-fluid #alerter_grid').show();
                                    $('.list-heading').html('There are no venues matching your search criteria.');
                                    $('.venue-more').css({'display': 'none'});
                                    $('.footer').css('margin-top', '1px');
                                }
                            }
                        });
                    }
                });
                var flag_venue = true;
                var offset = 40;
                $(document).on('click', '#venue_show_more', function () {

                    $('#venue-detail').css({'display': 'none'});
                    $('.div_venue_details').css({'margin-top': '0px'});
                    $('.list-container').css('height', 'auto');
                    $('.loading-image').removeClass('hide');
                    if (flag_venue == false) {

                        offset = parseInt(offset + 40);
                    }
                    flag_venue = false;
                    var venue_Looped2 = '';
                    var lat = '';
                    var long = '';
                    lat = $('#venue_lat').val();
                    long = $('#venue_long').val();
                    var venue_name_search = $.trim($('#venue_name_search').val());
                    var venue_location = $.trim($('#venue_location').val());
                    var venue_Looped = $('.filter-btn-active').attr('id');
                    if (venue_Looped > 0) {
                        venue_Looped2 = venue_Looped;
                    }
                    else
                    {
                        venue_Looped2 = '';
                    }
                    var data_string = 'category_id=' + $(".checkcat:checked").map(function () {
                        return this.id.match(/\d+/);
                    }).get();
                    data_string = data_string + '&venue_name_search=' + venue_name_search + '&venue_location=' + venue_location + '&venue_looped=' + venue_Looped2 + '&venue_lat=' + lat + '&venue_long=' + long + '&offset=' + offset;
                    $.ajax({
                        type: 'post',
                        url: '<?= BASEURL . 'landing/VenueGridView' ?>',
                        //dataType: 'json',
                        data: data_string,
                        beforeSend: function (xhr) {
                            $('#cat_btn').hide();
                            $('#cat_img').show();
                        },
                        complete: function (data) {

                            $('#cat_btn').show();
                            $('#cat_img').hide();
                        },
                        success: function (data) {
                            var inform = $.parseJSON(data);
                            var mapdata = inform.map_data;
                            var boxHtml = "";
                            var image_path = '<?= UPLOADS_URL . 'venues/' ?>';
                            var marker_image = '<?= IMAGE_URL ?>';
                            var test = [];
                            for (var key in mapdata)
                            {
                                var full_image_path;
                                var venue_class;
                                var loopedborderblue = '';
                                var loopheader = '';
                                var certified_image = '';
                                var venue_image_default = '';
                                test.push(mapdata[key].id);
                                var ImageMessage;
                                if (mapdata[key].type == 1) {
                                    venue_class = 'looped';
                                    loopheader = '';
                                }
                                else {
                                    venue_class = 'non-looped';
                                    loopheader = '<div class="non-looped-header">Not looped</div>';
                                }
                                //if image is blank
                                if (mapdata[key].venue_image == "")
                                {
                                    venue_image_default = mapdata[key].categories_image;
                                    // $('.cus-bg .b-lazy').css({ opacity: 0.8 });
                                    ImageMessage = '<span class="image_slogan"><p class="image_name_slogan list-overlay"><img class="no_img_avail" src="<?= IMAGE_URL . 'categoryPlace@2x3.png'; ?>"/></p></span>';
                                }
                                else {
                                    venue_image_default = mapdata[key].venue_image;
                                    ImageMessage = "";
                                }
                                //is certified condition
                                if (mapdata[key].is_certified == 1 && mapdata[key].is_certified != '')
                                {
                                    certified_image = "<img class='img-responsive loop-img' src='<?= IMAGE_URL . 'loop-certified.png'; ?>'style='padding-bottom:1px;' />";
                                    loopedborderblue = 'loopedborderblue';
                                }
                                else if (mapdata[key].type == '2' && mapdata[key].type != '')
                                {
                                    certified_image = ' <div class="loop-btn-container"><a class="loop-btn orange_request" href="#" id="request" data-grid="9">Request</a></div>';
                                    certified_image += '<input type="hidden" id="venue_req_id1" value="' + mapdata[key].id + '">';
                                    loopedborderblue = '';
                                }
                                // full_image_path = image_path + mapdata[key].id + '/' + ;
                                boxHtml += '<div class="search-venue-item col-md-4 col-sm-6 div_venue_details " data-id="' + mapdata[key].cid + '" id="' + mapdata[key].id + '"><div class="' + venue_class + ' ' + loopedborderblue + '"> <div class="cus-bg1" style="background:url(' + venue_image_default + ') no-repeat center; background-size:cover;">' + ImageMessage + '</div> ' + loopheader + ' <div class="overlay-container"> <div class="list-overlay"> <p><h1>' + mapdata[key].name + '</h1></p><p><i class="fa fa-th-large"></i>&nbsp;' + mapdata[key].ctype + '</p> </div> <div class="list-desc"> <div class="row"> <div class="col-xs-9"><div class="clearfix"></div> <div class="col-xs-1" style="padding-left:12px; top:12px;"><i class="fa  fa-map-marker" style="font-size: 18px;"></i></div><div class="col-xs-10 " style="float:left;left:-12px;">' + mapdata[key].address + '<br>' + mapdata[key].city + ', ' + mapdata[key].state + ' ' + mapdata[key].zipcode + '</div> </div> <div class="col-xs-3">' + certified_image + ' </div> </div> </div> </div> </div> </div>'
                                venue_class = '';
                                loopheader = '';
                            }
                            $('.list-container .container-fluid').append(boxHtml);
                            $('.loading-image').addClass('hide');
                            $(".search-venue-item").unbind("click");
                            setup_grid_venues();
                            if (test.length < 9)
                            {
                                $('.venue-more').hide();
                                offset = '';
                            }
                        }
                    });
                });
            });</script>

        <script>

            //check or uncheck filter code.
            $('.filter-btn').on('click', function () {
                $(this).addClass('filter-btn-active');
                $('button.filter-btn').not(this).removeClass('filter-btn-active');
            });
            function setup_checkbox() {
                $('.inputcheck').each(function (index, element) {
                    var $label = $(element);
                    $label.append('<img class="icon-check icon-thin" src="<?= IMAGE_URL . 'tick-icon.png'; ?>" /><img class="icon-plus icon-thin" src="<?= IMAGE_URL . 'plus-icon141f.png?v1.1'; ?>" />');
                    $label.find('input').change(function (e) {
                        if ($(this).prop('checked'))
                            $label.addClass('checked');
                        else
                            $label.removeClass('checked');
                        setTimeout(function () {
                            get_categories(display_categories);
                        }, 5);
                    });
                });
            }

            function get_categories(callback_function) {
                var data = $('#filter-box').serialize();
                $('#filter-box').data('selection', data);
                if (typeof (callback_function) != 'undefined')
                    callback_function(data);
                return data;
            }

            function display_categories(data) {
                console.log(data);
            }

            function setup_selects() {
                $(".select-stylize").selectmenu();
            }

            $(window).resize(function (e) {
                update_values();
            });
            ////////////////////////////////////////////////////////////////////////////////////
            //venue.js
            ////////////////////////////////////////////////////////////////

            var $SITEPATH = BASEURL;
            function getDetalis(id, venue_detail_notch_class, cat_id) {

                var data_string_cat = $(".checkcat:checked").map(function () {
                    return this.id.match(/\d+/);
                }).get();
                var cat_id_name = '';
                if (data_string_cat != '') {
                    cat_id_name = cat_id;
                }
                var data_string = 'id=' + id + '&venue_detail_notch_class=' + venue_detail_notch_class + '&cat_id=' + cat_id_name;
                $.ajax({
                    type: 'post',
                    url: $SITEPATH + 'landing/getVenueDetail',
//                            url: '<?= BASEURL . 'landing/getVenueDetail' ?>',
                    data: data_string,
                    beforeSend: function (xhr) {
                        $('#blocked_venue').block({message: 'Please Wait...', css: {
                                border: 'none',
                                padding: '15px',
                                backgroundColor: '#0099e6',
                                '-webkit-border-radius': '10px',
                                '-moz-border-radius': '10px',
                                opacity: 0.8,
                                color: '#fff'
                            }});
                    },
                    complete: function (xhr) {
                        $('#blocked_venue').unblock();
                    },
                    success: function (html)
                    {
                        if (html)
                        {
                            $("#ven-det").html(html);
                            //setup_grid_venues();
                            $("abbr.timeago").timeago();
                            $('#slider').nivoSlider({effect: 'fold'});
                        }
                    }
                }
                );
            }
            var $last_venue_items = Array();
            var last_venue_selected = -1;
            var venue_details_pos_top = 0;
            function   setup_grid_venues() {

                $('.search-venue-item').each(function (index, element) {

                    setup_grid_data(index, element);
                    $(element).click(function (e) {

                        $('.footer').css({'margin-top': '1px'});
                        //count col position of venue.
                        var venue_details_col = $(this).data('col-pos');
                        var $last_no_margin_venue = $(element);
                        for (i = 1; i <= grid_col_total - venue_details_col; i++) {
                            $last_no_margin_venue = $last_no_margin_venue.next();
                        }
                        var $current_venue_items = [$last_no_margin_venue.next()];
                        for (i = 0; i < grid_col_total - 1; i++) {
                            $current_venue_items.push($current_venue_items[ i ].next());
                        }
                        for (i in $last_venue_items) {
                            $last_venue_items[ i ].css('margin-top', 0);
                        }
                        if (last_venue_selected != index) {

                            for (i in $current_venue_items) {
                                $current_venue_items[ i ].css('margin-top', 350);
                            }
                        }
                        if (last_venue_selected == index) {
                            $venue_detail.css('top', venue_details_pos_top).hide();
                            last_venue_selected = -1;
                        } else {

                            venue_details_pos_top = $(this).offset().top + $(this).height() - 6;
                            $venue_detail.css('top', venue_details_pos_top).show();
                            var venue_detail_notch_class = 'notch notch-' + venue_details_col + '-' + grid_col_total;
                            $('.notch').removeAttr('class').addClass('venue_detail_notch_class');
                            setTimeout(function () {

                                for (i in $current_venue_items) {
                                    $current_venue_items[ i ].css('margin-top', 350);
                                }
                            }, 10)
                            last_venue_selected = index;
                            $('html,body').animate({scrollTop: venue_details_pos_top - 120}, 350);
                        }
                        $('.list-container').css('height', 'auto');

                        var isVisible = $('#venue-detail').is(':visible');
                        if (isVisible) {
                            var tess = $('.list-container').height()
                            $(".list-container").height(" ");
                            $(".list-container").css('height', tess + parseInt(390));

                        }
                        var footer_offset = $('.footer').offset().top - 26;
                        var detail_offset = $('#venue-detail').offset().top + 102;
                        if (footer_offset == detail_offset || footer_offset == detail_offset - 101 || footer_offset == detail_offset + 15 || footer_offset == detail_offset - 15) {
                            var total_height = $('.list-container').height() + 390;
                            alwert('in');
                            // $('.list-container').css('min-height', total_height);
                        }
                        else
                        {// $('.list-container').css('height', 'auto');
                        }

                        $last_venue_items = $current_venue_items;
                        var id = $(this).attr('id');
                        var cat_id = $(this).data('id');
                        getDetalis(id, venue_detail_notch_class, cat_id);
                        $("abbr.timeago").timeago();
                    });
                });
            }
            var w_width = 0;
            var grid_col_total = 0;
            var $venue_detail;
            var $venue_detail_notch;
            function update_values() {
                w_width = $(window).width();
                if (w_width >= 992)
                    grid_col_total = 3;
                else if (w_width >= 768)
                    grid_col_total = 2;
                else
                    grid_col_total = 1;
            }
            ////////////////////////////////////////////////////////////////
            //end
            ////////////////////////////////////////////////////////////////////////
            update_values(); // INITILIZE VALUES
            var map;
            var latLng1;
            function setup_grid_data(index, element) {
                $(element).data('col-pos', index % grid_col_total + 1);
            }

            var mouse_is_inside = false;
            var status_of_opner = '';
            var state_opened = false;
            function setup_filter_box() {
                var $filter_opener = $('#filter-opener');
                if (typeof ($filter_opener.data('opened')) != 'undefined')
                    state_opened = $filter_opener.data('opened');
                $filter_opener.click(function (e) {
                    $('.sidebar-close').click();
                    e.preventDefault();
                    state_opened = !state_opened;
                    if (state_opened) {
                        $('#filter-box').slideDown();
                        status_of_opner = true;
                        $('#btn-select').show();
                        $('#btn-select').css('background', '');
                    }
                    else {
                        $('#filter-box').slideUp();
                        status_of_opner = false;
                        $('#btn-select').hide();
                        $('#btn-select').css('background', '#005fa3');
                    }
                    $filter_opener.data('opened', state_opened);
                });
            }
            // code for click anywhere dropdown close.
            $('.filter-container, #filter-opener').hover(function () {
                mouse_is_inside = true;
            }, function () {
                mouse_is_inside = false;
                status_of_opner = true;
            });
            // var test = true;
            $("body").click(function () {

                if (!mouse_is_inside) {
                    $('#filter-box').slideUp();
                }

            });
            // code end.
            $(window).load(function () {
                $('#ajax-content').unblock();
                $('#slider').nivoSlider({effect: 'fold'});
                $('#fd-slider').nivoSlider({effect: 'fold'});
                //getLocation();
            });
            function set_slider_text() {

                var img_index = $(".nivo-controlNav .active").attr('rel');
                $('#faq-slider-text').html($('.ufxd-slider-image').eq(img_index).attr('alt'));
            }
            // GOOGLE MAPS ----
            var directionsDisplay = new google.maps.DirectionsRenderer();
            var map;
            var mapCentre;
            function initialize(type) {

                var clusterMarkers = [];
                if (typeof (document.getElementById('map-canvas')) == 'undefined')
                    return;
                if (markers_data == '')
                {
                    var styles = [{
                            stylers: [{color: "#ffffff"}]
                        }
                    ];
                    //   var styledMap = new google.maps.StyledMapType(styles);
                    var centerlatlng = new google.maps.LatLng(34.4217, -119.7000);
                    var myOptions = {
                        zoom: 15,
                        center: centerlatlng
                    };
                    map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
                    $('.alerter1').css('display', 'block');
                }
                else
                {
                    $('.alerter1').css('display', 'none');
                    // Init Bounds ---
                    var bound = new google.maps.LatLngBounds();
                    // Init LatLng's array and extend bounding region ---
                    if (type == '1')
                    {
                        var json_obj = markers_data;
                    } else {
                        var json_obj = JSON.parse(markers_data);
                    }

                    for (var i in json_obj) {
                        bound.extend(json_obj[i]['latlng'] = new google.maps.LatLng(json_obj[ i ].lat, json_obj[ i ].lng));
                    }
                    // Map Options ---

                    var mapOptions;
                    if (localStorage.mapLat != null && localStorage.mapLng != null && localStorage.mapZoom != null) {

                        mapOptions = {
                            center: new google.maps.LatLng(localStorage.mapLat, localStorage.mapLng),
                            zoom: parseInt(localStorage.mapZoom),
                            scrollwheel: false,
                        };
                    }
                    else {
                        mapOptions = {
                            scrollwheel: false,
                            center: new google.maps.LatLng(34.4217, -119.7000),
                            // center: bound.getCenter(),
                            zoom: 3,
                        };
                    }
                    // Init Map and autofit area by bounding region ---

                    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
                    mapCentre = map.getCenter();
                    // map.fitBounds(bound);

                    //Set local storage variables.
                    localStorage.mapLat = mapCentre.lat();
                    localStorage.mapLng = mapCentre.lng();
                    localStorage.mapZoom = map.getZoom();
                    google.maps.event.addListener(map, "center_changed", function () {
                        //Set local storage variables.
                        mapCentre = map.getCenter();
                        localStorage.mapLat = mapCentre.lat();
                        localStorage.mapLng = mapCentre.lng();
                        localStorage.mapZoom = map.getZoom();
                    });
                    google.maps.event.addListener(map, "zoom_changed", function () {
                        //Set local storage variables.
                        mapCentre = map.getCenter();
                        localStorage.mapLat = mapCentre.lat();
                        localStorage.mapLng = mapCentre.lng();
                        localStorage.mapZoom = map.getZoom();
                        // for minimum  zoom level.
                        if (map.getZoom() < 2)
                            map.setZoom(2);
                    });
                    var print = "";
                    for (i in json_obj) {

                        print += '<p><strong> ' + json_obj[i].name + '</strong></p>' +
                                '<p>Venue Type: ' + json_obj[i].ctype + '</p>' +
                                '<p>Address: ' + json_obj[i].address + ',' + json_obj[i].city + ',' + json_obj[i].state + ',' + json_obj[i].zipcode + '</p>' +
                                '<p>Contact No: ' + json_obj[i].phone_number + '</p>' +
                                '<p>Email: <a href="mailto:' + json_obj[i].email + '">' + json_obj[i].email + '</a></p>' +
                                '<p>Website: ' + json_obj[i].website + '</p>' +
                                '<p style="color: blue;">------------------------------------------------------------------------------------------------------------------------</p>';
                        if (json_obj[i].type == '1')
                        {
                            margin = "-10px -12px 2px 2px";
                        } else {
                            margin = "-10px -12px 2px 2px";
                        }
                        /* -- INFOBOX -- */
                        json_obj[i]['infoBox'] = new InfoBox({
                            content: gen_infoBox_content(json_obj[i]),
                            disableAutoPan: false,
                            maxWidth: 0,
                            pixelOffset: new google.maps.Size(-175, -50),
                            zIndex: null,
                            boxStyle: {
                                opacity: 1,
                            },
                            closeBoxMargin: margin,
                            closeBoxzIndex: "99999",
                            closeBoxURL: "<?= IMAGE_URL . 'close_icon_info.png'; ?>",
                            infoBoxClearance: new google.maps.Size(1, 1),
                            alignBottom: true,
                        });
                        var cat_img;
                        // <?= UPLOADS_URL . 'category_icons/'; ?>' + json_obj[i].categories_pin

                        if (json_obj[i].type == '1')
                        {
                            if (json_obj[i].categories_pin != '')
                            {
                                cat_img = '<?= UPLOADS_URL . 'category_icons/'; ?>' + json_obj[i].categories_pin;
                            } else {
                                cat_img = '<?= UPLOADS_URL . 'map-pin-default.png'; ?>';
                            }


                        } else {
                            if (json_obj[i].categories_pin2 != '')
                            {
                                cat_img = '<?= UPLOADS_URL . 'category_icons/'; ?>' + json_obj[i].categories_pin2;
                            } else {
                                cat_img = '<?= UPLOADS_URL . 'map-pin-default_grey.png'; ?>';
                            }
                        }
                        json_obj[i]['marker'] = new google.maps.Marker({
                            position: json_obj[i]['latlng'],
                            map: map,
                            title: json_obj[ i ].name,
                            icon: cat_img
                        });
                        if (json_obj[ i ].categories_id == 9 && flag_categories == false) {

                            json_obj[i]['marker'].setVisible(false);
                        }
                        if (json_obj[ i ].categories_id == 21 && flag_categories == false) {

                            json_obj[i]['marker'].setVisible(false);
                        }

                        clusterMarkers.push(json_obj[i]['marker']);
                        (function (markers_data) {
                            google.maps.event.addListener(markers_data['marker'], 'click', function () {
                                $('.infoBox').hide();
                                markers_data[ 'infoBox' ].open(map, markers_data['marker']);
                                $('#full-detail').hide();
                                var temp = $('.infoBox').offset();
                            });
                        })(json_obj[i]);
                    }

                    $('#pdf_class').append(print);
                    if (clusterMarkers.length > 0) {

                        var options_markerclusterer = {
                            gridSize: 70,
                            maxZoom: 11,
                            zoomOnClick: true,
                            //batchSize: 50
                        };
                        var markerCluster = new MarkerClusterer(map, clusterMarkers, options_markerclusterer);
                        google.maps.event.addListener(markerCluster, 'clusterclick', function (cluster) {
                            // map.setZoom(map.getZoom() + parseInt(1));

                        });
                    }

                }

            }
            //function for search on address.
            function codeAddress() {

                markers_data = '<?= $all_marker_data ?>';
                var address = $("#venue_location").val(); //gets address
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({'address': address}, function (results, status) {

                    if (status === google.maps.GeocoderStatus.OK) { //is everything thumbs up?
                        map.setCenter(results[0].geometry.location); //center on the returned location
                        map.setZoom(7); //zoom in to the search area

                    } else {
                        alert("We couldn't find that location because: " + status); //show an error if they didn't type in an address
                    }
                });
                if (code_address_flag == true || address != "")
                {
                    initialize();
                }
            }
            //window.onload = initialize;


            // Init Google Map Now only if Google Map is loaded in Current Page ---
            if (typeof (google) != 'undefined' && typeof (google.maps) != 'undefined')
                google.maps.event.addDomListener(window, 'load', initialize);
            function gen_infoBox_content(data) {
                var certified_image = "";
                var classblue = "";
                var notloopedclass = '';
                var infoImage = ""
                var ImageMessage;
                if (data['is_certified'] == 1 && data['is_certified'] != '')
                {
                    certified_image = "<img class='img-responsive loop-img' src='<?= IMAGE_URL . 'loop-certified.png'; ?>'style='padding-bottom:1px;' />";
                    classblue = 'loopedborderblue';
                }
                else
                {
                    certified_image = "";
                    classblue = '';
                }

                if (data['type'] == '2')
                {
                    notloopedclass = '<div class="non-looped-header">Not looped</div>';
                }

                if (data['venue_image'] == "") {
                    infoImage = data['categories_image'];
                    ImageMessage = '<span class="image_slogan"><p class="image_name_slogan list-overlay"><img class="no_img_avail" src="<?= IMAGE_URL . 'categoryPlace@2x3.png'; ?>"/></p></span>';
                }
                else {
                    infoImage = data['venue_image'];
                    ImageMessage = ''
                }

                var html = '<div class="ufxd-infoBox">' +
                        '<div class="looped clearfix ' + classblue + '">' +
                        notloopedclass +
                        '<div class="cus-bg b-lazy" style="background:url(' + infoImage + ') no-repeat center; background-size:cover;">' + ImageMessage + '</div>' +
                        '<div class="overlay-container">' +
                        '<div class="list-overlay">' +
                        '<h1 id="fd-name">' + data['name'] + '</h1>' +
                        '<h4 style=""><i class="fa fa-th-large"></i><span id="fd-type"> ' + data['ctype'] + '</span> </h4>' +
                        '</div>' +
                        '<div id="position_count"></div>' +
                        '<div class="list-desc">' +
                        '<div class="row">' +
                        '<div class="col-xs-2 top10 paddingleft3"><i class="fa  fa-map-marker" style="font-size: 20px; margin-left:32px"></i></div>' +
                        '<div class="col-xs-7 nopaddingleft">' + data['address'] + '<br> ' + data['city'] + ', ' + data['state'] + ' ' + data['zipcode'] + '</div>' +
                        '<div class="col-xs-3" style="float:left; left:0px">' + certified_image +
                        '</div>' +
                        '</div>' +
                        '<div class="row">' +
                        '<div class="col-md-offset-3 col-xs-6 col-md-push-0" style="margin-top:3px;margin-left:29%"><a id="get_venue_details_map" class="btn btn-general btn-viewpane" onClick="GetDetailsViewData(event,' + data['id'] + ',' + data['cid'] + ')" style="width:82% !important;">More Details</a>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</div>';
                return html;
            }

            function GetDetailsViewData(event, venue_id, c_id) {
                event.preventDefault();
                var data_string_cat_map = $(".checkcat:checked").map(function () {
                    return this.id.match(/\d+/);
                }).get();
                var cat_id = '';
                if (data_string_cat_map != '') {
                    cat_id = c_id;
                }
                var data_string = 'id=' + venue_id + '&c_id=' + cat_id;
                $.ajax({
                    type: 'post',
                    url: '<?= BASEURL . 'landing/VenueDetailsForMap' ?>',
                    data: data_string,
                    beforeSend: function (xhr) {
                        $('#blocked_venue').block({message: 'Please Wait...', css: {
                                border: 'none',
                                padding: '15px',
                                backgroundColor: '#0099e6',
                                '-webkit-border-radius': '10px',
                                '-moz-border-radius': '10px',
                                opacity: 0.8,
                                color: '#fff'
                            }});
                    },
                    complete: function (xhr) {
                        $('#blocked_venue').unblock();
                    },
                    success: function (data)
                    {
                        if (data)
                        {
                            display_venue_detail_pane(data);
                        }
                    }
                }
                );
                $('#full-detail').show();
                var left = $('#full-detail').offset().left;
                $("#full-detail").css({right: '-300px'}).animate({"right": "0px"}, "slow");
            }
            function display_venue_detail_pane(venue) {

                $('.map_sidebar_btn').text('');
                //var data = JSON.parse(venue.data);
                obj = JSON.parse(venue);
                var image_icon = "<?= UPLOADS_URL . 'venues/'; ?>";
                if (obj.type == '1') {

                    $('.map_sidebar_btn').attr('id', 'feed-button-map');
                    $('.map_sidebar_btn').removeClass('orange_request');
                    $('#non_looped').removeClass('non-looped-header');
                    $('#non_looped').text('');
                    $('.map_sidebar_btn').text('Feedback');
                }
                else if (obj.type == '2') {

                    //$('#feed-button-map').hide();
                    //$('#request').show();
                    $('.map_sidebar_btn').attr('id', 'request');
                    $('#non_looped').addClass('non-looped-header');
                    $('#non_looped').text('Not looped');
                    $('.map_sidebar_btn').text('Request');
                    $('.map_sidebar_btn').addClass('orange_request');
                }
                else if (obj.type == '3') {
                    $('.map_sidebar_btn').hide();
                }
                var htmlslider = "";
                $('#fd-slider').nivoSlider({effect: 'fold'});
                var htmlslider = '<div id="fd-slider" class="nivoSlider">';
                if (obj.venue_images != '')
                {
                    for (i in obj.venue_images)
                    {
                        htmlslider += '<img src="' + obj.venue_images[ i ].images + '" style="100%"/>';
                    }
                }
                else {
                    htmlslider += '<img src="' + obj.categories_image + '" style="100%"/>';
                    var text_image = '<p class="image_name_slogan list-overlay"><img class="no_img_avail" src="<?= IMAGE_URL . 'categoryPlace@2x3.png' ?>"></p>';
                    $('#slider_message').html(text_image);
                }
                htmlslider += '</div>';
                $('#fd-slider').replaceWith(htmlslider);
                $('#fd-name').html(obj.venue_name);
                if (obj.category_id != '') {

                    $('#fd-type').html(obj.category_id);
                }
                if (obj.category_id == '')
                {
                    $('#fd-type').html(obj.category_name);
                }

                $('#fd-address').html(obj.address + '  <br>' + obj.city + ', ' + obj.state + ' ' + obj.zipcode);
                obj.phone_number.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
                var phone_remove =  obj.phone_number.replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
                var phone = phone_remove.split('-').join('');
                
                var formatted = '(' + phone.substr(0, 3) + ') ' + phone.substr(3, 3) + '-' + phone.substr(6, 5);
                // phone number
                if (formatted != '') {

                    $('.phone_num_b').show('');
                    $('#fd-phone').text(formatted); 
                }
                else {
                    $('#fd-phone').text('');
                    $('.phone_num_b').hide();
                }
                // Email address.
                if (obj.email != '') {
                    $('.email_b').show();
                    $('#fd-email').text(obj.email);
                }
                else {
                    $('#fd-email').text('');
                    $('.email_b').hide();
                }
                //website link.
                if (obj.website != '') {
                    $('.link_b').show();
                    $('#fd-web').text(obj.website);
                }
                else {
                    $('#fd-web').text('');
                    $('.link_b').hide();
                }
                if (obj.email != "") {
                    $('#email_link').attr("href", 'mailto:' + obj.email)
                }
                if (obj.website != "") {
                    $('#myLink').attr("href", obj.website);
                }
                $(".feed-btn1").data("id", obj.venue_id);
                html = '';
                // obj.venue_images[ i ].image
                //check if it's chapter or not.
                if (obj.type != '3') {
                    //check if venue rooms available.
                    if (obj.looped_rooms != '') {

                        for (i in obj.looped_rooms) {
                            if (obj.looped_rooms[i].last_verified != '')
                            {
                                var tz = jstz.determine();
                                var DateTime = toTimeZone(obj.looped_rooms[i].last_verified, tz.name());
                                html += '<p class="clearfix"><span class="pull-left">' + obj.looped_rooms[i].name + '</span><span class="pull-right">' +
                                        '<abbr class="timeago" title="' + DateTime + '"></abbr></span></p>'
                            } else {

                                html += '<p class="clearfix"><span class="pull-left">' + obj.looped_rooms[i].name + '</span><span class="pull-right">' +
                                        'Not yet verified</span></p>'
                            }
                        }
                        $('#fd-rooms').html(html);
                    } else {
                        html += '<p class="clearfix"><span class="pull-left">--NA--</span><span class="pull-right">' +
                                '--NA--</span></p>'
                        $('#fd-rooms').html(html);
                    }
                } else {
                    $("#chapter_div").show();
                    //  $(".chapter_text").text(obj.meetupdate);
                    $("#looped_room_div").hide();
                }
                $("abbr.timeago").timeago();
                $('#fd-slider').nivoSlider({effect: 'fold'});
            }
            // jquery time ago function
            $("abbr.timeago").timeago();
            $(document).on('click', '#venue_lat_log_com', function () {

                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        $("#latitude").val(position.coords.latitude);
                        $("#longitude").val(position.coords.longitude);
                        window.location.href = '<?= BASEURL . 'landing/getPlace/lat/' ?>' + position.coords.latitude + '/long/' + position.coords.longitude;
                    });
                } else {
                    x.innerHTML = "Geolocation is not supported by this browser.";
                }
            });
            function toTimeZone(time, zone) {

                var format = 'YYYY/MM/DD HH:mm:ss ZZ';
                return moment(time, format).tz(zone).format(format);
            }

        </script>

        <script>

            $('.icheck').each(function () {
                var checkboxClass = $(this).attr('data-checkbox') ? $(this).attr('data-checkbox') : 'iradio_line-grey';
                var radioClass = $(this).attr('data-radio') ? $(this).attr('data-radio') : 'iradio_line-grey';
                if (checkboxClass.indexOf('_line') > -1 || radioClass.indexOf('_line') > -1) {
                    $(this).iCheck({
                        checkboxClass: checkboxClass,
                        radioClass: radioClass,
                        insert: '<div class="icheck_line-icon"></div>' + $(this).attr("data-label")
                    });
                } else {
                    $(this).iCheck({
                        checkboxClass: checkboxClass,
                        radioClass: radioClass
                    });
                }
            });

        </script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-43424048-6', 'auto');
  ga('send', 'pageview');

</script>
    </body>
</html>