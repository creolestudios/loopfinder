<?php
$headerData = $this->headerlib->data();
$JsArr = array("SCRIPT" => $headerData['javascript'], 'PLUGINS' => $headerData['javascript_plugins']);
?>
<html  lang="en-us">
    <head>
        <?php $headerData['meta_tags']; ?>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <title><?= MAINTITLE; ?></title>


        <!------- BEGIN STYLESHEET   ---->
        <?php echo $headerData['stylesheets']; ?>
        <link href="<?= CSS_IMAGE_URL . 'frontsite/font-awesome.min.css' ?>" rel="stylesheet"> 
        <link href="<?= PLUGIN_URL ?>icheck/skins/all.css" rel="stylesheet"/>

        <!-- END THEME STYLESHEET -->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
        <!-- BEGIN THEME STYLES --> 
        <?php //echo $headerData['themestyles'];  ?>
        <!-- END THEME STYLES -->
        <!---------- favicon icon -------------->
        <link rel="shortcut icon" href="<?= IMAGE_URL ?>favicon.ico"/>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <style>
            .img_location{
                position: absolute;
                left:395px;
                top:15px;
                cursor: pointer;
            } 
            .loading-image{
                position: absolute;
                float: right;
                left: -15px;
                top: 30px;
            }
            .no_res
            {
                position: relative;
                margin: 0;
                padding: 3px 1em 3px .4em;
                cursor: pointer;
                min-height: 0;
            }
            img.lazy { background: no-repeat 50% 50%; }
            .left_li_padding{
                padding: 0 21px !important;
                float: left;
            }
        </style> 
    </head>
    <body>
        <?php //$this->load->view('include/landing_menu');   ?>

        <header class="header">
            <div class="container-fluid">
            </div>
            <div class="row menu_header_row hidden-xs row-no-padding" style="">
                <div class="col-sm-3 hidden-xs">
                    <a href="<?= BASEURL.'landing/index' ;?>"><img class="logo img-responsive" src="<?= IMAGE_URL.'new_logo269x49.png' ?>"></a>
                </div>
                <div class="col-sm-7 hidden-xs">
                    <div class="menu-container">
                        <ul class="main-menu">
                            <li class="left_li_padding "><a href="<?= BASEURL.'landing/index'; ?>" class="menu_active_li">Home</a></li>
                            <li class="left_li_padding"><a href="<?= BASEURL.'requestloop/'; ?>">Request / Add a Loop</a></li>
                            <li class="left_li_padding"><a href="<?= BASEURL.'contactus/' ;?>">Contact Us</a></li>
                            <li class="left_li_padding"><a href="<?= BASEURL.'faq/' ?>">FAQ</a></li>
                            <!--                                <li class="left_li_padding donation_li" id=""><a href="javascript:void(0)" id="donate_link">Donate</a></li>-->
                            <li class="left_li_padding donation_li" id=""><img src="<?= IMAGE_URL . 'Donate-Button.png'; ?>" style="margin-top: -7px; max-width: 74px;" alt="donation link" id="donate_link"></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-2 hidden-xs">
                    <a href="http://www.hearingloss.org/" target="_blank"><img class="logo img-responsive donation_image" style="width: 130px;" src="<?= IMAGE_URL.'HLAA-logo.png'?>"></a>
                </div>
            </div>
            <div class="row">
                <!--                <div class="col-sm-3 hidden-xs">
                                    <a href="<?= BASEURL . 'landing/index'; ?>"><img class="logo img-responsive" src="<?php echo IMAGE_URL . 'new_logo269x49.png'; ?>" /></a>
                                </div>
                                <div class="col-sm-7 hidden-xs">
                                    <div class="menu-container">
                                        <ul class="main-menu">
                                            <li class="left_li_padding"><a href="<?= BASEURL . 'landing/index'; ?>">Home</a></li>
                                            <li class="left_li_padding"><a href="<?= BASEURL . 'requestloop/'; ?>">Request / Add a Loop</a></li>
                                            <li class="left_li_padding"><a href="<?= BASEURL . 'contactus/'; ?>">Contact Us</a></li>
                                            <li class="left_li_padding"><a href="<?= BASEURL . 'faq/'; ?>">FAQ</a></li>
                                                                            <li class="left_li_padding donation_li" id=""><a href="javascript:void(0)" id="donate_link">Donate</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-sm-2 hidden-xs">
                                    <a href="http://www.hearingloss.org/" target="_blank"><img class="logo img-responsive donation_image" style="width: 130px;" src="<?php echo IMAGE_URL . 'For-Web.png'; ?>" /></a>
                                </div>-->
                <!-- mobile menu -->
                <div class="col-xs-4 visible-xs">
                    <a href="<?= BASEURL . 'landing/'; ?>"><img class="logo img-responsive" src="<?php echo IMAGE_URL . 'new_logo269x49.png'; ?>" /></a>
                </div>
                <div class="col-xs-4 visible-xs">
                    <a href="http://www.hearingloss.org/" target="_blank"><img class="logo img-responsive donation_image" style="width: 100px !important;" src="<?php echo IMAGE_URL . 'For-Web.png'; ?>" /></a>
                </div>
                <div class="col-xs-4 visible-xs text-right">
                    <div class="clearfix inline-block">
                        <a class="touchbtn dropdown-toggle blue" href="javascript:void(0);" style="padding-right: 10px;" type="button" data-toggle="dropdown"><span class="fa fa-bars" style="line-height: 2 !important;"></span></a>
                        <ul class="dropdown-menu dropdown-position" style="font-size: 14px; top: 46px !important;">
                            <li><a href="<?= BASEURL . 'landing/index'; ?>">Home</a></li>
                            <li><a href="<?= BASEURL . 'requestloop/'; ?>">Request / Add a Loop</a></li>
                            <li><a href="<?= BASEURL . 'contactus/'; ?>">Contact Us</a></li>
                            <li><a href="<?= BASEURL . 'faq/'; ?>">FAQ</a></li>
                            <li class="left_li_padding donation_li" id="donate_link">Donate</li>
                            <!--                                <li><a href="javascript:void(0)" id="donate_link">Donate</a></li>-->
                        </ul>                                
                    </div>
                    <!-- mobile menu end -->
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <h1 class="header-text">Find Hearing Loop enabled venues:</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 col-xs-12">
                    <div class="header-input-container">
                        <form action="<?php echo BASEURL . 'landing/getPlace'; ?>" method="post" id="form_landing">
                            <input type="text" class="input1" name="LocationForVenue" onkeypress="return runScript(event)" placeholder="City" id="venue_location"><input type="text" onkeypress="return runScript(event)" name="VenueName" class="input2" id="venue_name" placeholder="Name">
                            <input type="hidden" name="latitude" id="latitude" value=""/>
                            <input type="hidden" name="longitude" id="longitude" value=""/>
                            <button class="search" type="button" id="landing_submit"></button>

                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-md-offset-5 col-sm-offset-4 col-xs-offset-3" style="top:5px;">
                    <button type="submit" class="btn btn-general btn-contact-submit" id="search_venue" value="" style="padding:10px;" onClick="getLocation_lat()">Show all venues near me</button>
                </div>
            </div>
        </div>
    </header>
    <section class="venue-container">
        <div class="container-fluid" id="container">
            <h2 class="center venue-heading">Search by Venue Category:</h2>
            <?php $temp = UPLOADS_URL . 'category_image/'; ?>
            <div class="container-fluid inner-cat" id="container">
                <?php foreach ($result as $key => $r) { ?>

                    <div class="col-sm-3 col-xs-6" id ="<?php echo $r['id']; ?>">
                        <div class="venue-item">
                            <a href="<?= BASEURL . 'landing/getPlace/category/' . $r['id'] ?>" class="cate-id" data-id="<?php echo $r['id']; ?>">
                                <img class="img-responsive lazy" data-src="<?php echo $temp . $r['image']; ?>" border="0" alt="<?php echo $r['name']; ?>"  src="" style="max-height: 200px;"/>
                                <div class="overlay"><?php echo $r['name']; ?></div>
                            </a>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
    <section class="more-categories">
        <div class="container-fluid">
            <div class="col-sm-4 col-sm-offset-4">
                <div class="more-button-container" id="div_btn">
                    <button class="btn btn-general btn-contact-submit2" id="btn_show_more" value="" style="padding:10px; width: 65%;">More Categories</button>
                    <!--                        <a class="btn btn-more" type="button" href="javascript:void(0)" id="">More Categories</a>                        -->
                </div> 
                <div id="div_image" style="text-align:center;display: none;">
                    <img src="<?= IMAGE_URL . 'categories_loader.gif'; ?>"/>                       
                </div> 
            </div>
        </div>
    </section>
    <section class="appstore">
        <div class="container-fluid">
            <div class="col-sm-3 col-sm-offset-2  col-xs-offset-2">
                <div class="app-img-fix"><img src="<?php echo IMAGE_URL . 'mobile.png'; ?>" style="height:100%;" /></div>
            </div>
            <div class="col-sm-6 col-sm-offset-1">
                <div class="app-desc">
                    <h2 class="black">LoopFinder is also available on the App Store.</h2>
                    <div class="app-text">
                        Find hearing loop enabled venues near you and request hearing loops with your iPhone. 
                    </div>
                    <div class="apple-store">
                        <a href="https://itunes.apple.com/us/app/loopfinder/id992016120?mt=8"><img src="<?php echo IMAGE_URL . 'apple-store.png'; ?>   " /></a>

                    </div>
                </div>
            </div>
        </div>
        <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" id="donation_form" method="post" target="_top" style="display:none;">
            <input type="hidden" name="cmd" value="_s-xclick">
            <input type="hidden" name="hosted_button_id" value="8PHF7YVBNYS8E">
            <input type="hidden" name="tx" value="TransactionID"> 
<!--            <input type="image" src="Donate-Button.png" sy border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">-->
            <input type="submit" class="btn btn-general btn-contact-submit" style="width: 136px;   margin-top: 5px;"value="Donate"/>
            <img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
        </form>
    </section>
    <?php $this->load->view('include/footer', $JsArr); ?>
    <script src="<?= JS_URL . 'frontsite/jstz-1.0.4.min.js'; ?>"></script>  
    <script src="<?= JS_URL . 'frontsite/jquery.lazyload.js'; ?>"></script>  
    <script src="<?= PLUGIN_URL . 'jquery.cokie.min.js'; ?>"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript">

                        var timezone = jstz.determine().name();
                        $.cookie('venuetimezone', timezone);
                        $(document).ready(function () {


                            $("#donate_link").click(function () {
                                alert("Donation Coming Soon!!");
                                return false;
                                //$("#donation_form").submit();
                            });
                            $("img.lazy").lazy({
                                effect: "fadeIn",
                                effectTime: 1000,
                                // placeholder: 'Loading...'
                                placeholder: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABsElEQVR42n1TWaoCMRD0Bu8IHiVHmQP4kR8/BQURBPVHUUTQKO4gxgUXXAgqrohXmCPkCPOqh5dx1PEFCpqku1Jd6YRCb+vxeIRNnM1mJSHoLHDVajUrHo/r7XYb/iNQBIppj3OuS6WS9ZXgfr//JJNJncvlxDtBIpEQ0WhUU85HYavVsswBVPBYLOYMh0NmCDqdDotEIk6hUODmouv1anl9pdNpjUTdbDbdhEwmY+fzeVkulzkhlUpJKLDp7HK58PP5rAmeJ8fjMQx2gUS6xd7v94z2jELq/3Q6MRTbgAOIQEMnkwnDjdJf7H8ByJa32429HAghJKAI1WpVtdtt/s1kqOBQpwwOh4N8ISD0er2vBCj4JPCv+XzOYOTXFjabjSR/PpipoF6vi2Kx6ECFTYb5SSim3lerlb1YLJz1ev00kQJMl65UKnowGLjyu92u3e/35Ww24wQok9Pp1H1GpRRfLpcaRM9nxNB4gzQejznMdHAbG41GikCykUN73iDtdjsrcJQbjYaGAneUDQHFUCJAHjzKZiHBggfa9O8noD0ppYaR1r8/0j9hKJaE/77zL+acf+aovaWqAAAAAElFTkSuQmCC"
                                        //placeholder: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAEk0lEQVR42qVWfUyVVRx+3nvfexmgNE1LxiBmNAvMEoQk20okRHLRRonJR6uIxWq4EZklkgi2AmnOLWHUH00zvHw43Mhl0AeNSq4Ea0a6xI8LAhGGgXDve9+Pc/qd92Y1k68622/nveec93l+9znv+T1Hwg0t2dHPPW4VmqqDURiqBqbp0DUDhtf3LMag6+YzUw0wpvnWGwYhMAw6sqXreNKNBAmH+/j2dcG44mUwdEBlDFzn0MChGgyq5us1xuHVmTnvoXkvjWuSBScONGP46LNTEyTW9vGU5QvhGidIykhV6UUBSM+6Sj2FwsS4AU0ThIYZmiCzWDHa7sRQ/TQESY7LPbLHvWCJvyXY5i+DkSyGTjLogoBkor9lkDSakMhLY0IyAndPTmLwV/fQNY96ddiRHTUlgWhtTa0vLg4LrrpnZSRu3khrUzraGyJkTMeZH11Qrrnz4h9dU/3PlTclaKpvjbP7+3VseOyhv8aYCE5B2GIvVcpaN/+VSigGOr/rps3XH0h9MtE5I4Fohw5+yh9/aj2CvHvACZUbHJxzc8MZ7QmnELIxQ4VbCULL6QeRkbn+X3hTEhz+8Lgj7LYvNq1YWwFpoAIB6jZTGcmQIOmit5q/xaf2zVAhXJ6EuoxnUtJnTSDa+9XH+YZVRZgf9b1JEujZRRoJAkmoQuAqBseW45P+MuS+kHJTrGkJPqhyhHq1gL7YOw/i7mWx5ljgRAksRACSrNO1Ec7hTPjJ7rCcvPT+OROIVrX/ULhm+DXeGngpekt8Me2u1QSv7dpNhzGsyyZ50/Lysy5N9f6MBNdbRflHTWvvqk2NueNLdF9IwOcXNh97dVvmEzO9N2uCPaU1TUlLG1NXLW1DZ9/DaPk57dgbO3P/P8FbJZXhuq43+geFRBeufh6wWeG2hqCyuRASH+uSZTnt9eKC/yZRSdHboYpH6UvemIT7V8dj3ikJFnsgvAEROO/XirOnT6Lb+QPsdltYcdlrc9/kV/J38OzntiB8WaT5mc4f3g5u9weXDZyTP8MkC4Xr/E/oaO/AO+/unttnWpi/wxG54t5N6ZnpsPWsgTzxLST7PHAbTVrpBFs9+AVZ6B0vwsm2ExgdGakr31c6+4OWl1PAy/aWYsGZCEjeK5Q5IcsWAucmCbcwWO1ejChx+Kq3HO0tzdhfVT67UpGfWxC7ODTCWbT5LNB/ALD7kSy0lI4At1HFsxCJTAtlDquswDn4JhoaLNCU8bh91XtPzUjwckZ20X0xMaU5K7cSGCH5icxpqZCHwM3eKggo7FT4cDt2vbcVk7/17qysqSmbliC5bqDHO3r1lkXWiZCFlotkLIbpvRrVf2ZovvqvqeQBwlLJ9TgVcsmDi64ojE96BjRJHhs6kjW14SR+3Mefjl6EMfJklUqzTmXBMHy99qcHmBZqzjEzAZWsk1KgkNBe/zWGG6axzHVk+i89sgTnftfNLMklCZRM3RC+K5lGI8xeIWBFEz0zfVshX/ZIEi4fbcXQdATi2qLQtUV4LVOE3wpJNN+1ha4mXFxPdNV3ZTHD8Emn+fxaJDV45O9ryx+UubI3TsTNbgAAAABJRU5ErkJggg==";
                            });
                            $('.dropdown-toggle').dropdown();
                            var temp = '';
                            var availableTags = '<?= BASEURL . 'landing/getVenuesLocation' ?>'
                            var NoResultsLabel = "Oops! No suggestions found. Try a different search.";
                            $("#venue_location").autocomplete({
                                source: availableTags,
                                minLength: 2,
                                select: function (event, ui) {
                                    if (ui.item.value === NoResultsLabel) {
                                        event.preventDefault();
                                    }
                                },
                                focus: function (event, ui) {
                                    if (ui.item.value === NoResultsLabel) {
                                        event.preventDefault();
                                    }
                                }
                            });
                            //$('#venue_location')

                            var availableTags1 = '<?= BASEURL . 'landing/getVenuesName'; ?>';
                            $("#venue_name").autocomplete({
                                //source: availableTags1
                                source: function (request, response) {
                                    $.getJSON(availableTags1, {location: $('#venue_location').val(), term: request.term},
                                    response);
                                },
                                minLength: 2,
                                select: function (event, ui) {
                                    if (ui.item.value === NoResultsLabel) {
                                        event.preventDefault();
                                    }
                                },
                                focus: function (event, ui) {
                                    if (ui.item.value === NoResultsLabel) {
                                        event.preventDefault();
                                    }
                                }

                            });
                        })
    </script>
    <script>
        /***************************************
         * LOADMORE CATEGORIES 
         ****************************************/
        var controller = 'landing';
        var flag_venue = true;
        var offset = 8;
        $(document).on('click', '#btn_show_more', function (event) {
            if (flag_venue == false) {
                offset = parseInt(offset + 8);
            }
            flag_venue = false;
            $.ajax({
                //base_url+"index.php?/ajax_demo/give_more_data",
                url: '<?= BASEURL . 'landing/getCategories'; ?>',
                type: 'POST',
                data: 'offset=' + offset,
                beforeSend: function (xhr) {
                    $('#div_btn').hide();
                    $('#div_image').show();
                },
                complete: function (data) {

                    $('#div_btn').show();
                    $('#div_image').hide();
                },
                success: function (data)
                {
                    if (data) {
                        var inform = $.parseJSON(data);
                        var boxHtml = "";
                        for (var key in inform.data)
                        {
                            boxHtml += '<div class="col-sm-3 col-xs-6" id="' + inform.data[key].id + '">' +
                                    '<div class="venue-item">' +
                                    '<a href="<?= BASEURL . 'landing/getPlace/category/'; ?>' + inform.data[key].id + '" class="cate-id" data-id="3">' +
                                    '<img class="lazy img-responsive" data-src="<?= UPLOADS_URL . 'category_image/' ?>' + inform.data[key].image + '"  border="0"  alt="' + inform.data[key].name + '"  src="" style="max-height: 200px">' +
                                    '<div class="overlay">' + inform.data[key].name +
                                    '</div>' +
                                    '</a>' +
                                    '</div>' +
                                    '</div>';
                        }

                        if (inform.data.length < 8)
                        {
                            $('.more-categories').hide();
                        }
                        $(".inner-cat").append(boxHtml);
                        // $(".inner-cat").find("img.lazy").lazy();
                        $("img.lazy").lazy({
                            bind: "event",
                            effect: "fadeIn",
                            effectTime: 1000,
                            placeholder: 'Loading...'
                                    // placeholder: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAEk0lEQVR42qVWfUyVVRx+3nvfexmgNE1LxiBmNAvMEoQk20okRHLRRonJR6uIxWq4EZklkgi2AmnOLWHUH00zvHw43Mhl0AeNSq4Ea0a6xI8LAhGGgXDve9+Pc/qd92Y1k68622/nveec93l+9znv+T1Hwg0t2dHPPW4VmqqDURiqBqbp0DUDhtf3LMag6+YzUw0wpvnWGwYhMAw6sqXreNKNBAmH+/j2dcG44mUwdEBlDFzn0MChGgyq5us1xuHVmTnvoXkvjWuSBScONGP46LNTEyTW9vGU5QvhGidIykhV6UUBSM+6Sj2FwsS4AU0ThIYZmiCzWDHa7sRQ/TQESY7LPbLHvWCJvyXY5i+DkSyGTjLogoBkor9lkDSakMhLY0IyAndPTmLwV/fQNY96ddiRHTUlgWhtTa0vLg4LrrpnZSRu3khrUzraGyJkTMeZH11Qrrnz4h9dU/3PlTclaKpvjbP7+3VseOyhv8aYCE5B2GIvVcpaN/+VSigGOr/rps3XH0h9MtE5I4Fohw5+yh9/aj2CvHvACZUbHJxzc8MZ7QmnELIxQ4VbCULL6QeRkbn+X3hTEhz+8Lgj7LYvNq1YWwFpoAIB6jZTGcmQIOmit5q/xaf2zVAhXJ6EuoxnUtJnTSDa+9XH+YZVRZgf9b1JEujZRRoJAkmoQuAqBseW45P+MuS+kHJTrGkJPqhyhHq1gL7YOw/i7mWx5ljgRAksRACSrNO1Ec7hTPjJ7rCcvPT+OROIVrX/ULhm+DXeGngpekt8Me2u1QSv7dpNhzGsyyZ50/Lysy5N9f6MBNdbRflHTWvvqk2NueNLdF9IwOcXNh97dVvmEzO9N2uCPaU1TUlLG1NXLW1DZ9/DaPk57dgbO3P/P8FbJZXhuq43+geFRBeufh6wWeG2hqCyuRASH+uSZTnt9eKC/yZRSdHboYpH6UvemIT7V8dj3ikJFnsgvAEROO/XirOnT6Lb+QPsdltYcdlrc9/kV/J38OzntiB8WaT5mc4f3g5u9weXDZyTP8MkC4Xr/E/oaO/AO+/unttnWpi/wxG54t5N6ZnpsPWsgTzxLST7PHAbTVrpBFs9+AVZ6B0vwsm2ExgdGakr31c6+4OWl1PAy/aWYsGZCEjeK5Q5IcsWAucmCbcwWO1ejChx+Kq3HO0tzdhfVT67UpGfWxC7ODTCWbT5LNB/ALD7kSy0lI4At1HFsxCJTAtlDquswDn4JhoaLNCU8bh91XtPzUjwckZ20X0xMaU5K7cSGCH5icxpqZCHwM3eKggo7FT4cDt2vbcVk7/17qysqSmbliC5bqDHO3r1lkXWiZCFlotkLIbpvRrVf2ZovvqvqeQBwlLJ9TgVcsmDi64ojE96BjRJHhs6kjW14SR+3Mefjl6EMfJklUqzTmXBMHy99qcHmBZqzjEzAZWsk1KgkNBe/zWGG6axzHVk+i89sgTnftfNLMklCZRM3RC+K5lGI8xeIWBFEz0zfVshX/ZIEi4fbcXQdATi2qLQtUV4LVOE3wpJNN+1ha4mXFxPdNV3ZTHD8Emn+fxaJDV45O9ryx+UubI3TsTNbgAAAABJRU5ErkJggg=="
                        });
                    }
                }
            });
        });
        $(document).on('click', '#landing_submit', function (event) {

            event.preventDefault();
            var venue_city = $('#venue_location').val();
            var venue_name = $('#venue_name').val();
            var search;
            //geolocation
            var address_lat = $("venue_location").val();
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({'address': address_lat}, function (results, status) {

                if (status == google.maps.GeocoderStatus.OK) {

                    var location_lat = results[0].geometry.location.lat()
                    var location_lang = results[0].geometry.location.lng();

                } else {

                }
            });
            if (venue_city == '' && venue_name == '') {
                window.location.href = '<?= BASEURL . 'landing/getPlace/' ?>';
            } else if (venue_city != '' && venue_name == '')
            {
                //split string by comma
                window.location.href = '<?= BASEURL . 'landing/getPlace/city/' ?>' + venue_city + '/';
            } else if (venue_city == '' && venue_name != '')
            {
                window.location.href = '<?= BASEURL . 'landing/getPlace/venue/' ?>' + venue_name;
            } else if (venue_city != '' && venue_name != '') {

                window.location.href = '<?= BASEURL . 'landing/getPlace/city/' ?>' + venue_city + '/venue/' + venue_name;
            }
            else {
                window.location.href = '<?= BASEURL . 'landing/getPlace/' ?>';
            }
        });
        function getLocation_lat() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (position) {
                    $("#latitude").val(position.coords.latitude);
                    $("#longitude").val(position.coords.longitude);
                    var venue_lat = $('#latitude').val();
                    var venue_long = $('#longitude').val();
                    //submit form with latitude and longitude
                    // $('#form_landing').submit(); 
                    if (venue_lat != '' && venue_long != '')
                    {
                        window.location.href = '<?= BASEURL . 'landing/getPlace/lat/' ?>' + venue_lat + '/long/' + venue_long;
                    }
                });
            } else {
                x.innerHTML = "Geolocation is not supported by this browser.";
            }
        }
    </script>
    <script type="text/javascript">

        function runScript(e) {
            if (e.keyCode == 13)
            {
                $('#landing_submit').click();
            }
        }
    </script>
</body>    

</html>
