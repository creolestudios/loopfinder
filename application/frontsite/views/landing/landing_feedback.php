<div id="form_message" class="alert alert-success" style="display:none" tabindex="-1">hello</div>
<form action="<?= BASEURL . 'landing/getFeedbackSave'; ?>" method="post" id="feedback-from">

    <div class="feedback-margin input input-feedback clearfix">
        <div class="col-sm-7 col-xs-7" style="padding: 10px;">
            <div class="feedback-text">Looped Room</div>
        </div>
        <?php if (sizeof($result) > 1) { ?>
            <div class="col-sm-5">
                <select class="feedback-select select-stylize form-control" name="ufxd_looped" id="VenueNameList">
                    <?php foreach ($result as $VenueRoom) {
                        ?>
                        <option  id="feed_venue_room" value="<?php echo $VenueRoom['id']; ?>"><?= $VenueRoom['name']; ?></option>
                    <?php } ?>
                </select>
            </div>
        <?php } else { ?>
        <div class="col-sm-5 justify_room col-xs-5" style="top:9px;   left: -72px;">
               <?php foreach ($result as $VenueRoom) { ?>
                <input type="hidden" id="VenueNameList" value="<?php echo $VenueRoom['id']; ?>"/>
                <span class="singleclass"><?= $VenueRoom['name']; ?></span>
               <?php } ?>
            </div>
        <?php } ?>
    </div>
    <div class="feedback-margin input input-feedback clearfix">
        <div class="col-sm-8" style="padding: 10px;">
            <div class="feedback-text">Was the loop working well?</div>
        </div>
        <div class="col-sm-4 radio-5s">
            <div class="radio radio-no-text" >
                <label class="radio-label1"><input type="radio"   name="work_radio" id="1" class="radio-1 true_radio" value="1"><span class="radio-img-yes"></span></label>
                <label class="radio-label1"><input type="radio"  name="work_radio" class="radio-1" id="0" value="0"><span class="radio-img-no"></span></label>
            </div>
        </div>
    </div>
    <div class="feedback-margin input input-feedback clearfix" id="reason_slide" style="display: none;">
        <div class="col-sm-12" style="left: -5px;">
            <div class="feedback-text">What issues did you experience?</div>
        </div>
        <div class="col-sm-12">

            <div class="input-group">
                <div class="icheck-inline">
                    <div class="col-sm-6">
                        <label style="padding:5px">
                            <input type="radio" class="icheck"  name="reason_radio" data-radio="iradio_square-blue" data-id="1" id="1" value="1"> No sound</label>
                        <label style="padding:5px">
                            <input type="radio" class="icheck" name="reason_radio" data-radio="iradio_square-blue" data-id="3" id="3" value="3"> Headset broken</label>
                    </div>

                    <div class="col-sm-6">
                        <label style="padding:5px">
                            <input type="radio" class="icheck" name="reason_radio" data-radio="iradio_square-blue" data-id="2" id="2" value="2"> Poor sound quality</label><br>
                        <label style="padding:5px">
                            <input type="radio" class="icheck" name="reason_radio" data-radio="iradio_square-blue" data-id="4" id="4" value="4">    Other</label>
                    </div>


                </div>

            </div>


            <!--                                <div class="checkbox clearfix">
                                                <label for="no_sound" class="feedback-checkbox"><input type="checkbox" class="checkbox-square" data-id="1" id="no_sound" /><span class="checkbox-square-img"></span>No sound</label>
                                                <label for="headset" class="feedback-checkbox"><input type="checkbox" class="checkbox-square" data-id="3" id="headset" /><span class="checkbox-square-img"></span>Headset broken</label>
                                                <label for="poor_sound" class="feedback-checkbox"><input type="checkbox" class="checkbox-square" data-id="2" id="poor_sound" /><span class="checkbox-square-img"></span>Poor sound quality</label>
                                                <label for="others" class="feedback-checkbox"><input type="checkbox" class="checkbox-square" data-id="4" id="others" /><span class="checkbox-square-img"></span> Others</label>
                                            </div>-->
        </div>
    </div>


    <div class="inner-addon left-addon feedback-margin">
<!--        <i class="glyphicon" style="left:16px;"><img class="input-icon" src="<?= IMAGE_URL . 'chat-icon.png'; ?>" /></i>-->
        <i class="fa  fa-comment input-icon"></i>
        <textarea name="feed_comment" style="padding-left: 43px;"  class="form-control input" placeholder="Comment" rows="8" id="feed_comment" ></textarea>
    </div>
    <div class="inner-addon left-addon feedback-margin">
<!--        <i class="glyphicon" style="left:16px;"><img class="input-icon" src="<?= IMAGE_URL . 'user-icon.png'; ?>" /></i>-->
        <i class="fa  fa-user input-icon"></i>
        <input type="text" name="feed_name" style="padding-left: 43px;" class="form-control input" placeholder="Name" id="feed_name" />
    </div>
    <div class="inner-addon left-addon feedback-margin">
<!--        <i class="glyphicon" style="left:16px;"><img class="input-icon" src="<?= IMAGE_URL . 'email-icon.png'; ?>" /></i>-->
        <i class="fa  fa-envelope input-icon"></i>
        <input type="text" name="feed_email" style="padding-left: 43px;" class="form-control input" placeholder="E-Mail" id="feed_email" />
    </div>

    <div class="text-center">
        <button type="submit" class="btn btn-general btn-contact-submit" id="submit" value="Request a Loop" style="width:126px;height:42px;">Submit</button>
    </div>
</form>
<script>

    $('.icheck').each(function () {
        var checkboxClass = $(this).attr('data-checkbox') ? $(this).attr('data-checkbox') : 'iradio_square-blue';
        var radioClass = $(this).attr('data-radio') ? $(this).attr('data-radio') : 'iradio_square-blue';


        if (checkboxClass.indexOf('_line') > -1 || radioClass.indexOf('_line') > -1) {
            $(this).iCheck({
                checkboxClass: checkboxClass,
                radioClass: radioClass,
                insert: '<div class="icheck_line-icon"></div>' + $(this).attr("data-label")
            });
        } else {
            $(this).iCheck({
                checkboxClass: checkboxClass,
                radioClass: radioClass
            });
        }
    });
</script>