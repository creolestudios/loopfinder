<div class="container-fluid">

    <div class="list-detail-container">
        <div class="notch <?= $venue_detail_notch_class ?>"></div>
        <div class="clearfix" id="blocked_venue" style="background-color: #ffffff;">
            <div class="col-sm-4">
                <div class="slider-container">
                    <div class="slider-wrapper theme-default">
                        <?php if($data['venue_images']!='' && count($data['venue_images']) > 0 ){ ?>
                        <div id="slider" class="nivoSlider">
                            <?php
                            
                              foreach ($data['venue_images'] as $key => $value) {
                                ?>
                                <img class="img-responsive" src="<?= $value['images']; ?>" />
                            <?php } ?>
                        </div>
                        <?php }else{ ?>
                       
                        <div id="slider" class="nivoSlider">
                             <img class="img-responsive" src="<?= $data['venue_details']['categories_image']; ?>" />
                         </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="row row_venue">
                    <div class="col-sm-6">
                        <h1><?= $data['venue_details']['venue_name']; ?></h1>
                        <?php if(isset($data['category_id']) && !empty($data['category_id'])){
                            ?>
                        <h4><i class="fa fa-th-large" style="font-size:18px"></i> <?=$data['category_id']; ?> </h4>
                        <?php }else{
                         ?>
                        <h4><i class="fa fa-th-large" style="font-size:18px"></i> <?= $data['venue_details']['category_name']; ?> </h4>
                            <?php }?>
                        
                        <p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <?php if ($data['venue_details']['is_certified'] == 1) { ?>
                            <img class="img-responsive loop-img" src="<?= IMAGE_URL . 'loop-certified.png'; ?>"/>
                        <?php } ?>
                    </div>

                    <div class="col-sm-6" style="margin-top:10px !important;">
                        <p><i class="fa  fa-map-marker" style="font-size:18px"></i> <span style="padding-left:7px;"><?= $data['venue_details']['address'] . '<br>' ?> <?= str_repeat('&nbsp', 5) . $data['venue_details']['city'] ?>, <?= $data['venue_details']['state'] ?> <?= $data['venue_details']['zipcode']; ?></span></p>

                        <hr class="sidebar-section2">
                        <?php if($data['venue_details']['type'] != '3'){ ?>
                        <p class="section-head"><i class="fa  fa-headphones" style="font-size:18px"></i>&nbsp;Looped Room Details</p>
                        <p class="clearfix "><span class="pull-left blue">Room</span><span class="pull-right blue">Last Verified</span></p>

                        <?php
                        $timezone = $this->input->cookie('venuetimezone', false);
                        foreach ($data['looped_rooms'] as $key => $value){
                            $dt = new DateTime($value['last_verified']);
                            $tz = new DateTimeZone($timezone); // or whatever zone you're after
                            $dt->setTimezone($tz);
                            $VotesDateTime =  $dt->format('Y-m-d H:i:s');
                            ?> 
                            <p class="clearfix"><span class="pull-left"><?= $value['name']; ?></span><span class="pull-right">
                                    <?php if ($value['last_verified'] != '') { ?><abbr class="timeago" title="<?= $VotesDateTime ?>"></abbr></span></p><?php } else { ?><span class="pull-right">Not yet verified</span></p> <?php } ?>
                                <?php }
                                ?>
                        <?php }else{ ?>
                    <p class="clearfix "><span class="pull-left" style="font-size: 18px; font-weight: 600;font-size: 15px;"><i class="fa fa-calendar"></i>&nbsp;Meetup Date</span><span class="pull-right"><?= $data['venue_details']['meetupdate']?></span></p>
                          
                        <?php } ?>
                        <hr class="sidebar-section2">
                        <p class="section-head"><i class="fa   fa-phone" style="font-size: 18px;"></i>&nbsp; Contact Info</p>
                        <?php 
                            if($data['venue_details']['phone_number'] !="")
                                {
                                $phone = $data['venue_details']['phone_number'];
                                //mprd($phone);
                                $phone1 = str_replace('-', '', $phone);
                                $phone2 = '(' . substr_replace($phone1, ')', 3, 0);
                                $phone3 = substr_replace($phone2, '-', 8, 0);
                                $phone4 = substr_replace($phone3, ' ', 5, 0);
                                }
                                else{
                                    
                                     $phone4 = "";
                                }
                                if ($phone4 != '')
                              { ?>
                            <p class="clearfix"><span class="pull-left"><i class="fa fa-phone-square" style="font-size:18px"></i></span>&nbsp; <span id="fd-phone" class="v-middle"><?= $phone4 ?></span></p>
                                
                             <?php } ?>

                        <?php if ($data['venue_details']['email'] != '') { ?> <p class="clearfix"><span class="pull-left"><i class="fa  fa-envelope-square" style="font-size: 18px;"></i></span>&nbsp; <a href="mailto:<?= $data['venue_details']['email']; ?>" id="email_link1" style="color:#21a5e8 !important; text-decoration: underline;"><span id="fd-email" class="v-middle"><?= $data['venue_details']['email']; ?></span></a></p><?php } ?>

                        <?php if ($data['venue_details']['website'] != '') { ?> <p class="clearfix"><span class="pull-left"><i class="fa fa-globe" style="font-size:18px"></i></span>&nbsp; <a href="<?= $data['venue_details']['website']; ?>" id="email_link1" target="_blank" style="color:#21a5e8 !important; text-decoration: underline;"><span id="fd-web" class="v-middle"><?= $data['venue_details']['website']; ?></span></a></p> <?php } ?>

<!--                                        <p class="section-head"><img class="v-middle" src="<?= IMAGE_URL . 'info-icon.png'; ?>" />&nbsp; Additional Information</p>
                                        <p class="clearfix"><span class="pull-left"><?= $data['venue_details']['additional_information'] ?></span></p>-->
                        <hr class="sidebar-section2">
                    </div>
                </div>

                <div class="row row_venue_btn">
                    <!--                                    <div class="col-sm-6">
                                                            <input type="hidden" value="<?= $data['venue_details']['latitude'] ?>" class="grid_direction_lat"/><input type="hidden" value="<?= $data['venue_details']['longitude'] ?>" class="grid_direction_lng"/>
                                                            <a class="btn btn-general get-direction-btn" data-toggle="modal" data-target="#modal-getdirection_grid" data-lat="<?= $data['venue_details']['latitude'] ?>,<?= $data['venue_details']['longitude'] ?>" >Get Direction</a><input type="hidden" id="venue_lat_long" value="<?= $data['venue_details']['venue_id']; ?>" id="get-direction-grid"/> </div>-->
                    <div class="col-md-offset-3 col-xs-offset-3">
                        <?php if ($data['venue_details']['type'] == 1) { ?>
                            <a id="feed-button" class="btn btn-general" style="width:50% !important;">Feedback</a><i data-id="<?= $data['venue_details']['venue_id']; ?>" class="feed-btn2"></i><?php } ?>
                        <?php if ($data['venue_details']['type'] == 2) { ?>
                            <a class="btn btn-general orange_request" href="#" style=""  id="request" data-grid="<?= $data['venue_details']['venue_id']; ?>">Request a Loop</a>
                            <input type="hidden" id="venue_req_id" value="<?= $data['venue_details']['venue_id']; ?>"/>
                        <?php } ?>
                    </div>
                </div>

            </div>

        </div>  
    </div> <!-- END list-detail-container -->
</div>  
