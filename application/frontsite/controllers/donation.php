<?php

/*
 * @category   Categories
 * @package    Add/Update/Delete Categories
 * @author     Jignesh Virani <jignesh@creolestudios.com>
 * @author     Another Author <another@example.com>
 * @copyright  2014 Creole Studios
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    Release: 1.0 
 */


if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Donation extends CI_Controller {
    
    public function __construct() {
        parent::__construct();

       
        $this->load->model('landing_model');
        
    }

    //#################################################################
    // Name : index
    // Purpose : To fetch and display contact us message 
    // In Params :  Name,Email,Message
    // Out params : contact successfully view
    //#################################################################

    public function index() {

        $this->load->view('donation/donation_view');
    }
    
     //#################################################################
    // Name : thankForDonation
    // Purpose : thank for donation
    // In Params :  void
    // Out params : message.
    //#################################################################
    
    
    public function thankForDonation()
    {
        $getdata = $this->input->get();
        $this->landing_model->insertDonationData($getdata);
        $this->load->view('donation/thank_view');
    }
    
    //#################################################################
    // Name : cancelDonation
    // Purpose : To redirect cancel message.
    // In Params :  Name,Email,Message
    // Out params : contact successfully view
    //#################################################################
    
    public function cancelDonation(){
        
        $this->load->view('donation/cancel_view');
    }
    
}