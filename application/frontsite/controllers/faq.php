<?php

/*
 * @category  faq
 * @package    Add/Update/Delete Categories
 * @author     Jignesh Virani <jignesh@creolestudios.com>
 * @author     Another Author <another@example.com>
 * @copyright  2014 Creole Studios
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    Release: 1.0 
 */


if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class faq extends CI_Controller {
    
    
     //#################################################################
    // Name : index
    // Purpose : To fetch and display contact us message 
    // In Params :  Name,Email,Message
    // Out params : contact successfully view
    //#################################################################

    public function index() {

        $this->load->view('faq/faq_view');
    }
    

}