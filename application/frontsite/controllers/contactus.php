<?php

/*
 * @category   Categories
 * @package    Add/Update/Delete Categories
 * @author     Jignesh Virani <jignesh@creolestudios.com>
 * @author     Another Author <another@example.com>
 * @copyright  2014 Creole Studios
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    Release: 1.0 
 */


if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contactus extends CI_Controller {

    //#################################################################
    // Name : index
    // Purpose : To fetch and display contact us message 
    // In Params :  Name,Email,Message
    // Out params : contact successfully view
    //#################################################################

    public function index() {

        $this->load->view('contactus/contactus_view');
    }

    //#################################################################
    // Name : getContactDetail
    // Purpose : To get contact us detail in save the data 
    // In Params :  Name,Email,Message
    // Out params : contact successfully view
    //#################################################################

    public function getContactDetail() {
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $message = $this->input->post('message');
        
        if (isset($name) && !empty($name) && isset($email) && !empty($email) && isset($message) && !empty($message)) {

            ## send invoice email
            //email variable declaration
            $subject = 'LoopFinder ContactUs';
            $data = array(
                'name' => $name,
                'email' => $email,
                'message' => $message
            );

            $html = $this->load->view('email/email_template', $data, true);
            //mprd($html);
            //send email for forgot password
            $this->load->library('email');
            $config['charset'] = 'iso-8859-1';
            $config['wordwrap'] = TRUE;
            $config['mailtype'] = "html";
            $this->email->initialize($config);
            $this->email->set_newline("\r\n");
            $this->email->from($email);
           // $this->email->to('ADMIN_EMAIL');
            $this->email->to('jignesh@creolestudios.com');
            //$this->email->cc(ADMIN_EMAIL);
            $this->email->subject($subject);
            $this->email->message($html);
            $this->email->send();
            if (!$this->email->send()) {

                //$this->session->set_userdata('message_contact', '');
                $this->session->set_flashdata('error', 'Oops...Somthing went wrong.');
            } else {
                //$this->session->set_userdata('message_contact', 'your mail sucessfully send.');
                $this->session->set_flashdata('success', 'Thank you for contacting us, We will get back to you shortly.');
            }
            redirect('contactus/');
        }
        $this->load->view('contactus/contactus_view');
    }

}

/* End of file contactus.php */
/* Location: ./application/otojoy/frontsite/controllers/contactus.php */