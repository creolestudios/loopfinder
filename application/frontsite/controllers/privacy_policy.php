<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Privacy_policy extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');        
    }

    /**
     * [index_post FOR WELCOME MESSAGE]
     * offset[INT] [THIS IS OFFET THAT WILL BE PASSED TO QUERY, to limit 10]
     * @return [type] [description]
     */
    public function index() {

        $ContentInfo = $this->general_model->getContentInfo();
        $AccountInfo['contentdata'] = $ContentInfo['content_data'];

        $ResponseData['view'] = $this->load->view('privacy/privacy_policy', $AccountInfo);
    }

    public function mobile() {
        
        $ContentInfo = $this->general_model->getContentInfo();
        $ContentInfo = array();
        //get content from model
        //load view and assign content.
        $this->load->view('content/policy_mobile',$ContentInfo);
    }
    public function newtoloopbuds()
    {
          $ContentInfo = $this->general_model->getContentInfo();
          $ContentInfo =array();
          $this->load->view('content/newtoloopbuds',$ContentInfo);
    }

}
