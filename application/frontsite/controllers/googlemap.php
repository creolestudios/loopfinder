<?php
//
if (!defined('BASEPATH'))
    exit('No direct script access allowed');




class Googlemap extends CI_Controller {

    public function __construct() {
        parent::__construct();
$this->load->helper(array('form', 'url'));
    }
    
    //#################################################################
    // Name : index
    // Purpose : To fetch and display google map
    // In Params :
    // Out params :google map
    //#################################################################
    public function index_map() {
        $this->load->view('googlemap/google_route_map_view');
    }
    //#################################################################
    // Name : grid_index
    // Purpose : To fetch and display google map
    // In Params :
    // Out params :google map
    //#################################################################
    
     public function index_grid() {
        $this->load->view('googlemap/google_route_grid_view');
    }
    
    
    
    
}















?>