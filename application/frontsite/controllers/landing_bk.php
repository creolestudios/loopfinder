<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 * @landing    landing
 * @package    Add/Update/Delete Categories
 * @author     Jignesh Virani <jignesh@creolestudios.com>
 * @author     Another Author <another@example.com>
 * @copyright  2014 Creole Studios
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    Release: 1.0 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Landing extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('category_model');
        $this->load->model('landing_model');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->helper('cookie');
    }

    //#################################################################
    // Name : index
    // Purpose : To fetch and display categories listing
    // In Params :
    // Out params :categories images,name,pin 
    //#################################################################

    public function index() {

        $data = array();
        $data['result'] = $this->category_model->GetCategoriesIndex($offset = 0);
        $this->load->view('landing/landing_view', $data);
    }
    //#################################################################
    // Name : thankDonate
    // Purpose : To show thank you page on donation.
    // In Params : void
    // Out params : html view.
    //#################################################################

    public function thankDonate() {
        
        $getData['donation_dat']= $this->input->get();
        $getdata = $this->input->get();
       
        $this->landing_model->insertDonationData($getdata);
        $this->load->view('landing/thank_you', $getData);
    }

    //#################################################################
    // Name : getCategories
    // Purpose : To fetch and display categories listing
    // In Params :
    // Out params :categories images,name,pin 
    //#################################################################

    public function getCategories() {
        $offset = $_POST['offset'];
        $data = array();
        $data = $this->category_model->GetCategories($offset);
        
         if (isset($data) && $data['status'] == '1') {
                $ReturnData['data'] = $data['categories_data'];
            } else {
                $ReturnData = "-1";
            }
            //$response = $this->load->view('landing/landing_grid_view', $ReturnData);
            
            echo json_encode($ReturnData);
    }

    //#################################################################
    // Name : getVenuesLocation
    // Purpose : To fetch and display Location listing
    // In Params : address
    // Out params :venues locations, address 
    //#################################################################

    public function getVenuesLocation() {
        $location = $this->input->get('term', TRUE);
        $data['response'] = 'false'; //set default response to false
        //call to function venues location

        $query = $this->landing_model->GetVenuesLocation($location);
    }

    //#################################################################
    // Name : getVenuesName
    // Purpose : To fetch and displayvenues name listing
    // In Params : name
    // Out params :venues  name. 
    //#################################################################

    public function getVenuesName() {
        $CityWithState = $this->input->get('location', TRUE);
        $City = strstr($CityWithState, ',', true);
        $VenueName = $this->input->get('term', TRUE);
        $data['response'] = 'false';
        //call to function get venues name
        $query = $this->landing_model->getVenuesName($VenueName, $City);
    }

    //#################################################################
    // Name : getPlace
    // Purpose : To fetch and display venues place listing
    // In Params : city and place name
    // Out params : city and venue name location. 
    //######    ###########################################################
    public function getPlace() {

        $PostData = $this->input->post();

        if (isset($PostData) && !empty($PostData) > 0) {

            //remove space from post data
            $Params = array_map('trim', $PostData);
            $Params = array_map('urldecode', $Params);

            $Categories = '';
            $VenueName = '';
            $LocationForVenue = '';
            $GetFilterDetails = array();
            $VenueLatLong = array();
            $LoopedDetails = '';

            ## Get category
            if (isset($Params['category_id']) && $Params['category_id'] != '') {
                $Categories = $Params['category_id'];
            }

            ## Get venue location
            if (isset($Params['venue_location']) && $Params['venue_location'] != '') {
                $LocationArray = explode(", ", $Params['venue_location']);
                $LocationForVenue = $LocationArray[0];
            }

            if (isset($Params['venue_name_search']) && $Params['venue_name_search'] != '') {
                $VenueArray = explode(", ", $Params['venue_name_search']);
                $VenueName = $VenueArray[0];
            }
            ## Get venue location and name
            $GetFilterDetails = array('city' => $LocationForVenue, 'venue_name' => $VenueName);

            ## Venue looped details
            if (isset($Params['venue_looped']) && $Params['venue_looped'] != '') {
                $LoopedDetails = $Params['venue_looped'];
            }

            $MapData = $this->landing_model->GetMapData($Categories, $GetFilterDetails, $VenueLatLong, $LoopedDetails);

            if (isset($MapData) && $MapData['status'] == '1') {
                $ReturnData = $MapData['map_data'];
            } else {
                $ReturnData = "";
            }

            echo json_encode($ReturnData);
            exit;
        }

        ##intialize
        $LocationForVenue = '';
        $VenueName = '';
        $category_id = '';
        $GetFilterDetails = array();
        $venue_lat_long = array();
        $VenueNameFromUrl = '';
        $CityFromUrl = '';
        $latFromUrl = '';
        $longFromUrl = '';
        $category_id2='';
        ## Get category from url
        $keyword = $this->uri->segment(3);
        $category_id2 = $this->uri->segment(4);
        ## process keyword to get category,city,venue name
        if (isset($keyword) && $keyword == 'category') {
            //$category_id = base64_decode($this->uri->segment(4));
            $category_id = $this->uri->segment(4);
            $PlaceData['category_id'] = $category_id;
           
            ## set venue and city blank
            $GetFilterDetails = array('city' => '', 'venue_name' => '');
        } else if (isset($keyword) && $keyword == 'city') {

            ## get city from url
            $CityFromUrl = urldecode($this->uri->segment(4));
            $CityArray = explode(", ", $CityFromUrl);

            ## check if venue name is also searched
            $VenueNameFromUrl = urldecode($this->uri->segment(5));
            if (isset($VenueNameFromUrl) && $VenueNameFromUrl == 'venue') {
                $VenueNameFromUrl = urldecode($this->uri->segment(6));
                $VenueArray = explode(", ", $VenueNameFromUrl);
                //$GetFilterDetails = array('city' => $CityArray[0], 'venue_name' => $VenueArray[0]);
            } else {
                //$GetFilterDetails = array('city' => $CityArray[0], 'venue_name' => '');
            }
           
        } else if (isset($keyword) && $keyword == 'venue') {
            ## get venue name from url
            $VenueNameFromUrl = urldecode($this->uri->segment(4));
            $VenueArray = explode(", ", $VenueNameFromUrl);
            $GetFilterDetails = array('city' => '', 'venue_name' => $VenueArray[0]);
            $venue_lat_long = '';
        } else if (isset($keyword) && $keyword == 'lat') {

            $latFromUrl = $this->uri->segment(4);
            $longFromUrl = $this->uri->segment(6);
            $venue_lat_long = array('lat' => $latFromUrl, 'long' => $longFromUrl);
            //mprd($venue_lat_long);
        }
        ## call function to get venues categories
        $PlaceData['Categories'] = $this->landing_model->getVenuesCategories();

        $MapData = $this->landing_model->GetMapData($category_id, $GetFilterDetails, $venue_lat_long);

        if (isset($MapData) && $MapData['status'] == '1') {
            $PlaceData['marker_data'] = $MapData['map_data'];
        } else {
            $PlaceData['marker_data'] = array();
        }
        ## for get all map data.
         $AllMapData = $this->landing_model->GetMapData($category_id='', $GetFilterDetails='', $venue_lat_long="");

        if (isset($AllMapData) && $AllMapData['status'] == '1') {
            $PlaceData['all_marker_data'] = $AllMapData['map_data'];
        } else {
            $PlaceData['all_marker_data'] = array();
        }
        
        //else{  $PlaceData['all_marker_data'] = array();  }
        
        ## pass data in view
        $PlaceData['LocationForVenue'] = $CityFromUrl;
        $PlaceData['VenueName'] = $VenueNameFromUrl;
        $PlaceData['category_id'] = $category_id2;
        $PlaceData['lat'] = $latFromUrl;
        $PlaceData['long'] = $longFromUrl;
      
        ## Generate response
        $response = $this->load->view('landing/landing_search', $PlaceData);
    }
    //#################################################################
    // Name : getChapter
    // Purpose : To fetch and display chapters in map view.
    // In Params : chapter id,looped chapter, chapter location, chapter name
    // Out params : chapter data.
    //######    ###########################################################
    
    public function getChapter()
    {
        $PlaceData['category_id'] = "";
        $PlaceData['LocationForVenue']="";
        $PlaceData['VenueName']="";
        $PlaceData['lat']="";
        $PlaceData['long']="";
        ## call function to get venues categories
        $PlaceData['Categories'] = $this->landing_model->getVenuesCategories();
        ## for get all map data.
         $AllChapterData = $this->landing_model->GetChapterData();

        if (isset($AllChapterData) && $AllChapterData['status'] == '1') {
            $PlaceData['all_chapter_data'] = $AllChapterData['chapter_map_data'];
        } else {
            $PlaceData['all_chapter_data'] = array();
        }
          ## Generate response
          $response = $this->load->view('landing/landing_search',$PlaceData);
    }

    //#################################################################
    // Name : VenueGridView
    // Purpose : To fetch and display venues in grid view.
    // In Params : venue id,looped venue, venue location, venue name
    // Out params : venues data.
    //######    ###########################################################
    public function VenueGridView() {

        $PostData = $this->input->post();

        if (isset($PostData) && !empty($PostData) > 0) {

            //remove space from post data
            $Params = array_map('trim', $PostData);
            $Params = array_map('urldecode', $Params);
            $Categories = '';
            $VenueName = '';
            $LocationForVenue = '';
            $GetFilterDetails = array();
            $VenueLatLong = array();
            $LoopedDetails = '';
            $offset = 0;
            
          
            ## Get category
            if (isset($Params['category_id']) && $Params['category_id'] != '') {
                $Categories = $Params['category_id'];
            }

            ## Get venue location
            if (isset($Params['venue_location']) && $Params['venue_location'] != '') {
                $LocationArray = explode(", ", $Params['venue_location']);
                $LocationForVenue = $LocationArray[0];
            }

            if (isset($Params['venue_name_search']) && $Params['venue_name_search'] != '') {
                $VenueArray = explode(", ", $Params['venue_name_search']);
                $VenueName = $VenueArray[0];
            }
            ## Get venue location and name
            $GetFilterDetails = array('city' => $LocationForVenue, 'venue_name' => $VenueName);

            ## Venue looped details
            if (isset($Params['venue_looped']) && $Params['venue_looped'] != '') {
                $LoopedDetails = $Params['venue_looped'];
            }
            ## Get venue lat and long.
            if (isset($Params['venue_lat']) && !empty($Params['venue_lat'])) {
                $VenueLatLong = array('lat' => $Params['venue_lat'], 'long' => $Params['venue_long']);
            }
            if (isset($Params['offset']) && !empty($Params['offset'])) {
                $offset = $Params['offset'];
                
            }
            $MapData = $this->landing_model->GetVenueData($Categories, $GetFilterDetails, $VenueLatLong, $LoopedDetails, $offset);

            if (isset($MapData) && $MapData['status'] == '1') {
                $ReturnData['map_data'] = $MapData['map_data'];
                $ReturnData['total_venue'] = $MapData['total_venues'];
            } else {
                $ReturnData = "-1";
            }
            //$response = $this->load->view('landing/landing_grid_view', $ReturnData);
            echo json_encode($ReturnData);
            // echo $response;
            exit;
        }

        ##intialize
        $LocationForVenue = '';
        $VenueName = '';
        $category_id = '';
        $GetFilterDetails = array();
        $venue_lat_long = array();
        $VenueNameFromUrl = '';
        $CityFromUrl = '';
        $latFromUrl = '';
        $longFromUrl = '';

        ## Get category from url
        $keyword = $this->uri->segment(3);

        ## process keyword to get category,city,venue name
        if (isset($keyword) && $keyword == 'category') {
            $category_id = base64_decode($this->uri->segment(4));
            $PlaceData['category_id'] = $category_id;

            ## set venue and city blank
            $GetFilterDetails = array('city' => '', 'venue_name' => '');
        } else if (isset($keyword) && $keyword == 'city') {

            ## get city from url
            $CityFromUrl = urldecode($this->uri->segment(4));
            $CityArray = explode(", ", $CityFromUrl);

            ## check if venue name is also searched
            $VenueNameFromUrl = urldecode($this->uri->segment(5));
            if (isset($VenueNameFromUrl) && $VenueNameFromUrl == 'venue') {
                $VenueNameFromUrl = urldecode($this->uri->segment(6));
                $VenueArray = explode(", ", $VenueNameFromUrl);
                $GetFilterDetails = array('city' => $CityArray[0], 'venue_name' => $VenueArray[0]);
            } else {
                $GetFilterDetails = array('city' => $CityArray[0], 'venue_name' => '');
            }
        } else if (isset($keyword) && $keyword == 'venue') {
            ## get venue name from url
            $VenueNameFromUrl = urldecode($this->uri->segment(4));
            $VenueArray = explode(", ", $VenueNameFromUrl);
            $GetFilterDetails = array('city' => '', 'venue_name' => $VenueArray[0]);
        } else if (isset($keyword) && $keyword == 'lat') {

            $latFromUrl = $this->uri->segment(4);
            $longFromUrl = $this->uri->segment(6);
            $venue_lat_long = array('lat' => $latFromUrl, 'long' => $longFromUrl);
        }

        ## call function to get venues categories
        $PlaceData['Categories'] = $this->landing_model->getVenuesCategories();

        $MapData = $this->landing_model->GetVenueData($category_id, $GetFilterDetails, $venue_lat_long, $offset = 0);

        if (isset($MapData) && $MapData['status'] == '1') {
            $PlaceData = $MapData['map_data'];
        } else {
            $PlaceData = "";
        }
     
        ## pass data in view
        $PlaceData['LocationForVenue'] = $CityFromUrl;
        $PlaceData['VenueName'] = $VenueNameFromUrl;
        $PlaceData['category_id'] = $category_id;
        $PlaceData['lat'] = $latFromUrl;    
        $PlaceData['long'] = $longFromUrl;


        ## Generate response
        $response = $this->load->view('landing/landing_grid_view', $PlaceData);
        echo json_encode($PlaceData);
    }

    //#################################################################
    // Name : VenueDetailsForMap
    // Purpose : To fetch and display venues detail
    // In Params : venue id
    // Out params : city and venue name location. 
    //######    ###########################################################
    public function VenueDetailsForMap() {

        $PostData = $this->input->post();

        if (isset($PostData) && !empty($PostData) > 0) {

            //remove space from post data
            $Params = array_map('trim', $PostData);
            $Params = array_map('urldecode', $Params);

            $VenueData = $this->landing_model->getVenueDetails($Params['id'], $Params['c_id']);
            //generate response
            if (isset($VenueData) && $VenueData['status'] == 1) {

                $ResponseData = $VenueData['venue_details'];
                $ResponseData['venue_images'] = $VenueData['venue_images'];
                $ResponseData['looped_rooms'] = $VenueData['looped_rooms'];
                $ResponseData['category_id'] = $VenueData['category_id'];
            } else {

                $ResponseData['success'] = "0";
                $ResponseData['message'] = $this->lang->line('NO_VENUES');
            }
        } else {

            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->lang->line('INVALID_PARAMS');
        }
        echo json_encode($ResponseData);
    }

    //#################################################################
    // Name : getPlaceByCategories
    // Purpose : To fetch and display venues place listing
    // In Params : city and place name
    // Out params : city and venue name location. 
    //######    ###########################################################

    public function getPlaceByCategories() {
        // collect categories filter data.
        $placer['data']['LocationForVenue'] = '';
        $placer['data']['VenueName'] = '';
        $Categories = $this->input->post('id', TRUE);
        $LocationForVenueTemp = $this->input->post('venue_location', true);
        $LocationForVenue = strstr($LocationForVenueTemp, ', ', true);

        $VenueNameWithCity = $this->input->post('venue_name_search', TRUE);
        $VenueName = strstr($VenueNameWithCity, ', ', true);

        $data = array('city' => $LocationForVenue, 'venue_name' => $VenueName);

        $VenueLooped = $this->input->post('venue_looped', TRUE);
        // $data= "";
        // $VenueLooped= "";
        $PlaceData = array();
        // call function to get venues by categories
        $lat = $this->input->post('Latitude', TRUE);
        $lng = $this->input->post('Longitude', TRUE);
        $datalat_lng = array('Latitude' => $lat, 'Longitude' => $lng);
        //$PlaceData['data']= $this->landing_model->getLocationsByCategories($Categories ,$data, $VenueLooped);
        $placer = $this->landing_model->getLocationsByCategories($Categories, $data, $VenueLooped, $datalat_lng);
        // encode data
        $result = json_decode(json_encode($placer), true);
        if (count($result) > 0) {

            $returnData = array();
            foreach ($result as $key => $value) {

                if ($key = 'rooms') {
                    $rooms['rooms'] = explode('^', $value['rooms']);
                    $retunData = array_merge($value, $rooms);
                    //var_dump($retunData);exit;
                }
                if ($key = 'last_verified') {
                    $roomvarified['last_verified'] = explode('^', $value['last_verified']);
                    $returnData1 = array_merge($retunData, $roomvarified);
                }
                if ($key = 'images') {
                    $images['images'] = explode('^', $value['images']);
                    $VenuesOnMap1 = array_merge($returnData1, $images);
                }
                //$VenuesOnMap[]['LocationForVenue']= 'sdsdssd';
                //$VenuesOnMap[]['VenueName']= '';
                $VenuesOnMap[]['data'] = $VenuesOnMap1;
            }
            $LocationOnMap = json_encode($VenuesOnMap);
            echo $LocationOnMap;
        } else {
            echo "-1";
        }
        // return 
    }

    //#################################################################
    // Name : getVenueDeatil
    // Purpose : To fetch and displayvenues venue listing
    // In Params : venue id
    // Out params : venue city,address,location,email,phone number,is certified etc. 
    //#################################################################

    public function getVenueDetail() {

        $cat_id_in = '';
        $id = $this->input->post('id');
        $venue_detail_notch_class = $this->input->post('venue_detail_notch_class');
        $cat_id = $this->input->post('cat_id');
        if (isset($cat_id) && !empty($cat_id)) {

            $cat_id_in = $this->input->post('cat_id');
        }
        $data = array();
        // call to function get venues details
        $VenueDetail['data'] = $this->landing_model->getVenueDetails($id, $cat_id_in);
        $VenueDetail['venue_detail_notch_class'] = $venue_detail_notch_class;
        $Response = $this->load->view('landing/landing_gridview_feedback', $VenueDetail);

        echo $Response;
    }

    //#################################################################
    // Name : getVenueInGridview
    // Purpose : To fetch and displayvenues venue listing
    // In Params : venue id
    // Out params : venue city, address,location,email, phone number,is certified etc. 
    //#################################################################

    public function getVenueInGridview() {



        $requestUrlValue = $_SERVER['REQUEST_URI'];
        $getDataId = substr($requestUrlValue, 45);

        //store value of city and venue name
        $StoreCityGrid = $this->input->post('city');
        $StoreNameGrid = $this->input->post('VenueName');

        // call to function venue in grid.
        $CityName = array('city' => $StoreCityGrid, 'VenueName' => $StoreNameGrid);

        $result['Categories'] = $this->landing_model->getVenuesCategories();

        $result['data'] = $this->landing_model->getVenueInGrid($getDataId, $CityName);

        $this->load->view('landing/landing_grid_view', $result);
    }

    //#################################################################
    // Name : getFeedback
    // Purpose : To fetch and display venues feddback
    // In Params : venues id
    // Out params : venue feedback view(as per venue details)
    //#################################################################

    public function getFeedback() {
        $id = $this->input->post('id');

        $data = array();
        $GetVenueFeedback['result'] = $this->landing_model->getVenueFeedback($id);
        $response = $this->load->view('landing/landing_feedback', $GetVenueFeedback, TRUE);
        echo $response;
    }

    //#################################################################
    // Name : getFeedbackSave
    // Purpose : To fetch and display venues feddback
    // In Params : venues id
    // Out params : venue feedback view(as per venue details)
    //#################################################################

    public function getFeedbackSave() {

        $name = $this->input->post('Name', TRUE);
        $email = $this->input->post('Email', TRUE);
        $reasons = $this->input->post('Reasons', TRUE);
        $LoopRoomId = $this->input->post('VenueName', TRUE);
        $LoopWorking = $this->input->post('WorkWell', TRUE);
        $comment = $this->input->post('comment', TRUE);
        $SavedData = array('name' => $name, 'email' => $email, 'loop_not_working_reason' => $reasons, 'loop_room_id' => $LoopRoomId, 'loop_working' => $LoopWorking, 'comment' => $comment);

        $GetResponse = $this->landing_model->getFeedbackSave($SavedData);
        echo $GetResponse;
    }

    //#################################################################
    // Name : getFeedbackSave
    // Purpose : To fetch and display venues feddback
    // In Params : venues id
    // Out params : venue feedback view(as per venue details)
    //#################################################################
    public function getVote() {

        $email = $this->input->post('email', TRUE);
        $venue_id = $this->input->post('venue', TRUE);
        $grid_id = $this->input->post('grid_venue', true);
        $name = $this->input->post('name', TRUE);
        if ($venue_id != "") {
            $venueGridId = $venue_id;
        }
        if ($grid_id != "") {
            $venueGridId = $grid_id;
        }
        $dataRequest = array('id' => $venueGridId, 'email' => $email, 'name' => $name);

        $GetResponse = $this->landing_model->getRequestSave($dataRequest);
        echo $GetResponse;
    }

    public function lol() {
        $this->load->view('landing/new_landing');
    }

    //#################################################################
    // Name : getlanlng
    // Purpose : To fetch and display venues feddback
    // In Params : venues latitude, venues longitude
    // Out params : venue feedback view(as per venue details)
    //#################################################################

    public function getlanlng() {

        $lat = $this->input->post('Latitude', TRUE);
        $lng = $this->input->post('Longitude', TRUE);
        $datalat_lng = array('Latitude' => $lat, 'Longitude' => $lng);

        $response = $this->landing_model->getcity($datalat_lng);
    }

}

/* End of file landing.php */
/* Location: ./application/otojoy/controllers/landing.php */
?>