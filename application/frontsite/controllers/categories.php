<?php

/*
 * @category   Categories
 * @package    Add/Update/Delete Categories
 * @author     Jignesh Virani <bhargav@creolestudios.com>
 * @author     Another Author <another@example.com>
 * @copyright  2014 Creole Studios
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    Release: 1.0 
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Categories extends CI_Controller {

    var $viewData = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('category_model');
        $this->load->library('Datatables');
        $this->load->helper(array('form', 'url'));
       
    }
}

/* End of file categories.php */
/* Location: ./application/otojoy/frontsite/controllers/categories.php */
