<?php

/*
 * @category   Categories
 * @package    Add/Update/Delete Categories
 * @author     Jignesh Virani <jignesh@creolestudios.com>
 * @author     Another Author <another@example.com>
 * @copyright  2014 Creole Studios
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    Release: 1.0 
 */


if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Requestloop extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('request_model');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('form');
    }

    //#################################################################
    // Name : index
    // Purpose : To fetch and display contact us message 
    // In Params :  Name of venue, City, State, Comments etc..
    // Out params : request send successfully view
    //#################################################################

    public function index() {

        $this->load->view('request_loop/request_view');
    }

    //#################################################################
    // Name : RequestLoopSave
    // Purpose : to save a request loop 
    // In Params :  Name of venue, City, State, Comments etc..
    // Out params : void
    //#################################################################

    public function RequestLoopSave() {
        $requestData = $this->input->post();

        if (isset($requestData) && count($requestData) > 0) {
            $response = $this->request_model->saveRequestLoop($requestData);
            //after successfully save data load view.

            if (isset($response) && $response != '') {
                $this->session->set_flashdata('success', 'You have successfully requested a loop.');
            } else {
                $this->session->set_flashdata('error', 'Oops...Somthing went wrong.');
            }

            redirect('requestloop/');
        }
        $this->load->view('request_loop/request_view');
    }

}

/* End of file requestloop.php */
/* Location: ./application/otojoy/controllers/requestloop.php */
?>