<?php
$lang['LBL'] = 'label';
//#########################################################################
$lang['NO_CHANGES_MADE'] = 'You have made no changes.';
//###################### Customer section #################################
$lang['STATUS_CHANGE_ACTIVE'] = 'Status is changed to active.';
$lang['STATUS_CHANGE_INACTIVE'] = 'Status is changed to inactive.';
$lang['SUCCESS_CUSTOMER_DELETE'] = 'Customer deleted successfully.';
$lang['CUSTOMER_UPDATE_SUCCESS'] = 'Customer details updated successfully';
$lang['SUCCESS_FORGOT_PASSWORD'] = 'Password has been reset. New password has been emailed to the user.';
$lang['ERROR_FORGOT_PASSWORD'] = 'Oops! Something just went wrong. Please try again later.';

//###################### Download section #################################
$lang['SUCCESS_DOWNLOAD_DELETE'] = 'Download has been deleted.';

//###################### Album section #################################
$lang['SUCCESS_ALBUM_DELETE'] = 'Album deleted successfully.';
$lang['ALBUM_NAME_EXIST'] = 'Album with that name already exists.';
$lang['ALBUM_SUCCESS_CREATE'] = 'Album created successfully.';
$lang['ALBUM_SUCCESS_UPDATE'] = 'Album updated successfully.';
$lang['SUCCESS_ALBUM_IMAGE_DELETE'] = 'Image removed from the album.';

$lang['SUCCESS_NEWS_UPDATE'] = 'News image updated successfully.';
$lang['SUCCESS_NEWS_IMAGE_UPDATE'] = 'News image updated successfully.';