<?php

//################## General message ##############################/
$lang['INVALID_PARAMS'] = 'Oops! Something just went wrong. Please try again later.';
$lang['WELCOME_MESSAGE'] = 'Welcome to LoopFinder!';
$lang['GENERAL_ERROR'] = 'Oops! Something just went wrong. Please try again later.';
$lang['GENERAL_SUCCESS'] = 'Done!';
$lang['GENERAL_NO_CHANGES'] = 'You have made no changes.';
//################# User message ################################/
$lang['EMAIL_ALREADY_EXIST'] = 'Sorry, it looks like {EMAIL} belongs to an existing account.';
$lang['REGISTER_SUCCESS'] = 'Account created successfully.';
$lang['EMAIL_NOT_FOUND'] = 'Email address not found.';
$lang['LOGIN_ERROR'] = 'Invalid email address or password.';
$lang['LOGIN_SUCCESS'] = 'Authentication completed successfully.';
$lang['FORGOT_PASSWORD_SUCCESS'] =  'An email has been sent to you. Please check your inbox.';
$lang['CONTACT_SUCCESS'] =  'Thank you for your inquiry. We will get back to you shortly.';
$lang['FORGOT_PASSWORD_ERROR'] =  'The email address you entered is incorrect.';
$lang['CUSTOMER_NOT_FOUND'] = 'User not found.';
$lang['DETAIL_UPDATE_SUCCESS'] = 'Account details updated successfully.';
$lang['OLD_PASSWORD_REQUIRED'] = 'Please enter your old password.';
$lang['INVALID_OLD_PASSWORD'] = 'Your old password is incorrect.';
$lang['SUCCESS_PASSWORD_CHANGE'] = 'Password updated successfully.';
$lang['SAME_PASSWORD'] = 'You cannot set the current password as the new password. Please select a different password.';
$lang['USER_ALREADY_SUBSCRIBE'] = 'You already have the 1 year pass.';
$lang['SUCCESS_SUBSCRIBE'] = '1 year pass added to your account successfully.';
$lang['SUCCESS_SUBSCRIBE_EXTEND'] = '1 year pass extended for your account successfully.';
$lang['SUCCESS_SUBSCRIBE_RENEW'] = '1 year pass renewed for your account successfully.';
$lang['REMOVE_SUBSCRIBE'] = 'Your subscription has expired.';
$lang['ACCOUNT_INACTIVE'] = 'Your account has been disabled.';
$lang['INVALID_USER_ID'] = 'User not found.';
$lang['NO_ZIPCODE_DETAIS'] = 'No details avaialble for provided ZIP code.';
//################# Settings ################################/
$lang['NO_SETTINGS'] = 'No settings available.';
$lang['SETTINGS_UPDATE_SUCCESS'] = 'Settings updated successfully.';

//################# Venues ################################/
$lang['SUCCESS_CHECKIN'] = 'Done!';
$lang['SUCCESS_VOTES'] = 'Done!';
$lang['SUCCESS_REQUEST'] = 'Done!';
$lang['SUCCESS_FEEDBACK'] = 'Done!';

//################# History ################################/
$lang['NO_CHECKIN'] = 'No check-ins available.';
$lang['NO_NOTIFICATION'] = 'No notifications available.';
$lang['NO_VENUES'] = 'No venues available.';
$lang['NO_CITY_NAME'] = 'Oops! No suggestions found. Try a different search.';


//################# History ################################/
$lang['VENUE_NEAR_BY'] = 'You are near a loop enabled venue.';

##############   chapters  message ################

$lang['NO_CHAPTERS'] = 'No chapters available.';





?>