<?php
##############################################################################################
									//Body
##############################################################################################
$html	='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
       <title>Document</title>
</head>
<body>
    <table width="98%" cellspacing="0" cellpadding="0" border="0">
        <tbody>
            <tr>
                <td style="font-family:verdana;font-size:16px;line-height:1.3em;text-align:center;padding:15px;background: #265d9d;">
                    <p><img src="'.IMAGE_URL.'logo.png'.'"/></p>
                </td>
            </tr>
            <tr style="background:#E6E6E6;">
                <td valign="top" align="left" style="font-family:verdana;font-size:16px;line-height:1.3em;text-align:left;padding:15px;">
                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tr style="background:#F5F5F5;border-radius:5px;">
                            <td style="font-family:verdana;font-size:13px;line-height:1.3em;text-align:left;padding:15px;">
                                <h1 style="font-family:verdana;color:#424242;font-size:14px;line-height:normal;letter-spacing:-1px;">    
                                Hello there!
                                </h1>
                                
                                <p style="color:#ADADAD">
                                    '.$customer_name.' has sent you an e-card from the NYonAir iPhone App.
                                </p>
                                
                                <p style="color:#ADADAD">
                                    You can download the free app from the link below:
                                </p>
                                <p>
                                    <a href="https://itunes.apple.com/us/app/nyonair/id894082340?mt=8"><img src="'.IMAGE_URL.'app_store_logo.png'.'" style="max-width:150px"/></a>
                                </p>
                                <hr style="margin-top:30px;border-top:#ccc 1px solid;" />
                                <p>
                                    Thanks &amp; Regards,
                                    <br/>
                                    NYonAir Team
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>';

echo $html;
?>