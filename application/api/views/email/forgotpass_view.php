<?php

##############################################################################################
//Body
##############################################################################################
$html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
       <title>Document</title>
</head>
<body>
    <table width="98%" cellspacing="0" cellpadding="0" border="0">
        <tbody>
            <tr>
                <td style="font-family:verdana;font-size:16px;text-align:center;background: #0095D7;">
                    <p><img src="' . IMAGE_URL . 'logo_inner_hori.png' . '"/></p>
                </td>
            </tr>
            <tr style="background:#E6E6E6;">
                <td valign="top" align="left" style="font-family:verdana;font-size:16px;line-height:1.3em;text-align:left;padding:15px;">
                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tr style="background:#F5F5F5;border-radius:5px;">
                            <td style="font-family:verdana;font-size:13px;line-height:1.3em;text-align:left;padding:15px;">
                                <h1 style="font-family:verdana;color:#333;font-size:14px;line-height:normal;letter-spacing:-1px;">    
                                                                    Dear User,
                                </h1>
                                <p style="color:#333">
                                    We have resent your password.
                                </p>
                                <p style="color:#333"> 
                                    
                                    We have reset your password. Your new password is: <b style="color:#333;">' . $password . '</b>.                                    
                                </p>                                
                                <p style="color:#333">
                                    Please log into your account and update it.
                                </p>
                                <hr style="margin-top:15px;border-top:#0095D7 1px solid;" />
                                <p>
                                    Thank you!
                                    <br/>
                                    Your LoopFinder Team.
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>';

echo $html;
?>