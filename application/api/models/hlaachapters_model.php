<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Hlaachapters_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        
        $this->hlaachapters = 'hlaa';
    }
    
    //#################################################################
    // Name : GetAllChapters
    // Purpose : To select venue detail
    // In Params : customer id, venue id
    // Out params : venue detail
    //#################################################################    

    public function GetAllChapters($Params) {

       // global declaration.
        $ReturnData = array();
        //extract params.
        extract($Params);

        //set the limit
        $Limit = CHAPTER_PAGE_LIMIT;

        //set venue image path
        $chapterImagePath = UPLOADS_URL . 'venues/';
       // $DefaultVenueImage = UPLOADS_URL . 'default_venue.png';

        //define categories path
        $CategoryPinImagePath = UPLOADS_URL . 'category_icons/';
        $DefaultPinImage = UPLOADS_URL . 'map-pin-default.png';
        $CategoriesImage = UPLOADS_URL . 'category_image/';

        // select venue details in venues table
        $this->db->select(" SQL_CALC_FOUND_ROWS v.id as chapter_id, CONCAT('" . $CategoriesImage . "',c.image) as category_image, GROUP_CONCAT(distinct c.name SEPARATOR ', ') as category_name,v.name as chapter_name,v.address,v.city,v.state,if(v.zipcode != '',LPAD(v.zipcode, 5, '0'),v.zipcode)as zipcode,IF(v.venue_image = '','',CONCAT('" . $chapterImagePath . "',v.id,'/', v.venue_image)) as cover_image, v.latitude,v.longitude,TRUNCATE(( 3959 * acos ( cos ( radians('" . $latitude . "') ) * cos( radians( v.latitude ) ) * cos( radians( v.longitude ) - radians('" . $longitude . "') ) + sin ( radians('" . $latitude . "') ) * sin( radians( v.latitude ) ) ) ),2) AS `distance`", false);
        $this->db->from('venues as v');
        $this->db->join("categories as c", "c.id = 21", "left");

        //apply filter to the venue list
        ## Categories filter
        if (isset($category_id) && $category_id != '') {
          
            $this->db->where('vc.category_id != 21');
        }

//        ## Venue name filter
//        if (isset($venue_name) && $venue_name != '') {
//            //$this->db->like('v.name', $venue_name, 'both');
//            $this->db->where("REPLACE(v.name,'''','') like  '%{$venue_name}%'");
//            $this->db->or_where("REPLACE(v.name,'/','') like  '%{$venue_name}%'");
//            $this->db->or_where("REPLACE(v.name,'-','') like  '%{$venue_name}%'");
//        }

        ## Location(city) filter for venue
//        if (isset($location) && $location != '') {
//
//            $locationTemp = explode(', ', $location);
//            $location = $locationTemp[0];
//            //$this->db->like('v.city', $location, 'both');
//            $this->db->where("REPLACE(v.city,'''','') like  '{$location}%'");
//            $this->db->where("REPLACE(v.city,'/','') like  '{$location}%'");
//            $this->db->where("REPLACE(v.city,'-','') like  '{$location}%'");
//        }

         $this->db->where("v.venue_type","3");
        ## pagination settings
        if (isset($offset) && $offset == '-1') {
            //$this->db->where("v.id < $offset");
            $offset = 0;
        }

        ## apply distance filter
        if (isset($distance) && $distance != '') {
            $this->db->having("TRUNCATE(( 3959 * acos ( cos ( radians('" . $latitude . "') ) * cos( radians( v.latitude ) ) * cos( radians( v.longitude ) - radians('" . $longitude . "') ) + sin ( radians('" . $latitude . "') ) * sin( radians( v.latitude ) ) ) ), 2) <= ", $distance);
        }

        $this->db->group_by("v.id");
        if (isset($sort_by) && $sort_by == '1') {
            $this->db->order_by('distance', 'asc');
        }

        $this->db->limit($Limit, $offset);
        $getChaptersList = $this->db->get();
       // echo $this->db->last_query();exit;
        ## get total row count
        $TotalRows = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

        if ($getChaptersList->num_rows() > 0) {
            //fetch the data
            $ChapterData = $getChaptersList->result_array();

            //prepare for response
            $ReturnData['status'] = '1';
            $ReturnData['total_chapters'] = $TotalRows;
            $ReturnData['chapters'] = $ChapterData;
        } else {
            $ReturnData['status'] = '0';
        }
        return $ReturnData;
    }
    
    //#################################################################
    // Name : ChaptersDetails
    // Purpose : To select chapters detail
    // In Params : customer id, chapter id
    // Out params : chapters detail
    //#################################################################
    
    public function ChaptersDetails($Params){
         //extract params.
        extract($Params);

        // global declaration.
        $ReturnData = array();
        

        $chapterImagePath = UPLOADS_URL . 'venues/';

        $CategoryPinImagePath = UPLOADS_URL . 'category_icons/';
        $DefaultPinImage = UPLOADS_URL . 'map-pin-default.png';
        $CategoriesImage = UPLOADS_URL . 'category_image/';

        ## Get Venue data
        $this->db->select("v.id as chapter_id,v.name as chapter_name,IF(v.hlaa_image = '','',CONCAT('" . $chapterImagePath . "','" . $chapter_id . "','/', v.hlaa_image)) as hlaa_image,v.address,v.city,v.state,if(v.zipcode != '',LPAD(v.zipcode, 5, '0'),v.zipcode)as zipcode,v.latitude,v.longitude,v.email,v.phone,v.website,v.public_comment as additional_information", false);
        $this->db->from('venues as v');
        $this->db->where("v.id", $chapter_id);
        $this->db->group_by("v.id");
        $getChapterDetails = $this->db->get();


        ## venue details is found then fetch looped rooms and venue images
        if ($getChapterDetails->num_rows > 0) {

            ##Fetch vennue data
            $chapterDetails = $getChapterDetails->result_array();

            ##Fetch venue images
            $this->db->select(" (CONCAT('" . $chapterImagePath . "',hlaa_images.hlaa_id,'/', image)) as image", false);
            $this->db->from('hlaa_images');
            $this->db->where_in('hlaa_images.hlaa_id', $chapter_id);
            $getChaptersImages = $this->db->get();

            //process image data
            if ($getChaptersImages->num_rows > 0) {
                //fetch the data
                $chapterImages = $getChaptersImages->result_array();
            }

            ## generate output
            $ReturnData['status'] = '1';
            $ReturnData['chapter_details'] = $chapterDetails[0];
            ##Image data
            $ReturnData['chapter_images'] = $chapterImages;
           
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
        
        
    }
}