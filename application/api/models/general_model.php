<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class General_model extends CI_Model {
	var $device_token;
	var $category;
	public function __construct()
	{
		parent::__construct();		
		$this->device_token='device_token';
		$this->category='category';
	}
	function addDeviceTokkenSession($user_id,$device){
		$data = array(
					  'user_id'	=> $user_id,
					  'vDeviceTokken'	=> $device
					);
		$query = $this->db->insert($this->device_token,$data);
		if($this->db->affected_rows() > 0)
			return $this->db->insert_id();
		else
			return '';
    }
    function getCategories(){
    	$categoryURL=CATEGORY_URL;    	
    	$query=$this->db->query("SELECT category_id,
    		category_name,
    		CASE WHEN category_img!='' THEN CONCAT('$categoryURL',category_img) 
    		WHEN category_img='' THEN ('') END as category_img
    		FROM category WHERE status='Active'");
    	if($query->num_rows() > 0){
    		$res=$query->result_array();
    		return $res;
    	}
    	else{
    		return "";
    	}
    }
}

/* End of file general_model.php */
/* Location: ./application/api/models/general_model.php */