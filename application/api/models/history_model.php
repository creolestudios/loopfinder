<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class History_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->venues = 'venues';
        $this->venue_checkins = 'checkins';
        $this->venue_notification = 'notification';
    }

    //#################################################################
    // Name : GetCheckins
    // Purpose : To get all categories
    // In Params : last category id for paging
    // Out params : all data of categories
    //#################################################################
    public function GetCheckins($PostData) {

        //initialize
        $ReturnData = array();

        //extract params
        extract($PostData);

        //set image path
        $VenueImagePath = UPLOADS_URL . 'venues/';
        $categoryPath = UPLOADS_URL.'category_image/';
        $DefaultImage = UPLOADS_URL . 'default_album.png';
        $DefaultVenueImage = UPLOADS_URL . 'default_venue.png';

        //set limit param
        $Limit = CHECKIN_PAGE_LIMIT;

        //initilize return data
        $ReturnData = array();
        
        if($platform == 1)
        {   $platform_text = 'ios/';}
        else if($platform == 2)
        {   $platform_text = 'android/';}
        else
        {   $platform_text = 'ios/'; }

        //IF(v.venue_image = '','',CONCAT('" . $VenueImagePath .$platform_text "',v.id,'/', v.venue_image)) as venue_image;
        
        
        $this->db->select("v.id as venue_id,c.id as checkin_id, ca.name as category_name,CONCAT('".$categoryPath.$platform_text.'thumb/'."', ca.image) as category_image,c.checkin_date, v.name as venue_name,v.address as venue_address,v.state as venue_state,v.city as venue_city,if(v.zipcode != '',LPAD(v.zipcode, 5, '0'),v.zipcode)as zipcode,IF(v.venue_image = '','',CONCAT('".$VenueImagePath."',v.id,'/','".$platform_text.'thumb/'."', concat('thumb_','',v.venue_image))) as venue_image", false);
        $this->db->from("checkins as c");
        $this->db->join("venues as v", "c.venue_id = v.id", "left");
        $this->db->join("venue_categories as vc", "v.id = vc.venue_id", "left");
        $this->db->join("categories as ca", "ca.id = vc.category_id", "left");
        $this->db->where("c.customer_id", $customer_id);

        if (isset($offset) && $offset != "-1") {
            $this->db->where("c.id < $offset");
        }

        $this->db->order_by('c.id', 'DESC');
        $this->db->limit($Limit);

        //fetch data
        $GetCheckins = $this->db->get();

        if ($GetCheckins->num_rows() > 0) {

            //fetch the data
            $CheckinsData = $GetCheckins->result_array();

            //prepare for response
            $ReturnData['status'] = '1';

            //check for total records,if its less then limit then set offset to -1
            if (isset($CheckinsData) && count($CheckinsData) < $Limit) {
                $ReturnData['offset'] = "-1";
            } else {
                $OffsetNew = $CheckinsData[$Limit - 1]['checkin_id'];

                if (isset($OffsetNew) && $OffsetNew == '') {
                    $OffsetNew = "-1";
                }
                $ReturnData['offset'] = $OffsetNew;
            }
            $ReturnData['checkin_data'] = $CheckinsData;
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : GetNotifications
    // Purpose : To get all notification
    // In Params : offset,customer id
    // Out params : all data of categories
    //#################################################################
    public function GetNotifications($PostData) {

        //initialize
        $ReturnData = array();

        //extract params
        extract($PostData);

        //set image path
        $VenueImagePath = UPLOADS_URL . 'venues/';
         $categoryPath = UPLOADS_URL.'category_image/ios/thumb/';
        $DefaultImage = UPLOADS_URL . 'default_album.png';
        $DefaultVenueImage = UPLOADS_URL . 'default_venue.png';

        //set limit param
        $Limit = NOTIFICATION_PAGE_LIMIT;
        
         if($platform == 1)
        {   $platform_text = 'ios/';}
        else if($platform == 2)
        {   $platform_text = 'android/';}
        else
        {   $platform_text = 'ios/'; }
        

        //initilize return data
        $ReturnData = array();

        $this->db->select("v.id as venue_id,n.id as notification_id,n.notification_type,n.flag as read_status,ca.name as category_name,CONCAT('".$categoryPath."', ca.image) as category_image, n.notification_date,n.notification_text, v.name as venue_name,v.address as venue_address,v.state as venue_state,v.city as venue_city,if(v.zipcode != '',LPAD(v.zipcode, 5, '0'),v.zipcode)as zipcode,CONCAT('".$VenueImagePath."',v.id,'/','".$platform_text.'thumb/'."', concat('thumb_','',v.venue_image)) as venue_image", false);
        $this->db->from("notification as n");
        $this->db->join("venues as v", "n.venue_id = v.id", "left");
        $this->db->join("venue_categories as vc", "v.id = vc.venue_id", "left");
        $this->db->join("categories as ca", "ca.id = vc.category_id", "left");
        $this->db->where("n.customer_id", $customer_id);

        if (isset($offset) && $offset != "-1") {
            $this->db->where("n.id < $offset");
        }

        $this->db->order_by('n.id', 'DESC');
        $this->db->limit($Limit);

        //fetch data
        $GetNotification = $this->db->get();

        if ($GetNotification->num_rows() > 0) {

            //fetch the data
            $NotificationsData = $GetNotification->result_array();

            //prepare for response
            $ReturnData['status'] = '1';

            //check for total records,if its less then limit then set offset to -1
            if (isset($NotificationsData) && count($NotificationsData) < $Limit) {
                $ReturnData['offset'] = "-1";
            } else {
                $OffsetNew = $NotificationsData[$Limit - 1]['notification_id'];

                if (isset($OffsetNew) && $OffsetNew == '') {
                    $OffsetNew = "-1";
                }
                $ReturnData['offset'] = $OffsetNew;
            }
            $ReturnData['notification_data'] = $NotificationsData;
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //###########################################################
    //Function : UpdateToken
    //purpose : To update device token and device type after login
    //input : Device Token,Platform,Customer ID
    //outpur : success/error
    //###########################################################

    public function UpdateNotification($Params) {
        
        //extract params
        extract($Params);

        //prepare update array
        $updateData = array(
            "flag" => $read_status
        );

        //query to update
        $UpdateStatus = $this->db->update($this->venue_notification, $updateData, array("id " => $notification_id));
        
        return true;
    }

}

/* End of file customer_model.php */
/* Location: ./application/api/models/user_model.php */