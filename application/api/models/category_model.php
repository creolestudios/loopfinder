<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Category_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->category = 'categories';
    }

    //#################################################################
    // Name : GetCategories
    // Purpose : To get all categories
    // In Params : last category id for paging
    // Out params : all data of categories
    //#################################################################
    public function GetCategories($PostData) {

        //extract params
        extract($PostData);

        //set image path
        $CategoryImagePath = UPLOADS_URL . 'category_image/';
        $CategoryPinImagePath = UPLOADS_URL . 'category_image/';
        $DefaultImage = UPLOADS_URL . 'default_album.png';
        $DefaultPinImage = UPLOADS_URL . 'map-pin-default.png';

        //set limit param
        $Limit = CATEGORY_PAGE_LIMIT;

        //initilize return data
        $ReturnData = array();
        
         if($platform == 1)
        {   $platform_text = 'ios/';}
        else if($platform == 2)
        {   $platform_text = 'android/';}
        else
        {   $platform_text = 'ios/'; }
        
        $avail_image_types = array('1x','2x','3x');
        if(in_array($image_type,$avail_image_types))
        {   $image_type_query = '@'.$image_type.'/';}
        else if($image_type =='thumb'){ 
        $image_type_query = $image_type.'/'; }
        else{
             $image_type_query = '';
         }
        
         //IF(c.image = '','',CONCAT('" . $CategoryImagePath.$platform_text.$image_type_query. "',c.image)) as category_image
        $this->db->select("c.id as category_id,c.priority,c.name as category_name,IF(c.image = '','',CONCAT('" . $CategoryImagePath.$platform_text.$image_type_query. "',c.image)) as category_image,IF(c.pin = '','" . $DefaultPinImage . "',CONCAT('" . $CategoryPinImagePath . "',c.pin)) as category_pin", false);


        ## set filter for categories for city
        ## fetch venue from city then fetch categories for that venue
        if (isset($city) && $city != '') {
            $this->db->from("venues as v");
            $this->db->join("venue_categories as vc ", "v.id = vc.venue_id", "left");
            $this->db->join("categories as c ", "c.id = vc.category_id", "left");
            $this->db->like("v.city", $city, 'both');
            $this->db->group_by("c.id");
        } else {
            $this->db->from("categories as c");
            $this->db->join("venue_categories as vc ", "c.id = vc.category_id", "left");
            $this->db->group_by("c.id");
            $this->db->having("count(vc.venue_id) > ", "0");
        }

        if (isset($offset) && $offset != '-1') {
            $this->db->where("c.priority > $offset");
        }

        $this->db->order_by('c.priority', 'asc');
        $this->db->limit($Limit);

        //fetch data
        $GetCategory = $this->db->get();

        if ($GetCategory->num_rows() > 0) {

            //fetch the data
            $CategoryData = $GetCategory->result_array();

            //prepare for response
            $ReturnData['status'] = '1';

            //check for total records,if its less then limit then set offset to -1
            if (isset($CategoryData) && count($CategoryData) < $Limit) {
                $ReturnData['offset'] = "-1";
            } else {
                $OffsetNew = $CategoryData[$Limit - 1]['priority'];

                if (isset($OffsetNew) && $OffsetNew == '') {
                    $OffsetNew = "-1";
                }
                $ReturnData['offset'] = $CategoryData[$Limit - 1]['priority'];
            }
            $ReturnData['category_data'] = $CategoryData;
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : GetCategories
    // Purpose : To get all categories
    // In Params : last category id for paging
    // Out params : all data of categories
    //#################################################################

    public function getAllCategories() {
        //define categories path
        $CategoryPinImagePath = UPLOADS_URL . 'category_icons/';
        $DefaultPinImage = UPLOADS_URL . 'map-pin-default.png';
        $StaticArray = array();
        $CategoryArray = array();

        $this->db->select("c. id as id, c.name,c.priority, IF(c.pin = '','" . $DefaultPinImage . "',CONCAT('" . $CategoryPinImagePath . "',c.pin)) as category_pin,IF(c.not_looped_pin = '','" . $DefaultPinImage . "',CONCAT('" . $CategoryPinImagePath . "',c.not_looped_pin)) as not_looped_category_pin", false);
        $this->db->from("categories c");
        $this->db->join("venue_categories as vc ", "c.id = vc.category_id", "left");
        $this->db->group_by("c.id");
        $this->db->having("count(vc.venue_id) > ", "0");
        $this->db->order_by('c.priority', 'asc');
        //$this->db->where("c.id !=","9");
        // $this->db->where("c.id !=","21");
        $getCategories = $this->db->get();
        //echo $this->db->last_query();exit;

        if ($getCategories->num_rows() > 0) {

            //fetch the data
            $CategoryData = $getCategories->result_array();

            foreach ($CategoryData as $key => $val) {

                if ($val['id'] == '9' || $val['id'] == '21') {
                   $CategoryArray['static_array'][] = array('id' => $val['id'], 'name' => $val['name'], 'priority' => $val['priority'], 'category_pin' => $val['category_pin'], 'not_looped_category_pin' => $val['not_looped_category_pin']);
                } else {
                    $CategoryArray['category_array'][] = array('id' => $val['id'], 'name' => $val['name'], 'priority' => $val['priority'], 'category_pin' => $val['category_pin'], 'not_looped_category_pin' => $val['not_looped_category_pin']);
                }
            }

            //prepare for response
            $ReturnData['status'] = '1';

            //check for total records,if its less then limit then set offset to -1

            $ReturnData['category_data'] = $CategoryArray;


            return $ReturnData;
        }
    }

    //#################################################################
    // Name : searchForId
    // Purpose : To get all categories
    // In Params : last category id for paging
    // Out params : all data of categories
    //#################################################################
    public function searchForId($id, $array) {
        foreach ($array as $key => $val) {
            if ($val['uid'] === $id) {
                return $key;
            }
        }
        return null;
    }

}

/* End of file category_model.php */
/* Location: ./application/api/models/category_model.php */