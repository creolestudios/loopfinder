<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customer_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->customer = 'customers';
    }

    //###########################################################
    //Function : CheckEmailExist
    //purpose : To check email is exit or not
    //input : Email
    //outpur : success/error
    //###########################################################
    public function CheckEmailExist($Email, $CustomerId = "") {

        //global declaration
        $ReturnData = array();

        //check for email
        $this->db->select("id AS customer_id,user_type");

        if ($CustomerId != '') {
            $GetDataQuery = $this->db->get_where($this->customer, array(
                "email" => $Email,
                "id !=" => $CustomerId
            ));
        } else {
            $GetDataQuery = $this->db->get_where($this->customer, array(
                "email" => $Email
            ));
        }

        //if email found then return 1 else return 0
        if ($GetDataQuery->num_rows() > 0) {

            $CustomerDetails = $GetDataQuery->result_array();

            //process return data
            $ReturnData['status'] = 1;
            $ReturnData['user_details'] = $CustomerDetails[0];
        } else {

            $ReturnData['status'] = 0;            
        }

        //return data
        return $ReturnData;
    }

    //###########################################################
    //Function : AddCustomer
    //purpose : To add customer
    //input : First name,last name,email,password
    //outpur : success/error
    //###########################################################
    function AddCustomer($postData, $UserType = '1') {

        //extract post data
        extract($postData);

        //prepare array for insertion
        $InsertArray = array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'password' => md5($password),
            'profile_image' => $profile_image,
            'zipcode' => $zipcode,
            'state' => $state,
            'city' => $city,
            'gender' => $gender,
            'age' => $age,
            'hearing_level' => $hearing_level,
            'hearing_ad' => $hearing_ad,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'device_token' => $device_token,
            'device_type' => $platform,
            'user_type' => '1'
        );

        //insert customer data
        $AddUser = $this->db->insert($this->customer, $InsertArray);

        //check it insertion in success or not
        if ($this->db->affected_rows() > 0)
            return $this->db->insert_id();
        else
            return 0;
    }

    //###########################################################
    //Function : UpdateCustomerType
    //purpose : To update exist customer data customer
    //input : First name,last name,email,password
    //outpur : success/error
    //###########################################################
    function UpdateCustomerType($postData, $UserType = '1') {

        //extract post data
        extract($postData);

        //prepare array for insertion
        $UpdateData = array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'password' => md5($password),
            'profile_image' => $profile_image,
            'zipcode' => $zipcode,
            'state' => $state,
            'city' => $city,
            'gender' => $gender,
            'age' => $age,
            'hearing_level' => $hearing_level,
            'hearing_ad' => $hearing_ad,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'device_token' => $device_token,
            'device_type' => $platform,
            'user_type' => '1'
        );

        $UpdateDetails = $this->db->update($this->customer, $UpdateData, array("id " => $customer_id));

        if ($this->db->affected_rows() > 0) {
            $ReturnData['status'] = '1';
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //###########################################################
    //Function : CustomerLogin
    //purpose : To login with email and password
    //input : Email,password
    //outpur : success/error
    //###########################################################

    public function CustomerLogin($Email, $Password) {

        //global declaration
        $ReturnData = array();

        //check for login details
        $this->db->select("id AS customer_id,first_name,last_name,email,status");

        $GetDataQuery = $this->db->get_where($this->customer, array(
            "email" => $Email,
            "password" => md5($Password)
        ));

        //if user with email,password found then return 1 else return 0
        if ($GetDataQuery->num_rows() > 0) {

            //fetch customer details
            $CustomerDetails = $GetDataQuery->result_array();

            //process return data
            $ReturnData['status'] = 1;
            $ReturnData['customer_details'] = $CustomerDetails[0];
        } else {

            $ReturnData['status'] = 0;
        }

        //return data
        return $ReturnData;
    }

    //###########################################################
    //Function : UpdateToken
    //purpose : To update device token and device type after login
    //input : Device Token,Platform,Customer ID
    //outpur : success/error
    //###########################################################

    public function UpdateToken($Platform, $DeviceToken, $CustomerId) {

        //prepare update array
        $updateData = array(
            "device_token" => $DeviceToken,
            'device_type' => $Platform
        );

        //query to update
        $UpdateToken = $this->db->update($this->customer, $updateData, array("id " => $CustomerId));
    }

    //###########################################################
    //Function : GetCustomerByID
    //purpose : To get get details of customer
    //input : customer id
    //outpur : success/error
    //###########################################################
    function GetCustomerByID($CustomerId) {

        //global declaration
        $ReturnData = array();

        $CustomerImagePath = UPLOADS_URL . 'customer/';
        $DefaultCustomerImage = UPLOADS_URL . 'user_default.png';

        $this->db->select("id AS customer_id,first_name,last_name,email,if(zipcode != '',LPAD(zipcode, 5, '0'),zipcode)as zipcode,state,city,gender,age,hearing_level,hearing_ad,latitude,longitude,IF(profile_image = '','" . $DefaultCustomerImage . "',CONCAT('" . $CustomerImagePath . "',profile_image)) as profile_image", false);

        $GetDataQuery = $this->db->get_where($this->customer, array(
            "id" => $CustomerId
        ));

        if ($GetDataQuery->num_rows() > 0) {

            //get customer data
            $CustomerData = $GetDataQuery->result_array();

            $ReturnData['status'] = 1;
            $ReturnData['customer_details'] = $CustomerData[0];
        } else {
            //return blank array
            $ReturnData['status'] = 0;
        }
        //return data
        return $ReturnData;
    }

    //###########################################################
    //Function : CheckEmailValidation
    //purpose : To check email is valid or not
    //input : email
    //outpur : success/error
    //###########################################################
    function CheckEmailValidation($Email) {

        //global declaration
        $ReturnData = array();

        $this->db->select("id AS customer_id");

        $GetDataQuery = $this->db->get_where($this->customer, array(
            "email" => $Email
        ));

        if ($GetDataQuery->num_rows() > 0) {
            //if email found then return 1 else return 0
            //fetch customer details
            $CustomerId = $GetDataQuery->result_array();

            //process return data
            $ReturnData['status'] = 1;
            $ReturnData['customer_id'] = $CustomerId[0]['customer_id'];
        } else {
            //return blank array
            $ReturnData['status'] = 0;
        }
        return $ReturnData;
    }

    //###########################################################
    //Function : UpdatePassword
    //purpose : To update passsword in database
    //input : customer id, password
    //outpur : success/error
    //###########################################################
    public function UpdatePassword($CustomerId, $Password) {
        //prepare update array
        $updateData = array(
            "password" => md5($Password)
        );

        //query to update
        $UpdatePassword = $this->db->update($this->customer, $updateData, array("id " => $CustomerId));

        return 1;
    }

    //###########################################################
    //Function : UpdateCustomer
    //purpose : To update customer data
    //input : customer data and customer id
    //outpur : success/error
    //###########################################################
    public function UpdateCustomer($postData) {

        //global declaration
        $ReturnData = array();

        //extract post data
        extract($postData);

        //prepare array for insertion
        $UpdateData = array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'email' => $email,
            'zipcode' => $zipcode,
            'state' => $state,
            'city' => $city,
            'gender' => $gender,
            'age' => $age,
            'hearing_level' => $hearing_level,
            'hearing_ad' => $hearing_ad,
        );

        //if password is not blank then update the password also
        if (isset($password) && $password != '') {
            $UpdateData = array_merge($UpdateData, array('password' => md5($password)));
        }

        //check for profile
        //if image is updated then merge image array with update data array
        if (isset($profile_image) && $profile_image != '') {
            $UpdateData = array_merge($UpdateData, array('profile_image' => $profile_image));
        }
        //query to update
        $UpdateDetails = $this->db->update($this->customer, $UpdateData, array("id " => $customer_id));

        if ($this->db->affected_rows() > 0) {
            $ReturnData['status'] = '1';
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //###########################################################
    //Function : CheckOldPassword
    //purpose : To check old password is correct or not
    //input : customer id, password
    //outpur : success/error
    //###########################################################
    public function CheckOldPassword($OldPassword, $CustomerId) {
        //global declaration
        $ReturnData = array();

        $this->db->select("id AS customer_id,password");

        $GetDataQuery = $this->db->get_where($this->customer, array(
            "id" => $CustomerId,
            "password" => md5($OldPassword)
        ));

        if ($GetDataQuery->num_rows() > 0) {
            $Password = $GetDataQuery->result_array();

            //if email found then return 1 else return 0
            //process return data
            $ReturnData['status'] = 1;
            $ReturnData['password'] = $Password[0]['password'];
        } else {
            //return blank array
            $ReturnData['status'] = 0;
        }
        return $ReturnData;
    }

    //###########################################################
    //Function : UpdateToken
    //purpose : To update device token and device type after login
    //input : Device Token,Platform,Customer ID
    //outpur : success/error
    //###########################################################

    public function UpdateLocation($Params) {

        //global declaration
        $ReturnData = array();

        //extract params
        extract($Params);

        //prepare update array
        $updateData = array(
            "latitude" => $latitude,
            "longitude" => $longitude
        );

        //query to update
        $UpdateLocation = $this->db->update($this->customer, $updateData, array("id " => $customer_id));

        if ($this->db->affected_rows() > 0) {
            $ReturnData['status'] = '1';
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //###########################################################
    //Function : GetSettings
    //purpose : To get notification settings
    //input : Customer ID
    //outpur : success/error
    //###########################################################

    public function GetSettings($CustomerId) {

        //global declaration
        $ReturnData = array();


        $this->db->select("notify_venue_nearby,notify_new_loop_added,notify_new_loop_added_distance,notify_new_loop_requested,notify_new_loop_requested_distance,device_token");

        $GetSettingsQuery = $this->db->get_where($this->customer, array(
            "id" => $CustomerId
        ));

        if ($GetSettingsQuery->num_rows() > 0) {
            //if email found then return 1 else return 0
            //fetch customer details
            $Settings = $GetSettingsQuery->result_array();

            //process return data
            $ReturnData['status'] = 1;
            $ReturnData['settings_data'] = $Settings['0'];
        } else {
            //return blank array
            $ReturnData['status'] = 0;
        }
        return $ReturnData;
    }

    //###########################################################
    //Function : UpdateToken
    //purpose : To update device token and device type after login
    //input : Device Token,Platform,Customer ID
    //outpur : success/error
    //###########################################################

    public function UpdateSettings($Params) {

        //global declaration
        $ReturnData = array();

        //extract params
        extract($Params);

        //prepare update array
        $updateData = array(
            "notify_venue_nearby" => $notify_venue_nearby,
            "notify_new_loop_added" => $notify_new_loop_added,
            "notify_new_loop_added_distance" => $notify_new_loop_added_distance,
            "notify_new_loop_requested" => $notify_new_loop_requested,
            "notify_new_loop_requested_distance" => $notify_new_loop_requested_distance
        );

        //query to update
        $UpdateLocation = $this->db->update($this->customer, $updateData, array("id " => $customer_id));

        if ($this->db->affected_rows() > 0) {
            $ReturnData['status'] = '1';
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : GetZipCodeDetails
    // Purpose : To get state and city values from zipcode
    // In Params : city and state data
    // Out params : success/error message with status
    //#################################################################
    public function GetZipCodeDetails($Params) {

        //initialize data
        $ReturnData = array();

        //extract input post parameters
        extract($Params);

        //query to fetch city and state from given zipcode
        $this->db->select(" c.city,c.state_code,s.state ", false);
        $this->db->from("cities as c ");
        $this->db->join("states as s ", "c.state_code = s.state_code", "left");
        $this->db->where("c.zip", $zipcode);
        $this->db->order_by("c.id", "asc");
        $this->db->limit(1);

        $GetZipCodeDataQuery = $this->db->get();

        if ($GetZipCodeDataQuery->num_rows() > 0) {
            //fetch image data
            $ZipCodeData = $GetZipCodeDataQuery->result_array();
            //assign to array
            $ReturnData['status'] = 1;
            $ReturnData['zipcode'] = $ZipCodeData[0];
        } else {
            $ReturnData['status'] = 0;
        }

        return $ReturnData;
    }
    
    //#################################################################
    // Name : GetCities
    // Purpose : To get cities
    // In Params : city and state data
    // Out params : success/error message with status
    //#################################################################
    public function GetCities() {

        //initialize data
        $ReturnData = array();

        //query to fetch city and state from given zipcode
        $this->db->select(" v.id as venue_id,v.city ", false);
        $this->db->from("venues as v ");
        $this->db->group_by("v.city");
        $this->db->order_by("v.city", "asc");
        
        $GetCities = $this->db->get();

        if ($GetCities->num_rows() > 0) {
            //fetch image data
            $CityData = $GetCities->result_array();
            //assign to array
            $ReturnData['status'] = 1;
            $ReturnData['cities'] = $CityData;
        } else {
            $ReturnData['status'] = 0;
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : GetNotificationDetails
    // Purpose : To get notification settings
    // In Params : user id,latitude,longiture
    // Out params : notification and near by venue for user
    //#################################################################
    public function GetNotificationDetails($Params) {

        //extract params.
        extract($Params);

        $NearByDistance = NEAR_BY_DISTANCE;

        $this->db->select("v.id as venue_id,v.name as venue_name,v.latitude,v.longitude,TRUNCATE(( 3959 * acos ( cos ( radians('" . $latitude . "') ) * cos( radians( v.latitude ) ) * cos( radians( v.longitude ) - radians('" . $longitude . "') ) + sin ( radians('" . $latitude . "') ) * sin( radians( v.latitude ) ) ) ),2) AS `distance`", false);
        $this->db->from('venues as v');
        $this->db->having("distance <= ", $NearByDistance);
        $this->db->group_by("v.id");
        ## execute query
        $VenuesList = $this->db->get();

        if ($VenuesList->num_rows() > 0) {
            //fetch the data
            $NotitificationData = $VenuesList->result_array();

            //prepare for response
            $ReturnData['status'] = '1';
            $ReturnData['notification_data'] = $NotitificationData;
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : AddNotification
    // Purpose : To get notification
    // In Params : user id,latitude,longiture
    // Out params : notification and near by venue for user
    //#################################################################
    public function AddNotification($NotificationStr) {

        //initialize data
        $ReturnData = array();

        ## add notification to table
        $this->db->query('INSERT INTO notification(customer_id,venue_id,notification_text,notification_date,flag,notification_type) VALUES ' . $NotificationStr);

        if ($this->db->affected_rows() > 0) {
            $ReturnData['status'] = '1';
        } else {
            $ReturnData['status'] = '0';
        }
        return $ReturnData;        
    }

    //########################### Misc function#############
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

    //########################### Misc function#############
}

/* End of file customer_model.php */
/* Location: ./application/api/models/user_model.php */