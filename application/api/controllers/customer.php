<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Customer extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('customer_model');
    }

    /**
     * [index_post FOR WELCOME MESSAGE]
     * offset[INT] [THIS IS OFFET THAT WILL BE PASSED TO QUERY, to limit 10]
     * @return [type] [description]
     */
    public function index_post() {

        $ResponseData['welcome'] = $this->lang->line('WELCOME_MESSAGE');
        $this->response($ResponseData, 200);
    }

    //###########################################################
    //Function : Register
    //purpose : To register customer
    //input : first name,last name,email,password
    //outpur : success/error
    //###########################################################
    public function register_post() {

        //global declaration
        $ResponseData = array();

        //get data from request and process
        $PostData = $this->post();

        $TargetPath = UPLOADS . 'customer/';

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from all the data
            $Params = array_map('trim', $PostData);
            $Params = array_map('urldecode', $PostData);

            //Check for email address is exist or not
            $CheckEmail = $this->customer_model->CheckEmailExist($Params['email']);

            //if email address exist
            if (isset($CheckEmail) && $CheckEmail['status'] == 1) {

                //extract data to check user type
                extract($CheckEmail['user_details']);

                //if user type is 2 (guest from web) then update user to register with provided details
                if ($user_type == '2') {
                    //add customer

                    $Params['customer_id'] = $customer_id;

                    //upload customer profile image
                    if (isset($_FILES) && $_FILES['profile_image']['name'] != '') {


                        //get file upload data
                        $field = array_keys($_FILES);

                        ##########################  UPLOAD IMAGE CONFIG ############################            
                        $config['upload_path'] = $TargetPath;
                        $config['allowed_types'] = 'gif|jpg|png|jpeg|PNG|JPEG|JPG';
                        $config['max_size'] = 1024 * 5;
                        $config['encrypt_name'] = TRUE;
                        $config['overwrite'] = FALSE;

                        //load upload library
                        $this->load->library('upload', $config);

                        //initialize config
                        $this->upload->initialize($config);

                        //if uplaod is success
                        if ($this->upload->do_upload($field[0])) {
                            //extract data to get file name and make thumb
                            extract($this->upload->data());

                            //get file name
                            $Params['profile_image'] = $file_name; //get file name from extract and assign to param
                        } else {
                            //mprd($this->upload->display_errors());
                            //print error response
                            $ResponseData['success'] = "0";
                            $ResponseData['message'] = $this->upload->display_errors();
                        }
                    } else {
                        //print error response
                        $ResponseData['success'] = "0";
                        $ResponseData['message'] = $this->lang->line('FILE_EMPTY');
                    }

                    $UpdateExistCustomer = $this->customer_model->UpdateCustomerType($Params);

                    //check for return params
                    if (isset($UpdateExistCustomer) && $UpdateExistCustomer['status'] > 0) {

                        //print success
                        $ResponseData['success'] = "1";
                        $ResponseData['message'] = $this->lang->line('REGISTER_SUCCESS');
                        $ResponseData['data'] = array("customer_id" => $customer_id);
                    } else {
                        //print error response
                        $ResponseData['success'] = "0";
                        $ResponseData['message'] = $this->lang->line('GENERAL_NO_CHANGES');
                    }
                } else {
                    //get message from language library
                    $EmailExistMessage = $this->lang->line('EMAIL_ALREADY_EXIST');

                    //replace {EMAIL} with given email to show in response
                    $EmailExistMessage = str_replace("{EMAIL}", $Params['email'], $EmailExistMessage);

                    //print error response
                    $ResponseData['success'] = "0";
                    $ResponseData['message'] = $EmailExistMessage;
                }
            } else {

                //upload customer profile image
                if (isset($_FILES) && $_FILES['profile_image']['name'] != '') {


                    //get file upload data
                    $field = array_keys($_FILES);

                    ##########################  UPLOAD IMAGE CONFIG ############################            
                    $config['upload_path'] = $TargetPath;
                    $config['allowed_types'] = 'gif|jpg|png|jpeg|PNG|JPEG|JPG';
                    $config['max_size'] = 1024 * 5;
                    $config['encrypt_name'] = TRUE;
                    $config['overwrite'] = FALSE;

                    //load upload library
                    $this->load->library('upload', $config);

                    //initialize config
                    $this->upload->initialize($config);

                    //if uplaod is success
                    if ($this->upload->do_upload($field[0])) {
                        //extract data to get file name and make thumb
                        extract($this->upload->data());

                        //get file name
                        $Params['profile_image'] = $file_name; //get file name from extract and assign to param
                    } else {
                        //mprd($this->upload->display_errors());
                        //print error response
                        $ResponseData['success'] = "0";
                        $ResponseData['message'] = $this->upload->display_errors();
                    }
                } else {
                    //print error response
                    $ResponseData['success'] = "0";
                    $ResponseData['message'] = $this->lang->line('FILE_EMPTY');
                }

                //add customer
                $AddCustomer = $this->customer_model->AddCustomer($Params);

                //check for return params
                if (isset($AddCustomer) && $AddCustomer > 0) {
                    //print success
                    $ResponseData['success'] = "1";
                    $ResponseData['message'] = $this->lang->line('REGISTER_SUCCESS');
                    $ResponseData['data'] = array("customer_id" => $AddCustomer);
                } else {
                    //print error response
                    $ResponseData['success'] = "0";
                    $ResponseData['message'] = $this->lang->line('GENERAL_ERROR');
                }
            }
        } else {
            //print error response
            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->lang->line('INVALID_PARAMS');
        }

        $this->response($ResponseData, 200);
    }

    
    //###########################################################
    //Function : Login
    //purpose : To login customer
    //input : password
    //outpur : success/error
    //###########################################################
    public function login_post() {

        //get data from request and process
        $PostData = $this->post();

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from all the data
            $Params = array_map('trim', $PostData);

            //process facebook id to check customer already available in database or not.
            if (isset($Params['email']) && $Params['email'] != '') {

                //check for login details
                $CustomerLogin = $this->customer_model->CustomerLogin($Params['email'], $Params['password']);

                //if customer with given email,password found
                if (isset($CustomerLogin) && $CustomerLogin['status'] == '1') {

                    //fetch data for that customer
                    $CustomerData = $CustomerLogin['customer_details'];


                    if (isset($CustomerData) && $CustomerData['status'] == "1") {

                        //if login success and user status is active then update device token and platform (ios/android)

                        $UpdateToken = $this->customer_model->UpdateToken($Params['platform'], $Params['device_token'], $CustomerData['customer_id']);

                        //generate response
                        $ResponseData['success'] = "1";
                        $ResponseData['message'] = $this->lang->line('LOGIN_SUCCESS');
                        $ResponseData['data'] = $CustomerData;
                    } else {
                        //generate response
                        $ResponseData['success'] = "0";
                        $ResponseData['message'] = $this->lang->line('ACCOUNT_INACTIVE');
                    }
                } else { //if customer not found,then response error
                    //generate response
                    $ResponseData['success'] = "0";
                    $ResponseData['message'] = $this->lang->line('LOGIN_ERROR');
                }
            } else {
                //error for facebook id not found
                //print error response
                $ResponseData['success'] = "0";
                $ResponseData['message'] = $this->lang->line('EMAIL_NOT_FOUND');
            }
        } else {
            //print error response
            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->lang->line('INVALID_PARAMS');
        }

        $this->response($ResponseData, 200);
    }

    //###########################################################
    //Function : Logout
    //purpose : To logout customer
    //input : customer id
    //outpur : success/error
    //###########################################################

    public function logout_post() {

        //get data from request and process
        $PostData = $this->post();

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from all the data
            $Params = array_map('trim', $PostData);

            ## update customer device token to blank
            $UpdateToken = $this->customer_model->UpdateToken('1', '', $Params['customer_id']);

            //generate response
            $ResponseData['success'] = "1";
            $ResponseData['message'] = $this->lang->line('GENERAL_SUCCESS');
        } else {
            //print error response
            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->lang->line('INVALID_PARAMS');
        }

        $this->response($ResponseData, 200);
    }

    //###########################################################
    //Function : Forgot password
    //purpose : To send email in case of forgot password
    //input : email
    //outpur : success/error
    //###########################################################
    public function forgotpassword_post() {
        //global declaration
        $ResponseData = array();

        //get data from request and process
        $PostData = $this->post();

        if (isset($PostData) && count($PostData) > 0) {
            //remove space from all the data
            $Params = array_map('trim', $PostData);

            //check for email address is available or not
            $CheckEmail = $this->customer_model->CheckEmailValidation($Params['email']);

            //if customer with given email found then send mail
            if (isset($CheckEmail) && $CheckEmail['status'] == '1') {

                //email variable declaration
                $subject = 'Forgot Password';
                $email = $Params['email'];
                $NewPassword = $this->customer_model->generateRandomString(6);
                //email data
                $data = array(
                    'password' => $NewPassword
                );
                $html = $this->load->view('email/forgotpass_view', $data, true);

                //send email for forgot password
                $this->load->library('email');
                $config['charset'] = 'iso-8859-1';
                $config['wordwrap'] = TRUE;
                $config['mailtype'] = "html";
                $this->email->initialize($config);
                $this->email->set_newline("\r\n");
                $this->email->from(ADMIN_EMAIL);
                $this->email->to($email);
                $this->email->subject($subject);
                $this->email->message($html);

                if ($this->email->send()) {
                    //if email sent then update password in database
                    $UpdatePassword = $this->customer_model->UpdatePassword($CheckEmail['customer_id'], $NewPassword);
                    //print error response
                    $ResponseData['success'] = "1";
                    $ResponseData['message'] = $this->lang->line('FORGOT_PASSWORD_SUCCESS');
                } else {
                    $ResponseData['success'] = "0";
                    $ResponseData['message'] = $this->lang->line('GENERAL_ERROR');
                }
            } else {
                //response error
                //print error response
                $ResponseData['success'] = "0";
                $ResponseData['message'] = $this->lang->line('FORGOT_PASSWORD_ERROR');
            }
        } else {
            //print error response
            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->lang->line('INVALID_PARAMS');
        }

        $this->response($ResponseData, 200);
    }

    //###########################################################
    //Function : Customerdetails
    //purpose : To get customer details
    //input : customer id
    //outpur : success/error
    //###########################################################
    public function customerdetails_post() {
        //global declaration
        $ResponseData = array();

        //get data from request and process
        $PostData = $this->post();

        if (isset($PostData) && count($PostData) > 0) {
            //remove space from all the data
            $Params = array_map('trim', $PostData);

            if (isset($Params['customer_id']) && $Params['customer_id'] != '') {

                //fetch data for that customer
                $CustomerData = $this->customer_model->GetCustomerByID($Params['customer_id']);

                if (isset($CustomerData) && $CustomerData['status'] > 0) {
                    $CustomerData = $this->array_remove_empty($CustomerData);
                    //print response
                    $ResponseData['success'] = "1";
                    $ResponseData['message'] = $this->lang->line('GENERAL_SUCCESS');
                    $ResponseData['data'] = $CustomerData['customer_details'];
                } else {
                    //print error response
                    $ResponseData['success'] = "0";
                    $ResponseData['message'] = $this->lang->line('CUSTOMER_NOT_FOUND');
                }
            } else {
                //print error response
                $ResponseData['success'] = "0";
                $ResponseData['message'] = $this->lang->line('CUSTOMER_NOT_FOUND');
            }
        } else {
            //print error response
            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->lang->line('INVALID_PARAMS');
        }

        $this->response($ResponseData, 200);
    }

    //###########################################################
    //Function : UpdateDetails
    //purpose : To get customer details
    //input : customer id
    //outpur : success/error
    //###########################################################
    public function updatedetails_post() {
        //global declaration
        $ResponseData = array();
        $TargetPath = UPLOADS . 'customer/';

        //get data from request and process
        $PostData = $this->post();

        if (isset($PostData) && count($PostData) > 0) {
            //remove space from all the data
            $Params = array_map('trim', $PostData);
            $Params = array_map('urldecode', $PostData);

            if (isset($Params['customer_id']) && $Params['customer_id'] != '') {
                //Check for email address is exist or not
                $CheckEmail = $this->customer_model->CheckEmailExist($Params['email'], $Params['customer_id']);

                //if email address exist
                if (isset($CheckEmail) && $CheckEmail['status'] == 1) {

                    //get message from language library
                    $EmailExistMessage = $this->lang->line('EMAIL_ALREADY_EXIST');

                    //replace {EMAIL} with given email to show in response
                    $EmailExistMessage = str_replace("{EMAIL}", $Params['email'], $EmailExistMessage);

                    //print error response
                    $ResponseData['success'] = "0";
                    $ResponseData['message'] = $EmailExistMessage;
                } else {

                    //upload customer profile image
                    if (isset($_FILES) && $_FILES['profile_image']['name'] != '') {

                        //get file upload data
                        $field = array_keys($_FILES);

                        ##########################  UPLOAD IMAGE CONFIG ############################            
                        $config['upload_path'] = $TargetPath;
                        $config['allowed_types'] = 'gif|jpg|png|jpeg|PNG|JPEG|JPG';
                        $config['max_size'] = 1024 * 5;
                        $config['encrypt_name'] = TRUE;
                        $config['overwrite'] = false;

                        //load upload library
                        $this->load->library('upload', $config);

                        //initialize config
                        $this->upload->initialize($config);

                        //if uplaod is success
                        if ($this->upload->do_upload($field[0])) {
                            //extract data to get file name and make thumb
                            extract($this->upload->data());

                            //get file name
                            $Params['profile_image'] = $file_name; //get file name from extract and assign to param
                        } else {
                            //print error response
                            $ResponseData['success'] = "0";
                            $ResponseData['message'] = $this->upload->display_errors();
                        }
                    } else {
                        //print error response
                        $ResponseData['success'] = "0";
                        $ResponseData['message'] = $this->lang->line('FILE_EMPTY');
                    }

                    //update
                    $UpdateCustomer = $this->customer_model->UpdateCustomer($Params);

                    //check for return params
                    if (isset($UpdateCustomer) && $UpdateCustomer['status'] > 0) {

                        //fetch customer details
                        $CustomerData = $this->customer_model->GetCustomerByID($Params['customer_id']);
                        //print success
                        $ResponseData['success'] = "1";
                        $ResponseData['message'] = $this->lang->line('DETAIL_UPDATE_SUCCESS');
                        $ResponseData['data'] = $CustomerData['customer_details'];
                    } else {
                        //print error response
                        $ResponseData['success'] = "0";
                        $ResponseData['message'] = $this->lang->line('GENERAL_NO_CHANGES');
                    }
                }
            } else {
                //error for facebook id not found
                //print error response
                $ResponseData['success'] = "0";
                $ResponseData['message'] = $this->lang->line('CUSTOMER_NOT_FOUND');
            }
        } else {
            //print error response
            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->lang->line('INVALID_PARAMS');
        }

        $this->response($ResponseData, 200);
    }

    //###########################################################
    //Function : Change password
    //purpose : To change user password
    //input : old password,new password
    //outpur : success/error
    //###########################################################

    public function changepassword_post() {

        //global declaration
        $ResponseData = array();

        //get data from request and process
        $PostData = $this->post();

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from all the data
            $Params = array_map('trim', $PostData);

            //check for old password is correct or not
            if (isset($Params['old_password']) && $Params['old_password'] != '') {

                //check for customer's old password
                $CheckOldPassword = $this->customer_model->CheckOldPassword($Params['old_password'], $Params['customer_id']);

                //if old password is correct, then replace new password
                if (isset($CheckOldPassword) && $CheckOldPassword['status'] == '1') {

                    if ($CheckOldPassword['password'] == md5($Params['new_password'])) {

                        //print success response
                        $ResponseData['success'] = "0";
                        $ResponseData['message'] = $this->lang->line('SAME_PASSWORD');
                    } else {
                        $UpdatePassword = $this->customer_model->UpdatePassword($Params['customer_id'], $Params['new_password']);

                        //print success response
                        $ResponseData['success'] = "1";
                        $ResponseData['message'] = $this->lang->line('SUCCESS_PASSWORD_CHANGE');
                    }
                } else {
                    //print error response
                    $ResponseData['success'] = "0";
                    $ResponseData['message'] = $this->lang->line('INVALID_OLD_PASSWORD');
                }
            } else {
                //print error response
                $ResponseData['success'] = "0";
                $ResponseData['message'] = $this->lang->line('OLD_PASSWORD_REQUIRED ');
            }
        } else {
            //print error response
            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->lang->line('INVALID_PARAMS');
        }
        $this->response($ResponseData, 200);
    }

    //###########################################################
    //Function : UpdateLocation
    //purpose : To update user current location
    //input : customer id, latitude,longitude
    //outpur : success/error
    //###########################################################

//    public function updatelocation_post() {
//
//        //global declaration
//        $ResponseData = array();
//
//        //get data from request and process
//        $PostData = $this->post();
//
//        if (isset($PostData) && count($PostData) > 0) {
//
//            //remove space from all the data
//            $Params = array_map('trim', $PostData);
//
//            ## Load notification settings
//            $this->load->library('apn');
//            $this->apn->payloadMethod = 'enhance'; // you can turn on this method for debuggin purpose
//            $this->apn->connectToPush();
//
//            ## Notification time settings, ## Generate UTC data
//            $Timezone = "UTC";
//            date_default_timezone_set($Timezone);
//
//            $NotificationDateTime = gmdate("Y-m-d h:i:s");
//
//            //call function to update locatioan
//            $UpdateLocation = $this->customer_model->UpdateLocation($Params);
//
//            ################ send push notification for near by venue ################
//            ## Get notification setting of customer
//            $GetSettings = $this->customer_model->GetSettings($Params['customer_id']);
//
//            ## If setting details found
//            if (isset($GetSettings) && $GetSettings['status'] == '1') {
//                extract($GetSettings['settings_data']);
//            }
//
//            ## if notification setting is on then process for notification
//            if (isset($notify_venue_nearby) && $notify_venue_nearby == '1') {
//                ## Get all near by venue with notification settings
//                $GetNotificationDetails = $this->customer_model->GetNotificationDetails($Params);
//
//                if (isset($GetNotificationDetails) && $GetNotificationDetails['status'] == '1') {
//                    ## need to check condition for device token
//                    ## if device token not blank then only process for notification
//                    if (isset($device_token) && $device_token != '') {
//                        ## loop through near by venue and send notification
//                        foreach ($GetNotificationDetails['notification_data'] as $NotiKey => $NotiVal) {
//                            ## set venue id in key
//                            $this->apn->setData(array('venue_id' => $NotiVal['venue_id']));
//
//                            ## set message
//                            $NotificationMessage = $this->lang->line('VENUE_NEAR_BY') . " : " . $NotiVal['venue_name'] . ".";
//
//                            ## send notification
//                            $this->apn->sendMessage($device_token, $NotificationMessage, 1, 'default');
//                            ## print error if notification is not sent
//                            if (isset($send_result) && !$send_result) {
//                                $ResponseData['success'] = "0";
//                                $ResponseData['message'] = $this->apn->error;
//                            }
//                            ## prepare array to insert data in notification table
//                            $Notification[] = "('" . $Params['customer_id'] . "','" . $NotiVal['venue_id'] . "','" . $NotificationMessage . "','" . $NotificationDateTime . "','0','1')";
//                        }
//                    }
//
//                    ## process notification insert
//                    if (isset($Notification) && count($Notification) > 0) {
//                        $NotificationStr = implode(',', $Notification);
//
//                        ## insert notification data
//                        if (isset($NotificationStr) && $NotificationStr != '') {
//                            $AddNotification = $this->customer_model->AddNotification($NotificationStr);
//                        }
//                    }
//                }
//            }
//            ################ send push notification End ################
//            $ResponseData['success'] = "1";
//            $ResponseData['message'] = $this->lang->line('GENERAL_SUCCESS');
//        } else {
//            //print error response
//            $ResponseData['success'] = "0";
//            $ResponseData['message'] = $this->lang->line('INVALID_PARAMS');
//        }
//        $this->response($ResponseData, 200);
//    }

    //###########################################################
    //Function : GetSettings
    //purpose : To get notificatiaon settings
    //input : customer id
    //outpur : success/error
    //###########################################################
    public function getsettings_post() {

        //global declaration
        $ResponseData = array();

        //get data from request and process
        $PostData = $this->post();

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from all the data
            $Params = array_map('trim', $PostData);

            //call function to update locatioan
            $GetSettings = $this->customer_model->GetSettings($Params['customer_id']);

            if (isset($GetSettings) && $GetSettings['status'] == '1') {
                $ResponseData['success'] = "1";
                $ResponseData['message'] = $this->lang->line('GENERAL_SUCCESS');
                $ResponseData['data'] = $GetSettings['settings_data'];
            } else {
                $ResponseData['success'] = "0";
                $ResponseData['message'] = $this->lang->line('NO_SETTINGS');
            }
        } else {
            //print error response
            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->lang->line('INVALID_PARAMS');
        }
        $this->response($ResponseData, 200);
    }

    //###########################################################
    //Function : updatesettings
    //purpose : To update user current notification settings
    //input : customer id, all settings details
    //outpur : success/error
    //###########################################################

    public function updatesettings_post() {

        //global declaration
        $ResponseData = array();

        //get data from request and process
        $PostData = $this->post();

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from all the data
            $Params = array_map('trim', $PostData);

            //call function to update locatioan
            $UpdateSettings = $this->customer_model->UpdateSettings($Params);

            $ResponseData['success'] = "1";
            $ResponseData['message'] = $this->lang->line('SETTINGS_UPDATE_SUCCESS');
        } else {
            //print error response
            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->lang->line('INVALID_PARAMS');
        }
        $this->response($ResponseData, 200);
    }

    //#################################################################
    // Name : GetZipCodeData
    // Purpose : To get state and city data from zipcode
    // In Params : void
    // Out params : category view data
    //################################################################# 
    public function getzipcodedetails_post() {

        //get post data
        $PostData = $this->post();

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from post data
            $Params = array_map('trim', $PostData);

            $Response = $this->customer_model->GetZipCodeDetails($Params);

            if (isset($Response) && $Response['status'] == '1') {
                $ResponseData['success'] = "1";
                $ResponseData['message'] = $this->lang->line('GENERAL_SUCCESS');
                $ResponseData['data'] = $Response['zipcode'];
            } else {
                $ResponseData['success'] = "0";
                $ResponseData['message'] = $this->lang->line('NO_ZIPCODE_DETAIS');
            }
        } else {
            //print error response
            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->lang->line('INVALID_PARAMS');
        }
        $this->response($ResponseData, 200);
    }

    //#################################################################
    // Name : GetZipCodeData
    // Purpose : To get state and city data from zipcode
    // In Params : void
    // Out params : category view data
    //################################################################# 
    public function getcities_post() {

        $Response = $this->customer_model->GetCities();

        if (isset($Response) && $Response['status'] == '1') {
            $ResponseData['success'] = "1";
            $ResponseData['message'] = $this->lang->line('GENERAL_SUCCESS');
            $ResponseData['data'] = $Response['cities'];
        } else {
            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->lang->line('GENERAL_ERROR');
        }

        $this->response($ResponseData, 200);
    }

    public function notifications_post() {
        $this->load->library('apn');
        $this->apn->payloadMethod = 'enhance'; // you can turn on this method for debuggin purpose
        $this->apn->connectToPush();

        // adding custom variables to the notification
        $this->apn->setData(array('someKey' => true));

        $send_result = $this->apn->sendMessage('860dd52a3fff54453ea24751778893da0beb6c3508a950169182b3a995e41b74', 'Test notif #1 (TIME:' . date('H:i:s') . ')', /* badge */ 2, /* sound */ 'default');

        if ($send_result) {
            $ResponseData['success'] = "1";
            $ResponseData['message'] = 'Sending successful';
        } else {
            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->apn->error;
        }
        
        $this->apn->disconnectPush();

        $this->response($ResponseData, 200);
    }

    //###########################################################
    //Function : contactus
    //purpose : To send email for forgot password
    //input : email
    //outpur : success/error
    //###########################################################
    public function contactus_post() {
        //global declaration
        $ResponseData = array();

        //get data from request and process
        $PostData = $this->post();

        if (isset($PostData) && count($PostData) > 0) {
            //remove space from all the data
            $Params = array_map('trim', $PostData);

            //email variable declaration
            $subject = 'Someone say hello';
            $email = $Params['email'];
            $comment = $Params['comment'];
            //email data
            $data = array(
                'comment' => $comment
            );
            $html = $this->load->view('email/contact_view', $data, true);

            //send email for forgot password
            $this->load->library('email');
            $config['charset'] = 'iso-8859-1';
            $config['wordwrap'] = TRUE;
            $config['mailtype'] = "html";
            $this->email->initialize($config);
            $this->email->set_newline("\r\n");
            $this->email->from($email);
            $this->email->to(ADMIN_EMAIL);
            $this->email->subject($subject);
            $this->email->message($html);

            if ($this->email->send()) {
                //print error response
                $ResponseData['success'] = "1";
                $ResponseData['message'] = $this->lang->line('CONTACT_SUCCESS');
            } else {
                $ResponseData['success'] = "0";
                $ResponseData['message'] = $this->lang->line('GENERAL_ERROR');
            }
        } else {
            //print error response
            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->lang->line('INVALID_PARAMS');
        }

        $this->response($ResponseData, 200);
    }

    //################################################################# 
    // Name : array_remove_empty
    // Purpose : To remove all null values
    // In Params : array
    // Out params : all data of venues
    //#################################################################
    public function array_remove_empty($haystack) {
        foreach ($haystack as $key => $value) {
            if (is_array($value)) {
                $haystack[$key] = $this->array_remove_empty($haystack[$key]);
            }

            if (empty($haystack[$key])) {
                $haystack[$key] = "";
            }
        }

        return $haystack;
    }

}

/* End of file customer.php */
/* Location: ./application/api/controllers/customer.php */