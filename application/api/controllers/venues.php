<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Venues extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('venue_model');
        
    }

    /**
     * [index_post FOR WELCOME MESSAGE]
     * offset[INT] [THIS IS OFFET THAT WILL BE PASSED TO QUERY, to limit 10]
     * @return [type] [description]
     */
    public function index_post() {

        $ResponseData['welcome'] = $this->lang->line('WELCOME_MESSAGE');
        $this->response($ResponseData, 200);
    }

    //#################################################################
    // Name : getvenues
    // Purpose : To get all the venues 
    // In Params : void
    // Out params : all data of venues
    //#################################################################

    public function getvenues_post() {

        // global declaration.
        $ResponseData = array();

        //get request data
        $PostData = $this->post();
        //define initial offset
        $Offset = "-1";
      
        
        if (isset($PostData) && count($PostData) > 0) {

            //remove space from all the data
            $Params = array_map('trim', $PostData);
            $Params = array_map('urldecode', $PostData);

            // fetch venue detail from venues.
            $Venues = $this->venue_model->GetAllVenues($Params);

            if (isset($Venues) && $Venues['status'] == '1') {

                //remove all null key
                $Venues = $this->array_remove_empty($Venues);

                $ResponseData['success'] = "1";
                //ResponseData['offset'] = $Venues['offset'];
                $ResponseData['total_venues'] = $Venues['total_venues'];
                $ResponseData['data'] = $Venues['venues'];
            } else {
                $ResponseData['success'] = "0";
                $ResponseData['message'] = $this->lang->line('NO_VENUES');
            }
        } else {
            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->lang->line('INVALID_PARAMS');
        }
        $this->response($ResponseData, 200);
    }

    //################################################################# 
    // Name : array_remove_empty
    // Purpose : To remove all null values
    // In Params : array
    // Out params : all data of venues
    //#################################################################
    public function array_remove_empty($haystack) {
        
        foreach ($haystack as $key => $value) {
            if (is_array($value)) {
                $haystack[$key] = $this->array_remove_empty($haystack[$key]);
            }

            if (empty($haystack[$key]) && $haystack[$key] != '0') {
                $haystack[$key] = "";
            }
        }

        return $haystack;
    }

    //#################################################################
    // Name : getvenuesDetails
    // Purpose : To get all the venues details 
    // In Params : void
    // Out params : detail of venues
    //#################################################################

    public function venuedetails_post() {
        
        
        // global declaration.
        $ResponseData = array();

        //get request data
        $PostData = $this->post();

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from all the data
            $Params = array_map('trim', $PostData);
            $Params = array_map('urldecode', $PostData);

            // fetch venue detail from venues.
            $VenueDetail = $this->venue_model->VenueDetails($Params);

            //generate response
            if (isset($VenueDetail) && $VenueDetail['status'] == 1) {

                $ResponseData['success'] = "1";
                $ResponseData['data'] = $VenueDetail['venue_details'];

                if ($ResponseData['data']['vote_by_user'] == '') {
                    $ResponseData['data']['vote_by_user'] = '';
                }

                ## get check-in made by user or nor
                ## if user made check-in then check for the date and time of check-in
                ## if date and time is less then 3 hours then do not allow to check-in
                $GetCheckInDetails = $this->venue_model->GetCheckInDetails($Params);

                ## if checkin exist
                if (isset($GetCheckInDetails) && $GetCheckInDetails['status'] == '1') {
                    ## get current time
                    $timezone = "UTC";
                    date_default_timezone_set($timezone);

                    $CheckinDateTime = gmdate("Y-m-d h:i:s");

                    ## compare the last check-in time with current time
                    $LastCheckinTime = $GetCheckInDetails['checkin_details']['checkin_date'];

                    $TimeDff = ((strtotime($CheckinDateTime) - strtotime($LastCheckinTime)) / 3600);

                    ## if time different greater then 3 hours then only allow to check-in again
                    if (isset($TimeDff) && $TimeDff >= 3) {
                        $ResponseData['data']['checkin_by_user'] = "0";
                    } else {
                        $ResponseData['data']['checkin_by_user'] = "1";
                    }
                } else {
                    $ResponseData['data']['checkin_by_user'] = "0";
                }

                $ResponseData['data']['venue_images'] = $VenueDetail['venue_images'];
                $ResponseData['data']['looped_rooms'] = $VenueDetail['looped_rooms'];
            } else {

                $ResponseData['success'] = "0";
                $ResponseData['message'] = $this->lang->line('NO_VENUES');
            }
        } else {

            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->lang->line('INVALID_PARAMS');
        }
        $this->response($ResponseData, 200);
    }

    //#################################################################
    // Name : votes
    // Purpose : To vote up for requested venue
    // In Params : customer id,venue id,vote date
    // Out params : all data of categories
    //#################################################################
    public function votes_post() {

        //global declaration
        $ResponseData = array();

        //get data from request and process
        $PostData = $this->post();

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from all the data
            $Params = array_map('trim', $PostData);
            $Params = array_map('urldecode', $PostData);

            //generate utc date for checkin
            $timezone = "UTC";
            date_default_timezone_set($timezone);

            $VotesDateTime = gmdate("Y-m-d h:i:s");

            $Params['vote_date'] = $VotesDateTime;

            //check if user has already vote this venue or not
            //if user has already vote then just update record with vote up or down

            $CheckVoteExist = $this->venue_model->CheckVoteExist($Params);

            //if user has already vote then just update flag
            if (isset($CheckVoteExist) && $CheckVoteExist['status'] == '1') {

                //update vote
                $UpdateVotes = $this->venue_model->UpdateVote($Params);

                //check for return params
                if (isset($UpdateVotes) && $UpdateVotes['status'] == 1) {

                    //print success
                    $ResponseData['success'] = "1";
                    $ResponseData['message'] = $this->lang->line('SUCCESS_VOTES');
                } else {
                    //print error response
                    $ResponseData['success'] = "0";
                    $ResponseData['message'] = $this->lang->line('GENERAL_NO_CHANGES');
                }
            } else { //add new vote as up vote
                //add vote
                $Votes = $this->venue_model->VotesVenue($Params);

                //check for return params
                if (isset($Votes) && $Votes['status'] == 1) {

                    //print success
                    $ResponseData['success'] = "1";
                    $ResponseData['message'] = $this->lang->line('SUCCESS_VOTES');
                } else {
                    //print error response
                    $ResponseData['success'] = "0";
                    $ResponseData['message'] = $this->lang->line('GENERAL_NO_CHANGES');
                }
            }
        } else {

            //print error response
            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->lang->line('INVALID_PARAMS');
        }

        $this->response($ResponseData, 200);
    }

    //#################################################################
    // Name : Checkin
    // Purpose : To checkin for place
    // In Params : customer id,venue id,checkin date
    // Out params : all data of categories
    //#################################################################
    public function checkins_post() {
        //global declaration
        $ResponseData = array();

        //get data from request and process
        $PostData = $this->post();


        if (isset($PostData) && count($PostData) > 0) {

            //remove space from all the data
            $Params = array_map('trim', $PostData);
            $Params = array_map('urldecode', $PostData);

            //generate utc date for checkin
            $timezone = "UTC";
            date_default_timezone_set($timezone);

            $CheckinDateTime = gmdate("Y-m-d h:i:s");

            $Params['checkin_date'] = $CheckinDateTime;

            //add checkin
            $Checkin = $this->venue_model->CheckinUser($Params);

            //check for return params
            if (isset($Checkin) && $Checkin['status'] == 1) {

                //print success
                $ResponseData['success'] = "1";
                $ResponseData['message'] = $this->lang->line('SUCCESS_CHECKIN');
            } else {
                //print error response
                $ResponseData['success'] = "0";
                $ResponseData['message'] = $this->lang->line('GENERAL_ERROR');
            }
        } else {
            //print error response
            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->lang->line('INVALID_PARAMS');
        }

        $this->response($ResponseData, 200);
    }

    //#################################################################
    // Name : newrequest
    // Purpose : To request new venue
    // In Params : customer id,name,venue type(already loop/need new),state,city,comment
    // Out params : success/error response
    //#################################################################
    public function newrequest_post() {
        //global declaration
        $ResponseData = array();

        //get data from request and process
        $PostData = $this->post();


        if (isset($PostData) && count($PostData) > 0) {

            //remove space from all the data
            $Params = array_map('trim', $PostData);
            $Params = array_map('urldecode', $PostData);

            //generate utc date for checkin
            $timezone = "UTC";
            date_default_timezone_set($timezone);

            $RequestDateTime = gmdate("Y-m-d h:i:s");

            $Params['request_date'] = $RequestDateTime;

            //add checkin
            $RequestNewVenue = $this->venue_model->RequestNewVenue($Params);

            //check for return params
            if (isset($RequestNewVenue) && $RequestNewVenue['status'] == 1) {

                //print success
                $ResponseData['success'] = "1";
                $ResponseData['message'] = $this->lang->line('SUCCESS_REQUEST');
            } else {
                //print error response
                $ResponseData['success'] = "0";
                $ResponseData['message'] = $this->lang->line('GENERAL_ERROR');
            }
        } else {
            //print error response
            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->lang->line('INVALID_PARAMS');
        }

        $this->response($ResponseData, 200);
    }

    //#################################################################
    // Name : feedback
    // Purpose : To feedback for venue
    // In Params : customer id,venue id,loop room id,loop working(yes/no),loop not working reason,comment,feedback date
    // Out params : success/error response
    //#################################################################
    public function feedback_post() {
        //global declaration
        $ResponseData = array();

        //get data from request and process
        $PostData = $this->post();

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from all the data
            $Params = array_map('trim', $PostData);
            $Params = array_map('urldecode', $PostData);

            //generate utc date for checkin
            $timezone = "UTC";
            date_default_timezone_set($timezone);

            $FeedbackDateTime = gmdate("Y-m-d h:i:s");

            $Params['feedback_date'] = $FeedbackDateTime;

            //add checkin
            $RequestNewVenue = $this->venue_model->VenueFeedback($Params);

            //check for return params
            if (isset($RequestNewVenue) && $RequestNewVenue['status'] == 1) {

                //print success
                $ResponseData['success'] = "1";
                $ResponseData['message'] = $this->lang->line('SUCCESS_FEEDBACK');
            } else {
                //print error response
                $ResponseData['success'] = "0";
                $ResponseData['message'] = $this->lang->line('GENERAL_ERROR');
            }
        } else {
            //print error response
            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->lang->line('INVALID_PARAMS');
        }

        $this->response($ResponseData, 200);
    }

    //#################################################################
    // Name : getcity (Currently not using anywhere in web service)
    // Purpose : To get all cities names
    // In Params : void
    // Out params : Cities names
    //#################################################################

    public function getcity_post() {

        //get data from request and process
        $PostData = $this->post();

        $PostDataSend = array();
        if (isset($PostData) && count($PostData) > 0) {
            $PostDataSend = $PostData;
        } else {
            $PostDataSend['city'] = "";
        }

        $CityNameList = $this->venue_model->VenueCity($PostDataSend);


        if (isset($CityNameList) && $CityNameList['status'] == 1) {

            //print success
            $ResponseData['success'] = "1";
            $ResponseData['data'] = $CityNameList['city_name'];
        } else {
            //print error response
            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->lang->line('NO_CITY_NAME');
        }
        // } else {
        //print error response
        //   $ResponseData['success'] = "0";
        // $ResponseData['message'] = $this->lang->line('INVALID_PARAMS');
        // }
        $this->response($ResponseData, 200);
    }

    //#################################################################
    // Name : getvenuename
    // Purpose : To get all the venue name
    // In Params : void
    // Out params : Venue listing
    //#################################################################

    public function getvenuename_post() {

        //get data from request and process
        $PostData = $this->post();
        $PostDataSend = array();

        //fetch venue name 
        $VenueNameList = $this->venue_model->GetVenueName();

        if (isset($VenueNameList) && $VenueNameList['status'] == 1) {

            //print success
            $ResponseData['success'] = "1";
            $ResponseData['data'] = $VenueNameList['venues'];
        } else {
            //print error response
            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->lang->line('NO_VENUES');
        }

        $this->response($ResponseData, 200);
    }

    //#################################################################
    // Name : getNearbyVenues
    // Purpose : To get all near by venues
    // In Params : latitude and longitude
    // Out params : venue name, categories, looped room, image.
    //#################################################################

    public function getNearbyVenues_post() {
        //global declaration
        $ResponseData = array();

        //get data from request and process
        $PostData = $this->post();

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from all the data
            $Params = array_map('trim', $PostData);
            $Params = array_map('urldecode', $PostData);

            $VenueDetail = $this->venue_model->NearByVenues($Params);


            //generate response
            if (isset($VenueDetail) && $VenueDetail['status'] == 1) {

                foreach ($VenueDetail['venue_details']as $key => $val) {
                    $Looped_room_info = array();
                    $array2 = explode(",", $val['looped_rooms_id']);
                    $array3 = explode(",", $val['looped_rooms']);
                    foreach ($array2 as $id => $all_ids) {
                        $VenueDetail['venue_details'][$key]['Looped_room_info'][$id]['id'] = $all_ids;
                        $VenueDetail['venue_details'][$key]['Looped_room_info'][$id]['name'] = $array3[$id];
                    }
                }
                $ResponseData['success'] = "1";
                $ResponseData['data'] = $VenueDetail['venue_details'];
            } else {

                $ResponseData['success'] = "0";
                $ResponseData['message'] = $this->lang->line('NO_VENUES');
            }
        } else {
            //print error response
            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->lang->line('INVALID_PARAMS');
        }

        $this->response($ResponseData, 200);
    }

}

/* End of file customer.php */
/* Location: ./application/api/controllers/customer.php */