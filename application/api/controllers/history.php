<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class History extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('history_model');
    }

    /**
     * [index_post FOR WELCOME MESSAGE]
     * offset[INT] [THIS IS OFFET THAT WILL BE PASSED TO QUERY, to limit 10]
     * @return [type] [description]
     */
    public function index_post() {

        $ResponseData['welcome'] = $this->lang->line('WELCOME_MESSAGE');
        $this->response($ResponseData, 200);
    }

    //###########################################################
    //Function : checkins
    //purpose : To get all the checkins made by user
    //input : customer id,offset
    //outpur : success/error,checkin data
    //###########################################################
    public function checkins_post() {

        //global declaration
        $ResponseData = array();

        //get data from request and process
        $PostData = $this->post();

        //define initial offset
        $Offset = "-1";

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from all the data
            $Params = array_map('trim', $PostData);
            $Params = array_map('urldecode', $PostData);

            //add customer
            $Checkins = $this->history_model->GetCheckins($Params);

            //check for return params
            if (isset($Checkins) && $Checkins['status'] == 1) {

                //print success
                $ResponseData['success'] = "1";
                $ResponseData['offset'] = $Checkins['offset'];
                $ResponseData['data'] = $Checkins['checkin_data'];
            } else {
                //print error response
                $ResponseData['success'] = "0";
                $ResponseData['message'] = $this->lang->line('NO_CHECKIN');
            }
        } else {
            //print error response
            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->lang->line('INVALID_PARAMS');
        }

        $this->response($ResponseData, 200);
    }

    //###########################################################
    //Function : notifications
    //purpose : To get all the notification made by user
    //input : customer id,offset
    //outpur : success/error,checkin data
    //###########################################################
    public function notifications_post() {

        //global declaration
        $ResponseData = array();

        //get data from request and process
        $PostData = $this->post();

        //define initial offset
        $Offset = "-1";

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from all the data
            $Params = array_map('trim', $PostData);
            $Params = array_map('urldecode', $PostData);

            //add customer
            $Notifications = $this->history_model->GetNotifications($Params);

            //check for return params
            if (isset($Notifications) && $Notifications['status'] == 1) {

                //print success
                $ResponseData['success'] = "1";
                $ResponseData['offset'] = $Notifications['offset'];
                $ResponseData['data'] = $Notifications['notification_data'];
            } else {
                //print error response
                $ResponseData['success'] = "0";
                $ResponseData['message'] = $this->lang->line('NO_NOTIFICATION');
            }
        } else {
            //print error response
            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->lang->line('INVALID_PARAMS');
        }

        $this->response($ResponseData, 200);
    }

    //###########################################################
    //Function : updatenotification
    //purpose : To update notification status
    //input : notification id, read_status
    //outpur : success/error
    //###########################################################

    public function updatenotification_post() {

        //global declaration
        $ResponseData = array();

        //get data from request and process
        $PostData = $this->post();

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from all the data
            $Params = array_map('trim', $PostData);

            //call function to update locatioan
            $UpdateNotification = $this->history_model->UpdateNotification($Params);

            $ResponseData['success'] = "1";
            $ResponseData['message'] = $this->lang->line('GENERAL_SUCCESS');
        } else {
            //print error response
            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->lang->line('INVALID_PARAMS');
        }
        $this->response($ResponseData, 200);
    }

    public function sendnotifications_post() {
        $this->load->library('apn');
        $this->apn->payloadMethod = 'enhance'; // you can turn on this method for debuggin purpose
        $this->apn->connectToPush();

        // adding custom variables to the notification
        $this->apn->setData(array('someKey' => true));

        $send_result = $this->apn->sendMessage('860dd52a3fff54453ea24751778893da0beb6c3508a950169182b3a995e41b74', 'Test notif #1 (TIME:' . date('H:i:s') . ')', /* badge */ 2, /* sound */ 'default');

        if ($send_result)
            log_message('debug', 'Sending successful');
        else
            log_message('error', $this->apn->error);


        $this->apn->disconnectPush();
    }

}

/* End of file customer.php */
/* Location: ./application/api/controllers/customer.php */