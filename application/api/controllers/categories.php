<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Categories extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('category_model');
    }

    /**
     * [index_post FOR WELCOME MESSAGE]
     * offset[INT] [THIS IS OFFET THAT WILL BE PASSED TO QUERY, to limit 10]
     * @return [type] [description]
     */
    public function index_post() {

        $ResponseData['welcome'] = $this->lang->line('WELCOME_MESSAGE');
        $this->response($ResponseData, 200);
    }

    //###########################################################
    //Function : getcategories
    //purpose : To get all the categories
    //input : void
    //outpur : success/error,categories data
    //###########################################################
    public function getcategories_post() {

        //global declaration
        $ResponseData = array();

        //get data from request and process
        $PostData = $this->post();

        //define initial offset
        $Offset = "-1";

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from all the data
            $Params = array_map('trim', $PostData);
            $Params = array_map('urldecode', $PostData);

            //add customer
            $Categories = $this->category_model->GetCategories($Params);

            //check for return params
            if (isset($Categories) && $Categories['status'] == 1) {

                //print success
                $ResponseData['success'] = "1";
                $ResponseData['offset'] = $Categories['offset'];
                $ResponseData['data'] = $Categories['category_data'];
            } else {
                //print error response
                $ResponseData['success'] = "0";
                $ResponseData['message'] = $this->lang->line('GENERAL_ERROR');
            }
        } else {
            //print error response
            $ResponseData['success'] = "0";
            $ResponseData['message'] = $this->lang->line('INVALID_PARAMS');
        }

        $this->response($ResponseData, 200);
    }
     //###########################################################
    //Function : getallcategories
    //purpose : To get all the categories
    //input : void
    //outpur : success/error,categories data
    //###########################################################
    
    
    public function getallcategories_post()
    {
        
            //global declarattion
            $ResponseData = array();
        
            $getAllCategoriesData= $this->category_model->getAllCategories();
            
            if(isset($getAllCategoriesData) && $getAllCategoriesData['status'] == 1)
            {
                //mprd($getAllCategoriesData);
                //print success messsge
                $ResponseData['success'] = "1";
                $ResponseData['data'] = $getAllCategoriesData['category_data']['category_array'];
                $ResponseData['other_category'] = $getAllCategoriesData['category_data']['static_array'];
            }else {
                //print error response
                $ResponseData['success'] = "0";
                $ResponseData['message'] = $this->lang->line('GENERAL_ERROR');
            }
            $this->response($ResponseData, 200);
        
    }

}

/* End of file customer.php */
/* Location: ./application/api/controllers/customer.php */