<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

define('_PATH', substr(dirname(__FILE__), 0, -25));

define('_URL', substr($_SERVER['PHP_SELF'], 0, - (strlen($_SERVER['SCRIPT_FILENAME']) - strlen(_PATH))));

define('SITE_PATH', _PATH . "/");
define('SITE_URL', _URL . "/");

define('WEBSITE_URL', SITE_URL . '/');

define('DOMAIN_URL', 'http://' . $_SERVER['SERVER_NAME'] . '/otojoy/');
define('DOC_ROOT', $_SERVER['DOCUMENT_ROOT'] . '/otojoy/');

define('BASEURL', DOMAIN_URL . 'api/');
define('gmapkey', '');

define('MAINTITLE', 'NyOnAir : Sales App');
//define('ADMIN_EMAIL', 'info@otojoy.com');
define('ADMIN_EMAIL', 'bhargav@creolestudios.com');

define('ADMIN_URL', SITE_URL);
define('ADMIN_PATH', SITE_PATH);

// URLS FOR FILE UPLOADED
define('UPLOADS', DOC_ROOT . "uploads/");
define('PROFILER_PATH', DOC_ROOT . "logs/");
define('UPLOADS_URL', DOMAIN_URL . "uploads/");
define('UPLOADS_FILES', UPLOADS_URL . "files/");
define('INDEX_CONSTANT', DOC_ROOT . "uploads/index.php");
define('IMAGE_URL', DOMAIN_URL . "assets/images/");
define('VENUE_PAGE_LIMIT','10');
define('CHAPTER_PAGE_LIMIT','10');
define('CHECKIN_PAGE_LIMIT','10');
define('NOTIFICATION_PAGE_LIMIT','20');
define('CATEGORY_PAGE_LIMIT','10');
define('NEAR_BY_DISTANCE','1');

/*
############# SET DEFAULT IMAGES #################
*/

/* End of file constants.php */
/* Location: ./application/config/constants.php */