<?php
$lang['LBL'] = 'label';
//################## General Message Section ###############################
$lang['NO_CHANGES_MADE'] = 'You have made no changes.';
$lang['GENERAL_ERROR'] = 'Oops! Something just went wrong. Please try again later.';
//###################### Customer section #################################
$lang['STATUS_CHANGE_ACTIVE'] = 'Status is changed to active.';
$lang['STATUS_CHANGE_INACTIVE'] = 'Status is changed to inactive.';
$lang['SUCCESS_CUSTOMER_DELETE'] = 'Customer deleted successfully.';
$lang['CUSTOMER_UPDATE_SUCCESS'] = 'Customer details updated successfully';
$lang['SUCCESS_FORGOT_PASSWORD'] = 'Password has been reset. New password has been emailed to the user.';
$lang['ERROR_FORGOT_PASSWORD'] = 'Oops! Something just went wrong. Please try again later.';

//###################### Category section #################################
$lang['SUCCESS_CATEGORY_CREATE'] = 'Category created successfully.';
$lang['SUCCESS_CATEGORY_UPDATE'] = 'Category updated successfully.';
$lang['SUCCESS_CATEGORY_DELETE'] = 'Category deleted successfully.';

//###################### Venue section #################################
$lang['SUCCESS_VENUE_CREATE'] = 'Venue created successfully.';
$lang['SUCCESS_VENUE_UPDATE'] = 'Venue updated successfully.';
$lang['SUCCESS_VENUE_ROOM_DELETE'] = 'Looped room deleted successfully.';
$lang['SUCCESS_VENUE_IMAGE_DELETE'] = 'Image deleted successfully.';
$lang['SUCCESS_VENUE_DELETE'] = 'Venue deleted successfully.';
$lang['SUCCESS_VENUE_VERIFY'] = 'Looped room verified successfully.';
$lang['SUCCESS_VENUE_VERIFY_ALREADY'] = 'Looped room is already verified for today\'s date.';
$lang['SUCCESS_SET_IMAGE'] = 'Default image updated successfully.';
$lang['SUCCESS_VENUE_REGISTER'] = 'Venue registered successfully.';
$lang['SUCCESS_LOOP_ROOM_UPDATE'] = 'Looped room updated successfully.';
$lang['SUCCESS_CSV_IMPORT'] = 'CSV imported successfully.';

//###################### User section #################################
$lang['SUCCESS_USER_UPDATE'] = 'User updated successfully.';
$lang['SUCCESS_USER_DELETE'] = 'User deleted successfully.';

//###################### Request section #################################
$lang['SUCCESS_REQUEST_DELETE'] = 'Request deleted successfully.';
$lang['SUCCESS_REQUEST_ARCHIV'] = 'Request successfully archived.';
$lang['SUCCESS_REQUEST_READ'] = 'Request successfully unarchived.';

//###################### Feedback section #################################
$lang['SUCCESS_FEEDBACK_DELETE'] = 'Feedback deleted successfully.';
$lang['SUCCESS_FEEDBACK_ARCHIV'] = 'Feedback successfully archived.';
$lang['SUCCESS_FEEDBACK_READ'] = 'Feedback successfully unarchived.';

//###################### Notification section #################################
$lang['NEW_REGISTERED_VENUE'] = 'A new loop has been added in your area: {VENUE_NAME}.';
$lang['NEW_REQUESTED_VENUE'] = 'A new loop has been requested in your area: {VENUE_NAME}.';

//###################### Hlaa Chapters section #################################

$lang['SUCCESS_CHAPTERS_CREATE'] = 'Chapter created successfully.';
$lang['SUCCESS_CHAPTERS_UPDATE'] = 'Chapter updated successfully.';
$lang['SUCCESS_CHAPTER_DELETE'] = 'Chapter deleted successfully.';
$lang['SUCCESS_CHAPTER_UPDATE'] = 'Chapter updated successfully.';

