<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//require_once(DOCROOT."classes/libraries/user_agent.class.php");
class CI_Headerlib {

    public $title;
    public $body_id;
    public $body_function;
    public $iphone_css;
    public $safari_css;
    public $opera_css;
    public $icon_path;
    public $css_path;
    public $javascripts;
    public $javascript_path;
    public $stylesheets;
    public $javascript_plugins;
    public $login_javascripts;
    public $plugins;
    public $http_meta_tags;
    public $content_meta_tags;
    public $keywords;
    public $doctype;
    //public $user_agent;

    public $config;

    public function __construct() {

        $this->doctype = 'XHTML1.1'; // Set the Doctype definition
        $this->title = MAINTITLE; // Set the default page title
        $this->body_id = "mainbody"; // Set the default body id (leave blank for no id)
        $this->icon_path = ''; // Set default icon path for iPhone relative BASEURL.
        $this->css_path = CSS_URL; // Set default path to browser specific css files relative to BASEURL.
        $this->plugin_path = PLUGIN_URL; // Set default path to browser specific css files relative to BASEURL.
        $this->safari_css = TRUE; // Safari specific stylesheet
        $this->opera_css = FALSE; // Opera specific stylesheet
        $this->iphone_css = TRUE; // iPhone specific stylesheet
        $this->stylesheets = array();
        $this->plugins = array(); // Set a default stylesheet        
        $this->javascript_path = JS_URL; // Set path to javascripts
        $this->external_js = array('jquery203' => '//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js',
            "jQueryUI1.10.3" => "//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js",
            );
        // $this->external_js = array();
        // $this->javascripts = array('jquery-203minjs'=>'jquery-2.0.3.min.js',
        //  									'jquery-1102minjs'=>'jquery-1.10.2.min.js');
        $this->javascripts = array();
        $this->login_javascripts = array();
        $this->javascript_plugins = array(
            "bootstrap" => "bootstrap/js/bootstrap.min.js",
            "slimscroll" => "jquery-slimscroll/jquery.slimscroll.min.js",
            "blockui" => "jquery.blockui.min.js",
            "cookie" => "jquery.cokie.min.js",
            "uniform" => "uniform/jquery.uniform.min.js",
            "migrate"=>"jquery-migrate.min.js"
        );

        //$this->user_agent 			= new User_agent();
        $this->keywords = '';
        $this->http_meta_tags = array();
        $this->content_meta_tags = array('description' => '',
            'msapplication-TileColor' => '#5bc0de',
            'msapplication-TileImage' => IMAGE_PATH . 'metis-tile.png',
            'viewport' => 'width=device-width,initial-scale=1,user-scalable=no,maximum-scale=1'
        );
        //$this->config = $GLOBALS['config'];
        
        $this->golbalmandatory = array(
            "font-awesome"=>"font-awesome/css/font-awesome.min.css",
            "simple-line-icon"=>"simple-line-icons/simple-line-icons.min.css",
            "bootstrap"=>"bootstrap/css/bootstrap.min.css",
            "uniform-default"=>"uniform/css/uniform.default.css"
            
        ); //set global mandatory style sheet
        
        $this->themestyles = array(
            "component-round"=>"components-rounded.css",
            "plugins"=>"plugins.css",
            "layout"=>"layout.css",
            "theme-default"=>"themes/grey.css",
            "custom"=>"custom.css"
            
        ); //set theme style
    }

// End __construct function

    public function set_page_info($title, $body_id) {
        $this->title = $title;
        $this->body_id = ' id="' . $body_id . '" ';
    }

// End set_page_info function

    public function set_title($title) {
        $this->title = $title;
    }

// End set_title function

    public function set_body_id($body_id) {
        $this->body_id = ' id="' . $body_id . '"';
    }

// End set_body_id function

    public function add_stylesheet($name, $file) {
        $this->stylesheets[$name] = $file;
    }
    
    //add global mandatory styles
    public function add_themestyles($name, $file) {
        $this->themestyles[$name] = $file;
    }
    
    //add global mandatory styles
    public function add_golbalmandatory($name, $file) {
        $this->golbalmandatory[$name] = $file;
    }

// End add_stylesheet function

    public function add_plugin($name, $file) {
        $this->plugins[$name] = $file;
    }

// End add_plugins function

    public function add_javascript($name, $file) {
        $this->javascripts[$name] = $file;
    }

// End add_javascript function

    public function add_login_javascripts($name, $file) {
        $this->login_javascripts[$name] = $file;
    }

// End add_javascript function

    public function add_javascript_plugins($name, $file) {
        $this->javascript_plugins[$name] = $file;
    }

// End add_javascript_plugins function

    public function add_meta_tag($name, $content) {
        $this->meta_tags[$name] = $content;
    }

// End add_meta_tag function

    public function data() {
        $data['doctype'] = $this->_doctype();
        $data['meta_tags'] = $this->_meta_tags();
        $data['title'] = $this->title;
        $data['body_id'] = $this->body_id;
        $data['stylesheets'] = $this->_stylesheets();
        $data['golbalmandatory'] = $this->_golbalmandatory();        
        $data['themestyles'] = $this->_themestyles();
        $data['plugins'] = $this->_plugins();
        $data['javascript'] = $this->_javascript();
        $data['javascript_plugins'] = $this->_javascript_plugins();
        $data['login_javascripts'] = $this->_login_javascripts();
        $data['iejavascript'] = $this->_iejavascript();
        $data['gmap'] = $this->_gmap();
        $data['favicon'] = $this->_favicon();

        return $data;
    }

// End data function

    private function _doctype() {
        switch ($this->doctype) {
            case 'Strict':
                return '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"' . "\n" . '"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">' . "\n";
                break;
            case 'Transitional':
                return '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"' . "\n" . '"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">' . "\n";
                break;
            case 'Frameset':
                return '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN"' . "\n" . '"http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">' . "\n";
            case 'XHTML':
                return '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"' . "\n" . '"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">' . "\n";
            case 'XHTML1.0':
                return '<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">' . "\n";
            case 'XHTML1.1':
                return '<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">' . "\n";
        }
    }

// End _doctype function

    private function _meta_tags() {
        $meta_tags = "<meta charset='utf-8'/>\n";
        foreach ($this->http_meta_tags as $http => $content) {
            $meta_tags .= '<meta http-equiv="' . $http . '" content="' . $content . '"/>' . "\n";
        }
        foreach ($this->content_meta_tags as $name => $content) {
            $meta_tags .= '<meta name="' . $name . '" content="' . $content . '"/>' . "\n";
        }
        return $meta_tags;
    }

// End _meta_tags function

    private function _stylesheets() {
        $stylesheets = "";
        //$this->_browser_specific_stylesheet();
        foreach ($this->stylesheets as $name => $file) {
            $stylesheets .= '<link rel="stylesheet" href="' . $this->css_path . $file . '" type="text/css" media="screen" />' . "\n";
        }
        return $stylesheets;
    }
    
    private function _golbalmandatory() {
        $stylesheets = "";
        foreach ($this->golbalmandatory as $name => $file) {
            $stylesheets .= '<link rel="stylesheet" href="' . $this->plugin_path . $file . '" type="text/css" media="screen" />' . "\n";
        }
        
        return $stylesheets;
    }
    
    private function _themestyles() {
        $stylesheets = "";
        foreach ($this->themestyles as $name => $file) {
            $stylesheets .= '<link rel="stylesheet" href="' . $this->css_path . $file . '" type="text/css" media="screen" />' . "\n";
        }
        //mprd($stylesheets);
        return $stylesheets;
    }
    
/// End _stylesheets function

    private function _plugins() {
        $plugins = "";
        //$this->_browser_specific_stylesheet();
        foreach ($this->plugins as $name => $file) {
            $plugins .= '<link rel="stylesheet" href="' . $this->plugin_path . $file . '" type="text/css" media="screen" />' . "\n";
        }
        return $plugins;
    }

/// End PLUGINS function

    private function _javascript_plugins() {
        $javascript_plugins = "";
        //$this->_browser_specific_stylesheet();
        foreach ($this->javascript_plugins as $name => $file) {

            $javascript_plugins .= '<script src="' . $this->plugin_path . $file . '" type="text/javascript" charset="utf-8"></script>' . "\n";
        }

        return $javascript_plugins;
    }

/// End JAVASCRIPT PLUGINS function

    private function _login_javascripts() {
        $login_javascripts = "\n";
        //$this->_browser_specific_stylesheet();
        if (count($this->external_js) > 0) {
            foreach ($this->external_js as $name => $url) {
                $login_javascripts .= '<script type="text/javascript" id="' . $name . '" src="' . $url . '"></script>' . "\n";
            }
        }
        $login_javascripts .= '<script type="text/javascript">var BASEURL = "' . BASEURL . '"; </script>' . "\n";
        foreach ($this->login_javascripts as $name => $file) {
            $login_javascripts .= '<script src="' . $this->javascript_path . $file . '" type="text/javascript" charset="utf-8"></script>' . "\n";
        }
        return $login_javascripts;
    }

/// End BOTTOM JAVASCRIPT function

    private function _iejavascript() {
        $javascript_content = "\n";
        $javascript_content .= '<!--[if lte IE 7]>
									 <script src="http://ie7-js.googlecode.com/svn/version/2.0(beta3)/IE8.js" type="text/javascript">
									 </script>
									 <script src="http://ie7-js.googlecode.com/svn/version/2.0(beta3)/IE7-squish.js"  type="text/javascript">
									 </script>
									<![endif]-->';
        return $javascript_content;
    }

    private function _gmap() {
        $javascript_content = "\n";

        $javascript_content .= '<script id="gmapscript" src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=false&amp;key=' . gmapkey . '" type="text/javascript"></script>';
        return $javascript_content;
    }

    private function _javascript() {
        $javascript_content = "\n";

        if (count($this->external_js) > 0) {
            foreach ($this->external_js as $name => $url) {
                $javascript_content .= '<script type="text/javascript" id="' . $name . '" src="' . $url . '"></script>' . "\n";
            }
        }
        $javascript_content .= '<script type="text/javascript">var BASEURL = "' . BASEURL . '"; </script>' . "\n";
        foreach ($this->javascripts as $library => $file) {
            $javascript_content .= '<script src="' . $this->javascript_path . $file . '" type="text/javascript" charset="utf-8"></script>' . "\n";
        }
        return $javascript_content;
    }

    private function _favicon() {
        $favicon = '<link rel="shortcut icon" type="image/x-icon"  href="' . $this->icon_path . 'favicon.ico" />';
        return $favicon;
    }

// End _favicon function

    public function debug() {
        $data = $this->data();
        $info = "";
        foreach ($data as $key => $value) {
            $info .= $key . ' - ' . htmlentities($value) . "<br />";
        }
        return $info;
    }

// End debug function
}

// End Header class.
?>
