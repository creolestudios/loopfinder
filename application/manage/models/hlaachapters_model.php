<?php

/*
 * @category   Hlaa Chapters  Model
 * @package    Database activity for category
 * @author     Jignesh Virani <jignesh@creolestudios.com>
 * @author     Another Author <another@example.com>
 * @copyright  2014 Creole Studios
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    Release: 1.0 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class hlaachapters_model extends CI_Model {

    var $table;

    function __construct() {
        parent::__construct();
        $this->load->helper('cookie');
        $this->hlla = 'hlaa';
        $this->hlaa_images = 'hlaa_images';
        $this->venues = 'venues';
        $this->venue_images = 'venue_images';
    }

    //###########################################################
    //function : GetAllVenues
    //To get all venue
    //Input : void
    //Output : details of all venue
    //###########################################################

    public function GetAllChapters() {
        
    }

    //###########################################################
    //function : CountChapter
    //To get count venue
    //Input : void
    //Output : count of chapter 
    //###########################################################

    public function CountChapter() {

        $this->db->select("COUNT(v.id)as count", false);
        $this->db->from("$this->venues as v");
        $this->db->where("v.venue_type", "3");
        $GetChapterCounnt = $this->db->get();

        if ($GetChapterCounnt->num_rows() > 0) {
            //fetch the data
            $ChapterData = $GetChapterCounnt->result_array();
            $ReturnData['status'] = '1';
            $ReturnData['count'] = $ChapterData[0];
        }
        return $ReturnData;
    }

    //#################################################################
    // Name : CreateChapter
    // Purpose : To chapters venue
    // In Params : All chapters data
    // Out params : success/error message with status
    //#################################################################  
    public function CreateChapter($Params) {

        //initialize data
        $ReturnData = array();
      //  mprd($Params);
        //extract params
        extract($Params);
        $date = date('Y-m-d H:i:s');
        //  print_r($meetup_date);exit;
        //prepare array for insertion
        $InsertArray = array(
            'name' => $chapter_name,
            'address' => $address,
            'zipcode' => $zipcode,
            'state' => $state,
            'city' => $city,
            'phone_number' => $phone_number,
            'website' => $website,
            'email' => $email,
            'internal_contact' => $internal_contact,
            'internal_contact_ext' => $internal_contact_ext,
            'internal_email' => $internal_email,
            'internal_phone_number' => $internal_phone_number,
            'internal_comment' => $internal_comment,
            'public_comment' => $public_comment,
            'latitude' => $hdlatitude,
            'longitude' => $hdlongitude,
            'meetupdate' => date('Y-m-d H:i:s', strtotime($meetup_date)),
            'certified_date' => '',
            'status' => '1',
            'venue_type' => '3',
            'is_certified' => '2',
            'created_date' => $date
        );
       // print_r($InsertArray);exit;
        //insert customer data
        $AddChapters = $this->db->insert($this->venues, $InsertArray);

        //check it insertion in success or not
        if ($this->db->affected_rows() > 0) {
            $ReturnData['status'] = '1';
            $ReturnData['chapter_id'] = $this->db->insert_id();
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //###########################################################
    //function : SaveChaptersImages
    //To save venue images
    //Input : images
    //Output : customer data
    //########################################################### 
    public function SaveChaptersImages($Images) {
        //run query to add data in database
        $this->db->query("insert into venue_images(venue_id,image) values " . $Images);

        if ($this->db->affected_rows() > 0) {
            $ReturnData['status'] = '1';
        } else {
            $ReturnData['status'] = '0';
        }
        return $ReturnData;
    }

    //#################################################################
    // Name : SetDefaultImage
    // Purpose : To verify room 
    // In Params : room id
    // Out params : success/error message with status
    //################################################################# 
    public function SetVenueDefaultImage($ChapterId, $Image) {
        //initialize data
        $ReturnData = array();

        //prepare array to update
        $UpdateArray = array('venue_image' => $Image);
       
        //update the customer
        $UpdateStatus = $this->db->update($this->venues, $UpdateArray, array("id " => $ChapterId));
       
        if ($UpdateStatus > 0) {
            $ReturnData['status'] = '1';
            $ReturnData['message'] = $this->lang->line('SUCCESS_SET_IMAGE');
        } else {
            $ReturnData['status'] = '0';
            $ReturnData['message'] = $this->lang->line('GENERAL_ERROR');
            
        }
     
        return $ReturnData;
    }

    //###########################################################
    //function : UpdateCoverImage
    //To update cover images
    //Input : images
    //Output : customer data
    //###########################################################    
    public function UpdateCoverImage($CreatedChapterId, $CoverImage) {

        //initialize data
        $ReturnData = array();

        //prepare array to update
        $UpdateArray = array('venue_image' => $CoverImage);

        //update the customer
        $UpdateStatus = $this->db->update($this->venues, $UpdateArray, array("id" => $CreatedChapterId));

        if ($UpdateStatus > 0) {
            $ReturnData['status'] = '1';
        } else {
            $ReturnData['status'] = '0';
        }
        return $ReturnData;
    }

    //#################################################################
    // Name : CreateVenueCategory
    // Purpose : To create venue category
    // In Params : Category data
    // Out params : success/error message with status
    //#################################################################  
    public function CreateChapterCategory($CategoryVal) {

        //initialize data
        $ReturnData = array();

        //add category to table
        $this->db->query('INSERT INTO venue_categories(venue_id,category_id) VALUES ' . $CategoryVal);

        if ($this->db->affected_rows() > 0) {
            $ReturnData['status'] = '1';
        } else {
            $ReturnData['status'] = '0';
        }
        return $ReturnData;
    }

    public function GetChaptersDetails($ChapterId) {

        //initialize return data
        $ReturnData = array();
        //set image path for venue
        $ChapterImagePath = UPLOADS_URL . 'venues/';

        $this->db->select("v.id as chapter_id,v.name as chapter_name,v.address,if(v.zipcode != '',LPAD(v.zipcode, 5, '0'),v.zipcode)as zipcode,v.city,v.state,v.venue_image,v.phone_number,v.website,v.email,DATE_FORMAT(v.meetupdate,'%d %b %Y %h:%i %p')as meetupdate, v.created_date, v.latitude,v.longitude, v.internal_contact,v.internal_contact_ext,v.internal_email,v.internal_phone_number,v.internal_comment,v.public_comment", false);
        $this->db->from("$this->venues as v");
        $this->db->where("v.id", $ChapterId);

        $GetChapterDetails = $this->db->get();

        if ($GetChapterDetails->num_rows() > 0) {

            //fetch the data
            $ChapterData = $GetChapterDetails->result_array();

            //##################### Fetch Image Data ###################################
            //get venue image data if venue is available
            $this->db->select("vi.id as chapter_image_id,vi.image as original_image,CONCAT('" . $ChapterImagePath . "','" . $ChapterId . "','/',vi.image) as chapter_image,CONCAT('" . $ChapterImagePath . "','" . $ChapterId . "','/thumb/',vi.image) as chapter_image_thumb", false);
            $this->db->from("venue_images as vi");
            $this->db->where("vi.venue_id", $ChapterId);

            $GetChapterImages = $this->db->get();

            $ReturnData['status'] = '1';
            $ReturnData['chapter_data'] = $ChapterData[0];
            if ($GetChapterImages->num_rows() > 0) {
                //add image data to response
                $ReturnData['chapter_images'] = $GetChapterImages->result_array();
            }
        } else {
            $ReturnData['status'] = '0';
        }
        return $ReturnData;
    }

    //###########################################################
    //function : DeleteChapter
    //To delete venue
    //Input : venue id
    //Output : success/error response
    //###########################################################   
    public function DeleteChapter($ChapterId) {

        $this->db->delete($this->venues, array('id' => $ChapterId));

        $ReturnData['status'] = "1";
        $ReturnData['message'] = $this->lang->line('SUCCESS_CHAPTER_DELETE');

        return $ReturnData;
    }

    //#################################################################
    // Name : UpdateChapter
    // Purpose : To update chapter
    // In Params : All Chapter data
    // Out params : success/error message with status
    //#################################################################  
    public function UpdateChapter($Params) {

        //initialize data
        $ReturnData = array();

        //extract params
        extract($Params);
        
        //prepare array for insertion
        $UpdateArray = array(
            'name' => $chapter_name,
            'address' => $address,
            'zipcode' => $zipcode,
            'state' => $state,
            'city' => $city,
            'phone_number' => $phone_number,
            'website' => $website,
            'email' => $email,
            'meetupdate' => date('Y-m-d', strtotime($meetup_date)),
            'certified_date' => '',
            'internal_contact' => $internal_contact,
            'internal_contact_ext' => $internal_contact_ext,
            'internal_email' => $internal_email,
            'internal_phone_number' => $internal_phone_number,
            'internal_comment' => $internal_comment,
            'public_comment' => $public_comment,
            'latitude' => $hdlatitude,
            'longitude' => $hdlongitude,
            'status' => '1',
            'venue_type' => '3',
            'modified_date' => date('Y-m-d', strtotime($certified_date))
        );

        //insert customer data
        $AddVenue = $this->db->update($this->venues, $UpdateArray, array("id " => $chapter_id));
        //check it insertion in success or not
        $ReturnData['status'] = '1';

        return $ReturnData;
    }

    //###########################################################
    //function : DeleteVenueImage
    //To delete venue image
    //Input : image id
    //Output : success/error response
    //###########################################################   
    public function DeleteChapterImage($ImageId) {

        $this->db->delete($this->venue_images, array('id' => $ImageId));

        $ReturnData['status'] = "1";
        $ReturnData['message'] = $this->lang->line('SUCCESS_VENUE_IMAGE_DELETE');

        return $ReturnData;
    }

    //#################################################################
    // Name : CreateChaptersCategory
    // Purpose : To create venue category
    // In Params : Category data
    // Out params : success/error message with status
    //#################################################################  
    public function CreateChaptersCategory($CreatedChapterId) {
        
        //initialize data
        $ReturnData = array();
        
        //add category to table
        $this->db->query("INSERT INTO venue_categories(venue_id,category_id) VALUES ('" . $CreatedChapterId . "','21')");

        if ($this->db->affected_rows() > 0) {
            $ReturnData['status'] = '1';
        } else {
            $ReturnData['status'] = '0';
        }
        
        return $ReturnData;
    }
    
    //#################################################################
    // Name : CheckChapterExist
    // Purpose : To check it chapter is exist or not
    // In Params : chapter name and id at edit time
    // Out params : success/error message with status
    //#################################################################
    public function CheckChapterExist($Params) {

        //extract data
        extract($Params);

        $this->db->select('name');
        $this->db->from($this->venues);
        $this->db->where("name", $chapter_name);

        if (isset($chapter_id) && $chapter_id != '') {
            $this->db->where("id != $chapter_id");
        }

        $GetChapterQuery = $this->db->get();

        if ($GetChapterQuery->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

}
