<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class General_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->helper('csv');
        $this->content = 'content';
    }

    //************************************************************
    //Error Or success Msg View
    //************************************************************
    function getMessages() {
        if ($this->session->flashdata('ERROR') && array_count_values($this->session->flashdata('ERROR')) > 0)
            return $this->load->view('messages/error_view');
        else if ($this->session->flashdata('SUCCESS') && array_count_values($this->session->flashdata('SUCCESS')) > 0)
            return $this->load->view('messages/success_view');
    }

    //************************************************************
    //Check for Super Admin(If needed)
    //************************************************************
    function isSuperAdmin() {
        if ($this->session->userdata('ADMINTYPE') && $this->session->userdata('ADMINTYPE') == 'super')
            return 1;
        return 0;
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

    //************************************************************
    //If not find any record
    //************************************************************

    function noRecordsHere() {
        echo '<div class="alert i_magnifying_glass yellow"><strong>Opps!!&nbsp;&nbsp;:</strong>&nbsp;&nbsp;No Records available here.</div>';
    }

    //************************************************************
    //Truncatting string
    //************************************************************
    function myTruncate($string, $limit, $break = ".", $pad = ".") { // return with no change if string is shorter than $limit
        if (strlen($string) <= $limit)
            return $string; // is $break present between $limit and the end of the string?
        if (false !== ($breakpoint = strpos($string, $break, $limit))) {
            if ($breakpoint < strlen($string) - 1) {
                $string = substr($string, 0, $breakpoint) . $pad;
            }
        }
        return $string;
    }

    //**************************************************
    //BreadCrumb
    //**************************************************
    function getAdminBreadCrumb($arr) {
        $seg = $this->uri->segment(1);
        $str = '';
        foreach ($arr as $k => $v) {
            if (next($arr))
                $str .= "<li><a href='../$seg/'>" . $v . "</a></li>";
            else
                $str .="<li><a class='active'>" . $v . "</a></li>";
        }
        return $str;
    }

    function getAdminBreadCrumbL2($arr) {
        $seg = $this->uri->segment(1);
        $str = '';
        foreach ($arr as $k => $v) {
            if (next($arr))
                $str .= "<li><a href='../../../$seg'>" . $v . "</a></li>";
            else
                $str .="<li><a class='active'>" . $v . "</a></li>";
        }
        return $str;
    }

    function getLang() {
        $lang = $this->session->userdata('userLang');

        if ($this->session->userdata('userLang') && in_array($this->session->userdata('userLang'), $this->config->item('avail_languages')))
            return $this->session->userdata('userLang');

        return $this->config->item('language');
    }

    function chk_admin_session() {
        $session_login = $this->session->userdata("ADMINLOGIN");
        if ($session_login == '' && $this->uri->segment(1) != 'login' && $this->uri->segment(1) != 'forgot_pass' && $this->uri->segment(1) != 'confirmation') {
            redirect('login');
            exit;
        }

        /* if($session_login == 1 && $session_login == TRUE && ($this->uri->segment(1) == '' || $this->uri->segment(1) == 'login' || $this->uri->segment(1) == 'forgot_pass'))
          redirect('dashboard'); */
        // if(!empty($session_login) && $session_login!='' && $session_login == 1 && $session_login == TRUE && ($this->uri->segment(1) == '' || $this->uri->segment(1) == 'welcome' || $this->uri->segment(1) == 'forgot_pass' || $this->uri->segment(1) == 'confirmation'))
        // 	redirect('dashboard');
    }

    function chk_admin_cookie() {
        $loggedin = $this->chk_cookie();

        if ($loggedin === TRUE) {
            redirect('event');
        }
    }

    public function updateContent($data) {

    
        //extract params
        extract($data);
        //get current date
        $CurrentDate = date('Y-m-d H:i:s');
        $data1 = array(
            'title' => $old_title,
            'text' => trim($editor1),  
            'updated_date'=> $CurrentDate
        ); 
         $this->db->where('id','1'); 
         $this->db->update($this->content,$data1);
    }

}

/* End of file general_model.php */
/* Location: ./application/admin/models/general_model.php */