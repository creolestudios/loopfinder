<?php

/*
 * @category   Categories Model
 * @package    Database activity for category
 * @author     Bhargav Bhanderi <bhargav@creolestudios.com>
 * @author     Another Author <another@example.com>
 * @copyright  2014 Creole Studios
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    Release: 1.0 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class message_model extends CI_Model {

    var $table;

    function __construct() {
        parent::__construct();
        $this->load->helper('cookie');
        $this->request = 'request';
        $this->customers = 'customers';
    }

    //#################################################################
    // Name : GetAllRequest
    // Purpose : To get all the request for export
    // In Params : void
    // Out params : all user data
    //#################################################################
    public function GetAllRequest() {

        //initialize return data
        $ReturnData = array();

        $this->db->select("CONCAT(c.first_name,' ',c.last_name) as customer_name,c.email,r.venue_name,r.state,r.city,r.comment,DATE_FORMAT(r.request_date,'%d/%c/%Y %h:%i %p') as request_date,IF(r.request_type = 1,'Already Looped','Needs A Loop') as request_type,CASE r.venue_provide_headset WHEN '0' THEN 'No' WHEN '1' THEN 'Yes' WHEN '2' THEN 'Don\'t Know' ELSE '--NA--' END as venue_provide_headset", false);
        $this->db->from("$this->request as r");
        $this->db->join('customers as c', 'r.customer_id = c.id', 'left');
        $this->db->order_by("r.id",'DESC');
        $GetRequstDetails = $this->db->get();

        if ($GetRequstDetails->num_rows() > 0) {

            //fetch the data
            $RequestData = $GetRequstDetails->result_array();

            $ReturnData['status'] = '1';
            $ReturnData['request_data'] = $RequestData;
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : GetCount
    // Purpose : To get count of read and total message
    // In Params : void
    // Out params : count details
    //#################################################################

    public function GetCount() {

        //global initilization
        $ReturnData = array();

        //query to fetch count
        $GetCount = $this->db->query("SELECT (SELECT count(id) from request where status = '0') as total_unread_count,(SELECT count(id) from request where status = '2') as total_archive_count,count(id) as total_rows from request ");
        $CountData = $GetCount->result_array();

        if (isset($CountData) && !empty($CountData)) {
            $ReturnData['total_unread_count'] = $CountData[0]['total_unread_count'];
            $ReturnData['total_archive_count'] = $CountData[0]['total_archive_count'];
            $ReturnData['total_rows'] = $CountData[0]['total_rows'];
        } else {
            $ReturnData['total_unread_count'] = 0;
            $ReturnData['total_archive_count'] = 0;
            $ReturnData['total_rows'] = 0;
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : GetMessageDetails
    // Purpose : To get message details
    // In Params : message id
    // Out params : message details
    //#################################################################
    public function GetMessageDetails($MessageId) {

        //initialize return data
        $ReturnData = array();

        $this->db->select("c.id as customer_id,CONCAT(c.first_name,' ',c.last_name) as customer_name,c.email,r.id as request_id,r.venue_name,r.state,r.city,r.comment,r.request_date,IF(r.request_type = 1,'Already Looped','Needs A Loop') as request_type,r.status as status,CASE r.venue_provide_headset WHEN '0' THEN 'No' WHEN '1' THEN 'Yes' WHEN '2' THEN 'Don\'t Know' ELSE '--NA--' END as venue_provide_headset", false);
        $this->db->from("$this->request as r");
        $this->db->join('customers as c', 'r.customer_id = c.id', 'left');
        $this->db->where("r.id", $MessageId);

        $GetRequstDetails = $this->db->get();

        if ($GetRequstDetails->num_rows() > 0) {

            //fetch the data
            $RequestData = $GetRequstDetails->result_array();

            $ReturnData['status'] = '1';
            $ReturnData['request_data'] = $RequestData[0];
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //###########################################################
    //function : DeleteRequest
    //To delete request
    //Input : request id
    //Output : success/error response
    //###########################################################   
    public function DeleteRequest($RequestId) {

        $this->db->delete($this->request, array('id' => $RequestId));

        $ReturnData['status'] = "1";
        $ReturnData['message'] = $this->lang->line('SUCCESS_REQUEST_DELETE');

        return $ReturnData;
    }

    //###########################################################
    //function : UpdateMessageStatus
    //To update message status to read
    //Input : message id
    //Output : success/error response
    //###########################################################  
    public function UpdateMessageStatus($MessageId, $Status) {

        //prepare array to insert
        $UpdateArray = array(
            'status' => $Status
        );

        //update the customer
        $UpdateMessage = $this->db->update($this->request, $UpdateArray, array("id " => $MessageId));

        $ReturnData['status'] = "1";
        if ($Status == '2') {
            $ReturnData['message'] = $this->lang->line('SUCCESS_REQUEST_ARCHIV');
        } else {
            $ReturnData['message'] = $this->lang->line('SUCCESS_REQUEST_READ');
        }

        return $ReturnData;
    }

}
