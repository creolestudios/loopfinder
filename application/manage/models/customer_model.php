<?php

/*
 * @category   Categories Model
 * @package    Database activity for category
 * @author     Bhargav Bhanderi <bhargav@creolestudios.com>
 * @author     Another Author <another@example.com>
 * @copyright  2014 Creole Studios
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    Release: 1.0 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class customer_model extends CI_Model {

    var $table;

    function __construct() {
        parent::__construct();
        $this->load->helper('cookie');
        $this->customers = 'customers';
        $this->checkins = 'checkins';
        $this->votes = 'votes';
    }

    //#################################################################
    // Name : GetAllUsers
    // Purpose : To get all the users for export
    // In Params : void
    // Out params : all user data
    //#################################################################
    public function GetAllUsers() {

        $this->db->select("CONCAT(c.first_name,' ',c.last_name) as name,c.email as email,if(c.zipcode != '',LPAD(c.zipcode, 5, '0'),c.zipcode)as zipcode,c.city as city,c.state as state,c.age,CASE c.gender WHEN 1 THEN 'Male' WHEN 2 THEN 'Female' ELSE '' END as gender,CASE c.hearing_level WHEN 1 THEN 'None' WHEN 2 THEN 'Mild' WHEN 3 THEN 'Moderate' WHEN 4 THEN 'Severe' ELSE '' END as hearing_level,CASE c.hearing_ad WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' ELSE '' END as hearing_ad,count(distinct r.id) as requests,count(distinct v.id) as votes,count(distinct f.id) as feedbacks,count(distinct ch.id) as checkins,DATE_FORMAT(c.register_date,'%d/%c/%Y %h:%i %p') as register_date", false)->from(" customers as c");
        $this->db->join('request as r', 'r.customer_id = c.id', 'left');
        $this->db->join('votes as v', 'v.customer_id = c.id', 'left');
        $this->db->join('feedback as f', 'f.customer_id = c.id', 'left');
        $this->db->join('checkins as ch', 'ch.customer_id = c.id', 'left');
        $this->db->group_by('c.id');
        $this->db->order_by('c.id', 'DESC');
        $GetCustomeres = $this->db->get();

        if ($GetCustomeres->num_rows() > 0) {

            //fetch the data
            $CustomerData = $GetCustomeres->result_array();

            $ReturnData['status'] = '1';
            $ReturnData['customer_data'] = $CustomerData;
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : GetCategoryDetails
    // Purpose : To get category details
    // In Params : category id
    // Out params : category details
    //#################################################################
    public function GetUserDetails($CustomerId) {

        //initialize return data
        $ReturnData = array();

        //set image path
        $CustomerImagePath = UPLOADS_URL . 'customer/';
        $DefaultImage = UPLOADS_URL . 'user_default.png';

        $this->db->select("c.id as customer_id,c.first_name,c.last_name,c.email,if(c.zipcode != '',LPAD(c.zipcode, 5, '0'),c.zipcode)as zipcode,DATE_FORMAT(c.last_login_date,'%d %b %Y') as last_login,DATE_FORMAT(c.register_date,'%d %b %Y') as register_date, c.city,c.state,CASE c.gender WHEN 1 THEN 'Male' WHEN 2 THEN 'Female' ELSE '---' END as gender,c.age,CASE c.hearing_level WHEN 1 THEN 'None' WHEN 2 THEN 'Mild' WHEN 3 THEN 'Moderate' WHEN 4 THEN 'Severe' ELSE '---' END as hearing_level,CASE c.hearing_ad WHEN 1 THEN 'Yes' WHEN 2 THEN 'No' ELSE '---' END as hearing_ad,IF(c.profile_image = '','" . $DefaultImage . "',CONCAT('" . $CustomerImagePath . "',c.profile_image)) as profile_image,IF(c.notify_venue_nearby = '1','Yes','No') as notify_venue_nearby,IF(c.notify_new_loop_added = '1','Yes','No') as notify_new_loop_added,IF(c.notify_new_loop_requested = '1','Yes','No') as notify_new_loop_requested,c.notify_new_loop_added_distance,c.notify_new_loop_requested_distance", false);
        $this->db->from("$this->customers as c");
        $this->db->where("id", $CustomerId);

        $GetCustomeretails = $this->db->get();

        if ($GetCustomeretails->num_rows() > 0) {

            //fetch the data
            $CustomerData = $GetCustomeretails->result_array();

            $ReturnData['status'] = '1';
            $ReturnData['customer_data'] = $CustomerData[0];
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : GetAllUser
    // Purpose : To get all the users
    // In Params : void
    // Out params : all user data
    //#################################################################
    public function GetAllUser($CustomerId) {

        //initialize return data
        $ReturnData = array();

        //set image path
        $CustomerImagePath = UPLOADS_URL . 'customer/';
        $DefaultImage = UPLOADS_URL . 'user_default.png';

        $this->db->select("c.id as customer_id,c.first_name,c.last_name,c.email,if(c.zipcode != '',LPAD(c.zipcode, 5, '0'),c.zipcode)as zipcode,c.city,c.state,IF(c.gender = 1,'Male','Female') as gender,c.age,CASE c.hearing_level WHEN 1 THEN 'None' WHEN 2 THEN 'Mild' WHEN 3 THEN 'Moderate' ELSE 'Severe' END as hearing_level,IF(c.hearing_ad = 1,'Yes','No') as hearing_ad,IF(c.profile_image = '','" . $DefaultImage . "',CONCAT('" . $CustomerImagePath . "',c.profile_image)) as profile_image", false);
        $this->db->from("$this->customers as c");
        $this->db->where("id", $CustomerId);

        $GetCustomeretails = $this->db->get();

        if ($GetCustomeretails->num_rows() > 0) {

            //fetch the data
            $CustomerData = $GetCustomeretails->result_array();

            $ReturnData['status'] = '1';
            $ReturnData['customer_data'] = $CustomerData[0];
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : UpdateUserDetails
    // Purpose : To update user
    // In Params : all user data
    // Out params : success message and status
    //#################################################################
    public function UpdateUserDetails($Params) {
        extract($Params);
        //prepare array to insert
        $UpdateArray = array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'gender' => $gender,
            'age' => $age,
            'hearing_level' => $hearing_level,
            'hearing_ad' => $hearing_ad,
            'city' => $city,
            'state' => $state,
            'zipcode' => $zipcode,
            'notify_new_loop_added_distance' => $notify_new_loop_added_distance,
            'notify_new_loop_requested_distance' => $notify_new_loop_requested_distance,
            'notify_venue_nearby' => ($notify_venue_nearby == 'on') ? '1' : '0',
            'notify_new_loop_added' => ($notify_new_loop_added == 'on') ? '1' : '0',
            'notify_new_loop_requested' => ($notify_new_loop_requested == 'on') ? '1' : '0'
        );


        //update the customer
        $UpdateUser = $this->db->update($this->customers, $UpdateArray, array("id " => $customer_id));

        if ($this->db->affected_rows() > 0) {
            $ReturnData['status'] = '1';
        } else {
            $ReturnData['status'] = '0';
        }

        //return data
        return $ReturnData;
    }

    //###########################################################
    //function : DeleteCustomer
    //To delete customer
    //Input : customer id
    //Output : success/error response
    //###########################################################   
    public function DeleteCustomer($CustomerId) {

        $this->db->delete($this->customers, array('id' => $CustomerId));

        $ReturnData['status'] = "1";
        $ReturnData['message'] = $this->lang->line('SUCCESS_USER_DELETE');

        return $ReturnData;
    }

    //###########################################################
    //function : GetActivityTotal
    //To get total number of checkins and votes
    //Input : customer id
    //Output : success/error response
    //###########################################################
    public function GetActivityTotal($CustomerId) {

        //initialize return data
        $ReturnData = array();

        $this->db->select("count(c.id) as total_checkins", false);
        $this->db->from("$this->checkins as c");
        $this->db->where("c.customer_id", $CustomerId);
        $GetCheckins = $this->db->get();
        //fetch the data
        $TotalCheckins = $GetCheckins->result_array();

        //get total votes up
        $this->db->select("count(v.id) as total_votes", false);
        $this->db->from("$this->votes as v");
        $this->db->where("v.customer_id", $CustomerId);
        $GetVotes = $this->db->get();
        //fetch the data
        $TotalVotes = $GetVotes->result_array();

        $ReturnData['status'] = '1';
        $ReturnData['checkins'] = $TotalCheckins[0]['total_checkins'];
        $ReturnData['votes'] = $TotalVotes[0]['total_votes'];

        return $ReturnData;
    }

    //###########################################################
    //function : GetCustomerCount
    //To get total number of customers
    //Input : void
    //Output : success/error response
    //###########################################################
    public function GetCustomerCount() {

        //initialize global
        $ReturnData = array();

        $this->db->select('count(id) as total_customer');
        $this->db->from($this->customers);

        $GetCustomerQuery = $this->db->get();

        if ($GetCustomerQuery->num_rows() > 0) {

            //fetch the data
            $CustomerData = $GetCustomerQuery->result_array();

            $ReturnData['status'] = '1';
            $ReturnData['customer_count'] = $CustomerData[0]['total_customer'];
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //###########################################################
    //Function : GetSettings
    //purpose : To get notification settings
    //input : Customer ID
    //outpur : success/error
    //###########################################################

    public function GetSettingsForNearby($Latitude,$Longitude) {

        //global declaration
        $ReturnData = array();

        $this->db->select("c.id as customer_id,c.device_token,c.latitude, c.longitude,c.notify_new_loop_added,c.notify_new_loop_added_distance,c.notify_new_loop_requested,c.notify_new_loop_requested_distance, TRUNCATE(( 3959 * acos ( cos ( radians('".$Latitude."') ) * cos( radians( c.latitude ) ) * cos( radians( c.longitude ) - radians('".$Longitude."') ) + sin ( radians('".$Latitude."') ) * sin( radians( c.latitude ) ) ) ), 2) AS `distance`", false);
        $this->db->from('customers as c');
        $this->db->where('c.user_type', '1');
        $this->db->having("distance <= ", '50');
        $this->db->group_by("c.id");
        ## execute query
        $CustomerList = $this->db->get();

        if ($CustomerList->num_rows() > 0) {
            //fetch the data
            $NotitificationData = $CustomerList->result_array();

            //prepare for response
            $ReturnData['status'] = '1';
            $ReturnData['customer_data'] = $NotitificationData;
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : AddNotification
    // Purpose : To get notification
    // In Params : user id,latitude,longiture
    // Out params : notification and near by venue for user
    //#################################################################
    public function AddNotification($NotificationStr) {

        //initialize data
        $ReturnData = array();

        ## add notification to table
        $this->db->query('INSERT INTO notification(customer_id,venue_id,notification_text,notification_date,flag,notification_type) VALUES ' . $NotificationStr);

        if ($this->db->affected_rows() > 0) {
            $ReturnData['status'] = '1';
        } else {
            $ReturnData['status'] = '0';
        }
        return $ReturnData;
    }

}

/* End of file venue_model.php */
/* Location: ./application/otojoy/models/categories_model.php */
?>