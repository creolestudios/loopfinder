<?php

/*
 * @category   Categories Model
 * @package    Database activity for category
 * @author     Bhargav Bhanderi <bhargav@creolestudios.com>
 * @author     Another Author <another@example.com>
 * @copyright  2014 Creole Studios
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    Release: 1.0 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class venues_model extends CI_Model {

    var $table;

    function __construct() {
        parent::__construct();
        $this->load->helper('cookie');
        $this->category = 'categories';
        $this->cities = 'cities';
        $this->states = 'states';
        $this->venues = 'venues';
        $this->venue_images = 'venue_images';
        $this->venue_categories = 'venue_categories';
        $this->venue_rooms = 'venue_rooms';
    }

    //#################################################################
    // Name : GetAllVenues
    // Purpose : To get all the venues for export
    // In Params : void
    // Out params : all venue data
    //#################################################################
    public function GetAllVenues() {

        $this->db->select("IF(v.venue_type = '1','Registered','Requested') as venue_type,v.name,v.address,v.city,v.state,if(v.zipcode != '',LPAD(v.zipcode, 5, '0'),v.zipcode)as zipcode ,v.latitude,v.longitude,v.phone_number,v.email,v.website,IF(v.is_certified = '1','Yes','No') as certified,IF(v.certified_date != '0000-00-00',DATE_FORMAT(v.certified_date,'%d/%c/%Y %h:%i %p'),'') as certified_date,GROUP_CONCAT(distinct c.name SEPARATOR ', ') as categories,GROUP_CONCAT(distinct vr.name SEPARATOR ', ') as loop_rooms,count(distinct ch.id) as checkins,count(distinct f.id) as feedbacks,count(distinct vo.id) as votes,v.internal_contact,v.internal_email,v.internal_phone_number,v.internal_contact_ext,v.internal_comment,v.public_comment", false);
        $this->db->from("venues as v ");
        $this->db->join('venue_rooms as vr', 'vr.venue_id = v.id', 'left');
        $this->db->join('venue_categories as vc', 'vc.venue_id = v.id', 'left');
        $this->db->join('categories as c', 'vc.category_id = c.id', 'left');
        $this->db->join('checkins as ch', 'ch.venue_id = v.id', 'left');
        $this->db->join('feedback as f', 'f.venue_id = v.id', 'left');
        $this->db->join('votes as vo', 'vo.venue_id = v.id', 'left');
        $this->db->group_by('v.id');
        $this->db->order_by('v.id', 'DESC');
        $GetVenues = $this->db->get();

        if ($GetVenues->num_rows() > 0) {

            //fetch the data
            $VenueData = $GetVenues->result_array();

            $ReturnData['status'] = '1';
            $ReturnData['venue_data'] = $VenueData;
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : CheckVenueExist
    // Purpose : To check it venue is exist or not
    // In Params : venue name and id at edit time
    // Out params : success/error message with status
    //#################################################################
    public function CheckVenueExist($Params) {

        //extract data
        extract($Params);

        $this->db->select('name');
        $this->db->from($this->venues);
        $this->db->where("name", $venue_name);

        if (isset($venue_id) && $venue_id != '') {
            $this->db->where("id != $venue_id");
        }

        $GetVenueQuery = $this->db->get();

        if ($GetVenueQuery->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    //#################################################################
    // Name : VerifyVenueRoom
    // Purpose : To verify room 
    // In Params : room id
    // Out params : success/error message with status
    //################################################################# 
    public function VerifyVenueRoom($RoomId) {
        //initialize data
        $ReturnData = array();

        $CurrentDate = date('Y-m-d');
        //prepare array to update
        $UpdateArray = array('last_verified' => $CurrentDate);

        //update the customer
        $UpdateStatus = $this->db->update($this->venue_rooms, $UpdateArray, array("id " => $RoomId));

        if ($this->db->affected_rows() > 0) {
            $ReturnData['status'] = '1';
            $ReturnData['message'] = $this->lang->line('SUCCESS_VENUE_VERIFY');
            $ReturnData['date_verified'] = date('jS M Y', strtotime($CurrentDate));
        } else {
            $ReturnData['message'] = $this->lang->line('SUCCESS_VENUE_VERIFY_ALREADY');
            $ReturnData['status'] = '0';
        }
        return $ReturnData;
    }

    //#################################################################
    // Name : SetDefaultImage
    // Purpose : To verify room 
    // In Params : room id
    // Out params : success/error message with status
    //################################################################# 
    public function SetVenueDefaultImage($VenueId, $Image) {
        //initialize data
        $ReturnData = array();

        //prepare array to update
        $UpdateArray = array('venue_image' => $Image);

        //update the customer
        $UpdateStatus = $this->db->update($this->venues, $UpdateArray, array("id " => $VenueId));

        if ($this->db->affected_rows() > 0) {
            $ReturnData['status'] = '1';
            $ReturnData['message'] = $this->lang->line('SUCCESS_SET_IMAGE');
        } else {
            $ReturnData['message'] = $this->lang->line('GENERAL_ERROR');
            $ReturnData['status'] = '0';
        }
        return $ReturnData;
    }

    //#################################################################
    // Name : CreateVenue
    // Purpose : To create venue
    // In Params : All Venue data
    // Out params : success/error message with status
    //#################################################################  
    public function CreateVenue($Params) {

        //initialize data
        $ReturnData = array();

        //extract params
        extract($Params);

        //prepare array for insertion
        $InsertArray = array(
            'name' => $venue_name,
            'address' => $address,
            'zipcode' => $zipcode,
            'state' => $state,
            'city' => $city,
            'phone_number' => $phone_number,
            'website' => $website,
            'email' => $email,
            'meetupdate' =>'',
            'internal_contact' => $internal_contact,
            'internal_contact_ext' => $internal_contact_ext,
            'internal_email' => $internal_email,
            'internal_phone_number' => $internal_phone_number,
            'internal_comment' => $internal_comment,
            'public_comment' => $public_comment,
            'latitude' => $hdlatitude,
            'longitude' => $hdlongitude,
            'is_certified' => ($hdvenuetype == '1') ? $is_certified : '0',
            'venue_type' => $hdvenuetype,
            'status' => '1'
        );

        //if venue is certified then only add certified date
        if ($is_certified == '1' && $hdvenuetype == '1') {
            $InsertArray = array_merge($InsertArray, array('certified_date' => date('Y-m-d', strtotime($certified_date))));
        } else {
            $InsertArray = array_merge($InsertArray, array('certified_date' => '0000-00-00'));
        }

        //insert customer data
        $AddVenue = $this->db->insert($this->venues, $InsertArray);

        //check it insertion in success or not
        if ($this->db->affected_rows() > 0) {
            $ReturnData['status'] = '1';
            $ReturnData['venue_id'] = $this->db->insert_id();
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : UpdateVenue
    // Purpose : To update venue
    // In Params : All Venue data
    // Out params : success/error message with status
    //#################################################################  
    public function UpdateVenue($Params) {

        //initialize data
        $ReturnData = array();
        
        //extract params
        extract($Params);

        //prepare array for insertion
        $InsertArray = array(
            'name' => $venue_name,
            'address' => $address,
            'zipcode' => $zipcode,
            'state' => $state,
            'city' => $city,
            'phone_number' => $phone_number,
            'website' => $website,
            'email' => $email,
            'meetupdate' =>'',
            'internal_contact' => $internal_contact,
            'internal_contact_ext' => $internal_contact_ext,
            'internal_email' => $internal_email,
            'internal_phone_number' => $internal_phone_number,
            'internal_comment' => $internal_comment,
            'public_comment' => $public_comment,
            'latitude' => $hdlatitude,
            'longitude' => $hdlongitude,
            'is_certified' => ($hdvenuetype == '1') ? $is_certified : '0',
            'venue_type' => $hdvenuetype,
            'status' => '1'
        );

        //if venue is certified then only add certified date
        if ($is_certified == '1' && $hdvenuetype == '1') {
            $InsertArray = array_merge($InsertArray, array('certified_date' => date('Y-m-d', strtotime($certified_date))));
        } else {
            $InsertArray = array_merge($InsertArray, array('certified_date' => '0000-00-00'));
        }

        //insert customer data
        $AddVenue = $this->db->update($this->venues, $InsertArray, array("id " => $venue_id));

        //check it insertion in success or not
        $ReturnData['status'] = '1';

        return $ReturnData;
    }

    //#################################################################
    // Name : CreateVenueCategory
    // Purpose : To create venue category
    // In Params : Category data
    // Out params : success/error message with status
    //#################################################################  
    public function CreateVenueCategory($CategoryVal) {

        //initialize data
        $ReturnData = array();

        //add category to table
        $this->db->query('INSERT INTO venue_categories(venue_id,category_id) VALUES ' . $CategoryVal);

        if ($this->db->affected_rows() > 0) {
            $ReturnData['status'] = '1';
        } else {
            $ReturnData['status'] = '0';
        }
        return $ReturnData;
    }

    //#################################################################
    // Name : CreateLoopRooms
    // Purpose : To create loop rooms
    // In Params : Loop room data
    // Out params : success/error message with status
    //#################################################################  
    public function CreateLoopRooms($LoopRoomsVal) {

        //initialize data
        $ReturnData = array();

        //add category to table
        $this->db->query('INSERT INTO venue_rooms(venue_id,name) VALUES ' . $LoopRoomsVal);

        if ($this->db->affected_rows() > 0) {
            $ReturnData['status'] = '1';
        } else {
            $ReturnData['status'] = '0';
        }
        return $ReturnData;
    }

    //###########################################################
    //function : SaveVenueImages
    //To save venue images
    //Input : images
    //Output : customer data
    //########################################################### 
    public function SaveVenueImages($Images) {
        //run query to add data in database
        $this->db->query("insert into venue_images(venue_id,image) values " . $Images);

        if ($this->db->affected_rows() > 0) {
            $ReturnData['status'] = '1';
        } else {
            $ReturnData['status'] = '0';
        }
        return $ReturnData;
    }

    //###########################################################
    //function : UpdateCoverImage
    //To update cover images
    //Input : images
    //Output : customer data
    //###########################################################    
    public function UpdateCoverImage($VenueId, $CoverImage) {

        //initialize data
        $ReturnData = array();

        //prepare array to update
        $UpdateArray = array('venue_image' => $CoverImage);

        //update the customer
        $UpdateStatus = $this->db->update($this->venues, $UpdateArray, array("id " => $VenueId));

        if ($this->db->affected_rows() > 0) {
            $ReturnData['status'] = '1';
        } else {
            $ReturnData['status'] = '0';
        }
        return $ReturnData;
    }

    //#################################################################
    // Name : GetVenueDetails
    // Purpose : To get venue details
    // In Params : all venue details
    // Out params : success/error message with status
    //#################################################################
    public function GetVenueDetails($VenueId) {
        //initialize return data
        $ReturnData = array();
        //set image path for venue
        $VenueImagePath = UPLOADS_URL . 'venues/';

        $this->db->select("v.id as venue_id,v.name as venue_name,v.address,if(v.zipcode != '',LPAD(v.zipcode, 5, '0'),v.zipcode)as zipcode,v.city,v.state,v.venue_image,v.phone_number,v.website,v.email,v.is_certified,v.certified_date,v.created_date,v.venue_type,v.latitude,v.longitude,(select count(id) from feedback where venue_id = '" . $VenueId . "') as total_feedback,(select count(id) from checkins where venue_id = '" . $VenueId . "') as total_checkins,(select count(id) from votes where venue_id = '" . $VenueId . "') as total_votes,v.internal_contact,v.internal_contact_ext,v.internal_email,v.internal_phone_number,v.internal_comment,v.public_comment", false);
        $this->db->from("$this->venues as v");
        $this->db->where("v.id", $VenueId);

        $GetVenueDetails = $this->db->get();
        
        if ($GetVenueDetails->num_rows() > 0) {

            //fetch the data
            $VenueData = $GetVenueDetails->result_array();

            //##################### Fetch Image Data ###################################
            //get venue image data if venue is available
            $this->db->select("vi.id as venue_image_id,image as original_image,CONCAT('" . $VenueImagePath . "','" . $VenueId . "','/',image) as venue_image,CONCAT('" . $VenueImagePath . "','" . $VenueId . "','/thumb/',image) as venue_image_thumb", false);
            $this->db->from("$this->venue_images as vi");
            $this->db->where("vi.venue_id", $VenueId);

            $GetVenueImages = $this->db->get();
            //##################### Fetch Image Data ###################################
            //
            //##################### Fetch venue categories Data ###################################
            //get venue image data if venue is available
            $this->db->select(" GROUP_CONCAT(c.id) as category_id,GROUP_CONCAT(c.name SEPARATOR ', ') as category_name ", false);
            $this->db->from("venue_categories as vc ");
            $this->db->join("categories as c ", "vc.category_id = c.id", "left");
            $this->db->where("vc.venue_id", $VenueId);

            $GetVenueCategories = $this->db->get();
            //##################### Fetch venue categories Data end ###################################

            $ReturnData['status'] = '1';
            $ReturnData['venue_data'] = $VenueData[0];
            if ($GetVenueImages->num_rows() > 0) {
                //add image data to response
                $ReturnData['venue_images'] = $GetVenueImages->result_array();
            }
            //assign categories to response
            if ($GetVenueCategories->num_rows() > 0) {
                //add image data to response
                $CategoriesData = $GetVenueCategories->result_array();
                $ReturnData['venue_categories'] = $CategoriesData[0];
            }
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : GetLoopRooms
    // Purpose : To get all venue loop rooms
    // In Params : all loop room of venue
    // Out params : success/error message with status
    //#################################################################
    public function GetLoopRooms($VenueId) {

        //initialize return data
        $ReturnData = array();

        //get venue loop rooms
        $this->db->select("vr.id as venue_room_id,vr.name as room_name,vr.last_verified", false);
        $this->db->from("$this->venue_rooms as vr");
        $this->db->where("vr.venue_id", $VenueId);

        $GetVenueRooms = $this->db->get();

        if ($GetVenueRooms->num_rows() > 0) {
            //add image data to response
            $LoopRoomData = $GetVenueRooms->result_array();
            $ReturnData['status'] = '1';
            $ReturnData['venue_rooms'] = $LoopRoomData;
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : GetZipCodeDetails
    // Purpose : To get state and city values from zipcode
    // In Params : city and state data
    // Out params : success/error message with status
    //#################################################################
    public function GetZipCodeDetails($Params) {

        //initialize data
        $ReturnData = array();

        //extract input post parameters
        extract($Params);

        //query to fetch city and state from given zipcode
        
        
        $this->db->select(" c.city,c.state_code,c.state_code", false);
        $this->db->from("cities_new as c ");
        $this->db->where("c.zip", $ZipCode);
        $this->db->order_by("c.id", "asc");
        $this->db->limit(1);
        
        
        
//        $this->db->select(" c.city,c.state_code,c.state_code,s.state ", false);
//        $this->db->from("cities as c ");
//        $this->db->join("states as s ", "c.state_code = s.state_code", "left");
//        $this->db->where("c.zip", $ZipCode);
//        $this->db->order_by("c.id", "asc");
//        $this->db->limit(1);

        $GetZipCodeDataQuery = $this->db->get();
       
        if ($GetZipCodeDataQuery->num_rows() > 0) {
            //fetch image data
            $ZipCodeData = $GetZipCodeDataQuery->result_array();
            //assign to array
            $ReturnData['status'] = 1;
            $ReturnData['zipcode'] = $ZipCodeData;
        } else {
            $ReturnData['status'] = 0;
        }
        
        return $ReturnData;
    }

    //###########################################################
    //function : DeleteVenue
    //To delete venue
    //Input : venue id
    //Output : success/error response
    //###########################################################   
    public function DeleteVenue($VenueId) {

        $this->db->delete($this->venues, array('id' => $VenueId));

        $ReturnData['status'] = "1";
        $ReturnData['message'] = $this->lang->line('SUCCESS_VENUE_DELETE');

        return $ReturnData;
    }

    //###########################################################
    //function : DeleteVenue
    //To delete venue
    //Input : venue id
    //Output : success/error response
    //###########################################################   
    public function DeleteVenueRoom($RoomId) {

        $this->db->delete($this->venue_rooms, array('id' => $RoomId));

        $ReturnData['status'] = "1";
        $ReturnData['message'] = $this->lang->line('SUCCESS_VENUE_ROOM_DELETE');

        return $ReturnData;
    }

    //###########################################################
    //function : DeleteLoopRoomsVenue
    //To delete venue rooms
    //Input : venue id
    //Output : success/error response
    //###########################################################   
    public function DeleteLoopRoomsVenue($VenueId) {

        $this->db->delete($this->venue_rooms, array('venue_id' => $VenueId));

        $ReturnData['status'] = "1";
        return $ReturnData;
    }

    //###########################################################
    //function : DeleteVenueCategory
    //To delete venue category
    //Input : venue id
    //Output : success/error response
    //###########################################################   
    public function DeleteVenueCategory($VenueId) {

        $this->db->delete($this->venue_categories, array('venue_id' => $VenueId));

        $ReturnData['status'] = "1";
        return $ReturnData;
    }

    //###########################################################
    //function : DeleteVenueImage
    //To delete venue image
    //Input : image id
    //Output : success/error response
    //###########################################################   
    public function DeleteVenueImage($ImageId) {
        $this->db->delete($this->venue_images, array('id' => $ImageId));

        $ReturnData['status'] = "1";
        $ReturnData['message'] = $this->lang->line('SUCCESS_VENUE_IMAGE_DELETE');

        return $ReturnData;
    }

    //#################################################################
    // Name : UpdateVenueToRegister
    // Purpose : To update venue status 
    // In Params : venue id
    // Out params : success/error message with status
    //################################################################# 
    public function UpdateVenueToRegister($VenueId) {
        //initialize data
        $ReturnData = array();

        //prepare array to update
        $UpdateArray = array('venue_type' => '1');

        //update the customer
        $UpdateStatus = $this->db->update($this->venues, $UpdateArray, array("id " => $VenueId));

        if ($this->db->affected_rows() > 0) {
            $ReturnData['status'] = '1';
            $ReturnData['message'] = $this->lang->line('SUCCESS_VENUE_REGISTER');
        } else {
            $ReturnData['message'] = $this->lang->line('NO_CHANGES_MADE');
            $ReturnData['status'] = '0';
        }
        return $ReturnData;
    }

    //#################################################################
    // Name : UpdateVenueToRegister
    // Purpose : To update venue status 
    // In Params : venue id
    // Out params : success/error message with status
    //################################################################# 
    public function UpdateLoopedRoomsVenue($RoomId, $RoomName) {
        //initialize data
        $ReturnData = array();

        //prepare array to update
        $UpdateArray = array('name' => $RoomName);

        //update the customer
        $UpdateStatus = $this->db->update($this->venue_rooms, $UpdateArray, array("id " => $RoomId));

        if ($this->db->affected_rows() > 0) {
            $ReturnData['status'] = '1';
            $ReturnData['message'] = $this->lang->line('SUCCESS_LOOP_ROOM_UPDATE');
        } else {
            $ReturnData['message'] = $this->lang->line('NO_CHANGES_MADE');
            $ReturnData['status'] = '0';
        }
        return $ReturnData;
    }

    //#################################################################
    // Name : GetCounts
    // Purpose : To get count for venues
    // In Params : void
    // Out params : success/error message with status
    //################################################################# 
    public function GetCounts() {

        //global initilization
        $ReturnData = array();

        $GetVenueQuery = $this->db->query('SELECT (select count(id) from venues where venue_type = "1") as register_venue_count,(select count(id) from venues where venue_type = "2") as requested_venue_count');

        if ($GetVenueQuery->num_rows() > 0) {

            //fetch result to array
            $VenueData = $GetVenueQuery->result_array();

            $ReturnData['status'] = '1';
            $ReturnData['register_venue_count'] = $VenueData[0]['register_venue_count'];
            $ReturnData['requested_venue_count'] = $VenueData[0]['requested_venue_count'];
        }

        return $ReturnData;
    }

}

/* End of file venue_model.php */
/* Location: ./application/otojoy/models/venues_model.php */
?>