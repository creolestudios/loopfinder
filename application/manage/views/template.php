<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.1.2
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>Otojoy-Admin</title>

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
        <!--<link href="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>-->
        <link href="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL PLUGIN STYLES -->
        <!-- BEGIN PAGE STYLES -->
        <link href="<?php echo base_url(); ?>assets/metronics_3.5.0/admin/pages/css/tasks.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/metronics_3.5.0/admin/pages/css/search.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/bootstrap-select/bootstrap-select.min.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/select2/select2.css"/>
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/jquery-multi-select/css/multi-select.css"/>
        

        <!-- END PAGE STYLES -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?php echo base_url(); ?>assets/metronics_3.5.0/global/css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/metronics_3.5.0/global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/metronics_3.5.0/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/metronics_3.5.0/admin/layout/css/themes/default.css" rel="stylesheet" type="text/css" id="style_colord"/>
        <link href="<?php echo base_url(); ?>assets/metronics_3.5.0/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/bootstrap-toastr/toastr.min.css"  rel="stylesheet" type="text/css"/>

        <link href="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"  rel="stylesheet" type="text/css"/>

        <script>
            var __base_url = "<?php echo base_url(); ?>";
        </script>

        <script src="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/datatables/media/js/jquery.dataTables.min.js" type="text/javascript" ></script>

        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/metronics_3.5.0/admin/layout/img/fevicon-icon1.png"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
    <!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
    <!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
    <!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
    <!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
    <!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
    <!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
    <body class="page-quick-sidebar-over-content">

        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top" style="z-index:0">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner">
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->

        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <?php echo $left_column; ?>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper" >
                <div class="page-content">
                    <?php echo $content; ?>
                </div>
            </div>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
<!--        <div class="page-footer">
            <?php //echo $footer; ?>
        </div>
-->        <!-- END FOOTER -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->
        <!--[if lt IE 9]>
        <script src="../../assets/metronics_3.5.0/global/plugins/respond.min.js"></script>
        <script src="../../assets/metronics_3.5.0/global/plugins/excanvas.min.js"></script> 
        <![endif]-->


        <script src="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->

        <script src="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
        <!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
        <!--<script src="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script>-->
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo base_url(); ?>assets/metronics_3.5.0/global/scripts/metronic.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/metronics_3.5.0/admin/layout/scripts/layout.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/metronics_3.5.0/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>


        <script src="<?php echo base_url(); ?>assets/metronics_3.5.0/admin/pages/scripts/table-managed.js"></script>
        <script src="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/select2/select2.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js" type="text/javascript" ></script>
        <script src="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript" ></script>


        <!-- END DYNAMIC CONTAINT FROM DATABASE--> 
        <script src="<?php echo base_url(); ?>assets/metronics_3.5.0/admin/pages/scripts/form-validation.js"></script> 
        <script src="<?php echo base_url(); ?>assets/scripts/humanized_date_format_converter.js"></script> 
        <script src="<?php echo base_url(); ?>assets/scripts/jquery.moment.2.7.0.js"></script> 
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/jquery-validation/js/jquery.validate.min.js"></script> 
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url(); ?>assets/metronics_3.5.0/admin/pages/scripts/index.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/metronics_3.5.0/admin/pages/scripts/ui-general.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>assets/metronics_3.5.0/global/plugins/jquery-bootpag/jquery.bootpag.min.js" type="text/javascript"></script>
        <!-- <script src="<?php //echo base_url(); ?>assets/metronics_3.5.0/global/scripts/jquery.mask.min.js" type="text/javascript"></script> -->
        <!-- END PAGE LEVEL SCRIPTS -->
        <?php $this->carabiner->display('css'); ?>
        <?php $this->carabiner->display('js'); ?>
        <script>

            jQuery(document).ready(function() {
                Metronic.init(); // init metronic core componets
                Layout.init(); // init layout
                QuickSidebar.init() // init quick sidebar

                // Index.init();   
                Index.initDashboardDaterange();
                //Index.initJQVMAP(); // init index page's custom scripts
                //Index.initCalendar(); // init index page's custom scripts
                //Index.initCharts(); // init index page's custom scripts
                //Index.initChat();
                //Index.initMiniCharts();
                //Index.initIntro();
                //Tasks.initDashboardWidget();
                FormValidation.init();
            });
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-top-right", //"toast-top-full-width"
                "onclick": null,
                "showDuration": "1000",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            //for toaster
            function notification(type, message)
            {
                if (type == 'success') {
                    toastr.success(message, 'Done');
                } else if (type == 'error') {
                    toastr.error(message, 'Error');
                } else if (type == 'warning') {
                    toastr.warning(message, 'Warning');
                } else {
                    toastr.info(message, 'Done');
                }
            }
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>