<?php
##############################################################################################
									//Body
##############################################################################################
$html	='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
       <title>Document</title>
</head>
<body>
    <table width="98%" cellspacing="0" cellpadding="0" border="0">
        <tbody>
            <tr>
                <td align="left" style="font-family:verdana;font-size:16px;line-height:1.3em;text-align:left;padding:15px;background: #0041A6;">
                    <p style="color:#CDB380;font-size:14px;letter-spacing: 5.5px; line-height: 20px;font-weight:bold;">
                       '.MAINTITLE.'
                    </p>
                </td>
            </tr>
            <tr style="background:#E6E6E6;">
                <td valign="top" align="left" style="font-family:verdana;font-size:16px;line-height:1.3em;text-align:left;padding:15px;">
                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tr style="background:#F5F5F5;border-radius:5px;">
                            <td style="font-family:verdana;font-size:13px;line-height:1.3em;text-align:left;padding:15px;">
                                <h1 style="font-family:verdana;color:#424242;font-size:28px;line-height:normal;letter-spacing:-1px;">    
                                                                    Dear User,
                                </h1>
                                <p style="color:#ADADAD">
                                    Your Password Request is Accepted
                                </p>
                                <p> 
                                    <b style="color:#036564;">
                                        Your new password is: '.$password.'
                                    </b>
                                </p>
                                <hr style="margin-top:30px;border-top:#ccc 1px solid;" />
                                <p>
                                    Thank you!
                                    <br/>
                                    Your LoopFinder Team
                                </p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>';

echo $html;
?>