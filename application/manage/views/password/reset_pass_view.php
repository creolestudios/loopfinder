<?php
$headerData = $this->headerlib->data();

$JsArr = array("SCRIPT" => $headerData['javascript'], 'PLUGINS' => $headerData['javascript_plugins']);

/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES
 * *************************************************** */

/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES ENDS
 * *************************************************** */
?>
<!doctype html>
<html lang="en-us">
    <head>
        <title><?= MAINTITLE; ?></title>
        <?= $headerData['meta_tags']; ?>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?= $headerData['plugins']; ?>
        <?= $headerData['stylesheets']; ?>

        <!-- BEGIN PAGE LEVEL STYLES --> 
        <link rel="stylesheet" type="text/css" href="<?= CSS_URL ?>pages/login3.css" />
        <!-- END PAGE LEVEL SCRIPTS -->

        <!-- GLOBAL MANDATORY STYLES --> 
        <!-- BEGIN GLOBAL MANDATORY STYLES -->          
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <?= $headerData['golbalmandatory']; ?>
        <!-- END GLOBAL MANDATORY STYLES -->

        <!-- BEGIN THEME STYLES --> 
        <?= $headerData['themestyles']; ?>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="<?= IMAGE_URL ?>favicon.ico" />

    </head>
    <!-- BEGIN BODY -->
    <body class="login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <img src="<?= IMAGE_URL ?>login_logo.png" alt="" style="max-width:126px;max-height: 163px;"/> 
        </div>
        <!-- END LOGO -->

        <!-- BEGIN LOGIN -->
        <div class="content">

            <div class="alert hide" id="reset_pass_msg"></div>
            <!-- BEGIN LOGIN FORM -->
            <form class="reset-pass-form" action="" method="post">
                <input type="hidden" id='merchant_id' value="<?= $id; ?>">
                <input type="hidden" name='salt' value="<?= $flg; ?>" id='flg'>
                <h3 class="form-title">Reset Password</h3>

                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <div class="input-icon">
                        <i class="icon-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="new_password" id="new_password"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <div class="input-icon">
                        <i class="icon-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Confirm Password" name="confirm_password" id="confirm_password"/>
                    </div>
                </div>
                <div class="form-actions" style="margin-top: 25px;border-bottom: none;">
                    <button type="submit" class="btn green pull-right" style="margin-top: -18px">
                        Reset <i class="m-icon-swapright m-icon-white"></i>
                    </button>            
                </div>

            </form>
            <!-- END LOGIN FORM -->        


        </div>
        <!-- END LOGIN -->

        <!-- BEGIN GLOBAL MANDATORY JS -->
        <?php $this->load->view('include/footer_view', $JsArr); ?> 
        <!-- END GLOBAL MANDATORY JS -->

        <!-- BEGIN CORE PLUGINS -->   
        <!--[if lt IE 9]>
        <script src="assets/plugins/respond.min.js"></script>
        <script src="assets/plugins/excanvas.min.js"></script> 
        <![endif]-->   

        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?= PLUGIN_URL ?>jquery-validation/js/jquery.validate.min.js"></script>	
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo JS_URL ?>metronic.js" type="text/javascript"></script>
        <script src="<?php echo JS_URL ?>layout.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>login.js" type="text/javascript"></script> 
        <!-- END PAGE LEVEL SCRIPTS --> 
        <script>
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core componets
                Layout.init(); // init layout
                Login.init();
            });
        </script>
    </body>
    <!-- END BODY -->
</html>	