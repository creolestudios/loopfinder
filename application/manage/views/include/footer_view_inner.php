<div class="page-footer">
    <div class="page-footer-inner">
        &COPY; <?= date('Y') ?>, Otojoy LLC. All rights reserved.
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>

<?php echo $SCRIPT; ?>
<!--<script>window.jQuery || document.write('<script src="<?= PLUGIN_URL; ?>jquery-2.0.3.min.js"><\/script>')</script>  -->
<?php echo $PLUGINS; ?>
<!-- Extra JS -->