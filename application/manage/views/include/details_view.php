<?php
$headerData = $this->headerlib->data();
$JsArr = array("SCRIPT" => $headerData['javascript'], 'PLUGINS' => $headerData['javascript_plugins']);
/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES
 * *************************************************** */

/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES ENDS
 * *************************************************** */
if (isset($album_details) && count($album_details) > 0) {
    //extract album details
    extract($album_details);
}
?>
<!doctype html>
<html lang="en-us">
    <head>
        <title><?= MAINTITLE; ?></title>
        <?= $headerData['meta_tags']; ?>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?= $headerData['plugins']; ?>
        <?= $headerData['stylesheets']; ?>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="<?= PLUGIN_URL ?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?= PLUGIN_URL ?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?= PLUGIN_URL ?>uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->

        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="<?= PLUGIN_URL ?>dropzone/css/dropzone.css" rel="stylesheet"/>
        <link href="<?= PLUGIN_URL ?>fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
        <link href="<?= CSS_URL ?>pages/portfolio.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL STYLES -->

        <!-- BEGIN THEME STYLES --> 
        <link href="<?= CSS_URL ?>style-metronic.css" rel="stylesheet" type="text/css"/>
        <link href="<?= CSS_URL ?>style.css" rel="stylesheet" type="text/css"/>
        <link href="<?= CSS_URL ?>style-responsive.css" rel="stylesheet" type="text/css"/>
        <link href="<?= CSS_URL ?>plugins.css" rel="stylesheet" type="text/css"/>
        <link href="<?= CSS_URL ?>pages/tasks.css" rel="stylesheet" type="text/css"/>
        <link href="<?= CSS_URL ?>themes/default.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="<?= CSS_URL ?>custom.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="<?= IMAGE_URL ?>favicon.ico"/>
    </head>
    <!-- BEGIN BODY -->
    <body class="page-header-fixed">
        <!-- BEGIN HEADER -->   
        <div class="header navbar navbar-inverse navbar-fixed-top">
            <?php $this->load->view('include/header_view', $JsArr); ?> 
        </div>
        <!-- END HEADER -->
        <div class="clearfix"></div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar navbar-collapse collapse">
                <?php $this->load->view('include/sidebar_view', $JsArr); ?> 
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN PAGE -->
            <div class="page-content">

                <!-- BEGIN PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title">
                            Albums
                        </h3>
                        <ul class="page-breadcrumb breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?= BASEURL ?>">Home</a> 
                                <i class="icon-angle-right"></i>
                            </li>
                            <li>
                                <a href="<?= BASEURL . 'albums' ?>">Albums</a> 
                                <i class="icon-angle-right"></i>
                            </li>

                            <li>Update Album</li>
                        </ul>
                        <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
                </div>
                <!-- END PAGE HEADER-->

                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet box purple">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-camera"></i>Update Album</div>

                            </div>
                            <div class="portlet-body form">
                                <?php if ($this->session->flashdata('success')) { ?>

                                    <div class="alert alert-success"><?php echo $this->session->flashdata('success') ?></div>

                                <?php } ?>
                                    
                                <!-- BEGIN FORM-->
                                <form class="form-horizontal" role="form" method="post" action="<?php echo BASEURL . 'albums/update/' ?>" name="form_update_album" id="form_update_album">
                                    <input type="hidden" name="album_id" id="album_id" value="<?php echo $album_id ?>"/>
                                    <input type="hidden" name="image_names" id="image_names" value=""/>
                                    <input type="hidden" name="folder_name" id="folder_name" value="<?php echo $folder_name ?>"/>
                                    <div class="form-body">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Album Title</label>
                                            <div class="col-md-4">
                                                <div class="input-icon">
                                                    <i class="icon-camera-retro"></i>
                                                    <input type="text" class="form-control" name="album_name" id="album_name" placeholder="New York's christmas evenings" maxlength="50" value="<?php echo $album_name ?>" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" id="photo_div">
                                            <label class="col-md-3 control-label">Photos</label>
                                            <div class="col-md-6">
                                                <div class="dropzone" id="my-dropzone"></div>
                                                <span class="help-block hide" style="color:#b94a48" id="album_error">At least five images required .</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Price</label>
                                            <div class="col-md-4">
                                                <div class="input-icon">
                                                    <i class="icon-dollar"></i>
                                                    <input type="text" class="form-control" name="album_price" id="album_price" placeholder="49.99" maxlength="10" value="<?php echo $album_price ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Product Id</label>
                                            <div class="col-md-4">
                                                <div class="input-icon">
                                                    <i class="icon-question-sign tooltips" data-placement="top" data-original-title="Product id will be used for In-App purchase. e.g. com.albumname"></i>
                                                    <input type="text" class="form-control" name="inapp_product_id" id="inapp_product_id" placeholder="com.albumname" value="<?php echo $inapp_product_id ?>">
                                                </div>
                                                <span class="help-block">Product id will be used for In-App purchase.</span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Downloads</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static">
                                                    <?php echo $album_download_count ?>
                                                </p>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Views</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static">
                                                    <?php echo $album_view_count ?>
                                                </p>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Likes</label>
                                            <div class="col-md-9">
                                                <p class="form-control-static">
                                                    <?php echo $album_favorite_count ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions fluid">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="button" class="btn default" onclick="location.href = '<?php echo BASEURL . 'albums' ?>'"><i class="icon-remove"></i> Cancel</button>                              
                                            <button type="submit" class="btn green"><i class="icon-edit"></i> Update</button>
                                            <button type="button" class="btn red" id="delete_album_details"><i class="icon-trash"></i> Delete Album</button>

                                        </div>
                                    </div>
                                </form>
                                <!-- END FORM-->
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>


                </div>

                <div class="clearfix"></div>
                <?php
                //mprd($album_images);
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet box purple">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-picture"></i>Images Of <?php echo $album_name ?></div>

                            </div>
                            <div class="portlet-body form">
                                <div class="alert alert-success hide msg_imgaes"></div>
                                <div class="tab-content">   
                                    <!-- BEGIN FORM-->
                                    <div class="row mix-grid" style="padding: 10px;">
                                        <?php
                                        if (isset($album_images) && count($album_images)) {

                                            foreach ($album_images as $Image) {
                                                ?>
                                        <div class="col-md-3 col-sm-4 mix" id="row_<?php echo $Image['image_id'] ?>">
                                                    <div class="mix-inner">
                                                        <img class="img-responsive" src="<?php echo $Image['image_url'] ?>" alt="">
                                                        <div class="mix-details">
                                                            <a class="mix-link delete_album_image" data-attr-id="<?php echo $Image['image_id'] ?>" data-attr-image="<?php echo $Image['original_image'] ?>"><i class="icon-trash"></i></a>
                                                            <a class="mix-preview fancybox-button" href="<?php echo $Image['image_url'] ?>" title="<?php echo $Image['album_name'] ?>" data-rel="fancybox-button"><i class="icon-search"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                                <!-- END FORM-->
                            </div>
                        </div>
                        <!-- END EXAMPLE TABLE PORTLET-->
                    </div>
                </div>

                <div class="clearfix"></div>

            </div>
            <!-- END PAGE -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
<?php $this->load->view('include/footer_view_inner', $JsArr); ?>
        <!-- END FOOTER -->

        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->   
        <!--[if lt IE 9]>
        <script src="<?= PLUGIN_URL ?>respond.min.js"></script>
        <script src="<?= PLUGIN_URL ?>excanvas.min.js"></script> 
        <![endif]-->   

        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?= PLUGIN_URL ?>jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="<?= PLUGIN_URL ?>dropzone/dropzone.js"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>jquery-mixitup/jquery.mixitup.min.js"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>fancybox/source/jquery.fancybox.pack.js"></script>  
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= JS_URL ?>app.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>form-validation.js"></script>
        <script src="<?= JS_URL ?>form-dropzone.js"></script>
        <script src="<?= JS_URL ?>portfolio.js"></script> 
        <script src="<?= JS_URL ?>custom/ajax.js"></script> 
        <script src="<?= JS_URL ?>custom/functions.js"></script>
        <!-- END PAGE LEVEL SCRIPTS -->  
        <script>
                                                jQuery(document).ready(function() {
                                                    App.init(); // initlayout and core plugins
                                                    FormValidation.init();
                                                    FormDropzone.init();
                                                    Portfolio.init();
                                                    $(".alert-success").delay(5000).slideUp(800);
                                                });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>