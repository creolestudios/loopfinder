<?php
$crntollerName = $this->uri->segment(1);
$fuctionName = $this->uri->segment(2);
?>
<div class="page-sidebar-wrapper">
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu page-sidebar-menu-compact" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li style="padding: 10px;text-align: center;" class="logo_inner start">
                <a href="<?= BASEURL . 'dashboard' ?>" class="" style="text-align:center"><img src="<?php echo IMAGE_URL ?>login_logo.png" class="logo-default" style="max-height: 163px;" alt="alt"></a>
            </li>
            <li class="start <?php
            if ($crntollerName == 'dashboard') {
                echo "active";
            }
            ?>">
                <a href="<?= BASEURL . 'dashboard' ?>">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                </a>
            </li>

            <li class="<?php
            if ($crntollerName == 'users') {
                echo "active";
            }
            ?>">
                <a href="<?= BASEURL . 'users' ?>">
                    <i class="icon-users"></i>
                    <span class="title">Users</span>
                    <?php
                    if ($crntollerName == 'users') {
                        echo '<span class="selected"></span>';
                    }
                    ?>
                </a>
            </li>

            <li class="<?php
            if ($crntollerName == 'venues') {
                echo "active";
            }
            ?>">
                <a href="<?= BASEURL . 'venues' ?>">
                    <i class="icon-pointer"></i>
                    <span class="title">Venues</span>
                    <?php
                    if ($crntollerName == 'venues') {
                        echo '<span class="selected"></span>';
                    }
                    ?>
                </a>
            </li>
            <li class="<?php
            if ($crntollerName == 'hlaachapters') {
                echo "active";
            }
            ?>">
                <a href="<?= BASEURL . 'hlaachapters' ?>">
                    <i class="icon-note"></i>
                    <span class="title">HLAA Chapters</span>
                    <?php
                    if ($crntollerName == 'hlaachapters') {
                        echo '<span class="selected"></span>';
                    }
                    ?>
                </a>
            </li>

            <li class="<?php
            if ($crntollerName == 'categories') {
                echo "active";
            }
            ?>">
                <a href="<?= BASEURL . 'categories' ?>">
                    <i class="icon-list"></i>
                    <span class="title">Categories</span>
                    <?php
                    if ($crntollerName == 'categories') {
                        echo '<span class="selected"></span>';
                    }
                    ?>
                </a>
            </li>
            <li class="<?php
            if ($crntollerName == 'feedback') {
                echo "active";
            }
            ?>">
                <a href="<?= BASEURL . 'feedback' ?>">
                    <i class="icon-note"></i>
                    <span class="title">Feedbacks</span>
                    <?php
                    if ($crntollerName == 'feedback') {
                        echo '<span class="selected"></span>';
                    }
                    ?>
                </a>
            </li>
            <li class="<?php
            if ($crntollerName == 'requestmessage') {
                echo "active";
            }
            ?>">
                <a href="<?= BASEURL . 'requestmessage' ?>">
                    <i class="icon-envelope-letter"></i>
                    <span class="title">Requests</span>
                    <?php
                    if ($crntollerName == 'requestmessage') {
                        echo '<span class="selected"></span>';
                    }
                    ?>
                </a>
            </li>
            
            

            <li class="<?php
            if ($crntollerName == 'settings') {
                echo "active";
            }
            ?>">
                <a href="<?= BASEURL . 'settings' ?>">
                    <i class="icon-user"></i>
                    <span class="title">My Account</span>
                    <?php
                    if ($crntollerName == 'settings') {
                        echo '<span class="selected"></span>';
                    }
                    ?>
                </a>
            </li>

            <li class="last">
                <a href="<?= BASEURL . 'logout' ?>">
                    <i class="icon-key"></i>
                    <span class="title">Logout</span>                    
                </a>
            </li>

        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<?php
/* $crntollerName = $this->uri->segment(1);
  $fuctionName = $this->uri->segment(2);

  <!-- BEGIN SIDEBAR MENU -->
  <ul class="page-sidebar-menu">
  <li style="display: none">
  <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
  <div class="sidebar-toggler hidden-phone"></div>
  <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
  </li>

  <li class="start <?php if($crntollerName == 'dashboard'){echo "active";}?> ">
  <a href="<?= BASEURL . 'dashboard' ?>">
  <i class="icon-home"></i>
  <span class="title">Dashboard</span>
  <span class="selected"></span>
  </a>
  </li>
  <li class="<?php if($crntollerName == 'customers'){echo "active";}?>">
  <a href="<?= BASEURL . 'customers' ?>">
  <i class="icon-group"></i>
  <span class="title">Users</span>
  </a>
  </li>
  <li class="<?php if($crntollerName == 'albums'){echo "active";}?>">
  <a href="<?= BASEURL . 'albums' ?>">
  <i class="icon-camera"></i>
  <span class="title">Albums</span>
  </a>
  </li>
  <li class="<?php if($crntollerName == 'downloads'){echo "active";}?>">
  <a href="<?= BASEURL . 'downloads' ?>">
  <i class="icon-download-alt"></i>
  <span class="title">Downloads</span>
  </a>
  </li>
  <li class="<?php if($crntollerName == 'news'){echo "active";}?>">
  <a href="<?= BASEURL . 'news' ?>">
  <i class="icon-bullhorn"></i>
  <span class="title">News</span>
  </a>
  </li>
  <li class="<?php if($crntollerName == 'settings'){echo "active";}?>">
  <a href="<?=BASEURL.'settings'?>">
  <i class="icon-user"></i>
  <span class="title">My Profile</span>
  </a>
  </li>
  <li class="<?php if($crntollerName == 'logout'){echo "active";}?>">
  <a href="<?=BASEURL.'logout'?>">
  <i class="icon-signout"></i>
  <span class="title">Log Out</span>
  </a>
  </li>
  <li class="start logo_side_bar" style="margin-left: 30px;">
  <a class="navbar-brand" href="<?=BASEURL.'dashboard'?>">
  <img src="<?=IMAGE_URL?>logo-big.png" alt="logo" class="img-responsive" />
  </a>
  </li>

  </ul>
  <!-- END SIDEBAR MENU --> */
?>