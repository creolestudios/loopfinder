<?php
$crntollerName = $this->uri->segment(1);
$fuctionName = $this->uri->segment(2);

?>
<div class="page-header-inner">
    <!-- BEGIN LOGO -->
    <?php /*?><div class="page-logo " style="background: #4d5b69">
        <a href="<?= BASEURL . 'dashboard' ?>" style="margin-left: 8px " class="hide">
            <img src="<?php echo IMAGE_URL ?>logo_inner.png" alt="logo" class="logo-default" style="max-height: 163px;"/>
        </a>
        <div class="menu-toggler sidebar-toggler hide">
            <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
        </div>
    </div><?php */?>
    <!-- END LOGO -->
    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
    </a>
    <!-- END RESPONSIVE MENU TOGGLER -->

    <!-- BEGIN PAGE TOP -->
    <div class="page-top" style="box-shadow:none">

        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu" style="display: none;">
            <ul class="nav navbar-nav pull-right">                
                <!-- BEGIN USER LOGIN DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <img alt="" class="img-circle" src="<?php echo IMAGE_URL ?>avatar3_small.jpg"/>
                        <span class="username username-hide-on-mobile">
                            <?php echo $this->session->userdata('M_ADMINFIRSTNAME') . " " . $this->session->userdata('M_ADMINLASTNAME'); ?> </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="<?= BASEURL . 'settings' ?>">
                                <i class="icon-user"></i> My Account</a>
                        </li>
                        <li class="divider"></li>                        
                        <li>
                            <a href="<?= BASEURL . 'logout' ?>">
                                <i class="icon-key"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
                <!-- END USER LOGIN DROPDOWN -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END PAGE TOP -->
</div>
