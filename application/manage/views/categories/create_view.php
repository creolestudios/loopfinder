<?php
$headerData = $this->headerlib->data();
$JsArr = array("SCRIPT" => $headerData['javascript'], 'PLUGINS' => $headerData['javascript_plugins']);
/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES
 * *************************************************** */

/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES ENDS
 * *************************************************** */
?>
<!doctype html>
<html lang="en-us">
    <head>
        <title><?= MAINTITLE; ?></title>
        <?= $headerData['meta_tags']; ?>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?= $headerData['plugins']; ?>
        <?= $headerData['stylesheets']; ?>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->          
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <?= $headerData['golbalmandatory']; ?>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 

        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>bootstrap-toastr/toastr.min.css"/>
        <!-- END PAGE LEVEL PLUGIN STYLES -->

        <!-- BEGIN PAGE STYLES -->        
        <link rel="stylesheet" type="text/css" href="<?= CSS_URL ?>cropper.css"/>
        <!-- END PAGE STYLES -->
        <!-- BEGIN THEME STYLES --> 
        <?= $headerData['themestyles']; ?>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="<?= IMAGE_URL ?>favicon.ico"/>        
    </head>
    <!-- BEGIN BODY -->
    <body class="page-sidebar-closed-hide-logo page-header-fixed page-sidebar-fixed">
        <!-- BEGIN HEADER -->   
        <div class="page-header navbar navbar-fixed-top">
            <?php $this->load->view('include/header_view', $JsArr); ?> 
        </div>
        <!-- END HEADER -->
        <div class="clearfix"></div>

        <div class="page-container">

            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar navbar-collapse collapse">
                    <?php $this->load->view('include/sidebar_view', $JsArr); ?> 
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN PAGE -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">Categories</h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?= BASEURL . 'dashboard' ?>">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <i class="fa fa-list"></i>
                                <a href="<?= BASEURL . 'categories' ?>">Categories</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="javascript:void(0)">Create Category</a>
                            </li>
                        </ul>                        
                    </div>
                    <!-- END PAGE HEADER-->                   

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="row">
                                <div class="col-md-12">

                                    <!-- Begin: life time stats -->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-list font-green-meadow bold"></i>
                                                <span class="caption-subject font-green-meadow bold uppercase">Create Category</span>                                                
                                            </div>                                            
                                        </div>                                      

                                        <div class="portlet-body">
                                            <div class="portlet-body form">
                                                <!-- BEGIN FORM-->
                                                <form action="<?php echo BASEURL . 'categories/saveCategory' ?>" class="form-horizontal" name="catgory_form" id="category_form" method="post" enctype="multipart/form-data">
                                                    <input type="hidden" name="action" value="create"/>
                                                    <div class="form-body">                                                        
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Name : <span class="required" aria-required="true">* </span></label>
                                                            <div class="col-md-3">
                                                                <input type="text" name="category_name" id="category_name" class="form-control input-medium" placeholder="Category Name">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Priority : <span class="required" aria-required="true">* </span></label>
                                                            <div class="col-md-3">
                                                                <input type="text" name="category_priority" id="category_priority" class="form-control input-medium" placeholder="Category Priority">
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label">Image : <span class="required" aria-required="true">* </span></label>
                                                            <div class="col-md-9">
                                                                <div id="tmp_pre_image" 
                                                                     class="preview" 
                                                                     onclick="$('#category_file').click();"
                                                                     style="max-height:200px; max-width:200px; display: none;overflow: hidden"
                                                                     /> 
                                                                <img src="" 
                                                                     id="category_image" 
                                                                     class="img-responsive" 
                                                                     alt="" 
                                                                     style="cursor:pointer;"/>
                                                            </div>
                                                            <div id="pre_image" onclick="$('#category_file').click();" 
                                                                 style="max-height:200px;max-width:200px;" 
                                                                 title="Click for change image">         
                                                                <img src="<?php echo IMAGE_URL . 'category_default.png' ?>" 
                                                                     class="img-responsive" 
                                                                     alt="Image not available."  
                                                                     style="cursor:pointer; max-height:200px; max-width:200px;" /> 
                                                            </div>

                                                            <input type="file" name="category_file" id="category_file" onclick="this.value = '';" onchange="readURL(this);" style="display:none">
                                                            <input type="hidden" name="x1" id="x1" />
                                                            <input type="hidden" name="x2" id="x2" />
                                                            <input type="hidden" name="y1" id="y1" />
                                                            <input type="hidden" name="y2" id="y2" />
                                                            <input type="hidden" name="w" id="w" />
                                                            <input type="hidden" name="h" id="h" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Looped Pin : </label>
                                                        <div class="col-md-3">
                                                            <input type="file" name="category_pin" id="category_pin" style="margin-top: 8px"/>

                                                        </div>
                                                        <span>
                                                            <img src="<?php echo UPLOADS_URL . 'map-pin-default.png'; ?>" alt="Category Pin" name="cat_pin_change" id="cat_pin_change"/>
                                                        </span>
                                                    </div>
                                                        <div class="form-group">
                                                        <label class="col-md-3 control-label">Not Looped Pin : </label>
                                                        <div class="col-md-3">
                                                            <input type="file" name="category_pin2" id="category_pin2" style="margin-top: 8px"/>

                                                        </div>
                                                        <span>
                                                            <img src="<?php echo UPLOADS_URL . 'map-pin-default_grey.png'; ?>" alt="Category Pin2" name="cat_pin_change2" id="cat_pin_change"/>
                                                        </span>
                                                    </div>

                                                    <!--                                                    <div class="form-group">
                                                                                                            <label class="col-md-3 control-label">Pin : </label>
                                                                                                            <div class="col-md-3">
                                                                                                                <select class="form-control" name="category_pin" id="category_pin">
                                                                                                                    <option value="">-- Select Pin --</option>
                                                    <?php
                                                    if (isset($category_pins) && !empty($category_pins)) {
                                                        foreach ($category_pins as $PinVal) {
                                                            ?>
                                                                                                                                                            <option value="<?php echo $PinVal['image_name'] ?>"><?php echo $PinVal['name'] ?></option>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                                                                                </select>
                                                    
                                                                                                            </div>
                                                                                                            <span>
                                                                                                                <img src="<?php echo IMAGE_URL . 'map-pin-default.png'; ?>" alt="Category Pin" name="cat_pin_change" id="cat_pin_change"/>
                                                                                                            </span>
                                                                                                        </div>-->

                                            </div>
                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn green-meadow tooltips" title="Click to save"><i class="fa fa-save"></i> Save</button>
                                                        <button type="button" class="btn default tooltips" title="Click to cancel" onclick="location.href = '<?= BASEURL . 'categories' ?>'">Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                            </form>
                                            <!-- END FORM-->
                                        </div>
                                    </div>
                                </div>
                                <!-- End: life time stats -->
                            </div>
                        </div>

                    </div>  
                    <!-- END DASHBOARD STATS -->
                </div>
            </div>
            <!-- END PAGE -->
        </div>
    </div>
    <button class="btn btn-primary btn-lg" id="modal_call" data-toggle="modal" data-target="#image_crop" style="display:none">
        Open
    </button>
    <div class="modal fade" id="image_crop" tabindex="-1" role="dialog" aria-labelledby="image_crop" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="" id="modal_image" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default blue" data-dismiss="modal" onclick="make_select_img();">Ok</button>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <?php $this->load->view('include/footer_view_inner', $JsArr); ?>
    <!-- END FOOTER -->

    <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <!-- BEGIN CORE PLUGINS -->   
    <!--[if lt IE 9]>
    <script src="<?= PLUGIN_URL ?>respond.min.js"></script>
    <script src="<?= PLUGIN_URL ?>excanvas.min.js"></script> 
    <![endif]-->   

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="<?= PLUGIN_URL ?>bootstrap-toastr/toastr.min.js"></script>
    <script type="text/javascript" src="<?= PLUGIN_URL ?>jquery-validation/js/jquery.validate.min.js"></script>
    <script src="<?= JS_URL ?>cropper.js"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->

    <script src="<?= JS_URL ?>metronic.js" type="text/javascript"></script>
    <script src="<?= JS_URL ?>layout.js" type="text/javascript"></script>        
    <script src="<?= JS_URL ?>form-validation.js"></script>


    <!-- END PAGE LEVEL SCRIPTS -->  
    <script type="text/javascript">
                        var car_image = $('#category_image').attr('src');
                        selector = "#modal_image";
                        setImageCropper(selector);
                        function make_select_img() {
                            $new_ele = $("#tmp_pre_image").clone(true);
                            $new_ele.attr("id", "pre_image");
                            $new_ele.removeClass("preview");
                            $("#tmp_pre_image").css('display', 'block');
                            $("#pre_image").remove();
                            $("#main_mix_inner").append($new_ele);
                            $('#db_img').css('display', 'none');
                        }



                        function  readURL(input)
                        {
                            // $('#staff_image').attr('src', staff_img);
                            if (input.files && input.files[0])
                            {
                                var reader = new FileReader();
                                reader.onload = function (e)
                                {
                                    $('#modal_call').click();
                                    setTimeout(function () {
                                        //$('#modal_image').attr('src',e.target.result);
                                        $('#modal_image').cropper('setImgSrc', e.target.result);
                                    }, 150);
                                };
                                reader.readAsDataURL(input.files[0]);
                            }
                        }
                        function setImageCropper(selector) {
                            $(selector).cropper({
                                //aspectRatio: 1.6,
                                preview: "#tmp_pre_image",
                                data: {
                                    width: 800,
                                    height: 600                                   

                                },
                                resizable: false,
                                done: function (data) {
                                    console.log(data);

                                    $("#x1").val(data.x1);
                                    $("#x2").val(data.x2);
                                    $("#y1").val(data.y1);
                                    $("#y2").val(data.y2);
                                    $("#h").val(data.height);
                                    $("#w").val(data.width);
                                    $(selector).on("dragend", function ()
                                    {
                                        height_temp = $("#h").val();
                                        width_temp = $("#w").val();
                                        if (height_temp < 600 || width_temp < 800 || height_temp > 600 || width_temp > 800)
                                        {
                                            $(selector).cropper("setData", {width: 800, height: 600})
                                            //data.height
                                        }
                                    });
                                }
                            });
                        }
                        jQuery(document).ready(function () {
                            Metronic.init(); // initlayout and core plugins
                            FormValidation.init();
                            Layout.init();

                            //change icon on change on category pin dropdown
                            $(document).on("change", "#category_pin", function () {
                                //get image from category icon folder related to selected category
                                //$("#cat_pin_change").attr('src', "<?php echo UPLOADS_URL . 'category_icons/' ?>" + $(this).val());

                            });

                            $(document).keyup(function (e) {
                                if (e.keyCode === 27) {
                                    $('#db_img').css('display', 'block');
                                    $('#tmp_pre_image').css('display', 'none');
                                    $('#pre_image').css('display', 'none');
                                }
                            });

<?php if ($this->session->flashdata('error')) { ?>
                                toastr.error('<?php echo $this->session->flashdata('error') ?>', 'Categories');
<?php } ?>

<?php if ($this->session->flashdata('success')) { ?>
                                toastr.success('<?php echo $this->session->flashdata('success') ?>', 'Categories');
<?php } ?>
    
                       });
    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>