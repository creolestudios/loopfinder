<?php
$headerData = $this->headerlib->data();
$JsArr = array("SCRIPT" => $headerData['javascript'], 'PLUGINS' => $headerData['javascript_plugins']);
/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES
 * *************************************************** */

/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES ENDS
 * *************************************************** */
?>
<!doctype html>
<html lang="en-us">
    <head>
        <title><?= MAINTITLE; ?></title>
        <?= $headerData['meta_tags']; ?>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?= $headerData['plugins']; ?>
        <?= $headerData['stylesheets']; ?>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->          
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <?= $headerData['golbalmandatory']; ?>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>bootstrap-toastr/toastr.min.css"/>
        <!-- END PAGE LEVEL PLUGIN STYLES -->

        <!-- BEGIN PAGE STYLES -->

        <!-- END PAGE STYLES -->

        <!-- BEGIN THEME STYLES --> 
        <?= $headerData['themestyles']; ?>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="<?= IMAGE_URL ?>favicon.ico"/>        
    </head>
    <!-- BEGIN BODY -->
    <body class="page-sidebar-closed-hide-logo page-header-fixed page-sidebar-fixed">
        <!-- BEGIN HEADER -->   
        <div class="page-header navbar navbar-fixed-top">
            <?php $this->load->view('include/header_view', $JsArr); ?> 
        </div>
        <!-- END HEADER -->
        <div class="clearfix"></div>

        <div class="page-container">

            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar navbar-collapse collapse">
                    <?php $this->load->view('include/sidebar_view', $JsArr); ?> 
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN PAGE -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">Categories</h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?= BASEURL . 'dashboard' ?>">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="javascript:void(0)">Categories</a>
                            </li>
                        </ul>                        
                    </div>
                    <!-- END PAGE HEADER-->                   

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="row">
                                <div class="col-md-12">

                                    <!-- Begin: life time stats -->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-list font-green-meadow bold"></i>
                                                <span class="caption-subject font-green-meadow bold uppercase">Categories</span>                                                
                                            </div>
                                            <div class="actions">
                                                <a href="<?php echo BASEURL . 'categories/ExportCategories/' ?>" class="btn btn-circle green-meadow tooltips" title="Export to CSV">
                                                    <i class="fa fa-file-excel-o"></i> Export 
                                                </a>
                                                <a href="<?= BASEURL . 'categories/Create' ?>" class="btn btn-circle green-meadow tooltips" title="Add new category">
                                                    <i class="fa fa-plus"></i> New Category </a>

                                            </div>
                                        </div>                                      

                                        <div class="portlet-body">
                                            <table class="table table-striped table-bordered table-hover" id="category_table">
                                                <thead>
                                                    <tr>
                                                       
                                                        <th>
                                                            Category Name
                                                        </th>
                                                        <th>
                                                            Priority
                                                        </th>
                                                        <th>
                                                            Image
                                                        </th>
                                                        <th>
                                                           Looped Pin
                                                        </th>
                                                         <th>
                                                           Not Looped Pin
                                                        </th>
                                                        <th>
                                                            Details
                                                        </th>                                                        
                                                    </tr>
                                                    <tr>

                                                        <td>
                                                            <div class="input-icon right">
                                                                <i class="fa fa-search"></i>
                                                                <input type="text" class="form-control" placeholder="Category Name" name="search_name" id="search_name">
                                                            </div>

                                                        </td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </thead>

                                            </table>
                                        </div>
                                    </div>
                                    <!-- End: life time stats -->
                                </div>
                            </div>

                        </div>  
                        <!-- END DASHBOARD STATS -->
                    </div>
                </div>
                <!-- END PAGE -->
            </div>
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php $this->load->view('include/footer_view_inner', $JsArr); ?>
        <!-- END FOOTER -->

        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->   
        <!--[if lt IE 9]>
        <script src="<?= PLUGIN_URL ?>respond.min.js"></script>
        <script src="<?= PLUGIN_URL ?>excanvas.min.js"></script> 
        <![endif]-->   

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?= PLUGIN_URL ?>bootstrap-toastr/toastr.min.js"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>datatables/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
        <script src="<?= PLUGIN_URL ?>bootbox/bootbox.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= JS_URL ?>metronic.js" type="text/javascript"></script>        
        <script src="<?= JS_URL ?>layout.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>custom/ajax.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>custom/functions.js" type="text/javascript"></script>

        <!-- END PAGE LEVEL SCRIPTS -->  
        <script>
            jQuery(document).ready(function () {

                //initiliaze
                Metronic.init(); // initlayout and core plugins
                Layout.init();

                var big_table = $('#category_table').dataTable({
                    "bLengthChange": true,
                    "bProcessing": true,
                    "bServerSide": true,
                     "order": [1, 'asc' ],
                    "bSortCellsTop": true,
                    //"aaSorting": [[2, 'asc']],
                    "loadingMessage": 'Loading...',
                    "sPaginationType": "full_numbers",
                    "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ categorie(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No Categorie(s) found.",
                        "oPaginate": {
                            "sFirst": "<<",
                            "sLast": ">>",
                            "sNext": ">",
                            "sPrevious": "<",
                        },
                    },
                    "sAjaxSource": BASEURL + "categories/GetCategoryList",
                    "sServerMethod": "POST",
                    "fnServerParams": function (aoData) {
                        aoData.push(
                                {"name": "name", "value": $('#search_name').val()}


                        );
                    },
                    "aoColumns": [
                        {"data": "category_name", "width": "10%", "bSearchable": true, "bSortable": true},
                        {"data": "priority", "width": "5%", "bSearchable": true, "bSortable": true},
                        {"data": "category_image", "width": "15%", "bSortable": false, "bSearchable": false, "mRender": function (data, type, full) {

                                return '<img src="' + data + '" alt="category_image" style="max-width:100px;max-height:60px" />';
                            }},
                        {"data": "category_pin", "width": "5%", "bSortable": false, "bSearchable": false, "mRender": function (data, type, full) {

                                return '<img src="' + data + '" alt="category_pin" style="max-width:100px;max-height:60px" />';
                            }},
                        {"data": "category_pin2", "width": "5%", "bSortable": false, "bSearchable": false, "mRender": function (data, type, full) {

                                return '<img src="' + data + '" alt="category_pin2" style="max-width:100px;max-height:60px" />';
                            }},
                        {"data": "detail", "width": "10%", "bSearchable": false, "bSortable": false, "mRender": function (data, type, full) {
                                return '<a href="' + BASEURL + 'categories/Update/' + base64_encode(data) + '" class="btn btn-circle btn-sm blue"><i class="fa fa-edit"></i> Edit </a><a href="javascript:void(0)" class="btn btn-circle btn-sm red delete_category" data-attr-id="' + data + '"><i class="fa fa-trash-o"></i> Delete </a>';
                            }}
                    ]
                });
                $('.dataTables_filter').css("display", "none");
//                $('.dataTables_filter input').attr("placeholder", "Search by name");
//                $('.dataTables_filter').addClass('input-group');
//                $('.dataTables_filter').append('<span class="input-group-btn"><button class="btn green-meadow search_btn"" type="button"><i class="fa fa-search"></i></button></span>');
//                $('.dataTables_filter input').removeClass('input-small').addClass('input-medium');
//                $('.dataTables_filter label').css('margin-bottom', '0px');
//                $('.dataTables_filter input').unbind();
//                $('.dataTables_filter input').on('keyup', function (e)
//                {
//                    if (e.keyCode == 13)
//                    {
//                        big_table.fnFilter(this.value);
//                    }
//                });
//                $(".search_btn").on('click', function () {
//                    var string_search = $('.dataTables_filter input').val();
//                    big_table.fnFilter(string_search);
//                });

                $('#search_name').change(function () {
                    big_table.fnFilter()
                });


                //toastr notification settings
                toastr.options = {
                    "closeButton": true,
                    "positionClass": "toast-top-right",
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "3000",
                    "extendedTimeOut": "1000",
                    "showMethod": "slideDown",
                    "hideMethod": "slideUp"
                }
                //message section for each operation
<?php if ($this->session->flashdata('success')) { ?>
                    toastr.success('<?php echo $this->session->flashdata('success') ?>', 'Categories');
<?php } ?>

<?php if ($this->session->flashdata('error')) { ?>
                    toastr.error('<?php echo $this->session->flashdata('success') ?>', 'Categories');
<?php } ?>
            });

        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>