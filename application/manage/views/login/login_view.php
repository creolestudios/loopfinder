<?php
$headerData = $this->headerlib->data();

$JsArr = array("SCRIPT" => $headerData['javascript'], 
                'PLUGINS' => $headerData['javascript_plugins']);

/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES
 * *************************************************** */

/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES ENDS
 * *************************************************** */
?>
<!doctype html>
<html lang="en-us">
    <head>
        <title><?= MAINTITLE; ?></title>
        <?= $headerData['meta_tags']; ?>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?= $headerData['plugins']; ?>
        
        <!-- BEGIN PAGE LEVEL STYLES --> 
            <link rel="stylesheet" type="text/css" href="<?= CSS_URL ?>pages/login3.css" />
        <!-- END PAGE LEVEL SCRIPTS -->

        <!-- GLOBAL MANDATORY STYLES --> 
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <?= $headerData['golbalmandatory']; ?>
        <!-- END GLOBAL MANDATORY STYLES -->
        
        <!-- BEGIN THEME STYLES --> 
        <?= $headerData['themestyles']; ?>        
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="<?= IMAGE_URL ?>favicon.ico" />

    </head>
    <!-- BEGIN BODY -->
    <body class="login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <img src="<?= IMAGE_URL ?>login_logo.png" alt="" style="max-width:126px;max-height: 163px;"/> 
        </div>
        <!-- END LOGO -->

        <!-- BEGIN LOGIN -->
        <div class="content">

            <div class="alert hide" id="login_msg"></div>
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="" method="post">
		<h3 class="form-title">Login to your account</h3>
		<div class="alert alert-danger display-hide">
			<button class="close" data-close="alert"></button>
			<span>
			Enter any username and password. </span>
		</div>
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">Email</label>
			<div class="input-icon">
				<i class="fa fa-user"></i>
				<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" id="login_email"/>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Password</label>
			<div class="input-icon">
				<i class="fa fa-lock"></i>
				<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" id="login_password"/>
			</div>
		</div>
		<div class="form-actions">
			<label class="checkbox">
			<input type="checkbox" name="remember" id="remember"/> Remember me </label>
			<button type="submit" class="btn green-meadow pull-right">
			Login <i class="m-icon-swapright m-icon-white"></i>
			</button>
		</div>		
		<div class="forget-password">
                    <h4><a href="javascript:;" id="forget-password" style="text-decoration: none;color: #000">Forgot your password ?</a></h4>			
		</div>
		
	</form>
       
            <!-- BEGIN FORGOT PASSWORD FORM -->

            <form class="forget-form" action="" method="post">
                <div id="forgotPass_msg"></div>
                <h3 >Forget Password ?</h3>
                <p>Enter your e-mail address to reset your password.</p>
                <div class="form-group">
                    <div class="input-icon">
                        <i class="icon-envelope"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="forgot_pass_email" id="forgot_pass_email" />
                    </div>
                </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn">
                        <i class="m-icon-swapleft"></i> Back
                    </button>
                    <button type="submit" class="btn green-meadow pull-right">
                        Submit <i class="m-icon-swapright m-icon-white"></i>
                    </button>            
                </div>
            </form>
            <!-- END FORGOT PASSWORD FORM -->

        </div>
        <!-- END LOGIN -->

        <!-- BEGIN GLOBAL MANDATORY JS -->
        <?php $this->load->view('include/footer_view', $JsArr); ?> 
        <!-- END GLOBAL MANDATORY JS -->

        <!-- BEGIN CORE PLUGINS -->   
        <!--[if lt IE 9]>
        <script src="<?= PLUGIN_URL ?>respond.min.js"></script>
        <script src="<?= PLUGIN_URL ?>excanvas.min.js"></script> 
        <![endif]-->   

        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?= PLUGIN_URL ?>jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>	

        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo JS_URL ?>metronic.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>login.js" type="text/javascript"></script> 
        <!-- END PAGE LEVEL SCRIPTS --> 
        <script>
            jQuery(document).ready(function () {
                Metronic.init();
                Login.init();
            });
        </script>
    </body>
    <!-- END BODY -->
</html>