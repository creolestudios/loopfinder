<?php
$headerData = $this->headerlib->data();
$JsArr = array("SCRIPT" => $headerData['javascript'], 'PLUGINS' => $headerData['javascript_plugins']);
/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES
 * *************************************************** */

/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES ENDS
 * *************************************************** */
if (isset($account_settings_data) && count($account_settings_data) > 0) {
    extract($account_settings_data);
    extract();
}
?>
<!doctype html>
<html lang="en-us">
    <head>
        <title><?= MAINTITLE; ?></title>
        <?= $headerData['meta_tags']; ?>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?= $headerData['plugins']; ?>
        <?= $headerData['stylesheets']; ?>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->          
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <?= $headerData['golbalmandatory']; ?>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 

        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>bootstrap-toastr/toastr.min.css"/>
        <link rel="stylesheet" type="text/css" href="<?= CSS_URL ?>editor.css"/>
        <link rel="stylesheet" type="text/css" href="<?= CSS_URL ?>components.css"/>
        <!-- END PAGE LEVEL PLUGIN STYLES -->

        <!-- BEGIN PAGE STYLES -->

        <!-- END PAGE STYLES -->
        <!-- BEGIN THEME STYLES --> 
        <?= $headerData['themestyles']; ?>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="<?= IMAGE_URL ?>favicon.ico"/>
        <style>
            .control-label{text-align: left !important}
        </style>
    </head>
    <!-- BEGIN BODY -->
    <body class="page-sidebar-closed-hide-logo page-header-fixed page-sidebar-fixed">
        <!-- BEGIN HEADER -->   
        <div class="page-header navbar navbar-fixed-top">
            <?php $this->load->view('include/header_view', $JsArr); ?> 
        </div>
        <!-- END HEADER -->
        <div class="clearfix"></div>

        <div class="page-container">

            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar navbar-collapse collapse">
                    <?php $this->load->view('include/sidebar_view', $JsArr); ?> 
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN PAGE -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">My Account</h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?= BASEURL . 'dashboard' ?>">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="javascript:void(0)">My Account</a>
                            </li>
                        </ul>                        
                    </div>
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="row">
                                <div class="col-md-12">

                                    <!-- Begin: life time stats -->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-user font-green-sharp bold"></i>
                                                <span class="caption-subject font-green-sharp bold uppercase">My Account</span>                                                
                                            </div>                                            
                                        </div>
                                        <div class="portlet-body" id="div_account_info">
                                            <form class="form-horizontal" role="form">
                                                <div class="form-body">
                                                    <h2 class="margin-bottom-20  bold"><?= $admin_fname . " " . $admin_lname ?> </h2>                                                    
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">First Name:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?= $admin_fname ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->


                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Last Name:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?= $admin_lname ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/row-->
                                                    </div>

                                                    <div class="row ">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Email:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?= $admin_email ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->

                                                    </div>

                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-9">
                                                                    <button type="button" class="btn green-meadow tooltips" title="Click to edit details" id="btn_account_edit"><i class="fa fa-pencil-square-o"></i> Edit</button>                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>

                                        <div class="portlet-body form" id="div_update_account" style="display: none;">
                                            <!-- BEGIN FORM-->
                                            <form action="" name="account_info" id="account_info"  method="post" class="form-horizontal">
                                                <input type="hidden" name="admin_id" id="admin_id" value="<?= $admin_id ?>"/>
                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" style="width: 15%">First Name</label>
                                                        <div class="col-md-4">
                                                            <div class="input-icon">
                                                                <i class="fa fa-lock"></i>
                                                                <input type="text" placeholder="John" class="form-control" name="admin_fname" id="admin_fname" value="<?= $admin_fname ?>">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label"  style="width: 15%">Last Name</label>
                                                        <div class="col-md-4">
                                                            <div class="input-icon">
                                                                <i class="fa fa-lock"></i>
                                                                <input type="text" placeholder="Doe" class="form-control"  name="admin_lname" id="admin_lname" value="<?= $admin_lname ?>">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group last">
                                                        <label class="col-md-3 control-label"  style="width: 15%">Email</label>
                                                        <div class="col-md-4">
                                                            <div class="input-icon">
                                                                <i class="fa fa-lock"></i>
                                                                <input type="text" placeholder="John.doe@gmail.com" class="form-control" name="admin_email" id="admin_email" value="<?= $admin_email ?>">
                                                            </div>
                                                        </div>
                                                    </div>  

                                                    <div class="form-group last" style="display: none;">
                                                        <label class="col-md-3 control-label"  style="width: 15%">MObile</label>
                                                        <div class="col-md-4">
                                                            <div class="input-icon">
                                                                <i class="fa fa-lock"></i>
                                                                <input type="text" placeholder="+1 646 580" class="form-control" name="mobile_number" id="mobile_number"  value="<?= $mobile_number ?>">
                                                            </div>
                                                        </div>
                                                    </div>  

                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-9">
                                                            <button type="submit" class="btn green-meadow"><i class="fa fa-save"></i> Save</button>
                                                            <button type="button" class="btn" id="cancel_account_update">Cancel</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <!-- END FORM-->
                                        </div>                                        
                                    </div>
                                    <!-- End: life time stats -->
                                </div>
                            </div>

                        </div>  
                        <!-- END DASHBOARD STATS -->
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="row">
                                <div class="col-md-12">

                                    <!-- Begin: life time stats -->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-lock font-green-meadow bold"></i>
                                                <span class="caption-subject font-green-meadow bold uppercase">Update Password</span>                                                
                                            </div>                                            
                                        </div>                                      

                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <form action="" name="change_password" id="change_password"  method="post" class="form-horizontal">

                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" style="width: 15%">Current Password</label>
                                                        <div class="col-md-4">
                                                            <div class="input-icon">
                                                                <i class="fa fa-lock"></i>
                                                                <input type="password" class="form-control" name="old_password" id="old_password" placeholder="Current Password">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label"  style="width: 15%">New Password</label>
                                                        <div class="col-md-4">
                                                            <div class="input-icon">
                                                                <i class="fa fa-lock"></i>
                                                                <input type="password" class="form-control" name="new_password" id="new_password" placeholder="New Password">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group last">
                                                        <label class="col-md-3 control-label"  style="width: 15%">Confirm Password</label>
                                                        <div class="col-md-4">
                                                            <div class="input-icon">
                                                                <i class="fa fa-lock"></i>
                                                                <input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="Confirm Password">
                                                            </div>
                                                        </div>
                                                    </div>                                                       

                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-9">
                                                            <button type="submit" class="btn green-meadow tooltips" title="Click to update password"><i class="fa fa-save"></i> Update</button>
                                                            <a href="<?= BASEURL . 'dashboard' ?>" class="btn default">Cancel</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <!-- END FORM-->
                                        </div>
                                    </div>
                                    <!-- End: life time stats -->
                                </div>
                            </div>

                        </div>  
                        <!-- END DASHBOARD STATS -->
                    </div>
                 
                    <!---- PRIVACY POLICY CONTENT ----->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="row">
                                <div class="col-md-12">

                                    <!-- Begin: life time stats -->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-lock font-green-meadow bold"></i>
                                                <span class="caption-subject font-green-meadow bold uppercase">Privacy Policy</span>                                                
                                            </div>                                            
                                        </div>                                      
                                     
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <form name="change_content" id="change_content" action="<?= BASEURL.'settings/savecontent'?>"  method="post" class="form-horizontal">

                                                <div class="form-body">
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label" style="width: 15%">Title</label>
                                                        <div class="col-md-4">
                                                            <div class="input-icon">
                                                                <i class="fa fa-font"></i>
                                                                <input type="text" class="form-control" name="old_title" id="old_title" placeholder="" value="<?=$contentdata['title'] ?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group last">
                                                        <label class="col-md-3 control-label" style="width: 15%">Content</label>
                                                        <div class="col-md-9">
                                                            <textarea class="ckeditor form-control" name="editor1" rows="6"><?php echo $contentdata['text']; ?></textarea>
                                                        </div>
                                                    </div>                                                      

                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-9">
                                                            <button type="submit" class="btn green-meadow tooltips" title="Click to update content"><i class="fa fa-save"></i> Update</button>
                                                            <a href="<?= BASEURL . 'dashboard' ?>" class="btn default">Cancel</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <!-- END FORM-->
                                        </div>
                                    </div>
                                    <!-- End: life time stats -->
                                </div>
                            </div>

                        </div>  

                    </div>
                    <!-- END DASHBOARD----->
                </div>
                <!-- END PAGE -->
            </div>
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php $this->load->view('include/footer_view_inner', $JsArr); ?>
        <!-- END FOOTER -->

        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->   
        <!--[if lt IE 9]>
        <script src="<?= PLUGIN_URL ?>respond.min.js"></script>
        <script src="<?= PLUGIN_URL ?>excanvas.min.js"></script> 
        <![endif]-->   

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?= PLUGIN_URL ?>bootstrap-toastr/toastr.min.js"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>jquery-validation/js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>datatables/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= JS_URL ?>metronic.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>layout.js" type="text/javascript"></script>        
        <script src="<?= JS_URL ?>form-validation.js"></script>
        <script src="<?= PLUGIN_URL ?>ckeditor/ckeditor.js" type="text/javascript"></script>

        <!-- END PAGE LEVEL SCRIPTS -->  
        <script>
            jQuery(document).ready(function () {
                Metronic.init(); // initlayout and core plugins
                FormValidation.init();
                Layout.init();
                //show/hide update and info div
                $(document).on("click", "#btn_account_edit", function () {
                    $("#div_account_info").hide();
                    $("#div_update_account").show();
                })
                //cancel button event
                $(document).on("click", "#cancel_account_update", function () {
                    $("#div_account_info").show();
                    $("#div_update_account").hide();
                })

                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "positionClass": "toast-top-right",
                    "onclick": null,
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "3000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "slideDown",
                    "hideMethod": "slideUp"
                }

            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>