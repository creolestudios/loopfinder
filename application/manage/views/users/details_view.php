<?php
$headerData = $this->headerlib->data();
$JsArr = array("SCRIPT" => $headerData['javascript'], 'PLUGINS' => $headerData['javascript_plugins']);
/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES
 * *************************************************** */

/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES ENDS
 * *************************************************** */
//mprd($customer_details);
if (isset($customer_details) && !empty($customer_details)) {
    //extract user data
    extract($customer_details);
}
?>
<!doctype html>
<html lang="en-us">
    <head>
        <title><?= MAINTITLE; ?></title>
        <?= $headerData['meta_tags']; ?>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?= $headerData['plugins']; ?>
        <?= $headerData['stylesheets']; ?>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->          
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <?= $headerData['golbalmandatory']; ?>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>bootstrap-toastr/toastr.min.css"/>
        <!-- END PAGE LEVEL PLUGIN STYLES -->

        <!-- BEGIN PAGE STYLES -->        

        <!-- END PAGE STYLES -->
        <!-- BEGIN THEME STYLES --> 
        <?= $headerData['themestyles']; ?>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="<?= IMAGE_URL ?>favicon.ico"/>        
    </head>
    <!-- BEGIN BODY -->
    <body class="page-sidebar-closed-hide-logo page-header-fixed page-sidebar-fixed">
        <!-- BEGIN HEADER -->   
        <div class="page-header navbar navbar-fixed-top">
            <?php $this->load->view('include/header_view', $JsArr); ?> 
        </div>
        <!-- END HEADER -->
        <div class="clearfix"></div>

        <div class="page-container">

            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar navbar-collapse collapse">
                    <?php $this->load->view('include/sidebar_view', $JsArr); ?> 
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN PAGE -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">Users</h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?= BASEURL . 'dashboard' ?>">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <i class="fa fa-users"></i>
                                <a href="<?= BASEURL . 'users' ?>">Users</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="javascript:void(0)">User Details</a>
                            </li>
                        </ul>                        
                    </div>
                    <!-- END PAGE HEADER-->                   

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="row">
                                <div class="col-md-12">

                                    <!-- Begin: life time stats -->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-user font-green-meadow bold"></i>
                                                <span class="caption-subject font-green-meadow bold uppercase">User Details</span>                                                
                                            </div>   
                                            <div class="actions">
                                                    <i class="fa fa-calendar font-green-meadow"></i> Register On : <?=$register_date?> 
                                                   &nbsp; <i class="fa fa-history font-green-meadow"></i> Last Seen : <?=$last_login?>
                                            </div> 
                                        </div>
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <form class="form-horizontal" role="form">
                                                <input type="hidden" name="customer_id" id="customer_id" value="<?php echo $customer_id ?>"/>
                                                <div class="form-body">
                                                    <h2 class="margin-bottom-20 font-green-meadow bold"><?php echo $first_name . ' ' . $last_name ?> </h2>
                                                    <h4 class="form-section">Personal Info</h4>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">First Name:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo $first_name; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Last Name:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo $last_name; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <!--/row-->
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Email:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <a href="mailto:<?php echo $email; ?>?subject=Hearing Loop For [Venue Name]"><?php echo $email; ?></a>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Gender:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo $gender; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <!--/row-->
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Age:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo ($age != '') ? $age :'---'; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--/span-->

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Hearing Loss:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo $hearing_level; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Hearing Aid:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo $hearing_ad; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <h4 class="form-section">Address</h4>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">City:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo ($city !='')? $city: '---'; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">State:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo ($state !='')? $state: '---'; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">ZIP:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo ($zipcode !='')? $zipcode: '---'; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->                                                        
                                                    </div>

                                                    <h4 class="form-section">Notification Settings</h4>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3" style="white-space: nowrap">Notify Venue Near By:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold" style="margin-left: 8px;">
                                                                        <?php echo $notify_venue_nearby; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->                                                        
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3" style="white-space: nowrap">Notify New Loop Added:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold" style="margin-left: 23px;">
                                                                        <?php echo $notify_new_loop_added; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Distance:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo $notify_new_loop_added_distance; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3" style="white-space: nowrap">Notify New Loop Requested:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold" style="margin-left: 50px;">
                                                                        <?php echo $notify_new_loop_requested; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Distance:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo $notify_new_loop_requested_distance; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-offset-0 col-md-9">
                                                                    <a href="<?php echo BASEURL . 'users/' . 'Update/' . base64_encode($customer_id) ?>" class="btn green-meadow tooltips" title="Click to edit user"><i class="fa fa-pencil-square-o"></i> Edit</a>
                                                                    <a href="javascript:void(0)" class="btn red-soft tooltips" title="Click to delete user" id="btn_delete_user" data-attr-id="<?php echo $customer_id ?>"><i class="fa fa-trash-o"></i> Delete User</a>                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <!-- END FORM-->
                                        </div>
                                    </div>
                                    <!-- End: life time stats -->
                                </div>
                            </div>

                        </div>  
                        <!-- END DASHBOARD STATS -->
                    </div>

                    <!-- User Activity Details start -->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="row">
                                <div class="col-md-12">

                                    <!-- Begin: life time stats -->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-list font-green-meadow bold"></i>
                                                <span class="caption-subject font-green-meadow bold uppercase">Activity List</span>                                                
                                            </div>                                            
                                        </div>
                                        <div class="portlet-body">

                                            <div class="tabbable-custom">
                                                <ul class="nav nav-tabs ">
                                                    <li class="active">
                                                        <a href="#tab_15_1" data-toggle="tab" aria-expanded="false">
                                                            All <span class="badge badge-danger"><?php echo $checkins + $votes ?></span> </a>
                                                    </li>
                                                    <li class="">
                                                        <a href="#tab_15_2" data-toggle="tab" aria-expanded="false">
                                                            Check-Ins <span class="badge badge-danger"><?php echo $checkins ?></span> </a>
                                                    </li>
                                                    <li class="">
                                                        <a href="#tab_15_3" data-toggle="tab" aria-expanded="true">
                                                            Votes  <span class="badge badge-danger"><?php echo $votes ?></span> </a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab_15_1">
                                                        <div class="table-scrollable" style="border:none;overflow: hidden">
                                                            <table class="table table-striped table-bordered table-hover" id="activity_table">
                                                                <thead>
                                                                    <tr>
                                                                        <th width="10%">
                                                                            Date
                                                                        </th> 
                                                                        <th width="20%">
                                                                            Venue Name
                                                                        </th>                                                                                                                                               
                                                                        <th width="10%">
                                                                            City
                                                                        </th>
                                                                        <th width="10%">
                                                                            State
                                                                        </th>
                                                                        <th width="10%">
                                                                            Activity Type
                                                                        </th>
                                                                    </tr>                                                                    
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div> 
                                                    </div>
                                                    <div class="tab-pane " id="tab_15_2">
                                                        <div class="table-scrollable" style="border:none;overflow: hidden">
                                                            <table class="table table-striped table-bordered table-hover" id="checkins_table">
                                                                <thead>
                                                                    <tr>
                                                                        <th width="12%">
                                                                            Date
                                                                        </th>
                                                                        <th width="25%">
                                                                            Venue Name
                                                                        </th>

                                                                        <th width="10%">
                                                                            City
                                                                        </th>
                                                                        <th width="10%">
                                                                            State
                                                                        </th> 

                                                                    </tr>                                                                    
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>  
                                                    </div>
                                                    <div class="tab-pane " id="tab_15_3">
                                                        <div class="table-scrollable" style="border:none;overflow: hidden">
                                                            <table class="table table-striped table-bordered table-hover" id="votes_table">
                                                                <thead>
                                                                    <tr>

                                                                        <th width="6%">
                                                                            Date
                                                                        </th>
                                                                        <th width="15%">
                                                                            Venue Name
                                                                        </th>
                                                                        <th width="10%">
                                                                            City
                                                                        </th>
                                                                        <th width="10%">
                                                                            State
                                                                        </th> 

                                                                    </tr>                                                                    
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>

                                    <!-- Portlet for Feedback start -->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-speech font-green-meadow bold"></i>
                                                <span class="caption-subject font-green-meadow bold uppercase">Feedbacks</span>                                                
                                            </div>                                            
                                        </div>
                                        <div class="portlet-body">
                                            <div class="table-scrollable" style="border:none;overflow: hidden">
                                                <table class="table table-striped table-bordered table-hover" id="feedback_table">
                                                    <thead>
                                                        <tr>

                                                            <th width="16%">
                                                                Date
                                                            </th>
                                                            <th width="27%">
                                                                Venue Name
                                                            </th>
                                                            <th width="8%">
                                                                Looped Room
                                                            </th>
                                                            <th width="10%">
                                                                City
                                                            </th>
                                                            <th width="6%">
                                                                State
                                                            </th> 
                                                            <th width="5%">
                                                                Satisfied 
                                                            </th> 
                                                            <th width="8%">
                                                                Reason
                                                            </th>
                                                            <th width="18%">
                                                                Comment
                                                            </th>
                                                        </tr>                                                                    
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div> 
                                        </div>
                                    </div>
                                    <!-- Portlet for Feedbak Ends -->

                                    <!-- Portlet for Request start -->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-note font-green-meadow bold"></i>
                                                <span class="caption-subject font-green-meadow bold uppercase">Requests</span>                                                
                                            </div>                                            
                                        </div>
                                        <div class="portlet-body">
                                            <div class="table-scrollable" style="border:none;overflow: hidden">
                                                <table class="table table-striped table-bordered table-hover" id="request_table">
                                                    <thead>
                                                        <tr>                                                            
                                                            <th width="10%">
                                                                Date
                                                            </th>
                                                            <th width="15%">
                                                                Venue Name
                                                            </th>
                                                            <th width="10%">
                                                                City
                                                            </th>
                                                            <th width="10%">
                                                                State
                                                            </th> 
                                                            <th width="20%">
                                                                Comment
                                                            </th>
                                                        </tr>                                                                    
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div> 
                                        </div>
                                    </div>
                                    <!-- Portlet for Request Ends -->
                                </div>
                            </div>

                        </div>  
                        <!-- END DASHBOARD STATS -->
                    </div>
                    <!-- User Activity Details End -->
                </div>
                <!-- END PAGE -->
            </div>
        </div>


        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php $this->load->view('include/footer_view_inner', $JsArr); ?>
        <!-- END FOOTER -->

        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->   
        <!--[if lt IE 9]>
        <script src="<?= PLUGIN_URL ?>respond.min.js"></script>
        <script src="<?= PLUGIN_URL ?>excanvas.min.js"></script> 
        <![endif]-->   

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?= PLUGIN_URL ?>bootstrap-toastr/toastr.min.js"></script>
        <script src="<?= PLUGIN_URL ?>bootbox/bootbox.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>datatables/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->

        <script src="<?= JS_URL ?>metronic.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>layout.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>custom/ajax.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>custom/functions.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>custom/activities.js" type="text/javascript"></script>

        <!-- END PAGE LEVEL SCRIPTS -->  
        <script>
            jQuery(document).ready(function () {
                Metronic.init(); // initlayout and core plugins
                Layout.init();

                //toastr notification settings
                toastr.options = {
                    "closeButton": true,
                    "positionClass": "toast-top-right",
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "3000",
                    "extendedTimeOut": "1000",
                    "showMethod": "slideDown",
                    "hideMethod": "slideUp"
                }
<?php if ($this->session->flashdata('error')) { ?>
                    toastr.warning('<?php echo $this->session->flashdata('error') ?>', 'Users');
<?php } ?>

<?php if ($this->session->flashdata('success')) { ?>
                    toastr.success('<?php echo $this->session->flashdata('success') ?>', 'Users');
<?php } ?>

            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>