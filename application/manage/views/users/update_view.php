<?php
$headerData = $this->headerlib->data();
$JsArr = array("SCRIPT" => $headerData['javascript'], 'PLUGINS' => $headerData['javascript_plugins']);
/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES
 * *************************************************** */

/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES ENDS
 * *************************************************** */
//mprd($customer_details);
if (isset($customer_details) && !empty($customer_details)) {
    //extract user data
    extract($customer_details);
}
?>
<!doctype html>
<html lang="en-us">
    <head>
        <title><?= MAINTITLE; ?></title>
        <?= $headerData['meta_tags']; ?>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?= $headerData['plugins']; ?>
        <?= $headerData['stylesheets']; ?>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->          
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <?= $headerData['golbalmandatory']; ?>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>bootstrap-toastr/toastr.min.css"/>
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>bootstrap-switch/css/bootstrap-switch.min.css"/>

        <!-- END PAGE LEVEL PLUGIN STYLES -->
        <link href="<?= PLUGIN_URL ?>jquery-ui/jquery-ui-1.10.3.custom.min.css" rel="stylesheet"/>
        <!-- BEGIN PAGE STYLES -->        

        <!-- END PAGE STYLES -->
        <!-- BEGIN THEME STYLES --> 
        <?= $headerData['themestyles']; ?>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="<?= IMAGE_URL ?>favicon.ico"/>        
    </head>
    <!-- BEGIN BODY -->
    <body class="page-sidebar-closed-hide-logo page-header-fixed page-sidebar-fixed">
        <!-- BEGIN HEADER -->   
        <div class="page-header navbar navbar-fixed-top">
            <?php $this->load->view('include/header_view', $JsArr); ?> 
        </div>
        <!-- END HEADER -->
        <div class="clearfix"></div>

        <div class="page-container">

            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar navbar-collapse collapse">
                    <?php $this->load->view('include/sidebar_view', $JsArr); ?> 
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN PAGE -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">Users</h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?= BASEURL . 'dashboard' ?>">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <i class="fa fa-users"></i>
                                <a href="<?= BASEURL . 'users' ?>">Users</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="javascript:void(0)">Update User Details</a>
                            </li>
                        </ul>                        
                    </div>
                    <!-- END PAGE HEADER-->                   

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="row">
                                <div class="col-md-12">

                                    <!-- Begin: life time stats -->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-user font-green-meadow bold"></i>
                                                <span class="caption-subject font-green-meadow bold uppercase">Update User Details</span>                                                
                                            </div>                                            
                                        </div>
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <form class="form-horizontal" role="form" method="post" action="<?php echo BASEURL . 'users/SaveUser'; ?>" name="UserForm" id="UserForm">
                                                <input type="hidden" name="customer_id" id="customer_id" value="<?php echo $customer_id ?>"/>
                                                <input type="hidden" name="notify_new_loop_added_distance" id="notify_new_loop_added_distance" value="<?php echo $notify_new_loop_added_distance ?>"/>
                                                <input type="hidden" name="notify_new_loop_requested_distance" id="notify_new_loop_requested_distance" value="<?php echo $notify_new_loop_requested_distance ?>"/>
                                                <div class="form-body">
                                                    <h2 class="margin-bottom-20 font-green-meadow bold"><?php echo $first_name . ' ' . $last_name ?> </h2>
                                                    <h4 class="form-section">Person Info</h4>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">First Name :</label>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control" placeholder="First Name" name="first_name" id="last_name" value="<?php echo $first_name ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Last Name :</label>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control" placeholder="Last Name" name="last_name" id="last_name" value="<?php echo $last_name ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Gender :</label>
                                                                <div class="col-md-6">
                                                                    <div class="radio-list">
                                                                        <label class="radio-inline">                                                                       

                                                                            <input type="radio" name="gender" id="Male" value="1" <?php if ($gender == 'Male') { ?> checked="checked" <?php } ?>>
                                                                            Male 
                                                                        </label>
                                                                        <label class="radio-inline">
                                                                            <input type="radio" name="gender" id="Female" value="2" <?php if ($gender == 'Female') { ?> checked="checked" <?php } ?>>
                                                                            Female 
                                                                        </label>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Age :</label>
                                                                <div class="col-md-6">
                                                                    <select name="age" id="age" class="form-control">
                                                                        <option value="">-- Select Age --</option>
                                                                        <option value="10-19" <?php if ($age == '10-19') { ?> selected="selected"<?php } ?>>10-19</option>
                                                                        <option value="20-29" <?php if ($age == '20-29') { ?> selected="selected"<?php } ?>>20-29</option>
                                                                        <option value="30-39" <?php if ($age == '30-39') { ?> selected="selected"<?php } ?>>30-39</option>
                                                                        <option value="40-49" <?php if ($age == '40-49') { ?> selected="selected"<?php } ?>>40-49</option>
                                                                        <option value="50-59" <?php if ($age == '50-59') { ?> selected="selected"<?php } ?>>50-59</option>
                                                                        <option value="60-69" <?php if ($age == '60-69') { ?> selected="selected"<?php } ?>>60-69</option>
                                                                        <option value="70-79" <?php if ($age == '70-79') { ?> selected="selected"<?php } ?>>70-79</option>
                                                                        <option value="80-89" <?php if ($age == '80-89') { ?> selected="selected"<?php } ?>>80-89</option>
                                                                        <option value="90-99" <?php if ($age == '90-99') { ?> selected="selected"<?php } ?>>90-99</option>
                                                                        <option value="100+" <?php if ($age == '100+') { ?> selected="selected"<?php } ?>>100+</option>

                                                                    </select>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>

                                                    <!--/row-->

                                                    <!--/row-->
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Hearing Loss:</label>
                                                                <div class="col-md-6">

                                                                    <select name="hearing_level" id="hearing_level" class="form-control">
                                                                        <option value="">-- Select Level --</option>
                                                                        <option value="1" <?php if ($hearing_level == 'None') { ?> selected="selected"<?php } ?>>None</option>
                                                                        <option value="2" <?php if ($hearing_level == 'Mild') { ?> selected="selected"<?php } ?>>Mild</option>
                                                                        <option value="3" <?php if ($hearing_level == 'Moderate') { ?> selected="selected"<?php } ?>>Moderate</option>
                                                                        <option value="4" <?php if ($hearing_level == 'Severe') { ?> selected="selected"<?php } ?>>Severe</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Hearing Aid:</label>
                                                                <div class="col-md-6">
                                                                    <div class="radio-list">
                                                                        <label class="radio-inline">                                                                       

                                                                            <input type="radio" name="hearing_ad" id="Yes" value="1" <?php if ($hearing_ad == 'Yes') { ?> checked="checked" <?php } ?>>
                                                                            Yes 
                                                                        </label>
                                                                        <label class="radio-inline">
                                                                            <input type="radio" name="hearing_ad" id="No" value="2" <?php if ($hearing_ad == 'No') { ?> checked="checked" <?php } ?>>
                                                                            No 
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>

                                                    <h4 class="form-section">Address</h4>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">City:</label>

                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control" placeholder="City" name="city" id="city" value="<?php echo $city ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">State:</label>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control" placeholder="State" name="state" id="state" value="<?php echo $state ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">ZIP:</label>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control" placeholder="ZIP" name="zipcode" id="zipcode" value="<?php echo $zipcode ?>">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->                                                    
                                                    </div>

                                                    <h4 class="form-section">Notification Settings</h4>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3" style="padding-top: 2px; white-space: nowrap">Notify Venue Near By:</label>

                                                                <div class="col-md-6" style="margin-left:10px">
                                                                    <input data-size="small" type="checkbox" name="notify_venue_nearby" id="notify_venue_nearby" class="make-switch" <?php
                                                                    if ($notify_venue_nearby == 'Yes') {
                                                                        echo "checked='checked'";
                                                                    }
                                                                    ?>data-on-text="Yes" data-off-text="No" data-on-color="success" data-off-color="danger">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->                                                        
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3" style="padding-top: 2px; white-space: nowrap">Notify New Loop Added:</label>

                                                                <div class="col-md-6" style="margin-left:25px">
                                                                    <input data-size="small" type="checkbox" name="notify_new_loop_added" id="notify_new_loop_added" class="make-switch" <?php
                                                                    if ($notify_new_loop_added == 'Yes') {
                                                                        echo "checked='checked'";
                                                                    }
                                                                    ?>data-on-text="Yes" data-off-text="No" data-on-color="success" data-off-color="danger">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Distance:</label>
                                                                <div class="col-md-6">
                                                                    <div class="slider new_loop_added bg-blue-madison"></div>
                                                                    Notify New Loop Added Distance : <span id="notify_new_loop_added_distance_span"><?php echo $notify_new_loop_added_distance ?></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->                                                        
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3" style="padding-top: 2px; white-space: nowrap">Notify New Loop Requested:</label>

                                                                <div class="col-md-6" style="margin-left:50px">
                                                                    <input data-size="small" type="checkbox" name="notify_new_loop_requested" id="notify_new_loop_requested" class="make-switch" <?php
                                                                    if ($notify_new_loop_requested == 'Yes') {
                                                                        echo "checked='checked'";
                                                                    }
                                                                    ?>data-on-text="Yes" data-off-text="No" data-on-color="success" data-off-color="danger">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Distance:</label>
                                                                <div class="col-md-6">
                                                                    <div class="slider new_loop_requested bg-green-haze"></div>
                                                                    Notify New Loop Requested Distance: <span id="notify_new_loop_requested_distance_span"><?php echo $notify_new_loop_requested_distance ?></span>                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->                                                        
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-offset-0 col-md-9">
                                                                    <button type="submit" class="btn green-meadow tooltips" title="Click to save"><i class="fa fa-save"></i> Save</button>
                                                                    <button type="button" class="btn tooltips" title="Click to cancel" onclick="location.href = '<?= BASEURL . 'users/Details/' . base64_encode($customer_id) ?>'">Cancel</button>                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <!-- END FORM-->
                                        </div>
                                    </div>
                                    <!-- End: life time stats -->
                                </div>
                            </div>

                        </div>  
                        <!-- END DASHBOARD STATS -->
                    </div>

                </div>
                <!-- END PAGE -->
            </div>
        </div>


        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php $this->load->view('include/footer_view_inner', $JsArr); ?>
        <!-- END FOOTER -->

        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->   
        <!--[if lt IE 9]>
        <script src="<?= PLUGIN_URL ?>respond.min.js"></script>
        <script src="<?= PLUGIN_URL ?>excanvas.min.js"></script> 
        <![endif]-->   

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?= PLUGIN_URL ?>bootstrap-toastr/toastr.min.js"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>jquery-validation/js/jquery.validate.min.js"></script>
        <script src="<?= PLUGIN_URL ?>bootbox/bootbox.min.js" type="text/javascript"></script>
        <script src="<?= PLUGIN_URL ?>bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->

        <script src="<?= JS_URL ?>metronic.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>layout.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>form-validation.js"></script>



        <!-- END PAGE LEVEL SCRIPTS -->  
        <script>
                                                                        jQuery(document).ready(function () {
                                                                            Metronic.init(); // initlayout and core plugins
                                                                            Layout.init();
                                                                            FormValidation.init();
                                                                            //ComponentsjQueryUISliders.init();
                                                                            var values = [1, 2, 5, 10, 25, 50];
                                                                            var slider = $(".new_loop_added").slider({
                                                                                min: 1,
                                                                                max: 50,
                                                                                value: <?php echo $notify_new_loop_added_distance ?>,
                                                                                slide: function (event, ui) {
                                                                                    var includeLeft = event.keyCode != $.ui.keyCode.RIGHT;
                                                                                    var includeRight = event.keyCode != $.ui.keyCode.LEFT;
                                                                                    slider.slider('option', 'value', findNearest(includeLeft, includeRight, ui.value));
                                                                                    $("#notify_new_loop_added_distance").val(slider.slider('values', 0));
                                                                                    $("#notify_new_loop_added_distance_span").html(slider.slider('values', 0));
                                                                                    return false;
                                                                                }
                                                                            });
                                                                            
                                                                            var sliderRequested = $(".new_loop_requested").slider({
                                                                                min: 1,
                                                                                max: 50,
                                                                                value: <?php echo $notify_new_loop_added_distance ?>,
                                                                                slide: function (event, ui) {
                                                                                    var includeLeft = event.keyCode != $.ui.keyCode.RIGHT;
                                                                                    var includeRight = event.keyCode != $.ui.keyCode.LEFT;
                                                                                    sliderRequested.slider('option', 'value', findNearest(includeLeft, includeRight, ui.value));
                                                                                    $("#notify_new_loop_requested_distance").val(sliderRequested.slider('values', 0));
                                                                                    $("#notify_new_loop_requested_distance_span").html(sliderRequested.slider('values', 0));
                                                                                    return false;
                                                                                }
                                                                            });                                                                           
                                                                            
                                                                            
                                                                            function findNearest(includeLeft, includeRight, value) {
                                                                                var nearest = null;
                                                                                var diff = null;
                                                                                for (var i = 0; i < values.length; i++) {
                                                                                    if ((includeLeft && values[i] <= value) || (includeRight && values[i] >= value)) {
                                                                                        var newDiff = Math.abs(value - values[i]);
                                                                                        if (diff == null || newDiff < diff) {
                                                                                            nearest = values[i];
                                                                                            diff = newDiff;
                                                                                        }
                                                                                    }
                                                                                }
                                                                                return nearest;
                                                                            }



<?php if ($this->session->flashdata('error')) { ?>
                                                                                toastr.error('<?php echo $this->session->flashdata('error') ?>', 'Users');
<?php } ?>

<?php if ($this->session->flashdata('success')) { ?>
                                                                                toastr.success('<?php echo $this->session->flashdata('success') ?>', 'Users');
<?php } ?>
                                                                        });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>