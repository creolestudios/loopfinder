<?php
$headerData = $this->headerlib->data();
$JsArr = array("SCRIPT" => $headerData['javascript'], 'PLUGINS' => $headerData['javascript_plugins']);
/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES
 * *************************************************** */

/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES ENDS
 * *************************************************** */
?>
<!doctype html>
<html lang="en-us">
    <head>
        <title><?= MAINTITLE; ?></title>
        <?= $headerData['meta_tags']; ?>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?= $headerData['plugins']; ?>
        <?= $headerData['stylesheets']; ?>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->          
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <?= $headerData['golbalmandatory']; ?>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>bootstrap-toastr/toastr.min.css"/>
        <!-- END PAGE LEVEL PLUGIN STYLES -->

        <!-- BEGIN PAGE STYLES -->
        <link rel="stylesheet" type="text/css" href="<?= CSS_URL ?>cropper-master/cropper.css"/>
        <!-- END PAGE STYLES -->

        <!-- BEGIN THEME STYLES --> 
        <?= $headerData['themestyles']; ?>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="<?= IMAGE_URL ?>favicon.ico"/>        
    </head>
    <!-- BEGIN BODY -->
    <body class="page-sidebar-closed-hide-logo page-header-fixed page-sidebar-fixed">
        <!-- BEGIN HEADER -->   
        <div class="page-header navbar navbar-fixed-top">
            <?php $this->load->view('include/header_view', $JsArr); ?> 
        </div>
        <!-- END HEADER -->
        <div class="clearfix"></div>

        <div class="page-container">

            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar navbar-collapse collapse">
                    <?php $this->load->view('include/sidebar_view', $JsArr); ?> 
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN PAGE -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">Users</h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?= BASEURL . 'dashboard' ?>">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="javascript:void(0)">Users</a>
                            </li>
                        </ul>                        
                    </div>
                    <!-- END PAGE HEADER-->                   

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="row">
                                <div class="col-md-12">

                                    <!-- Begin: life time stats -->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-users font-green-meadow bold"></i>
                                                <span class="caption-subject font-green-meadow bold uppercase">Users</span>                                                
                                            </div>
                                            <div class="actions ">
                                                <a href="<?php echo BASEURL . 'users/ExportUsers/' ?>" class="btn btn-circle green-meadow tooltips" title="Export to CSV" id="user_export_csv">
                                                    <i class="fa fa-file-excel-o"></i> Export 
                                                </a>                                                

                                            </div>
                                        </div>                                      

                                        <div class="portlet-body">
                                            <div class="table-container">

                                                <table class="table table-striped table-bordered table-hover" id="user_table">
                                                    <thead>
                                                        <tr>
                                                            <th width="15%">
                                                                Name
                                                            </th>
                                                            <th width="15%">
                                                                Email Address
                                                            </th>

                                                            <th width="10%">
                                                                City
                                                            </th>
                                                            <th width="10%">
                                                                State
                                                            </th>
                                                            <th width="8%">
                                                                ZIP
                                                            </th>
                                                            <th  width="5%">
                                                                Requests
                                                            </th>
                                                            <th width="5%">
                                                                Votes
                                                            </th>
                                                            <th width="5%">
                                                                Feedbacks
                                                            </th>
                                                            <th width="5%">
                                                                Check-Ins
                                                            </th>
                                                            <th width="8%">
                                                                Details
                                                            </th>                                                            
                                                        </tr>
                                                        <tr>

                                                            <td>
                                                                <div class="input-icon right">
                                                                    <i class="fa fa-search"></i>
                                                                    <input type="text" class="form-control" placeholder="User Name" name="search_name" id="search_name">
                                                                </div>

                                                            </td>
                                                            <td>
                                                                <div class="input-icon right">
                                                                    <i class="fa fa-search"></i>
                                                                    <input type="text" class="form-control" placeholder="Email Address" name="search_email" id="search_email">
                                                                </div>                                                                
                                                            </td>                                                            
                                                            <td>
                                                                <div class="input-icon right">
                                                                    <i class="fa fa-search"></i>
                                                                    <input type="text" class="form-control" placeholder="City" name="search_city" id="search_city">
                                                                </div>                                                                
                                                            </td>
                                                            <td>
                                                                <div class="input-icon right">
                                                                    <i class="fa fa-search"></i>
                                                                    <input type="text" class="form-control" placeholder="State" name="search_state" id="search_state">
                                                                </div>                                                                
                                                            </td>
                                                            <td>
                                                                <div class="input-icon right">
                                                                    <i class="fa fa-search"></i>
                                                                    <input type="text" class="form-control" placeholder="ZIP" name="search_zipcode" id="search_zipcode">
                                                                </div>
                                                            </td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End: life time stats -->
                                </div>
                            </div>

                        </div>  
                        <!-- END DASHBOARD STATS -->
                    </div>
                </div>
                <!-- END PAGE -->
            </div>
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php $this->load->view('include/footer_view_inner', $JsArr); ?>
        <!-- END FOOTER -->

        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->   
        <!--[if lt IE 9]>
        <script src="<?= PLUGIN_URL ?>respond.min.js"></script>
        <script src="<?= PLUGIN_URL ?>excanvas.min.js"></script> 
        <![endif]-->   

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?= PLUGIN_URL ?>bootstrap-toastr/toastr.min.js"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>datatables/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->

        <script src="<?= JS_URL ?>metronic.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>layout.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>custom/functions.js" type="text/javascript"></script>

        <!-- END PAGE LEVEL SCRIPTS -->  
        <script>
            jQuery(document).ready(function () {
                Metronic.init(); // initlayout and core plugins
                Layout.init();
                var big_table = $('#user_table').dataTable({
                    "bLengthChange": true,
                    "bProcessing": true,
                    "bServerSide": true,
                    "bSortCellsTop": true,
                    "aaSorting": [],
                    "bAutoWidth": false,
                    "loadingMessage": 'Loading...',
                    "sPaginationType": "full_numbers",
                    "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ User(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No User(s) found.",
                        "oPaginate": {
                            "sFirst": "<<",
                            "sLast": ">>",
                            "sNext": ">",
                            "sPrevious": "<",
                        },
                    },
                    "sAjaxSource": BASEURL + "users/GetUsers",
                    "sServerMethod": "POST",
                    "fnServerParams": function (aoData) {
                        aoData.push(
                                {"name": "name", "value": $('#search_name').val()},
                        {"name": "email", "value": $('#search_email').val()},
                        {"name": "zipcode", "value": $('#search_zipcode').val()},
                        {"name": "city", "value": $('#search_city').val()},
                        {"name": "state", "value": $('#search_state').val()}

                        );
                    },
                    "aoColumns": [
                        {"data": "name", "bSortable": true, "bSearchable": false},
                        {"data": "email", "bSortable": true, "bSearchable": false},
                        {"data": "city", "bSortable": true, "bSearchable": false, },
                        {"data": "state", "bSortable": true, "bSearchable": false, },
                        {"data": "zipcode", "bSortable": true, "bSearchable": false},
                        {"data": "requests", "bSortable": true, "bSearchable": false},
                        {"data": "votes", "bSortable": true, "bSearchable": false},
                        {"data": "feedbacks", "bSortable": true, "bSearchable": false},
                        {"data": "checkins", "bSortable": true, "bSearchable": false},
                        {"data": "detail", "bSearchable": true, "bSortable": false, "mRender": function (data, type, full) {

                                return '<a href="' + BASEURL + 'users/Details/' + base64_encode(data) + '" class="btn btn-circle blue tooltips" title="Click to view details"><i class="fa fa-eye"></i> View </a>';
                            }}
                    ],
                });
                $('.dataTables_filter').css("display", "none");
                $('.dataTables_filter').addClass('input-group');
                $('.dataTables_filter').append('<span class="input-group-btn"><button class="btn green-meadow search_btn"" type="button"><i class="fa fa-search"></i></button></span>');
                $('.dataTables_filter input').removeClass('input-small').addClass('input-medium');
                $('.dataTables_filter label').css('margin-bottom', '0px');
                $('.dataTables_filter input').unbind();
                $('.dataTables_filter input').on('keyup', function (e)
                {
                    if (e.keyCode == 13)
                    {
                        big_table.fnFilter(this.value);
                    }
                });
                $(".search_btn").on('click', function () {
                    var string_search = $('.dataTables_filter input').val();
                    big_table.fnFilter(string_search);
                    /*				var e = jQuery.Event("keyup");
                     e.which = 13; // # Some key code value
                     $('.dataTables_filter input').trigger(e);*/

                });

                $('#search_name').change(function () {
                    big_table.fnFilter()
                });
                $('#search_email').change(function () {
                    big_table.fnFilter()
                });
                $('#search_zipcode').change(function () {
                    big_table.fnFilter()
                });
                $('#search_city').change(function () {
                    big_table.fnFilter()
                });
                $('#search_state').change(function () {
                    big_table.fnFilter()
                });

            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>