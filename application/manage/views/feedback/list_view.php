<?php
$headerData = $this->headerlib->data();
$JsArr = array("SCRIPT" => $headerData['javascript'], 'PLUGINS' => $headerData['javascript_plugins']);
/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES
 * *************************************************** */

/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES ENDS
 * *************************************************** */
?>
<!doctype html>
<html lang="en-us">
    <head>
        <title><?= MAINTITLE; ?></title>
        <?= $headerData['meta_tags']; ?>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?= $headerData['plugins']; ?>
        <?= $headerData['stylesheets']; ?>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->          
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <?= $headerData['golbalmandatory']; ?>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 

        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>bootstrap-toastr/toastr.min.css"/>
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>bootstrap-datepicker/css/datepicker.css"/>
        <!-- END PAGE LEVEL PLUGIN STYLES -->

        <!-- BEGIN PAGE STYLES -->
        <link rel="stylesheet" type="text/css" href="<?= CSS_URL ?>cropper-master/cropper.css"/>
        <!-- END PAGE STYLES -->

        <!-- BEGIN THEME STYLES --> 
        <?= $headerData['themestyles']; ?>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="<?= IMAGE_URL ?>favicon.ico"/>        
    </head>
    <!-- BEGIN BODY -->
    <body class="page-sidebar-closed-hide-logo page-header-fixed page-sidebar-fixed">
        <!-- BEGIN HEADER -->   
        <div class="page-header navbar navbar-fixed-top">
            <?php $this->load->view('include/header_view', $JsArr); ?> 
        </div>
        <!-- END HEADER -->
        <div class="clearfix"></div>

        <div class="page-container">

            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar navbar-collapse collapse">
                    <?php $this->load->view('include/sidebar_view', $JsArr); ?> 
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN PAGE -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">Feedbacks</h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?= BASEURL . 'dashboard' ?>">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="javascript:void(0)">Feedbacks</a>
                            </li>
                        </ul>                        
                    </div>
                    <!-- END PAGE HEADER-->                   

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="row">
                                <div class="col-md-12">

                                    <!-- Begin: life time stats -->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-note font-green-meadow bold"></i>
                                                <span class="caption-subject font-green-meadow bold uppercase">Feedbacks</span>                                                
                                            </div>
                                            <div class="actions">
                                                <a href="<?php echo BASEURL . 'feedback/ExportFeedbacks/' ?>" class="btn btn-circle green-meadow tooltips" title="Export to CSV">
                                                    <i class="fa fa-file-excel-o"></i> Export 
                                                </a>                                                

                                            </div>
                                        </div>                                      

                                        <div class="portlet-body">

                                            <div class="tabbable-custom">
                                                <ul class="nav nav-tabs ">
                                                    <li class="active">
                                                        <a href="#tab_15_1" data-toggle="tab" aria-expanded="true">
                                                            Negative <span class="badge badge-danger"><?php echo $total_unread_count ?></span> </a>
                                                    </li>
                                                    <li class="">
                                                        <a href="#tab_15_2" data-toggle="tab" aria-expanded="false">
                                                            All <span class="badge badge-danger"><?php echo $total_rows - $total_archive_count ?></span> </a>
                                                    </li> 

                                                    <li class="">
                                                        <a href="#tab_15_3" data-toggle="tab" aria-expanded="false">
                                                            Archived <span class="badge badge-danger"><?php echo $total_archive_count ?></span> </a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab_15_1">
                                                        <div class="table-scrollable" style="border:none;overflow: hidden">
                                                            <table class="table table-striped table-bordered table-hover" id="unread_table">
                                                                <thead>
                                                                    <tr>
                                                                        <th width="20%">
                                                                            Date
                                                                        </th>
                                                                        <th width="25%">
                                                                            Venue Name
                                                                        </th>
                                                                        <th width="15%">
                                                                            City
                                                                        </th>
                                                                        <th width="10%">
                                                                            State
                                                                        </th>
                                                                        <th width="10%">
                                                                            ZIP
                                                                        </th>
                                                                        <th width="8%">
                                                                            Satisfied
                                                                        </th>
                                                                        <th width="5%">
                                                                            Details
                                                                        </th>
                                                                    </tr>
                                                                    <tr>
                                                                        <td width="20%">
                                                                            <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy" data-date-end-date="+0d">
                                                                                <input type="text" class="form-control form-filter input-sm" readonly name="from_date" id="from_date" placeholder="From">
                                                                                <span class="input-group-btn">
                                                                                    <button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
                                                                                </span>
                                                                            </div>
                                                                            <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                                                                <input type="text" class="form-control form-filter input-sm" readonly name="to_date" id="to_date" placeholder="From">
                                                                                <span class="input-group-btn">
                                                                                    <button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
                                                                                </span>
                                                                            </div>

                                                                        </td>
                                                                        <td>
                                                                            <div class="input-icon right">
                                                                                <i class="fa fa-search"></i>
                                                                                <input type="text" class="form-control" placeholder="Venue Name" name="search_name" id="search_name">
                                                                            </div>

                                                                        </td>

                                                                        <td>
                                                                            <div class="input-icon right">
                                                                                <i class="fa fa-search"></i>
                                                                                <input type="text" class="form-control" placeholder="City" name="search_city" id="search_city">
                                                                            </div>                                                                
                                                                        </td>
                                                                        <td>
                                                                            <div class="input-icon right">
                                                                                <i class="fa fa-search"></i>
                                                                                <input type="text" class="form-control" placeholder="State" name="search_state" id="search_state">
                                                                            </div>                                                                
                                                                        </td>
                                                                        <td>
                                                                            <div class="input-icon right">
                                                                                <i class="fa fa-search"></i>
                                                                                <input type="text" class="form-control" placeholder="Zip" name="search_zip" id="search_zip">
                                                                            </div>                                                                
                                                                        </td>   
                                                                        <td></td>
                                                                        <td></td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab_15_2">
                                                        <div class="table-scrollable" style="border:none;overflow: hidden">
                                                            <table class="table table-striped table-bordered table-hover" id="all_message_table">
                                                                <thead>
                                                                    <tr>

                                                                        <th width="20%">
                                                                            Date
                                                                        </th>
                                                                        <th width="25%">
                                                                            Venue Name
                                                                        </th>
                                                                        <th width="15%">
                                                                            City
                                                                        </th>
                                                                        <th width="10%">
                                                                            State
                                                                        </th>
                                                                        <th width="10%">
                                                                            ZIP
                                                                        </th>
                                                                        <th width="8%">
                                                                            Satisfied
                                                                        </th>
                                                                        <th width="5%">
                                                                            Details
                                                                        </th>
                                                                    </tr>
                                                                    <tr>

                                                                        <td width="20%">
                                                                            <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy" data-date-end-date="+0d">
                                                                                <input type="text" class="form-control form-filter input-sm" readonly name="from_all_date" id="from_all_date" placeholder="From">
                                                                                <span class="input-group-btn">
                                                                                    <button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
                                                                                </span>
                                                                            </div>

                                                                            <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy" data-date-end-date="+0d">
                                                                                <input type="text" class="form-control form-filter input-sm" readonly name="to_all_date" id="to_all_date" placeholder="From">
                                                                                <span class="input-group-btn">
                                                                                    <button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
                                                                                </span>
                                                                            </div>

                                                                        </td>
                                                                        <td width="20%">
                                                                            <div class="input-icon right">
                                                                                <i class="fa fa-search"></i>
                                                                                <input type="text" class="form-control" placeholder="Venue Name" name="search_all_name" id="search_all_name">
                                                                            </div>

                                                                        </td>
                                                                        <td>
                                                                            <div class="input-icon right">
                                                                                <i class="fa fa-search"></i>
                                                                                <input type="text" class="form-control" placeholder="City" name="search_all_city" id="search_all_city">
                                                                            </div>                                                                
                                                                        </td>

                                                                        <td>
                                                                            <div class="input-icon right">
                                                                                <i class="fa fa-search"></i>
                                                                                <input type="text" class="form-control" placeholder="State" name="search_all_state" id="search_all_state">
                                                                            </div>                                                                
                                                                        </td>
                                                                        <td>
                                                                            <div class="input-icon right">
                                                                                <i class="fa fa-search"></i>
                                                                                <input type="text" class="form-control" placeholder="Zip" name="all_search_zip" id="all_search_zip">
                                                                            </div>                                                                
                                                                        </td>   
                                                                        <td></td>
                                                                        <td></td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>  
                                                    </div>


                                                    <div class="tab-pane" id="tab_15_3">
                                                        <div class="table-scrollable" style="border:none;overflow: hidden">
                                                            <table class="table table-striped table-bordered table-hover" id="archive_message_table">
                                                                <thead>
                                                                    <tr>

                                                                        <th width="20%">
                                                                            Date
                                                                        </th>
                                                                        <th width="25%">
                                                                            Venue Name
                                                                        </th>
                                                                        <th width="15%">
                                                                            City
                                                                        </th>
                                                                        <th width="10%">
                                                                            State
                                                                        </th>
                                                                        <th width="10%">
                                                                            ZIP
                                                                        </th>
                                                                        <th width="8%">
                                                                            Satisfied
                                                                        </th>
                                                                        <th width="5%">
                                                                            Details
                                                                        </th>
                                                                    </tr>
                                                                    <tr>

                                                                        <td width="20%">
                                                                            <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy" data-date-end-date="+0d">
                                                                                <input type="text" class="form-control form-filter input-sm" readonly name="from_arch_date" id="from_arch_date" placeholder="From">
                                                                                <span class="input-group-btn">
                                                                                    <button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
                                                                                </span>
                                                                            </div>

                                                                            <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy" data-date-end-date="+0d">
                                                                                <input type="text" class="form-control form-filter input-sm" readonly name="to_arch_date" id="to_arch_date" placeholder="From">
                                                                                <span class="input-group-btn">
                                                                                    <button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
                                                                                </span>
                                                                            </div>

                                                                        </td>
                                                                        <td width="20%">
                                                                            <div class="input-icon right">
                                                                                <i class="fa fa-search"></i>
                                                                                <input type="text" class="form-control" placeholder="Venue Name" name="search_arch_name" id="search_arch_name">
                                                                            </div>

                                                                        </td>
                                                                        <td>
                                                                            <div class="input-icon right">
                                                                                <i class="fa fa-search"></i>
                                                                                <input type="text" class="form-control" placeholder="City" name="search_arch_city" id="search_arch_city">
                                                                            </div>                                                                
                                                                        </td>

                                                                        <td>
                                                                            <div class="input-icon right">
                                                                                <i class="fa fa-search"></i>
                                                                                <input type="text" class="form-control" placeholder="State" name="search_arch_state" id="search_arch_state">
                                                                            </div>                                                                
                                                                        </td>
                                                                        <td>
                                                                            <div class="input-icon right">
                                                                                <i class="fa fa-search"></i>
                                                                                <input type="text" class="form-control" placeholder="Zip" name="arch_search_zip" id="arch_search_zip">
                                                                            </div>                                                                
                                                                        </td>   
                                                                        <td></td>
                                                                        <td></td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>  
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End: life time stats -->
                                </div>
                            </div>

                        </div>  
                        <!-- END DASHBOARD STATS -->
                    </div>
                </div>
                <!-- END PAGE -->
            </div>
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php $this->load->view('include/footer_view_inner', $JsArr); ?>
        <!-- END FOOTER -->

        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->   
        <!--[if lt IE 9]>
        <script src="<?= PLUGIN_URL ?>respond.min.js"></script>
        <script src="<?= PLUGIN_URL ?>excanvas.min.js"></script> 
        <![endif]-->   

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?= PLUGIN_URL ?>bootstrap-toastr/toastr.min.js"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>datatables/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>bootstrap-datepicker/js/bootstrap-datepicker.js"></script>


        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->

        <script src="<?= JS_URL ?>metronic.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>layout.js" type="text/javascript"></script>        
        <script src="<?= JS_URL ?>custom/functions.js" type="text/javascript"></script>

        <!-- END PAGE LEVEL SCRIPTS -->  
        <script>
            jQuery(document).ready(function () {
                Metronic.init(); // initlayout and core plugins
                Layout.init();

//                $('.date-picker').datepicker({
//                    rtl: Metronic.isRTL(),
//                    autoclose: true
//                });

                var startDate = new Date();
                var FromEndDate = new Date();
                var ToEndDate = new Date();

                $('#from_date').datepicker({
                    endDate: FromEndDate,
                    autoclose: true
                }).on('changeDate', function (selected) {
                    startDate = new Date(selected.date.valueOf());
                    startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
                    $('#to_date').datepicker('setStartDate', startDate);
                });

                $('#to_date').datepicker({
                    endDate: ToEndDate,
                    autoclose: true
                }).on('changeDate', function (selected) {
                    FromEndDate = new Date(selected.date.valueOf());
                    FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
                    $('#from_date').datepicker('setEndDate', FromEndDate);
                });


                //##################### For all requests
                $('#from_all_date').datepicker({
                    endDate: FromEndDate,
                    autoclose: true
                }).on('changeDate', function (selected) {
                    startDate = new Date(selected.date.valueOf());
                    startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
                    $('#to_all_date').datepicker('setStartDate', startDate);
                });
                $('#to_all_date').datepicker({
                    endDate: ToEndDate,
                    autoclose: true
                }).on('changeDate', function (selected) {
                    FromEndDate = new Date(selected.date.valueOf());
                    FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
                    $('#from_all_date').datepicker('setEndDate', FromEndDate);
                });

                //##################### For archive
                $('#from_arch_date').datepicker({
                    endDate: FromEndDate,
                    autoclose: true
                }).on('changeDate', function (selected) {
                    startDate = new Date(selected.date.valueOf());
                    startDate.setDate(startDate.getDate(new Date(selected.date.valueOf())));
                    $('#to_arch_date').datepicker('setStartDate', startDate);
                });
                $('#to_arch_date').datepicker({
                    endDate: ToEndDate,
                    autoclose: true
                }).on('changeDate', function (selected) {
                    FromEndDate = new Date(selected.date.valueOf());
                    FromEndDate.setDate(FromEndDate.getDate(new Date(selected.date.valueOf())));
                    $('#from_arch_date').datepicker('setEndDate', FromEndDate);
                });
                //load data tables for unread messages
                var unread_table = $('#unread_table').dataTable({
                    "bLengthChange": true,
                    "bProcessing": true,
                    "bServerSide": true,
                    "bSortCellsTop": true,
                    "bAutoWidth": false,
                    "aaSorting": [],
                    "loadingMessage": 'Loading...',
                    "sPaginationType": "full_numbers",
                    "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ Feedback(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No Feedback(s) found.",
                        "oPaginate": {
                            "sFirst": "<<",
                            "sLast": ">>",
                            "sNext": ">",
                            "sPrevious": "<",
                        },
                    },
                    "sAjaxSource": BASEURL + "feedback/UnreadMessages",
                    "sServerMethod": "POST",
                    "fnServerParams": function (aoData) {
                        aoData.push(
                                {"name": "name", "value": $('#search_name').val()},
                        {"name": "city", "value": $('#search_city').val()},
                        {"name": "state", "value": $('#search_state').val()},
                        {"name": "from_date", "value": $('#from_date').val()},
                        {"name": "to_date", "value": $('#to_date').val()},
                        {"name": "search_zip", "value": $('#search_zip').val()}

                        );
                    },
                    "aoColumns": [
                        {"data": "feedback_date", "bSortable": true, "bSearchable": true},
                        {"data": "venue_name", "bSortable": true, "bSearchable": true, "mRender": function (data, type, full) {

                                //split user name to get id
                                var Temp = data.split("_");

                                return '<a href="' + BASEURL + 'venues/Details/' + base64_encode(Temp[1]) + '" class="" style="color:#0095D7">' + Temp[0] + '</a>';
                            }},
                        {"data": "venue_city", "bSortable": true, "bSearchable": true},
                        {"data": "venue_state", "bSortable": true, "bSearchable": true},
                        {"data": "venue_zip", "bSortable": true, "bSearchable": true},
                        {"data": "satisfy", "bSortable": true, "bSearchable": true},
                        {"data": "detail", "bSearchable": false, "bSortable": false, "mRender": function (data, type, full) {

                                //split the data to get id and status
                                var Temp = data.split('_');

                                //set icon according to status
                                if (Temp[1] == '1')
                                {
                                    return '<a href="' + BASEURL + 'feedback/Details/' + base64_encode(Temp[0]) + '" class="btn btn-circle default green-meadow"><i class="icon-envelope-open"></i> View </a>';
                                } else {
                                    return '<a href="' + BASEURL + 'feedback/Details/' + base64_encode(Temp[0]) + '" class="btn btn-circle blue"><i class="icon-envelope"></i> View </a>';
                                }


                            }}
                    ],
                });
                $('.dataTables_filter').css("display", "none");


                $('#search_name').change(function () {
                    unread_table.fnFilter()
                });

                $('#search_city').change(function () {
                    unread_table.fnFilter()
                });
                $('#search_state').change(function () {
                    unread_table.fnFilter()
                });
                //search_state
                $('#to_date').change(function (event) {

                    unread_table.fnFilter()
                });
                //search_zip
                $('#search_zip').change(function (event) {

                    unread_table.fnFilter()
                });


                //############# Total request message tables
                var total_message_table = $('#all_message_table').dataTable({
                    "bLengthChange": true,
                    "bProcessing": true,
                    "bServerSide": true,
                    "bSortCellsTop": true,
                    "bAutoWidth": false,
                    "aaSorting": [],
                    "loadingMessage": 'Loading...',
                    "sPaginationType": "full_numbers",
                    "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ Feedback(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No Feedback(s) found.",
                        "oPaginate": {
                            "sFirst": "<<",
                            "sLast": ">>",
                            "sNext": ">",
                            "sPrevious": "<",
                        },
                    },
                    "sAjaxSource": BASEURL + "feedback/AllMessages",
                    "sServerMethod": "POST",
                    "fnServerParams": function (aoData) {
                        aoData.push(
                                {"name": "name", "value": $('#search_all_name').val()},
                        {"name": "city", "value": $('#search_all_city').val()},
                        {"name": "state", "value": $('#search_all_state').val()},
                        {"name": "from_all_date", "value": $('#from_all_date').val()},
                        {"name": "to_all_date", "value": $('#to_all_date').val()},
                        {"name": "search_zip", "value": $('#all_search_zip').val()}

                        );
                    },
                    "aoColumns": [
                        {"data": "feedback_date", "bSortable": true, "bSearchable": true},
                        {"data": "venue_name", "bSortable": true, "bSearchable": true, "mRender": function (data, type, full) {

                                //split user name to get id
                                var Temp = data.split("_");

                                return '<a href="' + BASEURL + 'venues/Details/' + base64_encode(Temp[1]) + '" class="" style="color:#0095D7">' + Temp[0] +'</a>';
                            }},
                        {"data": "venue_city", "bSortable": true, "bSearchable": true},
                        {"data": "venue_state", "bSortable": true, "bSearchable": true},
                        {"data": "venue_zip", "bSortable": true, "bSearchable": true},
                        {"data": "satisfy", "bSortable": true, "bSearchable": true},
                        {"data": "detail", "bSearchable": false, "bSortable": false, "mRender": function (data, type, full) {

                                //split the data to get id and status
                                var Temp = data.split('_');

                                //set icon according to status
                                if (Temp[1] == '1')
                                {
                                    return '<a href="' + BASEURL + 'feedback/Details/' + base64_encode(Temp[0]) + '" class="btn btn-circle default green-meadow"><i class="icon-envelope-open"></i> View </a>';
                                } else {
                                    return '<a href="' + BASEURL + 'feedback/Details/' + base64_encode(Temp[0]) + '" class="btn btn-circle blue"><i class="icon-envelope"></i> View </a>';
                                }


                            }}
                    ],
                });

                $('#search_all_name').change(function () {
                    total_message_table.fnFilter()
                });
                $('#search_all_city').change(function () {
                    total_message_table.fnFilter()
                });

                $('#to_all_date').change(function (event) {

                    total_message_table.fnFilter()
                });

                $('#search_all_state').change(function () {
                    total_message_table.fnFilter()
                });
                $('#all_search_zip').change(function (event) {
                    total_message_table.fnFilter()
                });

                //############# Total archive message tables
                var archive_message_table = $('#archive_message_table').dataTable({
                    "bLengthChange": true,
                    "bProcessing": true,
                    "bServerSide": true,
                    "bSortCellsTop": true,
                    "bAutoWidth": false,
                    "aaSorting": [],
                    "loadingMessage": 'Loading...',
                    "sPaginationType": "full_numbers",
                    "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ Feedback(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No Feedback(s) found.",
                        "oPaginate": {
                            "sFirst": "<<",
                            "sLast": ">>",
                            "sNext": ">",
                            "sPrevious": "<",
                        },
                    },
                    "sAjaxSource": BASEURL + "feedback/ArchiveMessages",
                    "sServerMethod": "POST",
                    "fnServerParams": function (aoData) {
                        aoData.push(
                                {"name": "name", "value": $('#search_arch_name').val()},
                        {"name": "city", "value": $('#search_arch_city').val()},
                        {"name": "state", "value": $('#search_arch_state').val()},
                        {"name": "from_arch_date", "value": $('#from_arch_date').val()},
                        {"name": "to_arch_date", "value": $('#to_arch_date').val()},
                        {"name": "search_zip", "value": $('#arch_search_zip').val()}

                        );
                    },
                    "aoColumns": [
                        {"data": "feedback_date", "bSortable": true, "bSearchable": true},
                        {"data": "venue_name", "bSortable": true, "bSearchable": true, "mRender": function (data, type, full) {

                                //split user name to get id
                                var Temp = data.split("_");

                                return '<a href="' + BASEURL + 'venues/Details/' + base64_encode(Temp[1]) + '" class="" style="color:#0095D7">' + Temp[0] + '</a>';
                            }},
                        {"data": "venue_city", "bSortable": true, "bSearchable": true},
                        {"data": "venue_state", "bSortable": true, "bSearchable": true},
                        {"data": "venue_zip", "bSortable": true, "bSearchable": true},
                        {"data": "satisfy", "bSortable": true, "bSearchable": true},
                        {"data": "detail", "bSearchable": false, "bSortable": false, "mRender": function (data, type, full) {

                                //split the data to get id and status
                                var Temp = data.split('_');

                                //set icon according to status

                                return '<a href="' + BASEURL + 'feedback/Details/' + base64_encode(Temp[0]) + '/action/archive" class="btn btn-circle blue"><i class="fa fa-archive"></i> View </a>';



                            }}
                    ],
                });

                $('#search_arch_name').change(function () {
                    archive_message_table.fnFilter()
                });
                $('#search_arch_city').change(function () {
                    archive_message_table.fnFilter()
                });

                $('#to_arch_date').change(function (event) {

                    archive_message_table.fnFilter()
                });

                $('#search_arch_state').change(function () {
                    archive_message_table.fnFilter()
                });

                $('#arch_search_zip').change(function (event) {
                    archive_message_table.fnFilter()
                });
                $('.dataTables_filter').css("display", "none");

            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>