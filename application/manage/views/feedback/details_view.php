<?php
$headerData = $this->headerlib->data();
$JsArr = array("SCRIPT" => $headerData['javascript'], 'PLUGINS' => $headerData['javascript_plugins']);
/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES
 * *************************************************** */

/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES ENDS
 * *************************************************** */

if (isset($feedback) && !empty($feedback)) {
    //extract data
    extract($feedback);
}
?>
<!doctype html>
<html lang="en-us">
    <head>
        <title><?= MAINTITLE; ?></title>
        <?= $headerData['meta_tags']; ?>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?= $headerData['plugins']; ?>
        <?= $headerData['stylesheets']; ?>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->          
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <?= $headerData['golbalmandatory']; ?>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>bootstrap-toastr/toastr.min.css"/>
        <!-- END PAGE LEVEL PLUGIN STYLES -->

        <!-- BEGIN PAGE STYLES -->        
        <link rel="stylesheet" type="text/css" href="<?= CSS_URL ?>cropper-master/cropper.min.css"/>
        <!-- END PAGE STYLES -->
        <!-- BEGIN THEME STYLES --> 
        <?= $headerData['themestyles']; ?>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="<?= IMAGE_URL ?>favicon.ico"/>        
    </head>
    <!-- BEGIN BODY -->
    <body class="page-sidebar-closed-hide-logo page-header-fixed page-sidebar-fixed">
        <!-- BEGIN HEADER -->   
        <div class="page-header navbar navbar-fixed-top">
            <?php $this->load->view('include/header_view', $JsArr); ?> 
        </div>
        <!-- END HEADER -->
        <div class="clearfix"></div>

        <div class="page-container">

            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar navbar-collapse collapse">
                    <?php $this->load->view('include/sidebar_view', $JsArr); ?> 
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN PAGE -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">Feedbacks</h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?= BASEURL . 'dashboard' ?>">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <i class="fa fa-pencil-square-o"></i>
                                <a href="<?= BASEURL . 'feedback' ?>">Feedbacks</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="javascript:void(0)">Feedback Details</a>
                            </li>
                        </ul>                        
                    </div>
                    <!-- END PAGE HEADER-->                   

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="row">
                                <div class="col-md-12">

                                    <!-- Begin: life time stats -->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-note font-green-meadow bold"></i>
                                                <span class="caption-subject font-green-meadow bold uppercase">Feedback Details</span>   
                                                <?php
                                                if ($status == '2') {
                                                    ?>
                                                    <span class="label label-sm bold label-danger" style="padding: 3px;">Archived</span>                                              
                                                <?php } ?>
                                            </div>                                            
                                        </div>
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <form class="form-horizontal" role="form">
                                                <input type="hidden" name="feedback_id" id="feedback_id" value="<?php echo $feedback_id ?>"/>
                                                <div class="form-body">
                                                    <h2 class="left margin-bottom-20">Sender : <span class="font-green-meadow bold"><a href="<?php echo BASEURL . 'users/Details/' . base64_encode($customer_id) ?>" style="color:#0095D7" title="click to view user details" class="tooltips bold"><?php echo $customer_name ?></a></span></h2>
                                                    <h4 class="form-section">Feedback Info</h4>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Venue Name:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <a href="<?php echo BASEURL . 'venues/Details/' . base64_encode($venue_id) ?>" style="color:#0095D7" title="click to view venue details" class="tooltips"><?php echo $venue_name ?></a>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Date:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo date('jS M Y, h:i a', strtotime($feedback_date)); ?>

                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <!--/row-->
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Satisfied:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo $satisfy ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Reason:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo $reason ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Looped Room:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo $loop_room_name ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>                                                        
                                                        <!--/span-->
                                                    </div>
                                                    <!--/row-->
                                                    <div class="row hide">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">City:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo $city ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">State:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo $state ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>                                                    
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Comment:</label>
                                                                <div class="col-md-9 bg-grey left">
                                                                    <p class="form-control-static bold " style="padding: 10px;vertical-align: top">
                                                                        <?php echo $comment ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row bg-grey hide">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-6">
                                                                    <p class="form-control-static bold" style="margin-left: 45px">
                                                                        <?php echo $comment ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-offset-0 col-md-9">
                                                                    <button type="button" class="btn green-meadow tooltips" title="Click to go back" onclick="location.href = '<?php echo BASEURL . 'feedback/' ?>'"><i class="fa fa-arrow-circle-o-left"></i> Back</button>                                                                    
                                                                    <?php
                                                                    if ($status == '2') {
                                                                        ?>
                                                                        <button type="button" data-attr-id="<?php echo $feedback_id ?>" class="btn blue tooltips" id="btn_messge_archive_feedback" title="Click to mark as read" data-attr-status="1"><i class="fa fa-check"></i> Unarchive</button>
                                                                    <?php } else { ?>
                                                                        <button type="button" data-attr-id="<?php echo $feedback_id ?>" class="btn blue tooltips" id="btn_messge_archive_feedback" title="Click to archive" data-attr-status="2"><i class="fa fa-archive"></i> Archive</button>
                                                                    <?php } ?>
                                                                    <button type="button" data-attr-id="<?php echo $feedback_id ?>" class="btn red-soft tooltips" id="btn_delete_feedback" title="Click to delete feedback"><i class="fa fa-trash-o"></i> Delete</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <!-- END FORM-->
                                        </div>
                                    </div>
                                    <!-- End: life time stats -->
                                </div>
                            </div>

                        </div>  
                        <!-- END DASHBOARD STATS -->
                    </div>                    
                </div>
                <!-- END PAGE -->
            </div>
        </div>


        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php $this->load->view('include/footer_view_inner', $JsArr); ?>
        <!-- END FOOTER -->

        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->   
        <!--[if lt IE 9]>
        <script src="<?= PLUGIN_URL ?>respond.min.js"></script>
        <script src="<?= PLUGIN_URL ?>excanvas.min.js"></script> 
        <![endif]-->   

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?= PLUGIN_URL ?>bootstrap-toastr/toastr.min.js"></script>
        <script src="<?= PLUGIN_URL ?>bootbox/bootbox.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->

        <script src="<?= JS_URL ?>metronic.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>layout.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>custom/ajax.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>custom/functions.js" type="text/javascript"></script>


        <!-- END PAGE LEVEL SCRIPTS -->  
        <script>
                                                                        jQuery(document).ready(function () {
                                                                            Metronic.init(); // initlayout and core plugins
                                                                            Layout.init();

                                                                            toastr.options = {
                                                                                "closeButton": true,
                                                                                "positionClass": "toast-top-right",
                                                                                "showDuration": "1000",
                                                                                "hideDuration": "1000",
                                                                                "timeOut": "3000",
                                                                                "extendedTimeOut": "1000",
                                                                                "showMethod": "slideDown",
                                                                                "hideMethod": "slideUp"
                                                                            }

                                                                        });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>