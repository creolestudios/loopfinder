<?php
$headerData = $this->headerlib->data();
$JsArr = array("SCRIPT" => $headerData['javascript'], 'PLUGINS' => $headerData['javascript_plugins']);
/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES
 * *************************************************** */

/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES ENDS
 * *************************************************** */

if (isset($chapter_details) && !empty($chapter_details)) {
    //extract data
    extract($chapter_details);
}
if(isset($chapter_images)  && !empty($chapter_images)){
    extract($chapter_images);
}
//mprd($venue_rooms);
?>
<!doctype html>
<html lang="en-us">
    <head>
        <title><?= MAINTITLE; ?></title>
        <?= $headerData['meta_tags']; ?>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?= $headerData['plugins']; ?>
        <?= $headerData['stylesheets']; ?>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->          
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <?= $headerData['golbalmandatory']; ?>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>bootstrap-toastr/toastr.min.css"/>
        <link href="<?= PLUGIN_URL ?>fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>datatables/plugins/bootstrap/dataTables.bootstrap.css"/>

        <!-- END PAGE LEVEL PLUGIN STYLES -->

        <!-- BEGIN PAGE STYLES -->        
        <link href="<?= CSS_URL ?>pages/portfolio.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE STYLES -->
        <!-- BEGIN THEME STYLES --> 
        <?= $headerData['themestyles']; ?>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="<?= IMAGE_URL ?>favicon.ico"/>        
    </head>
    <!-- BEGIN BODY -->
    <body class="page-sidebar-closed-hide-logo page-header-fixed page-sidebar-fixed">
        <!-- BEGIN HEADER -->   
        <div class="page-header navbar navbar-fixed-top">
            <?php $this->load->view('include/header_view', $JsArr); ?> 
        </div>
        <!-- END HEADER -->
        <div class="clearfix"></div>

        <div class="page-container">

            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar navbar-collapse collapse">
                    <?php $this->load->view('include/sidebar_view', $JsArr); ?> 
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN PAGE -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <!--<h3 class="page-title">HLAA Chapters</h3>-->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?= BASEURL . 'dashboard' ?>">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <i class="fa fa-map-marker"></i>
                                <a href="<?= BASEURL . 'venues' ?>">HLAA Chapters</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="javascript:void(0)">HLAA Chapter Details</a>
                            </li>
                        </ul>                        
                    </div>
                    <!-- END PAGE HEADER-->                   

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="row">
                                <div class="col-md-12">

                                    <!-- Begin: life time stats -->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-pointer font-green-meadow bold"></i>
                                                <span class="caption-subject font-green-meadow bold uppercase">Chapter Details</span>
                                            </div>
<!--                                            <div class="actions">
                                                <span class="timeline-body-alerttitle font-green-meadow hide" style="font-size:16px;font-weight:700">Registered Venue</span>
                                            </div>-->
                                        </div>
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <form class="form-horizontal" role="form">
                                                <input type="hidden" name="hdvenueid" id="hdvenueid" value="<?php echo $chapter_id; ?>"/>
                                                <div class="form-body">
                                                    <h4 class="left margin-bottom-20 bold"><?php echo $chapter_name; ?>&nbsp;</h4>
                                                   
                                                    <h4 class="form-section">Chapter Info</h4>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Meetup On :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo $meetupdate; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Created On:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo date('jS M Y', strtotime($created_date)); ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        

                                                        <!--/span-->
                                                    </div>
                                                    <!--/row-->
                                                                                                      
                                                    <h4 class="form-section">Address </h4>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Address :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo $address; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">City :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo $city; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--/span-->
                                                    </div>
                                                    <!--/row-->
                                                    <div class="row">

                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">State :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo $state; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">ZIP :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo $zipcode; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <h4 class="form-section">Contact Information </h4>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Phone :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php $phone_number = str_replace("()-", '', $phone_number); ?>
                                                                        <?php echo "(" . substr($phone_number, 0, 3) . ") " . substr($phone_number, 3, 3) . "-" . substr($phone_number, 6); ?>
                                                                        <?php //echo $phone_number; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Email :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php
                                                                        if ($email != '') {
                                                                            ?>
                                                                            <a href="mailto:<?php echo $email; ?>?subject=HLAA Meetup<?php echo $chapter_name; ?>"><?php echo $email; ?></a>
                                                                        <?php
                                                                        } else {
                                                                            echo '---';                                                                            
                                                                        }
                                                                        ?>                                                                        
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Website :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <a href="<?php echo $website; ?>" target="_blank"><?php echo $website; ?></a>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>

                                                    <h4 class="form-section">Internal Contact Information</h4>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Contact Person :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo ($internal_contact != '') ? $internal_contact : '---'; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Email :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php
                                                                        if ($internal_email != '') {
                                                                            ?>
                                                                            <a href="mailto:<?php echo $internal_email; ?>?subject=Hearing Loop For <?php echo $venue_name; ?>"><?php echo $internal_email; ?></a>
                                                                        <?php
                                                                        } else {
                                                                            echo '---';                                                                            
                                                                        }
                                                                        ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Phone :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
<?php $internal_phone_number = str_replace("-", '', $internal_phone_number); ?>
<?php echo ($internal_phone_number != '') ? "(" . substr($internal_phone_number, 0, 3) . ") " . substr($internal_phone_number, 3, 3) . "-" . substr($internal_phone_number, 6) : '---'; ?>
<?php echo ($internal_contact_ext != '') ? " x " . $internal_contact_ext : ''; ?>

                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Comment :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
<?php echo ($internal_comment != '') ? $internal_comment : '---'; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>

                                                    <h4 class="form-section">Additional Information (Public)</h4>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Comment :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
<?php echo ($public_comment != '') ? $public_comment : '---'; ?>

                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <h4 class="form-section">Chapter Images </h4>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="tabbable tabbable-custom tabbable-noborder">

                                                                <div class="tab-content">

                                                                    <!-- BEGIN FILTER -->
                                                                    <div class="margin-top-10">
                                                                        <div class="row mix-grid">
                                                                            <?php 
                                                                            if (isset($chapter_images) && !empty($chapter_images)) {
                                                                                foreach ($chapter_images as $KeyImage => $ImageVal) {
                                                                                    ?>
                                                                                    <div class="col-md-3 col-sm-4 mix">
                                                                                        <div class="mix-inner">
                                                                                            <img class="img-responsive" src="<?php echo $ImageVal['chapter_image_thumb'] ?>" alt="">
                                                                                            <div class="mix-details">
                                                                                                <?php if ($venue_image != $ImageVal['original_image']) { ?>
                                                                                                    <a class="mix-link setdefault2" title="set as default image" data-attr-image="<?php echo $ImageVal['original_image']; ?>">
                                                                                                        <i class="fa fa-reply"></i>
                                                                                                    </a>
                                                                                                <?php } else { ?>
                                                                                                    <a class="mix-link" title="Default image" style="cursor: default">
                                                                                                        <i class="fa fa-check"></i>
                                                                                                    </a>
                                                                                                   <?php } ?>
                                                                                                <a class="mix-preview fancybox-button" href="<?php echo $ImageVal['chapter_image'] ?>" title="<?php echo $chapter_name?>" data-rel="fancybox-button">
                                                                                                    <i class="fa fa-arrows-alt"></i>
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>  
                                                                                    <?php
                                                                                }
                                                                            } else {
                                                                                ?>
                                                                                <p class="bold" style="margin-left: 10px;">No image(s) available</p>
<?php } ?>

                                                                        </div>
                                                                    </div>
                                                                    <!-- END FILTER -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-offset-0 col-md-9">
                                                                <a href="<?php echo BASEURL . 'hlaachapters/EditHlaaChapters/' . base64_encode($chapter_id) ?>" class="btn blue"><i class="fa fa-edit"></i> Edit</a>
                                                                    <button type="button" class="btn red-soft" id="btn_delete_chapter" data-attr-id="<?php echo $chapter_id ?>"><i class="fa fa-map-marker"></i> Delete Chapter</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <!-- END FORM-->
                                        </div>
                                    </div>
                                    <!-- End: life time stats -->
                                </div>
                            </div>

                        </div>  
                        <!-- END DASHBOARD STATS -->
                    </div>

<!--                    <div class="row">
                        <div class="col-md-12">
                             BEGIN Portlet PORTLET
                            <div class="row">
                                <div class="col-md-12">

                                     Begin: life time stats 
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-pencil font-green-meadow bold"></i>
                                                <span class="caption-subject font-green-meadow bold uppercase">Feedbacks (<?php echo $total_feedback; ?>)</span>                                                
                                            </div>
                                            <div class="actions">


                                            </div>
                                        </div>                                      

                                        <div class="portlet-body">
                                            <div class="table-container">
                                                <div class="table-actions-wrapper" style="display: block;float: right">
                                                        <?php if (isset($venue_rooms) && !empty($venue_rooms)) { ?>
                                                        <select class="table-group-action-input form-control input-inline input-small input-medium" name="venue_room_filter" id="venue_room_filter">
                                                            <option value="">--Select Room--</option>
                                                            <?php
                                                            foreach ($venue_rooms as $key => $RoomVal) {
                                                                ?>
                                                                <option value="<?php echo $RoomVal['venue_room_id'] ?>"><?php echo $RoomVal['room_name'] ?></option>                                                                
    <?php } ?>
                                                        </select>
<?php } ?>
                                                </div>
                                                <table class="table table-striped table-bordered table-hover" id="feedback_table">
                                                    <thead>
                                                        <tr>
                                                            <th width="16%">
                                                                Date
                                                            </th>
                                                            <th width="15%">
                                                                User Name
                                                            </th>
                                                            <th width="10%">
                                                                Email
                                                            </th> 

                                                            <th width="10%">
                                                                Looped Room 
                                                            </th> 
                                                            <th width="5%">
                                                                Satisfied
                                                            </th> 
                                                            <th width="8%">
                                                                Reason
                                                            </th>
                                                            <th width="25%">
                                                                Comments
                                                            </th>
                                                        </tr>                                                                    
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                     End: life time stats 
                                </div>
                            </div>

                        </div>  
                         END DASHBOARD STATS 
                    </div>-->

                    <!-- checkins portlet-->
<!--                    <div class="row">
                        <div class="col-md-12">
                             BEGIN Portlet PORTLET
                            <div class="row">
                                <div class="col-md-12">

                                     Begin: life time stats 
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-map-marker font-green-meadow bold"></i>
                                                <span class="caption-subject font-green-meadow bold uppercase">Check-Ins</span>                                                
                                            </div>

                                        </div>                                      

                                        <div class="portlet-body">
                                            <div class="table-container">

                                                <table class="table table-striped table-bordered table-hover" id="checkins_table">
                                                    <thead>
                                                        <tr>
                                                            <th width="8%">
                                                                Date
                                                            </th>
                                                            <th width="8%">
                                                                User Name
                                                            </th>
                                                            <th width="10%">
                                                                City
                                                            </th>
                                                            <th width="10%">
                                                                State
                                                            </th>
                                                            <th width="10%">
                                                                ZIP
                                                            </th>

                                                        </tr>                                                                    
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                     End: life time stats 
                                </div>
                            </div>

                        </div>  
                         END DASHBOARD STATS 
                    </div>-->
                </div>
                <!-- END PAGE -->
            </div>
        </div>


        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
<?php $this->load->view('include/footer_view_inner', $JsArr); ?>
        <!-- END FOOTER -->

        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->   
        <!--[if lt IE 9]>
        <script src="<?= PLUGIN_URL ?>respond.min.js"></script>
        <script src="<?= PLUGIN_URL ?>excanvas.min.js"></script> 
        <![endif]-->   

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?= PLUGIN_URL ?>bootstrap-toastr/toastr.min.js"></script>
        <script src="<?= PLUGIN_URL ?>bootbox/bootbox.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>jquery-mixitup/jquery.mixitup.min.js"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>fancybox/source/jquery.fancybox.pack.js"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>datatables/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->

        <script src="<?= JS_URL ?>metronic.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>layout.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>custom/ajax.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>custom/functions.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>custom/venues.js" type="text/javascript"></script>


        <!-- END PAGE LEVEL SCRIPTS -->  
        <script>
            jQuery(document).ready(function () {
                Metronic.init(); // initlayout and core plugins
                Layout.init();
                $('.mix-grid').mixitup();
                toastr.options = {
                    "closeButton": true,
                    "positionClass": "toast-top-right",
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "3000",
                    "extendedTimeOut": "1000",
                    "showMethod": "slideDown",
                    "hideMethod": "slideUp"
                }

            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>