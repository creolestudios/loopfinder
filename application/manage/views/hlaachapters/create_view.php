<?php
$headerData = $this->headerlib->data();
$JsArr = array("SCRIPT" => $headerData['javascript'], 'PLUGINS' => $headerData['javascript_plugins']);
/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES
 * *************************************************** */

/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES ENDS
 * *************************************************** */
?>
<!doctype html>
<html lang="en-us">
    <head>
        <title><?= MAINTITLE; ?></title>
        <?= $headerData['meta_tags']; ?>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?= $headerData['plugins']; ?>
        <?= $headerData['stylesheets']; ?>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->          
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <?= $headerData['golbalmandatory']; ?>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>select2/select2.css"/>        
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>bootstrap-toastr/toastr.min.css"/>
        <link href="<?= PLUGIN_URL ?>bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
        <link href="<?= PLUGIN_URL ?>bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>bootstrap-switch/css/bootstrap-switch.min.css"/>
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>bootstrap-datepicker/css/datepicker3.css"/>
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>jquery-multi-select/css/multi-select.css"/>
        <link href="<?= PLUGIN_URL ?>dropzone/css/dropzone.css" rel="stylesheet"/>        
        <!-- END PAGE LEVEL PLUGIN STYLES -->

        <!-- BEGIN PAGE STYLES -->        
        <link rel="stylesheet" type="text/css" href="<?= CSS_URL ?>cropper-master/cropper.min.css"/> 
        <link rel="stylesheet" type="text/css" href="<?= CSS_URL ?>bootstrap-timepicker.min.css"/> 
        <link rel="stylesheet" type="text/css" href="<?= CSS_URL ?>datetimepicker.css"/>

        <!-- END PAGE STYLES -->
        <!-- BEGIN THEME STYLES --> 
        <?= $headerData['themestyles']; ?>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="<?= IMAGE_URL ?>favicon.ico"/>
        <style>
            .bootstrap-switch {border-color: #e1e1e1 !important;}
        </style>
    </head>
    <!-- BEGIN BODY -->
    <body class="page-sidebar-closed-hide-logo page-header-fixed page-sidebar-fixed">
        <!-- BEGIN HEADER -->   
        <div class="page-header navbar navbar-fixed-top">
            <?php $this->load->view('include/header_view', $JsArr); ?> 
        </div>
        <!-- END HEADER -->
        <div class="clearfix"></div>

        <div class="page-container">

            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar navbar-collapse collapse">
                    <?php $this->load->view('include/sidebar_view', $JsArr); ?> 
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN PAGE -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">HLAA Chapters</h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?= BASEURL . 'dashboard' ?>">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <i class="fa fa-map-marker"></i>
                                <a href="<?= BASEURL . 'venues' ?>">HLAA Chapters</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="javascript:void(0)">Add New HLAA Chapters</a>
                            </li>
                        </ul>                        
                    </div>
                    <!-- END PAGE HEADER-->                   

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="row">
                                <div class="col-md-12">

                                    <!-- Begin: life time stats -->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-pointer font-green-meadow bold"></i>
                                                <span class="caption-subject font-green-meadow bold uppercase">Add New HLAA Chapters</span>                                                
                                            </div>

<!--                                            <div class="actions" style="float:left;margin-left: 10px;margin-top: -5px;">

                                                <input data-size="large" type="checkbox" name="venue_type" id="venue_type" class="make-switch" checked data-on-text="&nbsp;Registered&nbsp;" data-off-text="&nbsp;Requested&nbsp;" data-on-color="success" data-off-color="danger">
                                            </div>-->
                                        </div>
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <form action="<?php echo BASEURL . 'hlaachapters/Savehlaachapters' ?>" class="form-horizontal" method="post" name="Addhlaa" id="Addhlaa">
                                                <!-- hidden field for latitude,longitude and category-->
                                               
                                                <input type="hidden" name="hdlatitude" id="hdlatitude" value="34.4399391"/>
                                                <input type="hidden" name="hdlongitude" id="hdlongitude" value="-119.7386568"/>
                                                <input type="hidden" name="hdcategory" id="hdcategory" value="21"/>
                                                <input type="hidden" name="hdimages" id="hdimages" value=""/>
                                                <input type="hidden" name="hdvenuetype" id="hdvenuetype" value="1"/>
                                                
                                                <div class="form-body">

                                                    <h4 class="form-section">HLAA Chapters Details</h4>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Chapter Name : </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control" placeholder="Chapter Name" name="chapter_name" id="chapter_name">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Category : </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control" value="HLAA Chapter" readonly="readonly" name="chapter_cat" id="chapter_cat">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <!--/row-->
<!--                                                    <div class="row" id="hide_row_1">
                                                        <div class="col-md-6" id="div_append_looped_rooms">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Looped Rooms : </label>
                                                                <div class="col-md-6 input_fields_wrap">
                                                                    <div class="input-group input-small">
                                                                        <input type="text" class="form-control input-medium" placeholder="Looped Room" style="float: left" name="loop_room[]" id="loop_room">
                                                                        <span class="input-group-btn">
                                                                            <button class="btn btn-icon-only green-meadow add_more_loop_room" type="button" title="Click to add more room"><i class="fa fa-plus"></i></button>
                                                                        </span>
                                                                    </div>
                                                                </div>                                                                
                                                            </div>

                                                        </div>
                                                        /span

                                                    </div>-->
                                                    <!--/row-->
                                                    <div class="row" id="hide_row_2">
<!--                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3" style="white-space:nowrap">OtoJoy Certified : </label>
                                                                <div class="col-md-9">
                                                                    <div class="radio-list">
                                                                        <label class="radio-inline">                                                                       

                                                                            <input type="radio" name="is_certified" id="yes" value="1" checked="">
                                                                            Yes 
                                                                        </label>
                                                                        <label class="radio-inline">
                                                                            <input type="radio" name="is_certified" id="no" value="0" checked="">
                                                                            No 
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>-->
                                                        <!--/span-->
                                                        <div class="col-md-6 " >
<!--                                                            <div class="form-group">
                                                                <label class="control-label col-md-3" style="white-space:nowrap">Meetup Date : </label>
                                                                <div class="col-md-6">
                                                                    <div class="input-group input-medium date date-picker form_datetime data-date-format="dd-mm-yyyy">
                                                                        <input type="text" class="form-control input-medium" readonly  name="meetup_date" id="meetup_date" style="width: 234px !important" placeholder="Meetup Date">
                                                                        <span class="input-group-btn">
                                                                            <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            </div>-->
                                                             <div class="form-group">
										<label class="control-label col-md-3" style="white-space: nowrap;">Meetup Date:</label>
										<div class="col-md-4">
											<div class="input-group input-medium  date date-picker form_datetime" data-datetime-format="dd-mm-yyyy">
												<input type="text" size="16" readonly="" class="form-control input-medium" name="meetup_date" id="meetup_date" style="width: 234px !important" placeholder="Meetup Date">
												<span class="input-group-btn">
												<button class="btn default date-set" type="button"><i class="fa fa-calendar"></i></button>
												</span>
											</div>
											<!-- /input-group -->
										</div>
									</div>
                                                        </div>
                                                        <!--/span-->

                                                        <!--/span-->
                                                    </div>
                                                    <h4 class="form-section">Address</h4>
                                                    <!--/row-->
                                                    <div class="row">

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Address : </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control" name="address" id="address">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">City : </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control" name="city" id="city">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">State : </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control" name="state" id="state">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">ZIP : </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control" name="zipcode" id="zipcode" maxlength="5">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-offset-9 col-md-6">
                                                                    <a class="btn green-meadow btn-block m-icon tooltips" data-placement="top" data-original-title="Click to open map" data-toggle="modal" onClick="$('#FindOnMap').modal('show');"><i class="icon-pointer"></i> Find On Map</a>                                                                    

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <h4 class="form-section">Contact Information</h4>
                                                    <!--/row-->
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Phone : </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control" name="phone_number" id="phone_number">
                                                                    <span class="help-block"> e.g. (999) 999-9999 </span>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Email : </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control" name="email" id="email">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Website : </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control" name="website" id="website">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--/row-->

                                                    <h4 class="form-section">Internal Contact Information</h4>

                                                    <div class="row">

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Contact Person : </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control" name="internal_contact" id="internal_contact">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Email : </label>
                                                                <div class="col-md-6">
                                                                    <input type="text" class="form-control" name="internal_email" id="internal_email">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Phone : </label>
                                                                <div class="col-md-4">
                                                                    <input type="text" class="form-control" name="internal_phone_number" id="internal_phone_number">                                                                    

                                                                </div>
                                                                <div class="col-md-3" style="margin-left: -15px">
                                                                    <input type="text" class="form-control input-xsmall" name="internal_contact_ext" id="internal_contact_ext" placeholder="EXT" maxlength="5">                                                                    

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Comment : </label>
                                                                <div class="col-md-6">
                                                                    <textarea class="form-control" rows="3" id="internal_comment" name="internal_comment"></textarea>                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <h4 class="form-section">Additional Information (Public)</h4>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Comment : </label>
                                                                <div class="col-md-6">
                                                                    <textarea class="form-control" rows="3" id="public_comment" name="public_comment"></textarea>                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <h4 class="form-section">Add New Images</h4>
                                                    <div class="row">
                                                        <div class="col-md-6">

                                                            <div class="dropzone" id="my-dropzone"></div>
                                                            <span class="help-block">Image Dimension : 2560 X 1440 </span>
                                                        </div>

                                                    </div>

                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-offset-3 col-md-9">

                                                                    <button type="submit" class="btn green-meadow tooltips" title="Click to add HLAA chapters"><i class="fa fa-save"></i> Add Chapter</button>
                                                                    <button type="button" class="btn default tooltips" title="Click to cancel" onclick="location.href = '<?= BASEURL . 'hlaachapters' ?>'">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>

                                        <!-- END FORM-->
                                    </div>
                                </div>
                                <!-- End: life time stats -->
                            </div>
                        </div>

                    </div>  
                    <!-- END DASHBOARD STATS -->
                </div>                    
            </div>
            <!-- END PAGE -->
        </div>
    </div>

    <!-- Modal for googl map-->
    <div id="FindOnMap" class="modal fade" tabindex="-1" data-width="760">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title"><i class="icon-pointer"></i> Find On Map</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div  class="col-md-9">
                    <p>
                        <input type="text" class="form-control" id="AddressAutoComplete"/>
                    </p>
                </div>
                <div class="col-md-12">

                    <div id="LoadMap" style="height: 400px"></div>
                </div>

            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn green-meadow" data-dismiss="modal"><i class="fa fa-thumbs-up"></i> Done</button>
        </div>
    </div>

    <!-- END CONTAINER -->
    <!-- BEGIN FOOTER -->
    <?php $this->load->view('include/footer_view_inner', $JsArr); ?>
    <!-- END FOOTER -->

    <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <!-- BEGIN CORE PLUGINS -->   
    <!--[if lt IE 9]>
    <script src="<?= PLUGIN_URL ?>respond.min.js"></script>
    <script src="<?= PLUGIN_URL ?>excanvas.min.js"></script> 
    <![endif]-->   

    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script type="text/javascript" src='https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places,geometry'></script>
    <script src="<?= PLUGIN_URL ?>bootstrap-toastr/toastr.min.js"></script>
    <script type="text/javascript" src="<?= PLUGIN_URL ?>select2/select2.min.js"></script>
    <script type="text/javascript" src="<?= PLUGIN_URL ?>bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="<?= PLUGIN_URL ?>jquery-validation/js/jquery.validate.min.js"></script>
    <script src="<?= PLUGIN_URL ?>bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?= PLUGIN_URL ?>bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="<?= PLUGIN_URL ?>jquery-multi-select/js/jquery.multi-select.js"></script>
    <script src="<?= PLUGIN_URL ?>bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
    <script src="<?= PLUGIN_URL ?>bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
    <script src="<?= PLUGIN_URL ?>dropzone/dropzone.js"></script>
    <script type="text/javascript" src="<?= PLUGIN_URL ?>jquery-inputmask/jquery.inputmask.bundle.min.js"></script>


    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->

    <script src="<?= JS_URL ?>metronic.js" type="text/javascript"></script>
    <script src="<?= JS_URL ?>layout.js" type="text/javascript"></script>
    <script src="<?= JS_URL ?>form-validation.js"></script>
    <script src="<?= JS_URL ?>custom/ajax.js" type="text/javascript"></script>
    <script src="<?= JS_URL ?>custom/functions.js" type="text/javascript"></script>
    <script src="<?= JS_URL ?>custom/locationpicker.jquery.js" type="text/javascript"></script>
    <script src="<?= JS_URL ?>bootstrap-datetimepicker.min.js"></script>
    <script src="<?= JS_URL ?>form-dropzone.js"></script>

    <!-- END PAGE LEVEL SCRIPTS -->  
    <script>
                                                                        jQuery(document).ready(function () {
                                                                            Metronic.init(); // initlayout and core plugins
                                                                            Layout.init();
                                                                            FormValidation.init();
                                                                            FormDropzone.init();
                                                                           
                                                                            
                                                                            $("#phone_number").inputmask("mask", {
                                                                                "mask": "(999) 999-9999"
                                                                            }); //specifying fn & options

                                                                            $("#internal_phone_number").inputmask("mask", {
                                                                                "mask": "(999) 999-9999"
                                                                            }); //specifying fn & options
                                                                            toastr.options = {
                                                                                "closeButton": true,
                                                                                "positionClass": "toast-top-right",
                                                                                "showDuration": "1000",
                                                                                "hideDuration": "1000",
                                                                                "timeOut": "3000",
                                                                                "extendedTimeOut": "1000",
                                                                                "showMethod": "slideDown",
                                                                                "hideMethod": "slideUp"
                                                                            }
                                                                            $('.date-picker').datetimepicker({
                                                                                rtl: Metronic.isRTL(),
                                                                                orientation: "left",
                                                                                autoclose: true,
                                                                                format: 'dd-mm-yyyy hh:ii'
                                                                            });

                                                                            

                                                                           
                                                                            //get the value on change and store it into hidden field
                                                                          

                                                                            $(window).on('shown.bs.modal', function (e)
                                                                            {
                                                                                var geocoder = new google.maps.Geocoder();
                                                                                var address ='';
                                                                                $( "#LoadMap" ).empty();
                                                                                address = $("#address").val() + ' ' + $("#city").val() + ' ' + $("#state").val() + ' ' + $("#zipcode").val();
                                                                                $("#LoadMap").remove(); 
                                                                                $('#FindOnMap .modal-body').append('<div id="LoadMap" style="height: 400px;"></div>');
                                                                                
                                                                                if (geocoder) {
                                                                                    geocoder.geocode({'address': address}, function (results, status) {
                                                                                        if (status == google.maps.GeocoderStatus.OK) {
                                                                                           
                                                                                            $('#LoadMap').locationpicker({
                                                                                                location: {latitude: results[0].geometry.location.lat(), longitude: results[0].geometry.location.lng()},
                                                                                                radius: 300,
                                                                                                inputBinding: {
                                                                                                    locationNameInput: $('#AddressAutoComplete')
                                                                                                },
                                                                                                enableAutocomplete: true,
                                                                                                onchanged: function (currentLocation, radius, isMarkerDropped) { 
                                                                                                    $('#address_loc0').val(currentLocation.latitude + ',' + currentLocation.longitude);
                                                                                                    //store latitude and longitude in hidden field
                                                                                                    $("#hdlatitude").val(currentLocation.latitude); 
                                                                                                    $("#hdlongitude").val(currentLocation.longitude);  
                                                                                                }

                                                                                            });
                                                                                            //console.log(results[0].geometry.location);
                                                                                        }
                                                                                        else {
                                                                                            $('#LoadMap').locationpicker({
                                                                                                location: {latitude: 34.4399391, longitude: -119.7386568},
                                                                                                radius: 300,
                                                                                                inputBinding: {
                                                                                                    locationNameInput: $('#AddressAutoComplete')
                                                                                                },
                                                                                                enableAutocomplete: true,
                                                                                                onchanged: function (currentLocation, radius, isMarkerDropped) {
                                                                                                    $('#address_loc0').val(currentLocation.latitude + ',' + currentLocation.longitude);
                                                                                                    //store latitude and longitude in hidden field
                                                                                                    $("#hdlatitude").val(currentLocation.latitude);
                                                                                                    //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                                                                                                }

                                                                                            });
                                                                                            //console.log("Geocoding failed: " + status);
                                                                                        }
                                                                                    });
                                                                                }
                                                                            });

                                                                            $("input[name='meetup_date']").click(function () {
                                                                               
                                                                                    $("#meetup_date").each(function () {
                                                                                        $(this).rules("add", {
                                                                                            required: true,
                                                                                            checkValue: true,
                                                                                            messages: {
                                                                                                required: "Meetup date is required."
                                                                                            }
                                                                                        });
                                                                                    });

                                                                            });

<?php if ($this->session->flashdata('error')) { ?>
                                                                                toastr.error('<?php echo $this->session->flashdata('error') ?>', 'Hlaa chapters');
<?php } ?>

<?php if ($this->session->flashdata('success')) { ?>
                                                                                toastr.success('<?php echo $this->session->flashdata('success') ?>', 'Hlaa chapters');
<?php } ?>
                                                                        });
    </script>
    <!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>