<?php
$headerData = $this->headerlib->data();
$JsArr = array("SCRIPT" => $headerData['javascript'], 'PLUGINS' => $headerData['javascript_plugins']);
/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES
 * *************************************************** */

/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES ENDS
 * *************************************************** */
if (isset($dashboard_data) && count($dashboard_data) > 0) {
    //extract data
    //extract($dashboard_data);
}
?>
<!doctype html>
<html lang="en-us">
    <head>
        <title><?= MAINTITLE; ?></title>
        <?= $headerData['meta_tags']; ?>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?= $headerData['plugins']; ?>
        <?= $headerData['stylesheets']; ?>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->          
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <?= $headerData['golbalmandatory']; ?>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
        <!-- END PAGE LEVEL PLUGIN STYLES -->

        <!-- BEGIN PAGE STYLES -->

        <!-- END PAGE STYLES -->
        <!-- BEGIN THEME STYLES --> 
        <?= $headerData['themestyles']; ?>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="<?= IMAGE_URL ?>favicon.ico"/>
    </head>
    <!-- BEGIN BODY -->
    <!-- BEGIN BODY -->
    <!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
    <!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
    <!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
    <!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
    <!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
    <!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
    <!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
    <body class="page-sidebar-closed-hide-logo page-header-fixed page-sidebar-fixed">    
        <!-- BEGIN HEADER -->   
        <div class="page-header navbar navbar-fixed-top">
            <?php $this->load->view('include/header_view', $JsArr); ?> 
        </div>
        <!-- END HEADER -->
        <div class="clearfix"></div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar navbar-collapse collapse">
                    <?php $this->load->view('include/sidebar_view', $JsArr); ?> 
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN PAGE -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">
                        Dashboard</h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?= BASEURL . 'dashboard' ?>">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="javascript:void(0)">Dashboard</a>
                            </li>
                        </ul>                        
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- BEGIN DASHBOARD STATS -->
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 col-20">
                            <a class="dashboard-stat dashboard-stat-light blue-soft" href="<?= BASEURL . 'users' ?>">
                                <div class="visual">
                                    <i class="fa fa-users"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <?php echo $customer_count; ?>
                                    </div>
                                    <div class="desc dashboard-font-custom">
                                        Users
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 col-20">
                            <a class="dashboard-stat dashboard-stat-light blue-soft" href="<?= BASEURL . 'venues' ?>">
                                <div class="visual">
                                    <i class="fa fa-university"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <?php echo $register_venue_count ?>
                                    </div>
                                    <div class="desc dashboard-font-custom">
                                        Registered Venues
                                    </div>                                  

                                </div>

                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 col-20">
                            <a class="dashboard-stat dashboard-stat-light blue-soft" href="<?= BASEURL . 'venues' ?>">
                                <div class="visual">
                                    <i class="fa fa-bank"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <?php echo $requested_venue_count; ?>
                                    </div>
                                    <div class="desc dashboard-font-custom">
                                        Requested Venues
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 col-20">
                            <a class="dashboard-stat dashboard-stat-light blue-soft" href="<?= BASEURL . 'requestmessage' ?>">
                                <div class="visual">
                                    <i class="fa fa-envelope"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <?php echo $request_count - $archive_request_count; ?>
                                    </div>
                                    <div class="desc dashboard-font-custom">
                                        Requests
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 col-20">
                            <a class="dashboard-stat dashboard-stat-light blue-soft" href="<?= BASEURL . 'feedback' ?>">
                                <div class="visual">
                                    <i class="fa fa-pencil-square-o"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <?php echo $feedback_count - $archive_feedback_count; ?>
                                    </div>
                                    <div class="desc dashboard-font-custom">
                                        New Feedbacks
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-pencil font-green-sharp"></i>
                                                <span class="caption-subject font-green-sharp bold uppercase">New Feedbacks</span>                                                
                                            </div>                                            
                                        </div>
                                        <div class="portlet-body">
                                            <div class="table-container">

                                                <table class="table table-striped table-bordered table-hover" id="feedback_table">
                                                    <thead>
                                                        <tr>
                                                            <th width="12%">
                                                                Date
                                                            </th>
                                                            <th width="20%">
                                                                Venue Name
                                                            </th>

                                                            <th width="10%">
                                                                City
                                                            </th>
                                                            <th width="10%">
                                                                State
                                                            </th>
                                                            <th width="5%">
                                                                Details
                                                            </th>
                                                        </tr>

                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- Begin: life time stats -->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-envelope font-green-sharp"></i>
                                                <span class="caption-subject font-green-sharp bold uppercase">New Requests</span>                                                
                                            </div>                                            
                                        </div>
                                        <div class="portlet-body">
                                            <div class="table-container">

                                                <table class="table table-striped table-bordered table-hover" id="all_message_table">
                                                    <thead>
                                                        <tr>
                                                            <th width="12%">
                                                                Date
                                                            </th>
                                                            <th width="20%">
                                                                Venue Name
                                                            </th>

                                                            <th width="10%">
                                                                City
                                                            </th>
                                                            <th width="10%">
                                                                State
                                                            </th>
                                                            <th width="5%">
                                                                Details
                                                            </th>
                                                        </tr>

                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>


                                    <!-- End: life time stats -->
                                </div>
                            </div>

                        </div>  
                        <!-- END DASHBOARD STATS -->
                    </div>
                </div>
                <!-- END PAGE -->
            </div>
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php $this->load->view('include/footer_view_inner', $JsArr); ?>
        <!-- END FOOTER -->

        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->   
        <!--[if lt IE 9]>
        <script src="<?= PLUGIN_URL ?>respond.min.js"></script>
        <script src="<?= PLUGIN_URL ?>excanvas.min.js"></script> 
        <![endif]-->   

        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script type="text/javascript" src="<?= PLUGIN_URL ?>datatables/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?php echo JS_URL ?>metronic.js" type="text/javascript"></script>
        <script src="<?php echo JS_URL ?>layout.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>custom/functions.js" type="text/javascript"></script>

        <!-- END PAGE LEVEL SCRIPTS -->  
        <script>
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core componets
                Layout.init(); // init layout


                var total_message_table = $('#all_message_table').dataTable({
                    "bLengthChange": true,
                    "bProcessing": true,
                    "bServerSide": true,
                    "bSortCellsTop": true,
                    "bAutoWidth": false,
                    "aaSorting": [[0, 'desc']],
                    "loadingMessage": 'Loading...',
                    "sPaginationType": "full_numbers",
                    "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ New Request(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No New Request(s) found.",
                        "oPaginate": {
                            "sFirst": "<<",
                            "sLast": ">>",
                            "sNext": ">",
                            "sPrevious": "<",
                        },
                    },
                    "sAjaxSource": BASEURL + "dashboard/AllMessages",
                    "sServerMethod": "POST",
                    "aoColumns": [
                        {"data": "request_date", "bSortable": true, "bSearchable": true},
                        {"data": "venue_name", "bSortable": true, "bSearchable": true},
                        {"data": "venue_city", "bSortable": true, "bSearchable": true, },
                        {"data": "venue_state", "bSortable": true, "bSearchable": true, },
                        {"data": "detail", "bSearchable": true, "bSortable": false, "mRender": function (data, type, full) {

                                //split the data to get id and status
                                var Temp = data.split('_');

                                //set icon according to status
                                if (Temp[1] == '1')
                                {
                                    return '<a href="' + BASEURL + 'requestmessage/Details/' + base64_encode(Temp[0]) + '" class="btn btn-circle default green-meadow"><i class="icon-envelope-open"></i> View </a>';
                                } else {
                                    return '<a href="' + BASEURL + 'requestmessage/Details/' + base64_encode(Temp[0]) + '" class="btn btn-circle blue"><i class="icon-envelope"></i> View </a>';
                                }
                            }}
                    ],
                });
                
                var feedback_table = $('#feedback_table').dataTable({
                    "bLengthChange": true,
                    "bProcessing": true,
                    "bServerSide": true,
                    "bSortCellsTop": true,
                    "bAutoWidth": false,
                    "aaSorting": [[0, 'desc']],
                    "loadingMessage": 'Loading...',
                    "sPaginationType": "full_numbers",
                    "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ New Feedback(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No New Feedback(s) found.",
                        "oPaginate": {
                            "sFirst": "<<",
                            "sLast": ">>",
                            "sNext": ">",
                            "sPrevious": "<",
                        },
                    },
                    "sAjaxSource": BASEURL + "dashboard/GetFeedbacks",
                    "sServerMethod": "POST",
                    "aoColumns": [
                        {"data": "feedback_date", "bSortable": true, "bSearchable": true},
                        {"data": "venue_name", "bSortable": true, "bSearchable": true, "mRender": function (data, type, full) {

                                //split user name to get id
                                var TempName = data.split("_");

                                return '<a href="' + BASEURL + 'venues/Details/' + base64_encode(TempName[1]) + '" class="" style="color:#0095D7">' + TempName[0] + '</a>';
                            }},
                        {"data": "venue_city", "bSortable": true, "bSearchable": true, },
                        {"data": "venue_state", "bSortable": true, "bSearchable": true, },
                        {"data": "detail", "bSearchable": true, "bSortable": false, "mRender": function (data, type, full) {

                                //split the data to get id and status
                                var Temp = data.split('_');

                                //set icon according to status
                                if (Temp[1] == '1')
                                {
                                    return '<a href="' + BASEURL + 'feedback/Details/' + base64_encode(Temp[0]) + '" class="btn btn-circle default green-meadow"><i class="icon-envelope-open"></i> View </a>';
                                } else {
                                    return '<a href="' + BASEURL + 'feedback/Details/' + base64_encode(Temp[0]) + '" class="btn btn-circle blue"><i class="icon-envelope"></i> View </a>';
                                }
                            }}
                    ],
                });
                $('.dataTables_filter').css("display", "none");
            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>