<?php
$headerData = $this->headerlib->data();
$JsArr = array("SCRIPT" => $headerData['javascript'], 'PLUGINS' => $headerData['javascript_plugins']);
/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES
 * *************************************************** */

/* * ***************************************************
 * *      DEFINE FORM ATTRIBUTES ENDS
 * *************************************************** */

if (isset($venue_details) && !empty($venue_details)) {
    //extract data
    extract($venue_details);

    //extract categories
    if (isset($venue_categories) && !empty($venue_categories)) {
        extract($venue_categories);
    }
}
//mprd($venue_rooms);
?>
<!doctype html>
<html lang="en-us">
    <head>
        <title><?= MAINTITLE; ?></title>
        <?= $headerData['meta_tags']; ?>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?= $headerData['plugins']; ?>
        <?= $headerData['stylesheets']; ?>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->          
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <?= $headerData['golbalmandatory']; ?>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>bootstrap-toastr/toastr.min.css"/>
        <link href="<?= PLUGIN_URL ?>fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>datatables/plugins/bootstrap/dataTables.bootstrap.css"/>

        <!-- END PAGE LEVEL PLUGIN STYLES -->

        <!-- BEGIN PAGE STYLES -->        
        <link href="<?= CSS_URL ?>pages/portfolio.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE STYLES -->
        <!-- BEGIN THEME STYLES --> 
        <?= $headerData['themestyles']; ?>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="<?= IMAGE_URL ?>favicon.ico"/>        
    </head>
    <!-- BEGIN BODY -->
    <body class="page-sidebar-closed-hide-logo page-header-fixed page-sidebar-fixed">
        <!-- BEGIN HEADER -->   
        <div class="page-header navbar navbar-fixed-top">
            <?php $this->load->view('include/header_view', $JsArr); ?> 
        </div>
        <!-- END HEADER -->
        <div class="clearfix"></div>

        <div class="page-container">

            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar navbar-collapse collapse">
                    <?php $this->load->view('include/sidebar_view', $JsArr); ?> 
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN PAGE -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">Venues</h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?= BASEURL . 'dashboard' ?>">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <i class="fa fa-map-marker"></i>
                                <a href="<?= BASEURL . 'venues' ?>">Venues</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="javascript:void(0)">Venue Details</a>
                            </li>
                        </ul>                        
                    </div>
                    <!-- END PAGE HEADER-->                   

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="row">
                                <div class="col-md-12">

                                    <!-- Begin: life time stats -->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-pointer font-green-meadow bold"></i>
                                                <span class="caption-subject font-green-meadow bold uppercase">Venue Details</span>
                                            </div>
                                            <div class="actions">
                                                <span class="timeline-body-alerttitle font-green-meadow hide" style="font-size:16px;font-weight:700">Registered Venue</span>
                                            </div>
                                        </div>
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <form class="form-horizontal" role="form">
                                                <input type="hidden" name="hdvenueid" id="hdvenueid" value="<?php echo $venue_id; ?>"/>
                                                <div class="form-body">
                                                    <h4 class="left margin-bottom-20 bold"><?php echo $venue_name; ?>&nbsp;<span class="label label-sm bold" style="padding: 5px;background-color: #35aa47">Registered Venue</span> <span class="label label-sm bold"  style="padding: 5px;background-color: #0095D7"><i class="fa fa-map-marker"></i> <?php echo $total_checkins; ?> Check-Ins</span></h4>
                                                    <span class="btn text-success bold hide" style="cursor: default"><i class="fa fa-map-marker"></i> <?php echo $total_checkins; ?> Check-Ins</span>
                                                    <h4 class="form-section">Venue Info</h4>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Category :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo $category_name; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Added On :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo date('jS M Y', strtotime($created_date)); ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--/span-->
                                                    </div>
                                                    <!--/row-->
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">OtoJoy Certified:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo ($is_certified == '1') ? 'Yes' : 'No'; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3" style="white-space:nowrap">Certification Date:</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">

                                                                        <?php
                                                                        if ($certified_date != '0000-00-00') {
                                                                            echo date('jS M Y', strtotime($certified_date));
                                                                        } else {
                                                                            echo '--NA--';
                                                                        }
                                                                        ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>


                                                    <h4 class="form-section">Looped Rooms </h4>
                                                    <div class="row">
                                                        <?php
                                                        if (isset($venue_rooms) && !empty($venue_rooms)) {
                                                            //loop through the loop rooms and display details
                                                            $Count = 1;
                                                            foreach ($venue_rooms as $key => $RoomVal) {
                                                                ?>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <?php if ($Count == 1) { ?>
                                                                            <label class="control-label col-md-3">Looped Rooms :</label>
                                                                        <?php } else { ?>
                                                                            <label class="control-label col-md-3" <?php if ($Count % 3 != '0') { ?>style="width: 12.666667% !important" <?php } ?>></label>
                                                                        <?php } ?>

                                                                        <div class="col-md-6 input_fields_wrap">


                                                                            <div class="input-group input-small">

                                                                                <div class="bold loop_room_custom">
                                                                                    <?php echo $RoomVal['room_name'] ?> <span id="span_room_<?php echo $RoomVal['venue_room_id'] ?>"><?php
                                                                                    if ($RoomVal['last_verified'] == '') {
                                                                                        echo "(Not yet verified)";
                                                                                    } else {
                                                                                        echo "Last Verified(" . date('jS M Y', strtotime($RoomVal['last_verified'])) . ")";
                                                                                    }
                                                                                    ?> </span>
                                                                                </div>
                                                                                <span class="input-group-btn">
                                                                                    <button class="btn green tooltips btn_verify_rooms" type="button" title="Click to veriify" data-attr-id="<?php echo $RoomVal['venue_room_id'] ?>">Verify Now</button>                                                                            
                                                                                </span>
                                                                            </div>

                                                                        </div>


                                                                    </div>
                                                                </div>
                                                                <?php
                                                                $Count++;
                                                            }
                                                            ?>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <div class="col-md-9">
                                                                <p class="form-control-static bold" style="color:#a94442">
                                                                    No room(s) found
                                                                </p>
                                                            </div>

                                                        <?php } ?>
                                                        <!--/span-->

                                                    </div>                                                    
                                                    <h4 class="form-section">Address </h4>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Address :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo $address; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">City :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo $city; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!--/span-->
                                                    </div>
                                                    <!--/row-->
                                                    <div class="row">

                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">State :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo $state; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">ZIP :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo ($zipcode != '') ? $zipcode : '---'; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>
                                                    <h4 class="form-section">Contact Information </h4>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Phone :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php $phone_number = str_replace("-", '', $phone_number); ?>
                                                                        <?php echo "(" . substr($phone_number, 0, 3) . ") " . substr($phone_number, 3, 3) . "-" . substr($phone_number, 6); ?>
                                                                        <?php //echo $phone_number; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Email :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php
                                                                        if ($email != '') {
                                                                            ?>
                                                                            <a href="mailto:<?php echo $email; ?>?subject=Hearing Loop For <?php echo $venue_name; ?>"><?php echo $email; ?></a>
                                                                        <?php
                                                                        } else {
                                                                            echo '---';                                                                            
                                                                        }
                                                                        ?>                                                                        
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Website :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <a href="<?php echo $website; ?>" target="_blank"><?php echo $website; ?></a>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>

                                                    <h4 class="form-section">Internal Contact Information</h4>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Contact Person :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php echo ($internal_contact != '') ? $internal_contact : '---'; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Email :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
                                                                        <?php
                                                                        if ($internal_email != '') {
                                                                            ?>
                                                                            <a href="mailto:<?php echo $internal_email; ?>?subject=Hearing Loop For <?php echo $venue_name; ?>"><?php echo $internal_email; ?></a>
                                                                        <?php
                                                                        } else {
                                                                            echo '---';                                                                            
                                                                        }
                                                                        ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Phone :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
<?php $internal_phone_number = str_replace("-", '', $internal_phone_number); ?>
<?php echo ($internal_phone_number != '') ? "(" . substr($internal_phone_number, 0, 3) . ") " . substr($internal_phone_number, 3, 3) . "-" . substr($internal_phone_number, 6) : '---'; ?>
<?php echo ($internal_contact_ext != '') ? " x " . $internal_contact_ext : ''; ?>

                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Comment :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
<?php echo ($internal_comment != '') ? $internal_comment : '---'; ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--/span-->
                                                    </div>

                                                    <h4 class="form-section">Additional Information (Public)</h4>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3">Comment :</label>
                                                                <div class="col-md-9">
                                                                    <p class="form-control-static bold">
<?php echo ($public_comment != '') ? $public_comment : '---'; ?>

                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <h4 class="form-section">Venue Images </h4>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="tabbable tabbable-custom tabbable-noborder">

                                                                <div class="tab-content">

                                                                    <!-- BEGIN FILTER -->
                                                                    <div class="margin-top-10">
                                                                        <div class="row mix-grid">
                                                                            <?php
                                                                                    // print_r($venue_image);//print_r($ImageVal['original_image']);                                                               
                                                                            if (isset($venue_images) && !empty($venue_images)) {
                                                                                foreach ($venue_images as $KeyImage => $ImageVal) {
                                                                                    ?>
                                                                                    <div class="col-md-3 col-sm-4 mix">
                                                                                        <div class="mix-inner">
                                                                                            <img class="img-responsive" src="<?php echo $ImageVal['venue_image_thumb'] ?>" alt="">
                                                                                            <div class="mix-details">
                                                                                                <?php if ($venue_image != $ImageVal['original_image']) { ?>
                                                                                                    <a class="mix-link setdefault" title="set as default image" data-attr-image="<?php echo $ImageVal['original_image']; ?>">
                                                                                                        <i class="fa fa-reply"></i>
                                                                                                    </a>
                                                                                                <?php } else { ?>
                                                                                                    <a class="mix-link" title="Default image" style="cursor: default">
                                                                                                        <i class="fa fa-check"></i>
                                                                                                    </a>
                                                                                               <?php } ?>
                                                                                                <a class="mix-preview fancybox-button" href="<?php echo $ImageVal['venue_image'] ?>" title="<?php echo $venue_name?>" data-rel="fancybox-button">
                                                                                                    <i class="fa fa-arrows-alt"></i>
                                                                                                </a>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>  
                                                                                    <?php
                                                                                }
                                                                            } else {
                                                                                ?>
                                                                                <p class="bold" style="margin-left: 10px;">No image(s) available</p>
                                                                                <?php } ?>

                                                                        </div>
                                                                    </div>
                                                                    <!-- END FILTER -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-actions">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="col-md-offset-0 col-md-9">
                                                                    <a href="<?php echo BASEURL . 'venues/EditVenue/' . base64_encode($venue_id) ?>" class="btn blue"><i class="fa fa-edit"></i> Edit</a>
                                                                    <button type="button" class="btn red-soft" id="btn_delete_venue" data-attr-id="<?php echo $venue_id ?>"><i class="fa fa-map-marker"></i> Delete Venue</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <!-- END FORM-->
                                        </div>
                                    </div>
                                    <!-- End: life time stats -->
                                </div>
                            </div>

                        </div>  
                        <!-- END DASHBOARD STATS -->
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="row">
                                <div class="col-md-12">

                                    <!-- Begin: life time stats -->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-pencil font-green-meadow bold"></i>
                                                <span class="caption-subject font-green-meadow bold uppercase">Feedbacks (<?php echo $total_feedback; ?>)</span>                                                
                                            </div>
                                            <div class="actions">


                                            </div>
                                        </div>                                      

                                        <div class="portlet-body">
                                            <div class="table-container">
                                                <div class="table-actions-wrapper" style="display: block;float: right">
                                                        <?php if (isset($venue_rooms) && !empty($venue_rooms)) { ?>
                                                        <select class="table-group-action-input form-control input-inline input-small input-medium" name="venue_room_filter" id="venue_room_filter">
                                                            <option value="">--Select Room--</option>
                                                            <?php
                                                            foreach ($venue_rooms as $key => $RoomVal) {
                                                                ?>
                                                                <option value="<?php echo $RoomVal['venue_room_id'] ?>"><?php echo $RoomVal['room_name'] ?></option>                                                                
    <?php } ?>
                                                        </select>
<?php } ?>
                                                </div>
                                                <table class="table table-striped table-bordered table-hover" id="feedback_table">
                                                    <thead>
                                                        <tr>
                                                            <th width="16%">
                                                                Date
                                                            </th>
                                                            <th width="15%">
                                                                User Name
                                                            </th>
                                                            <th width="10%">
                                                                Email
                                                            </th> 

                                                            <th width="10%">
                                                                Looped Room 
                                                            </th> 
                                                            <th width="5%">
                                                                Satisfied
                                                            </th> 
                                                            <th width="8%">
                                                                Reason
                                                            </th>
                                                            <th width="25%">
                                                                Comments
                                                            </th>
                                                        </tr>                                                                    
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End: life time stats -->
                                </div>
                            </div>

                        </div>  
                        <!-- END DASHBOARD STATS -->
                    </div>

                    <!-- checkins portlet-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="row">
                                <div class="col-md-12">

                                    <!-- Begin: life time stats -->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="fa fa-map-marker font-green-meadow bold"></i>
                                                <span class="caption-subject font-green-meadow bold uppercase">Check-Ins</span>                                                
                                            </div>

                                        </div>                                      

                                        <div class="portlet-body">
                                            <div class="table-container">

                                                <table class="table table-striped table-bordered table-hover" id="checkins_table">
                                                    <thead>
                                                        <tr>
                                                            <th width="8%">
                                                                Date
                                                            </th>
                                                            <th width="8%">
                                                                User Name
                                                            </th>
                                                            <th width="10%">
                                                                City
                                                            </th>
                                                            <th width="10%">
                                                                State
                                                            </th>
                                                            <th width="10%">
                                                                ZIP
                                                            </th>

                                                        </tr>                                                                    
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End: life time stats -->
                                </div>
                            </div>

                        </div>  
                        <!-- END DASHBOARD STATS -->
                    </div>
                </div>
                <!-- END PAGE -->
            </div>
        </div>


        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
<?php $this->load->view('include/footer_view_inner', $JsArr); ?>
        <!-- END FOOTER -->

        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->   
        <!--[if lt IE 9]>
        <script src="<?= PLUGIN_URL ?>respond.min.js"></script>
        <script src="<?= PLUGIN_URL ?>excanvas.min.js"></script> 
        <![endif]-->   

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?= PLUGIN_URL ?>bootstrap-toastr/toastr.min.js"></script>
        <script src="<?= PLUGIN_URL ?>bootbox/bootbox.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>jquery-mixitup/jquery.mixitup.min.js"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>fancybox/source/jquery.fancybox.pack.js"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>datatables/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>

        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->

        <script src="<?= JS_URL ?>metronic.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>layout.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>custom/ajax.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>custom/functions.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>custom/venues.js" type="text/javascript"></script>


        <!-- END PAGE LEVEL SCRIPTS -->  
        <script>
            jQuery(document).ready(function () {
                Metronic.init(); // initlayout and core plugins
                Layout.init();
                $('.mix-grid').mixitup();
                toastr.options = {
                    "closeButton": true,
                    "positionClass": "toast-top-right",
                    "showDuration": "1000",
                    "hideDuration": "1000",
                    "timeOut": "3000",
                    "extendedTimeOut": "1000",
                    "showMethod": "slideDown",
                    "hideMethod": "slideUp"
                }

            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>