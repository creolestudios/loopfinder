<?php
$headerData = $this->headerlib->data();
$JsArr = array("SCRIPT" => $headerData['javascript'], 'PLUGINS' => $headerData['javascript_plugins']);
?>
<!doctype html>
<html lang="en-us">
    <head>
        <title><?= MAINTITLE; ?></title>
        <?= $headerData['meta_tags']; ?>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <?= $headerData['plugins']; ?>
        <?= $headerData['stylesheets']; ?>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->          
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <?= $headerData['golbalmandatory']; ?>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES --> 
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>select2/select2.css"/>
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>bootstrap-toastr/toastr.min.css"/>
        <link href="<?= PLUGIN_URL ?>bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css"/>
        <link href="<?= PLUGIN_URL ?>bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="<?= PLUGIN_URL ?>bootstrap-fileinput/bootstrap-fileinput.css"/>

        <!-- END PAGE LEVEL PLUGIN STYLES -->

        <!-- BEGIN PAGE STYLES -->
        <link rel="stylesheet" type="text/css" href="<?= CSS_URL ?>cropper-master/cropper.css"/>
        <!-- END PAGE STYLES -->

        <!-- BEGIN THEME STYLES --> 
        <?= $headerData['themestyles']; ?>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="<?= IMAGE_URL ?>favicon.ico"/>        
    </head>
    <!-- BEGIN BODY -->
    <body class="page-sidebar-closed-hide-logo page-header-fixed page-sidebar-fixed">
        <!-- BEGIN HEADER -->   
        <div class="page-header navbar navbar-fixed-top">
            <?php $this->load->view('include/header_view', $JsArr); ?> 
        </div>
        <!-- END HEADER -->
        <div class="clearfix"></div>

        <div class="page-container">

            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar navbar-collapse collapse">
                    <?php $this->load->view('include/sidebar_view', $JsArr); ?> 
                </div>
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN PAGE -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h3 class="page-title">Venues</h3>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?= BASEURL . 'dashboard' ?>">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="javascript:void(0)">Venues</a>
                            </li>
                        </ul>                        
                    </div>
                    <!-- END PAGE HEADER-->                   

                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="row">
                                <div class="col-md-12">

                                    <!-- Begin: life time stats -->
                                    <div class="portlet light bordered">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-pointer font-green-meadow bold"></i>
                                                <span class="caption-subject font-green-meadow bold uppercase">Venues</span>                                                
                                            </div>

                                            <div class="actions">


                                                <a href="javascript:void(0)" class="btn btn-circle green-meadow tooltips" title="Import CSV" data-toggle="modal" onClick="$('#FindOnMap').modal('show');">
                                                    <i class="fa fa-upload"></i> Import 
                                                </a>
                                                <a href="<?php echo BASEURL . 'venues/ExportVenues/' ?>" class="btn btn-circle green-meadow tooltips" title="Export to CSV">
                                                    <i class="fa fa-file-excel-o"></i> Export 
                                                </a>
                                                <a href="<?php echo BASEURL . 'venues/Create' ?>" class="btn btn-circle green-meadow tooltips" title="Add new venue">
                                                    <i class="fa fa-plus"></i> New Venue 
                                                </a>
                                            </div>                                            
                                        </div>                                      

                                        <div class="portlet-body">

                                            <div class="tabbable-custom">
                                                <ul class="nav nav-tabs ">
                                                    <li class="active">
                                                        <a href="#tab_15_1" data-toggle="tab" aria-expanded="true">
                                                            Registered <span class="badge badge-danger"><?php echo $register_venue_count; ?> </span> </a>
                                                    </li>
                                                    <li class="">
                                                        <a href="#tab_15_2" data-toggle="tab" aria-expanded="false">
                                                            Requested <span class="badge badge-danger"><?php echo $requested_venue_count; ?></span> </a>
                                                    </li>
                                                    <li class="">
                                                        <a href="#tab_15_3" data-toggle="tab" aria-expanded="false">
                                                            All Venues <span class="badge badge-danger"><?php echo $requested_venue_count + $register_venue_count; ?></span> </a>
                                                    </li> 
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab_15_1">
                                                        <div class="table-scrollable" style="border:none;overflow: hidden">
                                                            <table class="table table-striped table-bordered table-hover" id="register_venue_table">
                                                                <thead>
                                                                    <tr>
                                                                        <th width="20%">
                                                                            Venue Name
                                                                        </th>
                                                                        <th width="20%">
                                                                            Category
                                                                        </th>
                                                                        <th width="12%">
                                                                            City
                                                                        </th> 
                                                                        <th width="10%">
                                                                            State
                                                                        </th>
                                                                        <th width="8%">
                                                                            ZIP
                                                                        </th>

                                                                        <th width="8%">
                                                                            Check-Ins
                                                                        </th>
                                                                        <th width="8%">
                                                                            Feedback
                                                                        </th>
                                                                        <th width="15%">
                                                                            Details
                                                                        </th>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="input-icon right">
                                                                                <i class="fa fa-search"></i>
                                                                                <input type="text" class="form-control" placeholder="Venue Name" name="venue_name" id="venue_name">
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <?php if (isset($categories) && !empty($categories)) { ?>
                                                                                <select class="table-group-action-input form-control input-inline input-small input-sm" name="filter_category" id="filter_category">
                                                                                    <option value="">--Category--</option>
                                                                                    <?php
                                                                                    foreach ($categories as $key => $CatVal) {
                                                                                        ?>
                                                                                        <option value="<?php echo $CatVal['category_id'] ?>"><?php echo $CatVal['category_name'] ?></option>                                                                
                                                                                    <?php } ?>
                                                                                </select>
                                                                            <?php } ?>
                                                                        </td>
                                                                        <td>
                                                                            <div class="input-icon right">
                                                                                <i class="fa fa-search"></i>
                                                                                <input type="text" class="form-control" placeholder="City" name="search_city" id="search_city">
                                                                            </div>                                                                
                                                                        </td>
                                                                        <td>
                                                                            <div class="input-icon right">
                                                                                <i class="fa fa-search"></i>
                                                                                <input type="text" class="form-control" placeholder="State" name="state" id="state">
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="input-icon right">
                                                                                <i class="fa fa-search"></i>
                                                                                <input type="text" class="form-control" placeholder="ZIP" name="zipcode" id="zipcode">
                                                                            </div>
                                                                        </td>

                                                                        <td></td>
                                                                        <td></td>
                                                                        <td></td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab_15_2">
                                                        <div class="table-scrollable" style="border:none;overflow: hidden">
                                                            <table class="table table-striped table-bordered table-hover" id="request_venue_table">
                                                                <thead>
                                                                    <tr>
                                                                        <th width="15%">
                                                                            Venue Name
                                                                        </th>
                                                                        <th width="20%">
                                                                            Category
                                                                        </th>
                                                                        <th width="12%">
                                                                            City
                                                                        </th> 
                                                                        <th width="10%">
                                                                            State
                                                                        </th>
                                                                        <th width="8%">
                                                                            ZIP
                                                                        </th>

                                                                        <th width="8%">
                                                                            Votes
                                                                        </th>
                                                                        <th width="15%">
                                                                            Details
                                                                        </th>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="input-icon right">
                                                                                <i class="fa fa-search"></i>
                                                                                <input type="text" class="form-control" placeholder="Venue Name" name="venue_name" id="req_venue_name">
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <?php if (isset($categories) && !empty($categories)) { ?>
                                                                                <select class="table-group-action-input form-control input-inline input-small input-sm" name="req_filter_category" id="req_filter_category">
                                                                                    <option value="">--Category--</option>
                                                                                    <?php
                                                                                    foreach ($categories as $key => $CatVal) {
                                                                                        ?>
                                                                                        <option value="<?php echo $CatVal['category_id'] ?>"><?php echo $CatVal['category_name'] ?></option>                                                                
                                                                                    <?php } ?>
                                                                                </select>
                                                                            <?php } ?>
                                                                        </td>
                                                                        <td>
                                                                            <div class="input-icon right">
                                                                                <i class="fa fa-search"></i>
                                                                                <input type="text" class="form-control" placeholder="City" name="search_city" id="req_search_city">
                                                                            </div>                                                                
                                                                        </td>
                                                                        <td>
                                                                            <div class="input-icon right">
                                                                                <i class="fa fa-search"></i>
                                                                                <input type="text" class="form-control" placeholder="State" name="state" id="req_state">
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <div class="input-icon right">
                                                                                <i class="fa fa-search"></i>
                                                                                <input type="text" class="form-control" placeholder="ZIP" name="zipcode" id="req_zipcode">
                                                                            </div>
                                                                        </td>

                                                                        <td></td>

                                                                        <td></td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>  
                                                    </div>

                                                    <div class="tab-pane" id="tab_15_3">
                                                        <div class="table-scrollable" style="border:none;overflow: hidden">
                                                            <table class="table table-striped table-bordered table-hover" id="all_venue_table">
                                                                <thead>
                                                                    <tr>
                                                                        <th width="15%">
                                                                            Venue Name
                                                                        </th>
                                                                        <th width="20%">
                                                                            Category
                                                                        </th>
                                                                        <th width="12%">
                                                                            City
                                                                        </th> 
                                                                        <th width="10%">
                                                                            State
                                                                        </th>
                                                                        <th width="8%">
                                                                            ZIP
                                                                        </th>
                                                                        <th width="15%">
                                                                            Details
                                                                        </th>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <div class="input-icon right">
                                                                                <i class="fa fa-search"></i>
                                                                                <input type="text" class="form-control" placeholder="Venue Name" name="all_venue_name" id="all_venue_name">
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <?php if (isset($categories) && !empty($categories)) { ?>
                                                                                <select class="table-group-action-input form-control input-inline input-small input-sm" name="all_filter_category" id="all_filter_category">
                                                                                    <option value="">--Category--</option>
                                                                                    <?php
                                                                                    foreach ($categories as $key => $CatVal) {
                                                                                        ?>
                                                                                        <option value="<?php echo $CatVal['category_id'] ?>"><?php echo $CatVal['category_name'] ?></option>                                                                
                                                                                    <?php } ?>
                                                                                </select>
                                                                            <?php } ?>
                                                                        </td>
                                                                        <td>
                                                                            <div class="input-icon right">
                                                                                <i class="fa fa-search"></i>
                                                                                <input type="text" class="form-control" placeholder="City" name="all_search_city" id="all_search_city">
                                                                            </div>                                                                
                                                                        </td>
                                                                        <td>
                                                                            <div class="input-icon right">
                                                                                <i class="fa fa-search"></i>
                                                                                <input type="text" class="form-control" placeholder="State" name="all_state" id="all_state">
                                                                            </div>
                                                                        </td>  
                                                                        <td>
                                                                            <div class="input-icon right">
                                                                                <i class="fa fa-search"></i>
                                                                                <input type="text" class="form-control" placeholder="ZIP" name="all_zipcode" id="all_zipcode">
                                                                            </div>
                                                                        </td>


                                                                        <td></td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                </tbody>
                                                            </table>
                                                        </div>   
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End: life time stats -->
                                </div>
                            </div>

                        </div>  
                        <!-- END DASHBOARD STATS -->
                    </div>
                </div>
                <!-- END PAGE -->
            </div>
        </div>

        <div id="FindOnMap" class="modal fade" tabindex="-1" data-width="760">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-upload"></i> Import Venue CSV </h4>

            </div>
            <form class="" role="form" name="import_csv" id="import_csv" method="post" enctype="multipart/form-data" action="<?php echo BASEURL . 'venues/ImportCsv' ?>">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <span class="btn default btn-file">
                                        <span class="fileinput-new">
                                            Choose CSV file </span>
                                        <span class="fileinput-exists">
                                            Change </span>
                                        <input type="file" name="file_venue" id="file_venue"/>
                                    </span>
                                    <span class="fileinput-filename">
                                    </span>
                                    &nbsp; <a href="javascript:void(0)" class="close fileinput-exists" data-dismiss="fileinput">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <button type="submit" class="btn green-meadow tooltips" title="Import CSV"><i class="fa fa-upload"></i> Import</button>
                        <a href="<?php echo UPLOADS_URL . 'sample.csv' ?>" class="btn green-meadow tooltips" title="Download Sample CSV">
                            <i class="fa fa-download"></i> Sample CSV 
                        </a>
                    </div>
                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn green-meadow" data-dismiss="modal"><i class="fa fa-close"></i> Close</button>
            </div>
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php $this->load->view('include/footer_view_inner', $JsArr); ?>
        <!-- END FOOTER -->

        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->   
        <!--[if lt IE 9]>
        <script src="<?= PLUGIN_URL ?>respond.min.js"></script>
        <script src="<?= PLUGIN_URL ?>excanvas.min.js"></script> 
        <![endif]-->   

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?= PLUGIN_URL ?>bootstrap-toastr/toastr.min.js"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>datatables/media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>        
        <script src="<?= PLUGIN_URL ?>bootstrap-modal/js/bootstrap-modalmanager.js" type="text/javascript"></script>
        <script src="<?= PLUGIN_URL ?>bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>jquery-validation/js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="<?= PLUGIN_URL ?>bootstrap-fileinput/bootstrap-fileinput.js"></script>

        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->

        <script src="<?= JS_URL ?>metronic.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>layout.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>custom/venues.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>custom/ajax.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>custom/functions.js" type="text/javascript"></script>
        <script src="<?= JS_URL ?>form-validation.js"></script>

        <!-- END PAGE LEVEL SCRIPTS -->  
        <script>
                                                    jQuery(document).ready(function () {
                                                        Metronic.init(); // initlayout and core plugins
                                                        Layout.init();
                                                        FormValidation.init();
                                                        toastr.options = {
                                                            "closeButton": true,
                                                            "positionClass": "toast-top-right",
                                                            "showDuration": "1000",
                                                            "hideDuration": "1000",
                                                            "timeOut": "3000",
                                                            "extendedTimeOut": "1000",
                                                            "showMethod": "slideDown",
                                                            "hideMethod": "slideUp"
                                                        }

<?php if ($this->session->flashdata('error')) { ?>
                                                            toastr.error('<?php echo $this->session->flashdata('error') ?>', 'Venues');
<?php } ?>

<?php if ($this->session->flashdata('success')) { ?>
                                                            toastr.success('<?php echo $this->session->flashdata('success') ?>', 'Venues');
                                                            //if notification flag is set then call send notification function

<?php } ?>
                                                    });
        </script>
        <?php if (isset($action) && $action == 'notification') { ?>
        <script>SendNotification('<?php echo $venue_id ?>','<?php echo $venue_type ?>','<?php echo $venue_name ?>','<?php echo $latitude ?>','<?php echo $longitude ?>');</script>
        <?php } ?>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>