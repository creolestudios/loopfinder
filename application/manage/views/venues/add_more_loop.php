<div class="form-group" id="main_loop_div_<?php echo $div_id ?>">
    <label class="control-label col-md-3"></label>
    <div class="col-md-6 input_fields_wrap">
        <div class="input-group input-small">
            <input type="text" class="form-control input-medium" placeholder="Looped Room" style="float: left" name="loop_room[]" id="<?php echo $div_id ?>">
            <span class="input-group-btn">
                <button class="btn btn-icon-only red-soft remove_field" type="button" data-attr-id="<?php echo $div_id ?>"><i class="fa fa-minus"></i></button>
            </span>
        </div>
        
    </div>
</div>
