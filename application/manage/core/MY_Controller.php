<?php

class MY_Controller extends CI_Controller {

    static protected $activity_info = array();

    public function __construct() {
        parent::__construct();
        # Load authentication library #

        //$this->load->library('authentication');

        $this->set_template_obj();
    }

    function cstm_write_view($region, $view_file, $view_data = array(), $overwrite = FALSE) {

        if (!is_object($GLOBALS['_template_lib_obj'])) {
            $this->set_template_obj();
        }

        return $GLOBALS['_template_lib_obj']->write_view($region, $view_file, $view_data, $overwrite);
    }

    function cstm_render($region = NULL, $buffer = FALSE, $parse = FALSE) {
        if (!is_object($GLOBALS['_template_lib_obj'])) {
            $this->set_template_obj();
        }
        return $GLOBALS['_template_lib_obj']->render($region, $buffer, $parse);
    }

    function set_template_obj() {
        //print_r($this->template);exit;
        if (!is_object($GLOBALS['_template_lib_obj'])) {
            $GLOBALS['_template_lib_obj'] = $this->template;
        }
    }

    function __destruct() {
    }

}
