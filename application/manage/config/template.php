<?php

$template['active_template'] = 'default';
$template['default']['template'] = 'template.php'; 

$template['default']['regions'] = array(
  'left_column',
  'content',
  'footer'
);