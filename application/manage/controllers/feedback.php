<?php

/*
 * @category   Categories
 * @package    Add/Update/Delete Categories
 * @author     Bhargav Bhanderi <bhargav@creolestudios.com>
 * @author     Another Author <another@example.com>
 * @copyright  2014 Creole Studios
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    Release: 1.0 
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require APPPATH . '/libraries/php-export-data.class.php';

class Feedback extends CI_Controller {

    var $viewData = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('feedback_model');
        $this->load->library('Datatables');
        $this->load->helper(array('form', 'url'));
        if (!$this->session->userdata('M_ADMINLOGIN'))
            redirect('login');
    }

    //#################################################################
    // Name : index
    // Purpose : To fetch and display account settings/quote settings data
    // In Params : merchant id
    // Out params : load account setting view
    //#################################################################
    public function index() {

        //get count data for tab
        $GetCount = $this->feedback_model->GetCount();

        //load view
        $this->load->view('feedback/list_view', $GetCount);
    }

    //#################################################################
    // Name : ExportFeedbacks
    // Purpose : To export feedback data
    // In Params : void
    // Out params : export feedback data to csv
    //#################################################################
    public function ExportFeedbacks() {
        // 'browser' tells the library to stream the data directly to the browser.
        // other options are 'file' or 'string'
        // 'test.xls' is the filename that the browser will use when attempting to 
        // save the download        
        //get users data
        $GetFeedback = $this->feedback_model->GetAllFeedback();

        //if user data found
        if (isset($GetFeedback) && $GetFeedback['status'] == '1') {
            //call export csv data
            $exporter = new ExportDataCSV('browser', 'Feedbacks_' . date("d_M_Y") . '.csv');

            $exporter->initialize(); // starts streaming data to web browser
            // pass addRow() an array and it converts it to Excel XML format and sends 
            // it to the browser
            //add header row for titles
            $exporter->addRow(array("User Name", "Email", "Venue Name", "City", "State", "Looped Room", "Satisfied", "Reason", "Feedback Date", "Comment"));

            //looped through array
            foreach ($GetFeedback['feedback_data'] as $FeedbackKey => $FeedbackVal) {
                $exporter->addRow(array($FeedbackVal['customer_name'], $FeedbackVal['email'], $FeedbackVal['venue_name'], $FeedbackVal['city'], $FeedbackVal['state'], $FeedbackVal['loop_room_name'], $FeedbackVal['satisfy'], $FeedbackVal['reason'], $FeedbackVal['feedback_date'], $FeedbackVal['comment']));
            }

            $exporter->finalize(); // writes the footer, flushes remaining data to browser.
        }
        exit(); // all done
    }

    //#################################################################
    // Name : details
    // Purpose : To show user's details
    // In Params : user id
    // Out params : load user details view
    //#################################################################    

    public function Details() {

        //initialize
        $ViewData = array();

        //get category id to fetch data
        $FeedbackId = base64_decode($this->uri->segment(3));

        //get action from url
        $Action = $this->uri->segment(5);

        //if action is archive then dont update message status
        if (isset($Action) && $Action != 'archive') {
            //update message status to read when details oepn
            $Status = "1";
            $UpdateMessage = $this->feedback_model->UpdateFeedbackStatus($FeedbackId, $Status);
        }

        //get category details based on category id
        $FeedbackDetails = $this->feedback_model->GetFeedbackDetails($FeedbackId);
        //mprd($FeedbackDetails);
        if (isset($FeedbackDetails) && $FeedbackDetails['status'] == '1') {
            $ViewData['feedback'] = $FeedbackDetails['feedback_data'];
        }

        //Load udpate category view
        $this->load->view('feedback/details_view', $ViewData);
    }

//###########################################################
    //function : DeleteFeedback
    //To delete feedback
    //Input : feedback id
    //Output : message
    //###########################################################
    public function DeleteFeedback() {

        $PostData = $this->input->post();

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from post data
            $Params = array_map('trim', $PostData['data']);

            //process customer data
            extract($Params);

            //call function to change status
            $DeleteFeedback = $this->feedback_model->DeleteFeedback($feedback_id);

            //print response
            echo json_encode($DeleteFeedback);
        }
        exit;
    }

    //###########################################################
    //function : AddArchive
    //To archive message
    //Input : message id
    //Output : message
    //###########################################################
    public function AddArchive() {

        $PostData = $this->input->post();

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from post data
            $Params = array_map('trim', $PostData['data']);

            //process customer data
            extract($Params);

            //call function to change status
            $UpdateFeedback = $this->feedback_model->UpdateFeedbackStatus($feedback_id, $status);

            //print response
            echo json_encode($UpdateFeedback);
        }
        exit;
    }

    //#################################################################
    // Name : UnreadMessages
    // Purpose : To get all unread messages
    // In Params : void
    // Out params : load all unread message
    //#################################################################    

    public function UnreadMessages() {
        //get data from input params
        $GetData = $this->input->post();

        //process data
        if (isset($GetData) && !empty($GetData)) {
            //remove space from params
            $Params = array_map('trim', $GetData);

            //query to select checkin data

            $this->datatables->select("CONCAT(v.name,'_',v.id) as venue_name,DATE_FORMAT(f.feedback_date,'%d %b %Y %h:%i %p') as feedback_date,v.city as venue_city,v.state as venue_state,v.zipcode as venue_zip,CONCAT(f.id,'_',f.flag) as detail,CASE f.loop_working WHEN '0' THEN 'No' WHEN '1' THEN 'Yes' WHEN '2' THEN 'Other' END as satisfy", false);
            $this->datatables->from("feedback as f ");
            $this->datatables->join('venues as v', 'f.venue_id = v.id', 'left');
            $this->datatables->where("f.loop_working", '0');
            $this->datatables->where("f.flag != '2'");

            ## name condition
            if (isset($_POST['name']) && $_POST['name'] != '') {
                $this->datatables->like('v.name', trim($_POST['name']), '%');
            }

            ## zipcode condition
            if (isset($_POST['search_zip']) && $_POST['search_zip'] != '') {
                $this->datatables->like('v.zipcode', trim($_POST['search_zip']), 'after');
            }

            ## city condition
            if (isset($_POST['city']) && $_POST['city'] != '') {
                $this->datatables->like('v.city', trim($_POST['city']), '%');
            }

            ## state condition
            if (isset($_POST['state']) && $_POST['state'] != '') {
                $this->datatables->like('v.state', trim($_POST['state']), '%');
            }

            ## date condition
            if ($_POST['from_date'] != '' && $_POST['to_date'] != '') {

                //convert date to mysql format
                $DateTemp = explode('/', $_POST['from_date']);
                $DateEndTemp = explode('/', $_POST['to_date']);

                ## Condition for the date comparisn
                $RequestStartDate = date('Y-m-d', strtotime($DateTemp[2] . '-' . $DateTemp[0] . '-' . $DateTemp[1]));
                $RequestEndDate = date('Y-m-d', strtotime($DateEndTemp[2] . '-' . $DateEndTemp[0] . '-' . $DateEndTemp[1]));

                $this->datatables->where("DATE(f.feedback_date) >=  '" . $RequestStartDate . "'");
                $this->datatables->where("DATE(f.feedback_date) <=  '" . $RequestEndDate . "'");

                //$this->datatables->where("DATE(r.request_date) <= ");
            }
            if ($_POST['from_date'] != '' && $_POST['to_date'] == '') {
                //convert date to mysql format
                $DateTemp = explode('/', $_POST['from_date']);

                ## Condition for the date comparisn
                $RequestStartDate = date('Y-m-d', strtotime($DateTemp[2] . '-' . $DateTemp[0] . '-' . $DateTemp[1]));

                $this->datatables->where("DATE(f.feedback_date) >=  '" . $RequestStartDate . "'");
            }
            if ($_POST['from_date'] == '' && $_POST['to_date'] != '') {
                //convert date to mysql format
                $DateEndTemp = explode('/', $_POST['to_date']);

                ## Condition for the date comparisn
                $RequestEndDate = date('Y-m-d', strtotime($DateEndTemp[2] . '-' . $DateEndTemp[0] . '-' . $DateEndTemp[1]));

                $this->datatables->where("DATE(f.feedback_date) <=  '" . $RequestEndDate . "'");
            }
            echo $this->datatables->generate();
        }
    }

//#################################################################
    // Name : AllMessages
    // Purpose : To all the messages
    // In Params : void
    // Out params : load all the messages
    //#################################################################    

    public function AllMessages() {
        //get data from input params
        $GetData = $this->input->post();

        //process data
        if (isset($GetData) && !empty($GetData)) {
            //remove space from params
            $Params = array_map('trim', $GetData);

            //query to select checkin data

            $this->datatables->select("CONCAT(v.name,'_',v.id) as venue_name,DATE_FORMAT(f.feedback_date,'%d %b %Y %h:%i %p') as feedback_date,v.city as venue_city,v.state as venue_state,CONCAT(f.id,'_',f.flag) as detail,CASE f.loop_working WHEN '0' THEN 'No' WHEN '1' THEN 'Yes' WHEN '2' THEN 'Other' END as satisfy,v.zipcode as venue_zip", false);
            $this->datatables->from("feedback as f ");
            $this->datatables->join('venues as v', 'f.venue_id = v.id', 'left');
            $this->datatables->where("f.flag != '2'");

            ## name condition
            if (isset($_POST['name']) && $_POST['name'] != '') {
                $this->datatables->like('v.name', trim($_POST['name']), '%');
            }

            ## zipcode condition
            if (isset($_POST['search_zip']) && $_POST['search_zip'] != '') {
                $this->datatables->like('v.zipcode', trim($_POST['search_zip']), 'after');
            }

            ## city condition
            if (isset($_POST['city']) && $_POST['city'] != '') {
                $this->datatables->like('v.city', trim($_POST['city']), '%');
            }
            ## state condition
            if (isset($_POST['state']) && $_POST ['state'] != '') {
                $this->datatables->like('v.state', trim($_POST['state']), '%');
            }

            ## date condition
            if ($_POST['from_all_date'] != '' && $_POST['to_all_date'] != '') {

                //convert date to mysql format
                $DateTemp = explode('/', $_POST['from_all_date']);
                $DateEndTemp = explode('/', $_POST['to_all_date']);

                ## Condition for the date comparisn
                $RequestStartDate = date('Y-m-d', strtotime($DateTemp[2] . '-' . $DateTemp[0] . '-' . $DateTemp[1]));
                $RequestEndDate = date('Y-m-d', strtotime($DateEndTemp[2] . '-' . $DateEndTemp[0] . '-' . $DateEndTemp[1]));

                $this->datatables->where("DATE(f.feedback_date) >=  '" . $RequestStartDate . "'");
                $this->datatables->where("DATE(f.feedback_date) <=  '" . $RequestEndDate . "'");

                //$this->datatables->where("DATE(r.request_date) <= ");
            }
            if ($_POST['from_all_date'] != '' && $_POST['to_all_date'] == '') {
                //convert date to mysql format
                $DateTemp = explode('/', $_POST['from_all_date']);

                ## Condition for the date comparisn
                $RequestStartDate = date('Y-m-d', strtotime($DateTemp[2] . '-' . $DateTemp[0] . '-' . $DateTemp[1]));

                $this->datatables->where("DATE(f.feedback_date) >=  '" . $RequestStartDate . "'");
            }
            if ($_POST['from_all_date'] == '' && $_POST['to_all_date'] != '') {
                //convert date to mysql format
                $DateEndTemp = explode('/', $_POST['to_all_date']);

                ## Condition for the date comparisn
                $RequestEndDate = date('Y-m-d', strtotime($DateEndTemp[2] . '-' . $DateEndTemp[0] . '-' . $DateEndTemp[1]));

                $this->datatables->where("DATE(f.feedback_date) <=  '" . $RequestEndDate . "'");
            }

            echo $this->datatables->generate();
        }
    }

    //#################################################################
    // Name : ArchiveMessages
    // Purpose : To all the messages
    // In Params : void
    // Out params : load all the messages
    //#################################################################
    public function ArchiveMessages() {
        //get data from input params
        $GetData = $this->input->post();

        //process data
        if (isset($GetData) && !empty($GetData)) {
            //remove space from params
            $Params = array_map('trim', $GetData);

            //query to select checkin data

            $this->datatables->select("CONCAT(v.name,'_',v.id) as venue_name,DATE_FORMAT(f.feedback_date,'%d %b %Y %h:%i %p') as feedback_date,v.city as venue_city,v.state as venue_state,f.id as detail,CASE f.loop_working WHEN '0' THEN 'No' WHEN '1' THEN 'Yes' WHEN '2' THEN 'Other' END as satisfy,v.zipcode as venue_zip", false);
            $this->datatables->from("feedback as f ");
            $this->datatables->join('venues as v', 'f.venue_id = v.id', 'left');
            $this->datatables->where("f.flag", '2');

            ## name condition
            if (isset($_POST['name']) && $_POST['name'] != '') {
                $this->datatables->like('v.name', trim($_POST['name']), '%');
            }

            ## zipcode condition
            if (isset($_POST['search_zip']) && $_POST['search_zip'] != '') {
                $this->datatables->like('v.zipcode', trim($_POST['search_zip']), 'after');
            }
            ## city condition
            if (isset($_POST['city']) && $_POST['city'] != '') {
                $this->datatables->like('v.city', trim($_POST['city']), '%');
            }

            ## state condition
            if (isset($_POST['state']) && $_POST['state'] != '') {
                $this->datatables->like('v.state', trim($_POST['state']), '%');
            }

            ## date condition
            if ($_POST['from_arch_date'] != '' && $_POST['to_arch_date'] != '') {

                //convert date to mysql format
                $DateTemp = explode('/', $_POST['from_arch_date']);
                $DateEndTemp = explode('/', $_POST['to_arch_date']);

                ## Condition for the date comparisn
                $RequestStartDate = date('Y-m-d', strtotime($DateTemp[2] . '-' . $DateTemp[0] . '-' . $DateTemp[1]));
                $RequestEndDate = date('Y-m-d', strtotime($DateEndTemp[2] . '-' . $DateEndTemp[0] . '-' . $DateEndTemp[1]));

                $this->datatables->where("DATE(f.feedback_date) >=  '" . $RequestStartDate . "'");
                $this->datatables->where("DATE(f.feedback_date) <=  '" . $RequestEndDate . "'");

                //$this->datatables->where("DATE(f.feedback_date) <= ");
            }
            if ($_POST['from_arch_date'] != '' && $_POST['to_arch_date'] == '') {
                //convert date to mysql format
                $DateTemp = explode('/', $_POST['from_arch_date']);

                ## Condition for the date comparisn
                $RequestStartDate = date('Y-m-d', strtotime($DateTemp[2] . '-' . $DateTemp[0] . '-' . $DateTemp[1]));

                $this->datatables->where("DATE(f.feedback_date) >=  '" . $RequestStartDate . "'");
            }
            if ($_POST['from_arch_date'] == '' && $_POST['to_arch_date'] != '') {
                //convert date to mysql format
                $DateEndTemp = explode('/', $_POST['to_arch_date']);

                ## Condition for the date comparisn
                $RequestEndDate = date('Y-m-d', strtotime($DateEndTemp[2] . '-' . $DateEndTemp[0] . '-' . $DateEndTemp[1]));

                $this->datatables->where("DATE(f.feedback_date) <=  '" . $RequestEndDate . "'");
            }
            echo $this->datatables->generate();
        }
    }

}
