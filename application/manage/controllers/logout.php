<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends CI_Controller
{ 	
	var $viewData = array();
	function __construct()
	{
		parent::__construct();
	}
	
	function index()
	{	
		//$dt_last_login=$this->general_model->update_AdminLastLogin($this->session->userdata('ADMINID'));
                $this->session->unset_userdata();
		$this->session->sess_destroy();     
		redirect('/','refresh');
	}
}
?>