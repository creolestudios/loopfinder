<?php

/*
 * @category   Categories
 * @package    Add/Update/Delete Categories
 * @author     Bhargav Bhanderi <bhargav@creolestudios.com>
 * @author     Another Author <another@example.com>
 * @copyright  2014 Creole Studios
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    Release: 1.0 
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require APPPATH . '/libraries/php-export-data.class.php';

class Categories extends CI_Controller {

    var $viewData = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('categories_model');
        $this->load->library('Datatables');
        $this->load->helper(array('form', 'url'));
        if (!$this->session->userdata('M_ADMINLOGIN'))
            redirect('login');
    }

    //#################################################################
    // Name : index
    // Purpose : To fetch and display account settings/quote settings data
    // In Params : merchant id
    // Out params : load account setting view
    //#################################################################
    public function index() {
        //get merchant id from session

        $UserId = $this->session->userdata('M_ADMINID');
        //load view
        $this->load->view('categories/list_view');
    }

    //#################################################################
    // Name : ExportVenues
    // Purpose : To export venue data
    // In Params : void
    // Out params : all venue data
    //#################################################################
    public function ExportCategories() {
        // 'browser' tells the library to stream the data directly to the browser.
        // other options are 'file' or 'string'
        // 'test.xls' is the filename that the browser will use when attempting to 
        // save the download        
        //get users data
        $GetCategories = $this->categories_model->GetAllCategories();
        
        //mprd($GetVenues);
        //if user data found
        if (isset($GetCategories) && $GetCategories['status'] == '1') {
            //call export csv data
            $exporter = new ExportDataCSV('browser', 'Categories_' . date("d_M_Y") . '.csv');

            $exporter->initialize(); // starts streaming data to web browser
            // pass addRow() an array and it converts it to Excel XML format and sends 
            // it to the browser
            //add header row for titles
            $exporter->addRow(array("Category ID","Category Name"));

            //looped through array
            foreach ($GetCategories['category_data'] as $CatKey => $CatVal) {
                $exporter->addRow(array($CatVal['category_id'], $CatVal['category_name']));
            }

            $exporter->finalize(); // writes the footer, flushes remaining data to browser.
        }
        exit(); // all done
    }

    //#################################################################
    // Name : GetCategoryList
    // Purpose : To get category list
    // In Params : void
    // Out params : all category data
    //#################################################################
    public function GetCategoryList() {

        //set image path
        $CategoryImagePath = UPLOADS_URL . 'category_image/';
        $DefaultImage = UPLOADS_URL . 'default_album.png';

        $CategoryPinImagePath = UPLOADS_URL . 'category_icons/';
        $DefaultPinImage = UPLOADS_URL . 'map-pin-default.png';
        $DefaultPinImage2 = UPLOADS_URL . 'map-pin-default_grey.png';

            $this->datatables->select("name as category_name,priority, IF(image = '','" . $DefaultImage . "',CONCAT('" . $CategoryImagePath . "','thumb/',image)) as category_image,IF(pin = '','" . $DefaultPinImage . "',CONCAT('" . $CategoryPinImagePath . "',pin)) as category_pin,IF(not_looped_pin = '','" . $DefaultPinImage2 . "',CONCAT('" . $CategoryPinImagePath . "',not_looped_pin)) as category_pin2, id as detail", false)->from(" categories ");
        ## Email condition
        if (isset($_POST['name']) && $_POST['name'] != '') {
            $this->datatables->where('name like "%' . trim($_POST['name']) . '%"');
        }
       // $this->datatables->order_by('priority','asc');
        echo $this->datatables->generate();
    }

    //#################################################################
    // Name : create
    // Purpose : To create category
    // In Params : category name and id in 
    // Out params : load account setting view
    //#################################################################
    public function Create() {

        $ViewData = array();

        //select category pins data
        $CategoryPins = $this->categories_model->GetCategoryPins();

        if (isset($CategoryPins) && !empty($CategoryPins)) {
            $ViewData['category_pins'] = $CategoryPins['category_data'];
        }

        //Load create category view
        $this->load->view('categories/create_view', $ViewData);
    }

    //#################################################################
    // Name : create
    // Purpose : To create category
    // In Params : category name and id in 
    // Out params : load account setting view
    //#################################################################
    public function Update() {

        //initialize
        $ViewData = array();

        //get category id to fetch data
        $CategoryId = base64_decode($this->uri->segment(3));

        //get category details based on category id
        $CategoryDetails = $this->categories_model->GetCategoryDetails($CategoryId);

        if (isset($CategoryDetails) && $CategoryDetails['status'] == '1') {
            $ViewData['category'] = $CategoryDetails['category_data'];
        }

        //select category pins data
        $CategoryPins = $this->categories_model->GetCategoryPins();

        if (isset($CategoryPins) && !empty($CategoryPins)) {
            $ViewData['category_pins'] = $CategoryPins['category_data'];
        }

        //Load udpate category view
        $this->load->view('categories/update_view', $ViewData);
    }

    //#################################################################
    // Name : isNameAvail
    // Purpose : To check if venue with same name is available or not
    // In Params : venue name
    // Out params : success message
    //#################################################################

    public function isNameAvail() {

        $PostData = $this->input->post();

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from post data
            $Params = array_map('trim', $PostData);

            $ret = $this->categories_model->CheckCategoryExist($Params);
            echo json_encode($ret);
            exit;
        }
    }
     //#################################################################
    // Name : categoryCheck
    // Purpose : To check if category is already exist then swap priority.
    // In Params : category priority
    // Out params : success message
    //#################################################################

    public function categoryCheck() {
        $PostData = $this->input->post();

        if (isset($PostData) && count($PostData) > 0) {
             //remove space from post data
            $Params = array_map('trim', $PostData);
            $priority= $this->categories_model->CheckPriorityExist($Params);
            echo json_encode($priority);
            exit;
        }
    }

    //#################################################################
    // Name : saveCategory
    // Purpose : To save category details
    // In Params : category name and image
    // Out params : success message
    //#################################################################
    public function saveCategory() {

        //process post data and image for category
        $PostData = $this->input->post();
        $ImageName = "";

        //initialize file name
        $file_result['file_name'] = "";
      //  mpr($PostData);
       //mprd($_FILES);
        if (isset($PostData) && count($PostData) > 0) {
            //remove space from post data
            $Params = array_map('trim', $PostData);

            //process file data
            if ($_FILES['category_file']['tmp_name'] != "") {
                  
                //get image name and rename it
                $Temp = explode(".", strrev($_FILES['category_file']['name']));
                $Extention = strrev($Temp[0]);
                //resize image logic
                $ImageName = random_string() . "_" . time() . "." . $Extention;
                $Params['image'] = $ImageName;
                $config['image_library'] = 'gd2';
                $config['source_image'] = $_FILES['category_file']['tmp_name'];
                $config['create_thumb'] = FALSE;
                $config['new_image'] = CAT_IMG_PATH . $ImageName;
                $config['maintain_ratio'] = FALSE;
                $config['width'] = $PostData['w'];
                $config['height'] = $PostData['h'];
                $config['x_axis'] = $PostData['x1'];
                $config['y_axis'] = $PostData['y1'];

                //load image library
                $this->load->library('image_lib', $config);
              
                //crop the selected image from original image
                if (!$this->image_lib->crop()) {
                    $this->session->set_flashdata('error', $this->image_lib->display_errors());

                    if ($Params['action'] == 'create') {
                        redirect('categories/Create/');
                    } else {
                        redirect('categories/Update/');
                    }
                } else {
                    $config_thumb['image_library'] = 'gd2';
                    $config_thumb['source_image'] = $config['new_image'];
                    $config_thumb['create_thumb'] = TRUE;
                    $config_thumb['new_image'] = CAT_IMG_PATH . 'thumb/' . $ImageName;
                    $config_thumb['maintain_ratio'] = TRUE;
                    $config_thumb['thumb_marker'] = "";
                    $config_thumb['width'] = 200;
                    //$config_thumb['height'] = 200;
                    $this->image_lib->initialize($config_thumb);

                    if (!$this->image_lib->resize()) {
                        return $this->image_lib->display_errors();
                        $this->session->set_flashdata('error', $this->image_lib->display_errors());
                        if ($Params['action'] == 'create') {
                            redirect('categories/Create/');
                        } else {
                            redirect('categories/Update/');
                        }
                    }
                }
            }

            //process for category pin image
            if (isset($_FILES) && $_FILES['category_pin']['name'] != '') {

                $TargetPath = UPLOADS . 'category_icons/';
                $NewFileName = "";
                //get file upload data
                $field = array_keys($_FILES);

                ##########################  UPLOAD IMAGE CONFIG ############################            
                $config['upload_path'] = $TargetPath;
                $config['allowed_types'] = 'gif|jpg|png|jpeg|PNG|JPEG|JPG';
                //$config['max_size'] = 1024 * 5;
                $config['encrypt_name'] = TRUE;
                $config['overwrite'] = false;

                //load upload library
                $this->load->library('upload', $config);

                //initialize config
                $this->upload->initialize($config);

                if ($this->upload->do_upload($field[1])) {

                    //extract data to get file name and make thumb
                    extract($this->upload->data());

                    $Params['category_pin'] = $file_name;
                } else {
                    echo $this->upload->display_errors();
                    $NewFileName = '';
                }
            }
            //process for category(not looped venue) pin image
            if (isset($_FILES) && $_FILES['category_pin2']['name'] != '') {

                $TargetPath = UPLOADS . 'category_icons/';
                $NewFileName = "";
                //get file upload data
                $field = array_keys($_FILES);

                ##########################  UPLOAD IMAGE CONFIG ############################            
                $config['upload_path'] = $TargetPath;
                $config['allowed_types'] = 'gif|jpg|png|jpeg|PNG|JPEG|JPG';
                //$config['max_size'] = 1024 * 5;
                $config['encrypt_name'] = TRUE;
                $config['overwrite'] = false;

                //load upload library
                $this->load->library('upload', $config);

                //initialize config
                $this->upload->initialize($config);

                if ($this->upload->do_upload($field[2])) {

                    //extract data to get file name and make thumb
                    extract($this->upload->data());

                    $Params['category_pin2'] = $file_name;
                } else {
                    echo $this->upload->display_errors();
                    $NewFileName = '';
                }
            }
            
            //set process for update/create venue
            //if action flag set to udpate then process update else create
            
            if ($Params['action'] == 'create') {
                //insert category to the database
                $CreatCategory = $this->categories_model->CreateCategory($Params);

                //check if category added successfully or not
                if (isset($CreatCategory) && $CreatCategory['status'] == '1') {
                    $this->session->set_flashdata('success', $this->lang->line('SUCCESS_CATEGORY_CREATE'));
                    redirect('categories/');
                } else {
                    $this->session->set_flashdata('error', $this->lang->line('GENERAL_ERROR'));
                    redirect('categories/Create/');
                }
            } else {
                //process for update
                //insert category to the database
                $UpdateCategory = $this->categories_model->UpdateCategory($Params);
                

                //check if category added successfully or not
                if (isset($UpdateCategory) && $UpdateCategory['status'] == '1') {
                    $this->session->set_flashdata('success', $this->lang->line('SUCCESS_CATEGORY_UPDATE'));
                    redirect('categories/');
                } else {
                    $this->session->set_flashdata('error', $this->lang->line('GENERAL_ERROR'));
                    redirect('categories/Update/');
                }
            }
        }
    }

    //###########################################################
    //function : DeleteCategory
    //To delete category
    //Input : category id
    //Output : message
    //###########################################################
    public function DeleteCategory() {

        $PostData = $this->input->post();

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from post data
            $Params = array_map('trim', $PostData['data']);

            //process customer data
            extract($Params);

            //call function to change status
            $DeleteCategory = $this->categories_model->DeleteCategory($category_id);

            //print response
            echo json_encode($DeleteCategory);
        }
        exit;
    }

}
