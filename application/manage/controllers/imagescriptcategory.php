<?php

/*
 * @category   venues
 * @author     Jignesh Virani  <jignesh@creolestudios.com>
 * @author     Another Author <another@example.com>
 * @copyright  2014 Creole Studios
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    Release: 1.0 
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require APPPATH . '/libraries/php-export-data.class.php';

class Imagescriptcategory extends CI_Controller {

    var $viewData = array();
    var $table;

    public function __construct() {
        parent::__construct();
        $this->venues = 'venues';
        $this->load->helper('file');
        $this->load->helper(array('form', 'url'));
    }

    public function index() {


        $TargetFolder = UPLOADS . 'category_image/';
        ## process for 300* 225
        $results = scandir($TargetFolder, 1);
        $mainResizeImage = '';

        foreach ($results as $result) {

            if (!is_dir($result)) {
                $imageSource = $result;
            }
            if ($result === '.' or $result === '..')
                continue;
            //  if (is_dir($TargetFolder . '/' . $result)) {

            $newIosDir = $TargetFolder . 'ios';
            $MainResigeImage = '';
            $dirFirstx = $newIosDir . '/@1x';
            $dirSecondx = $newIosDir . '/@2x';
            $dirThirdx = $newIosDir . '/@3x';
            $dirThumb = $newIosDir . '/thumb';
            $tempFolder = $newIosDir . '/resize/';

            $allowedExtension = array("jpeg", "png");

            ##First of all resize image.
            ##Resize for 2560*1440.
            $this->load->library('image_lib');
            $config['image_library'] = 'gd2';
            $config['source_image'] = $TargetFolder . $result;
            $config['create_thumb'] = FALSE;
            $config['new_image'] = $tempFolder . $result;
            $config['maintain_ratio'] = TRUE;
            $config['master_dim'] = 'width';
            $config['quality'] = 60;
            $config['width'] = 460;
            $config['height'] = 345;
            $this->image_lib->initialize($config);
            if (!$this->image_lib->resize()) {
                echo $this->image_lib->display_errors();
            }
            $this->image_lib->clear();


            if (is_dir($newIosDir) === true) {

                if (is_dir($dirThirdx) === TRUE) {

                    ##Crop for 900*675.
                    $this->load->library('image_lib');
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $tempFolder . $result;
                    $config['create_thumb'] = FALSE;
                    $config['new_image'] = $dirThirdx . '/' . $result;

                    $config['maintain_ratio'] = FALSE;
                    $config['master_dim'] = 'width';
                    $config['quality'] = 60;
                    $config['width'] = 435;
                    $config['height'] = 345;
                    $this->image_lib->initialize($config);
                    if (!$this->image_lib->resize()) {
                        echo $this->image_lib->display_errors();
                    }
                    $this->image_lib->clear();
                }
                if (is_dir($dirSecondx)) {

                    ##Crop for 900*675.
                    $this->load->library('image_lib');
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $tempFolder . $result;
                    $config['create_thumb'] = FALSE;
                    $config['new_image'] = $dirSecondx . '/' . $result;
                    $config['maintain_ratio'] = FALSE;
                    $config['master_dim'] = 'width';
                    $config['quality'] = 60;
                    $config['width'] = 290;
                    $config['height'] = 230;
                    $this->image_lib->initialize($config);
                    if (!$this->image_lib->resize()) {
                        echo $this->image_lib->display_errors();
                    }
                    $this->image_lib->clear();
                }
                if (is_dir($dirFirstx)) {

                    ##Crop for 145*115.
                    $this->load->library('image_lib');
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $tempFolder . $result;
                    $config['create_thumb'] = FALSE;
                    $config['new_image'] = $dirFirstx . '/' . $result;
                    $config['maintain_ratio'] = FALSE;
                    $config['master_dim'] = 'width';
                    $config['quality'] = 60;
                    $config['width'] = 145;
                    $config['height'] = 115;
                    $this->image_lib->initialize($config);
                    if (!$this->image_lib->resize()) {
                        echo $this->image_lib->display_errors();
                    }
                    $this->image_lib->clear();
                }
                if (is_dir($dirThumb)) {

                    ##Crop for 240*135.
                    $this->load->library('image_lib');
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $tempFolder . $result;
                    $config['create_thumb'] = FALSE;
                    $config['new_image'] = $dirThumb . '/' . $result;
                    $config['maintain_ratio'] = FALSE;
                    $config['master_dim'] = 'width';
                    $config['quality'] = 60;
                    $config['width'] = 240;
                    $config['height'] = 135;
                    $this->image_lib->initialize($config);
                    if (!$this->image_lib->resize()) {
                        echo $this->image_lib->display_errors();
                    }
                    $this->image_lib->clear();
                }
            }
            delete_files($tempFolder . $result, TRUE);
            //exit;
        }

        ##For android users only.

        foreach ($results as $result) {

            if (!is_dir($result)) {
                $imageSource = $result;
            }
            if ($result === '.' or $result === '..')
                continue;
            //  if (is_dir($TargetFolder . '/' . $result)) {

            $newIosDir = $TargetFolder . 'android';
            $MainResigeImage = '';
            $dirFirstx = $newIosDir . '/@1x';
            $dirSecondx = $newIosDir . '/@2x';
            $dirThirdx = $newIosDir . '/@3x';
            $dirThumb = $newIosDir . '/thumb';
            $tempFolder = $newIosDir . '/resize/';

            $allowedExtension = array("jpeg", "png");

            ##First of all resize image.
            ##Resize for 2560*1440.
            $this->load->library('image_lib');
            $config['image_library'] = 'gd2';
            $config['source_image'] = $TargetFolder . $result;
            $config['create_thumb'] = FALSE;
            $config['new_image'] = $tempFolder . $result;
            $config['maintain_ratio'] = TRUE;
            $config['master_dim'] = 'width';
            $config['quality'] = 60;
            $config['width'] = 517;
            $config['height'] = 388;
            $this->image_lib->initialize($config);
            if (!$this->image_lib->resize()) {
                echo $this->image_lib->display_errors();
            }
            $this->image_lib->clear();


            if (is_dir($newIosDir) === true) {

                if (is_dir($dirThirdx) === TRUE) {

                    ##Crop for 900*675.
                    $this->load->library('image_lib');
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $tempFolder . $result;
                    $config['create_thumb'] = FALSE;
                    $config['new_image'] = $dirThirdx . '/' . $result;

                    $config['maintain_ratio'] = FALSE;
                    $config['master_dim'] = 'width';
                    $config['quality'] = 60;
                    $config['width'] = 491;
                    $config['height'] = 388;
                    $this->image_lib->initialize($config);
                    if (!$this->image_lib->resize()) {
                        echo $this->image_lib->display_errors();
                    }
                    $this->image_lib->clear();
                }
                if (is_dir($dirSecondx)) {

                    ##Crop for 900*675.
                    $this->load->library('image_lib');
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $tempFolder . $result;
                    $config['create_thumb'] = FALSE;
                    $config['new_image'] = $dirSecondx . '/' . $result;
                    $config['maintain_ratio'] = FALSE;
                    $config['master_dim'] = 'width';
                    $config['quality'] = 60;
                    $config['width'] = 327;
                    $config['height'] = 256;
                    $this->image_lib->initialize($config);
                    if (!$this->image_lib->resize()) {
                        echo $this->image_lib->display_errors();
                    }
                    $this->image_lib->clear();
                }
                if (is_dir($dirFirstx)) {

                    ##Crop for 145*115.
                    $this->load->library('image_lib');
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $tempFolder . $result;
                    $config['create_thumb'] = FALSE;
                    $config['new_image'] = $dirFirstx . '/' . $result;
                    $config['maintain_ratio'] = FALSE;
                    $config['master_dim'] = 'width';
                    $config['quality'] = 60;
                    $config['width'] = 246;
                    $config['height'] = 194;
                    $this->image_lib->initialize($config);
                    if (!$this->image_lib->resize()) {
                        echo $this->image_lib->display_errors();
                    }
                    $this->image_lib->clear();
                }
                if (is_dir($dirThumb)) {

                    ##Crop for 240*135.
                    $this->load->library('image_lib');
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $tempFolder . $result;
                    $config['create_thumb'] = FALSE;
                    $config['new_image'] = $dirThumb . '/' . $result;
                    $config['maintain_ratio'] = FALSE;
                    $config['master_dim'] = 'width';
                    $config['quality'] = 60;
                    $config['width'] = 200;
                    $config['height'] = 200;
                    $this->image_lib->initialize($config);
                    if (!$this->image_lib->resize()) {
                        echo $this->image_lib->display_errors();
                    }
                    $this->image_lib->clear();
                }
            }
            delete_files($tempFolder . $result, TRUE);
            //exit;
        }
    }

}
