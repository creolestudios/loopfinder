<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    var $viewData = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('message_model');
        $this->load->model('feedback_model');
        $this->load->model('venues_model');
        $this->load->model('customer_model');
        $this->load->library('Datatables');

        if (!$this->session->userdata('M_ADMINLOGIN'))
            redirect('login');
    }

    function index() {
        $ViewData = array();
        //get counter for dashboars
        $VenuedData = $this->venues_model->GetCounts();

        if (isset($VenuedData) && $VenuedData['status'] == '1') {
            $ViewData['register_venue_count'] = $VenuedData['register_venue_count'];
            $ViewData['requested_venue_count'] = $VenuedData['requested_venue_count'];
        }

        //get message data
        $GetRequest = $this->message_model->GetCount();
        
        if (isset($GetRequest) && !empty($GetRequest)) {
            $ViewData['request_count'] = $GetRequest['total_rows'];
            $ViewData['archive_request_count'] = $GetRequest['total_archive_count'];
        }
        
        //get feedback data
        $GetFeedback = $this->feedback_model->GetCount();
        
        if (isset($GetFeedback) && !empty($GetFeedback)) {
            $ViewData['feedback_count'] = $GetFeedback['total_rows'];
            $ViewData['archive_feedback_count'] = $GetFeedback['total_archive_count'];
        }
        //fetch customer data
        $CustomerData = $this->customer_model->GetCustomerCount();

        if (isset($CustomerData) && count($CustomerData) > 0) {
            $ViewData['customer_count'] = $CustomerData['customer_count'];
        }
        //load view
        $this->load->view('dashboard/dashboard_view', $ViewData);
    }

    //#################################################################
    // Name : AllMessages
    // Purpose : To all the messages
    // In Params : void
    // Out params : load all the messages
    //#################################################################    

    public function AllMessages() {
        //get data from input params
        $GetData = $this->input->post();

        //process data
        if (isset($GetData) && !empty($GetData)) {
            //remove space from params
            $Params = array_map('trim', $GetData);

            //query to select checkin data

            $this->datatables->select("DATE_FORMAT(r.request_date,'%d %b %Y %h:%i %p') as request_date,r.venue_name as venue_name,r.city as venue_city,r.state as venue_state,r.comment,CONCAT(r.id,'_',r.status) as detail", false);
            $this->datatables->from("request as r ");
            $this->datatables->join('customers as c', 'r.customer_id = c.id', 'left');
            $this->datatables->where('r.status', '0');

            echo $this->datatables->generate();
        }
    }

    //#################################################################
    // Name : GetFeedbacks
    // Purpose : To get all the feedbacks
    // In Params : void
    // Out params : load all the feedbacks
    //#################################################################    

    public function GetFeedbacks() {
        //get data from input params
        $GetData = $this->input->post();

        //process data
        if (isset($GetData) && !empty($GetData)) {
            //remove space from params
            $Params = array_map('trim', $GetData);

            //query to select feedback data
            $this->datatables->select("DATE_FORMAT(f.feedback_date,'%d %b %Y %h:%i %p') as feedback_date,CONCAT(v.name,'_',v.id) as venue_name,v.city as venue_city,v.state as venue_state,CONCAT(f.id,'_',f.flag) as detail ", false);
            $this->datatables->from("feedback as f ");
            $this->datatables->join('venues as v', 'f.venue_id = v.id', 'left');
            $this->datatables->where("f.loop_working", "0");
            $this->datatables->where("f.flag != '2'");

            echo $this->datatables->generate();
        }
    }

}
