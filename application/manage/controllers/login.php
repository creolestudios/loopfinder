<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    var $viewData = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');

        if ($this->session->userdata('M_ADMINLOGIN'))
            redirect('dashboard');
    }

    function index() {
        if (isset($_POST) && count($_POST) > 0) {
            $res = $this->user_model->login_check();

            if ($res == "VALIDATE")
                echo json_encode(true);
            else {
                echo json_encode($res);
                exit;
            }
        } else {
            $this->load->view('login/login_view', $this->viewData);
        }
    }
}
?>