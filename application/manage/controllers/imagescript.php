<?php

/*
 * @category   venues
 
 * @author     Jignesh Virani  <jignesh@creolestudios.com>
 * @author     Another Author <another@example.com>
 * @copyright  2014 Creole Studios
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    Release: 1.0 
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require APPPATH . '/libraries/php-export-data.class.php';

class Imagescript extends CI_Controller {

    var $viewData = array();
    var $table;

    public function __construct() {
        parent::__construct();
        
        $this->venues = 'venues';
        $this->load->helper('file');
        $this->load->helper(array('form', 'url'));
    }

    public function index() {

        $TargetFolderVenues = UPLOADS . 'venues/';
        $results = scandir($TargetFolderVenues);
     
        foreach ($results as $result) {

            if (!is_dir($result)) {
                $imageSourceFolder = $result;
            }
            if ($result === '.' or $result === '..')
                continue;
            if (is_dir($TargetFolderVenues . '/' . $result)) {

                $newIosDir = $TargetFolderVenues . '' . $result . '/ios';
               
                $dirFirstx = $newIosDir . '/@1x';
                $dirSecondx = $newIosDir . '/@2x';
                $dirThirdx = $newIosDir . '/@3x';
                $dirThumb = $newIosDir . '/thumb';
                $allowedExtension = array("jpeg", "png");

                if (is_dir($newIosDir) === true) {

                    $getFileName = scandir($TargetFolderVenues . '' . $result);
                    mkdir($newIosDir);
                    $resizeFolderTemp = $newIosDir . '/resize';

                    if (is_dir($dirThirdx) === true) {

                        mkdir($dirThirdx);
                        mkdir($newIosDir . '/resize');
                        foreach ($getFileName as $result1) {

                            if (get_mime_by_extension($result1)) {

                                ##Resize for 2560*1440.
                                $this->load->library('image_lib');
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $TargetFolderVenues . $imageSourceFolder . '/' . $result1;
                                $config['create_thumb'] = FALSE;
                                $config['new_image'] = $resizeFolderTemp . '/' . $result1;
                                $config['maintain_ratio'] = TRUE;
                                $config['master_dim'] = 'width';
                                $config['quality'] = 60;
                                $config['width'] = 1200;
                                $config['height'] = 675;
                                $this->image_lib->initialize($config);
                                if (!$this->image_lib->resize()) {
                                    echo $this->image_lib->display_errors();
                                }
                                $this->image_lib->clear();

                                ##Crop for 900*675.
                                $this->load->library('image_lib');
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                $config['create_thumb'] = FALSE;
                                $config['new_image'] = $dirThirdx . '/' . $result1;
                                $config['maintain_ratio'] = FALSE;
                                $config['master_dim'] = 'width';
                                $config['quality'] = 60;
                                $config['width'] = 900;
                                $config['height'] = 675;
                                $this->image_lib->initialize($config);
                                if (!$this->image_lib->resize()) {
                                    echo $this->image_lib->display_errors();
                                }
                                $this->image_lib->clear();
                            }
                        }
                    }

                    ##second Directory.
                    if (is_dir($dirSecondx) === true) {

                        mkdir($dirSecondx);

                        foreach ($getFileName as $result1) {
                          

                            if (get_mime_by_extension($result1)) {

                                $this->load->library('image_lib');
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                $config['create_thumb'] = FALSE;
                                $config['new_image'] = $dirSecondx . '/' . $result1;
                                $config['maintain_ratio'] = FALSE;
                                $config['master_dim'] = 'width';
                                $config['quality'] = 60;
                                $config['width'] = 600;
                                $config['height'] = 450;

                                $this->image_lib->initialize($config);
                                if (!$this->image_lib->resize()) {
                                    echo $this->image_lib->display_errors();
                                }
                                $this->image_lib->clear();
                            }
                        }
                    }
                    ##Third directory.
                    if (is_dir($dirFirstx) === true) {
                        mkdir($dirFirstx);

                        foreach ($getFileName as $result1) {
                           

                            if (get_mime_by_extension($result1)) {

                                $this->load->library('image_lib');
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                $config['create_thumb'] = FALSE;
                                $config['new_image'] = $dirFirstx . '/' . $result1;
                                $config['maintain_ratio'] = FALSE;
                                $config['master_dim'] = 'width';
                                $config['quality'] = 60;
                                $config['width'] = 300;
                                $config['height'] = 225;
                                $this->image_lib->initialize($config);
                                if (!$this->image_lib->resize()) {
                                    echo $this->image_lib->display_errors();
                                }
                                $this->image_lib->clear();
                            }
                        }
                    }

                    ## Create thumb folder.
                    if (is_dir($dirThumb) === true) {

                        mkdir($dirThumb);
                        foreach ($getFileName as $result1) {
                            

                            if (get_mime_by_extension($result1)) {

                                $this->load->library('image_lib');
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                $config['create_thumb'] = FALSE;
                                delete_files($dirThumb . '/'.$result1, TRUE);
                                $config['new_image'] = $dirThumb . '/' .'thumb_'.$result1;
                                $config['maintain_ratio'] = FALSE;
                                $config['master_dim'] = 'width';
                                $config['quality'] = 60;
                                $config['width'] = 240;
                                $config['height'] = 135;
                                
                                $this->image_lib->initialize($config);
                                if (!$this->image_lib->resize()) {
                                    echo $this->image_lib->display_errors();
                                }
                                $this->image_lib->clear();
                               
                            }
                        }
                    }
                }
            }
            delete_files($resizeFolderTemp, TRUE);
        }

        ## Android.
     $results1 =  scandir($TargetFolderVenues);
        foreach ($results1 as $result2) {
            
            
            if (!is_dir($result2)) {
                $imageSourceFolder = $result2;
            }
            if ($result2 === '.' or $result2 === '..')
                continue;
            if (is_dir($TargetFolderVenues . '/' . $result2)) {

                $newIosDir = $TargetFolderVenues . '' . $result2 . '/android';
                $dirFirstx = $newIosDir . '/@1x';
                $dirSecondx = $newIosDir . '/@2x';
                $dirThirdx = $newIosDir . '/@3x';
                $dirThumb = $newIosDir . '/thumb';
                $allowedExtension = array("jpeg", "png");

                if (is_dir($newIosDir) === true) {

                    $getFileName = scandir($TargetFolderVenues . '' . $result2);
                    mkdir($newIosDir);
                    $resizeFolderTemp = $newIosDir . '/resize';

                    if (is_dir($dirThirdx) === true) {

                        mkdir($dirThirdx);
                        mkdir($newIosDir . '/resize');
                        foreach ($getFileName as $result1) {

                            if (get_mime_by_extension($result1)) {

                                ##Resize for 2560*1440.
                                $this->load->library('image_lib');
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $TargetFolderVenues . $imageSourceFolder . '/' . $result1;
                                $config['create_thumb'] = FALSE;
                                $config['new_image'] = $resizeFolderTemp . '/' . $result1;
                                $config['maintain_ratio'] = TRUE;
                                $config['master_dim'] = 'width';
                                $config['quality'] = 60;
                                $config['width'] = 1116;
                                $config['height'] = 628;
                                $this->image_lib->initialize($config);
                                if (!$this->image_lib->resize()) {
                                    echo $this->image_lib->display_errors();
                                }
                                $this->image_lib->clear();

                                ##Crop for 900*675.
                                $this->load->library('image_lib');
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                $config['create_thumb'] = FALSE;
                                $config['new_image'] = $dirThirdx . '/' . $result1;
                                $config['maintain_ratio'] = FALSE;
                                $config['master_dim'] = 'width';
                                $config['quality'] = 60;
                                $config['width'] = 1015;
                                $config['height'] = 628;
                                $this->image_lib->initialize($config);
                                if (!$this->image_lib->resize()) {
                                    echo $this->image_lib->display_errors();
                                }
                                $this->image_lib->clear();
                            }
                        }
                    }

                    ##second Directory.
                    if (is_dir($dirSecondx) === true) {

                        mkdir($dirSecondx);

                        foreach ($getFileName as $result1) {
                           

                            if (get_mime_by_extension($result1)) {

                                $this->load->library('image_lib');
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                $config['create_thumb'] = FALSE;
                                $config['new_image'] = $dirSecondx . '/' . $result1;
                                $config['maintain_ratio'] = FALSE;
                                $config['master_dim'] = 'width';
                                $config['quality'] = 60;
                                $config['width'] = 677;
                                $config['height'] = 419;
                                $this->image_lib->initialize($config);
                                if (!$this->image_lib->resize()) {
                                    echo $this->image_lib->display_errors();
                                }
                                $this->image_lib->clear();
                            }
                        }
                    }
                    ##Third directory.
                    if (is_dir($dirFirstx) === true) {
                        mkdir($dirFirstx);

                        foreach ($getFileName as $result1) {
                          

                            if (get_mime_by_extension($result1)) {

                                $this->load->library('image_lib');
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                $config['create_thumb'] = FALSE;
                                $config['new_image'] = $dirFirstx . '/' . $result1;
                                $config['maintain_ratio'] = FALSE;
                                $config['master_dim'] = 'width';
                                $config['quality'] = 60;
                                $config['width'] = 508;
                                $config['height'] = 314;

                                $this->image_lib->initialize($config);
                                if (!$this->image_lib->resize()) {
                                    echo $this->image_lib->display_errors();
                                }
                                $this->image_lib->clear();
                            }
                        }
                    }

                    ## Create thumb folder.
                    if (is_dir($dirThumb) === true) {

                        mkdir($dirThumb);
                        foreach ($getFileName as $result1) {
                           

                            if (get_mime_by_extension($result1)) {

                                $this->load->library('image_lib');
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                $config['create_thumb'] = FALSE;
                                $config['new_image'] = $dirThumb . '/' . $result1;
                                $config['maintain_ratio'] = FALSE;
                                $config['master_dim'] = 'width';
                                $config['quality'] = 60;
                                $config['width'] = 355;
                                $config['height'] = 200;
                                $this->image_lib->initialize($config);

                                if (!$this->image_lib->resize()) {
                                    echo $this->image_lib->display_errors();
                                }
                                $this->image_lib->clear();
                            }
                        }
                    }
                }
            }
            @unlink($resizeFolderTemp);
        }
    }

}
