<?php

class Changepassword extends CI_Controller {

    var $viewData = array();

    function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        if (!$this->session->userdata('M_ADMINLOGIN'))
            redirect('login');
    }

    function index() {

        $ReturnData = array();
        if (isset($_POST['data']['old_password']) && $_POST['data']['old_password'] != '') {

            $checkPassword = $this->user_model->checkOldPassword($_POST['data']['old_password']);

            if (isset($checkPassword) && $checkPassword['status'] == '1') {
                $updatePassword = $this->user_model->changePassword($_POST['data']);

                if ($updatePassword['status'] == '1') {
                    $ReturnData['status'] = '1';
                    $ReturnData['message'] = PASSWORD_CHANGED;
                } else {
                    $ReturnData['status'] = '0';
                    $ReturnData['message'] = PASSWORD_NOT_CHANGED;
                }
            } else {
                $ReturnData['status'] = '0';
                $ReturnData['message'] = OLD_PASSWORD_NOT_OK;
            }
        }
        echo json_encode($ReturnData);
        exit;
    }

}

/*
| -------------------------------------------------------------------
|  END OF CLASS FILE
| -------------------------------------------------------------------
*/
