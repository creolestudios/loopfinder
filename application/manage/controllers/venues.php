<?php

/*
 * @category   Categories
 * @package    Add/Update/Delete Categories
 * @author     Bhargav Bhanderi <bhargav@creolestudios.com>
 * @author     Another Author <another@example.com>
 * @copyright  2014 Creole Studios
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    Release: 1.0 
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require APPPATH . '/libraries/php-export-data.class.php';

class Venues extends CI_Controller {

    var $viewData = array();

    public function __construct() {
        parent::__construct();
        
        $this->load->library('Datatables');
        $this->load->model('categories_model');
        $this->load->model('venues_model');
        $this->load->model('customer_model');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('file');
        if (!$this->session->userdata('M_ADMINLOGIN'))
            redirect('login');
    }

    public function ImportCsv() {

        $Params = $this->input->post();

        if (isset($_FILES) && $_FILES['file_venue']['name'] != '') {

            //Import uploaded file to Database
            $handle = fopen($_FILES['file_venue']['tmp_name'], "r");
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $arrResult[] = $data;
            }


            //put seperate for loop if we need to import some of the data from the generated array
            for ($i = 1; $i < count($arrResult); $i++) {
                //prepare array for insertion
                //name,latitude,longitude,address,city,state,zipcode,phone_number,website,email,internal_contact,internal_email,internal_phone_number,internal_contact_ext,internal_comment,public_comment,venue_type
                //if phone is not blank then explode it with x to get extention
                if ($arrResult[$i][13] != '') {
                    $TempPhone = explode(' x', trim($arrResult[$i][13]));
                    $Phone = $TempPhone[0];
                    $PhoneExt = (empty($TempPhone[1])) ? '' : $TempPhone[1];
                }
                //create venue with available details
                $Params['venue_name'] = $arrResult[$i][1];
                $Params['hdlatitude'] = $arrResult[$i][2];
                $Params['hdlongitude'] = $arrResult[$i][3];
                $Params['address'] = $arrResult[$i][4];
                $Params['city'] = $arrResult[$i][5];
                $Params['state'] = $arrResult[$i][6];
                $Params['zipcode'] = $arrResult[$i][7];
                $Params['phone_number'] = $arrResult[$i][8];
                $Params['website'] = $arrResult[$i][9];
                $Params['email'] = $arrResult[$i][10];
                $Params['internal_contact'] = $arrResult[$i][11];
                $Params['internal_email'] = $arrResult[$i][12];
                $Params['internal_phone_number'] = $Phone;
                $Params['internal_contact_ext'] = $PhoneExt;
                $Params['internal_comment'] = $arrResult[$i][14];
                $Params['public_comment'] = $arrResult[$i][17];
                $Params['hdvenuetype'] = $arrResult[$i][18];
                $Params['is_certified'] = '0';
                $Params['certified_date'] = "";

                $CreateVenue = $this->venues_model->CreateVenue($Params);

                if (isset($CreateVenue) && $CreateVenue['status'] == '1') {

                    //get create venue id 
                    $CreatedVenueId = $CreateVenue['venue_id'];
                }

                ## Add category for venues
                if ($arrResult[$i][15] != '') {
                    $CategoryArray = explode(',', trim($arrResult[$i][15]));

                    //explode data
                    //loop through array and create string for batch insert
                    if (isset($CategoryArray) && count($CategoryArray) > 0) {
                        foreach ($CategoryArray as $CatKey => $CatVal) {
                            if ($CatVal != '') {
                                //prepare array for batch insert
                                $Category[] = "('" . $CreatedVenueId . "','" . $CatVal . "')";
                            }
                        }

                        //extact category array and create string
                        if (isset($Category) && count($Category) > 0) {
                            $CategoryInsert = implode(',', $Category);

                            //call function to insert category
                            $CreateVenueCategory = $this->venues_model->CreateVenueCategory($CategoryInsert);
                        }
                    }
                }
                ## End category for venues
                //########### Add loop rooms for venue start ######################
                if ($arrResult[$i][16] != '') {
                    $LoopeRoomTemp = explode(', ', trim($arrResult[$i][16]));

                    if (isset($LoopeRoomTemp) && count($LoopeRoomTemp) > 0) {
                        //loop through array and create string for batch insert

                        foreach ($LoopeRoomTemp as $LoopKey => $LoopVal) {
                            if ($LoopVal != '') {
                                //prepare array for batch insert
                                $LoopRooms[] = "('" . $CreatedVenueId . "','" . $LoopVal . "')";
                            }
                        }

                        //extact category array and create string
                        if (isset($LoopRooms) && count($LoopRooms) > 0) {

                            $LoopRoomInsert = implode(',', $LoopRooms);

                            //call function to insert category
                            $CreateLoopRooms = $this->venues_model->CreateLoopRooms($LoopRoomInsert);
                        }
                    }
                }
                //########### Add loop rooms for venue end ######################


                $Phone = "";
               
                $CategoryArray = array();
                $LoopeRoomTemp = array();
                $Category = array();
                $LoopRooms = array();
            }
            fclose($handle);
            $this->session->set_flashdata('success', $this->lang->line('SUCCESS_CSV_IMPORT'));
            redirect('venues/');
        } else {
            $this->session->set_flashdata('error', $this->lang->line('GENERAL_ERROR'));
            redirect('venues/');
        }
    }

    //#################################################################
    // Name : index
    // Purpose : To fetch and display account settings/quote settings data
    // In Params : merchant id
    // Out params : load account setting view
    //#################################################################
    public function index() {

        //initialize data
        $VenueData = array();

        //get total count for registered and rquested venue
        $GetCount = $this->venues_model->GetCounts();

        if (isset($GetCount) && $GetCount['status'] == '1') {
            $VenueData['register_venue_count'] = $GetCount['register_venue_count'];
            $VenueData['requested_venue_count'] = $GetCount['requested_venue_count'];
        }
        //select all category to for venue
        $GetCategories = $this->categories_model->GetAllCategories();

        if (isset($GetCategories) && $GetCategories['status'] == '1') {
            $VenueData['categories'] = $GetCategories['category_data'];
        }
        $VenueId = $this->session->userdata('venue_id');
        $VenueType = $this->session->userdata('venue_type');
        $VenueName = $this->session->userdata('venue_name');
        $Action = $this->session->userdata('action');
        $VenueLatitude = $this->session->userdata('latitude');
        $VenueLongitude = $this->session->userdata('longitude');

        ############ Send push notification to near by client ############
        if (isset($Action) && $Action == 'notification') {
            ## set all data to view to pass it in notification service
            $VenueData['venue_id'] = $VenueId;
            $VenueData['venue_type'] = $VenueType;
            $VenueData['venue_name'] = $VenueName;
            $VenueData['action'] = $Action;
            $VenueData['latitude'] = $VenueLatitude;
            $VenueData['longitude'] = $VenueLongitude;
        }
        ############ Send push notification end  ############
        //load view
        $this->load->view('venues/list_view', $VenueData);
    }

    //#################################################################
    // Name : ExportVenues
    // Purpose : To export venue data
    // In Params : void
    // Out params : all venue data
    //#################################################################
    public function ExportVenues() {
        // 'browser' tells the library to stream the data directly to the browser.
        // other options are 'file' or 'string'
        // 'test.xls' is the filename that the browser will use when attempting to 
        // save the download        
        //get users data
        $GetVenues = $this->venues_model->GetAllVenues();
        //mprd($GetVenues);
        //if user data found
        if (isset($GetVenues) && $GetVenues['status'] == '1') {
            //call export csv data
            $exporter = new ExportDataCSV('browser', 'Venues_' . date("d_M_Y") . '.csv');

            $exporter->initialize(); // starts streaming data to web browser
            // pass addRow() an array and it converts it to Excel XML format and sends 
            // it to the browser
            //add header row for titles
            $exporter->addRow(array("Venue Type", "Name", "Address", "City", "State", "ZIP", "Latitude", "Longitude", "Phone", "Email", "Website", "Certified", "Certification Date", "Categories", "Looped Rooms", "Check-Ins", "Feedbacks", "Votes", "Internal Contact", "Internal Email", "Internal Phone", "Extention", "Internal Comment", "Public Comment"));

            //looped through array
            foreach ($GetVenues['venue_data'] as $VenueKey => $VenueVal) {
                $exporter->addRow(array($VenueVal['venue_type'], $VenueVal['name'], $VenueVal['address'], $VenueVal['city'], $VenueVal['state'], $VenueVal['zipcode'], $VenueVal['latitude'], $VenueVal['longitude'], $VenueVal['phone_number'], $VenueVal['email'], $VenueVal['website'], $VenueVal['certified'], $VenueVal['certified_date'], $VenueVal['categories'], $VenueVal['loop_rooms'], $VenueVal['checkins'], $VenueVal['feedback'], $VenueVal['votes'], $VenueVal['internal_contact'], $VenueVal['internal_email'], $VenueVal['internal_phone_number'], $VenueVal['internal_contact_ext'], $VenueVal['internal_comment'], $VenueVal['public_comment']));
            }

            $exporter->finalize(); // writes the footer, flushes remaining data to browser.
        }
        exit(); // all done
    }

    //#################################################################
    // Name : details
    // Purpose : To show venue details
    // In Params : venue id
    // Out params : load venue view as per register OR requested
    //#################################################################    

    public function Details() {

        //get venue id from URL
        $VenueId = base64_decode($this->uri->segment(3));
        //global declaration
        $VenueData = array();
        //get venue details
        $VenueDetails = $this->venues_model->GetVenueDetails($VenueId);
        //mprd($VenueDetails);
        $VenueData['venue_details'] = $VenueDetails['venue_data'];
        $VenueData['venue_images'] = $VenueDetails['venue_images'];
        $VenueData['venue_categories'] = $VenueDetails['venue_categories'];

        //load view according to venue type
        if (isset($VenueDetails) && $VenueDetails['status'] == '1') {

            //if venue type is register then load register view
            if ($VenueDetails['venue_data']['venue_type'] == '1') {
                //if venue is register then fetch
                $VenueLoopRooms = $this->venues_model->GetLoopRooms($VenueId);

                //if loop room data found
                if (isset($VenueLoopRooms) && $VenueLoopRooms['status'] == '1') {
                    $VenueData['venue_rooms'] = $VenueLoopRooms['venue_rooms'];
                }
                //mprd($VenueData);
                //load register venue details view
                $this->load->view('venues/registered_details_view', $VenueData);
            } else {

                //load requested venue details view
                $this->load->view('venues/requested_details_view', $VenueData);
            }
        }
    }

    //#################################################################
    // Name : details
    // Purpose : To show venue details
    // In Params : venue id
    // Out params : load venue view as per register OR requested
    //#################################################################
    public function EditVenue() {

        //get venue id from URL
        $VenueId = base64_decode($this->uri->segment(3));
        //global declaration
        $VenueData = array();
        //get venue details
        $VenueDetails = $this->venues_model->GetVenueDetails($VenueId);

        $VenueData['venue_details'] = $VenueDetails['venue_data'];
        $VenueData['venue_images'] = $VenueDetails['venue_images'];
        $VenueData['venue_categories'] = $VenueDetails['venue_categories'];

        //get all categories
        //select all category to for venue
        $GetCategories = $this->categories_model->GetAllCategories();

        if (isset($GetCategories) && $GetCategories['status'] == '1') {
            $VenueData['categories'] = $GetCategories['category_data'];
        }

        //load view according to venue type
        if (isset($VenueDetails) && $VenueDetails['status'] == '1') {

            //if venue type is register then load register view
            if ($VenueDetails['venue_data']['venue_type'] == '1') {
                //if venue is register then fetch
                $VenueLoopRooms = $this->venues_model->GetLoopRooms($VenueId);

                //if loop room data found
                if (isset($VenueLoopRooms) && $VenueLoopRooms['status'] == '1') {
                    $VenueData['venue_rooms'] = $VenueLoopRooms['venue_rooms'];
                }

                //load register venue details view
                $this->load->view('venues/registered_edit_view', $VenueData);
            } else {
                
                //load requested venue details view
                $this->load->view('venues/requested_edit_view', $VenueData);
            }
        }
    }

    //#################################################################
    // Name : EditRequestedVenue
    // Purpose : To edit requested venue to register
    // In Params : venue id
    // Out params : load venue view as per register OR requested
    //#################################################################
    public function EditRequestedVenue() {

        //get venue id from URL
        $VenueId = base64_decode($this->uri->segment(3));
        //global declaration
        $VenueData = array();
        //get venue details
        $VenueDetails = $this->venues_model->GetVenueDetails($VenueId);

        $VenueData['venue_details'] = $VenueDetails['venue_data'];
        $VenueData['venue_images'] = $VenueDetails['venue_images'];
        $VenueData['venue_categories'] = $VenueDetails['venue_categories'];

        //get all categories
        //select all category to for venue
        $GetCategories = $this->categories_model->GetAllCategories();

        if (isset($GetCategories) && $GetCategories['status'] == '1') {
            $VenueData['categories'] = $GetCategories['category_data'];
        }

        //load view according to venue type
        if (isset($VenueDetails) && $VenueDetails['status'] == '1') {

            $this->load->view('venues/requested_to_register', $VenueData);
        }
    }

    //#################################################################
    // Name : isNameAvail
    // Purpose : To check if venue with same name is available or not
    // In Params : venue name
    // Out params : success message
    //#################################################################

    public function isNameAvail() {

        $PostData = $this->input->post();

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from post data
            $Params = array_map('trim', $PostData);

            $ret = $this->venues_model->CheckVenueExist($Params);
            echo json_encode($ret);
            exit;
        }
    }

    //#################################################################
    // Name : VerifyRoom
    // Purpose : To verify the loop room in venue
    // In Params : room id
    // Out params : success message
    //#################################################################
    public function VerifyRoom() {

        $PostData = $this->input->post();

        if (isset($PostData) && count($PostData['data']) > 0) {

            //remove space from post data
            $Params = array_map('trim', $PostData['data']);

            //extract params
            extract($Params);

            $VerifyResult = $this->venues_model->VerifyVenueRoom($room_id);
            echo json_encode($VerifyResult);
            exit;
        }
    }

    //#################################################################
    // Name : SetDefaultImage
    // Purpose : To set default image for venue
    // In Params : venue id,image
    // Out params : success message
    //#################################################################
    public function SetDefaultImage() {
        $PostData = $this->input->post();

        if (isset($PostData) && count($PostData['data']) > 0) {

            //remove space from post data
            $Params = array_map('trim', $PostData['data']);

            //extract params
            extract($Params);

            $VerifyResult = $this->venues_model->SetVenueDefaultImage($venue_id, $image);
            echo json_encode($VerifyResult);
            exit;
        }
    }

    //#################################################################
    // Name : Create
    // Purpose : To show venue create view
    // In Params : category data
    // Out params : load user details view
    //#################################################################    

    public function Create() {

        //initialize data
        $Data = array();

        //select all category to for venue.
        $GetCategories = $this->categories_model->GetAllCategories();

        if (isset($GetCategories) && $GetCategories['status'] == '1') {
            $Data['categories'] = $GetCategories['category_data'];
        }
        $this->load->view('venues/create_view', $Data);
    }

    //#################################################################
    // Name : AddMoreCategory
    // Purpose : To create add more category view
    // In Params : void
    // Out params : category view data
    //#################################################################        
    public function AddMoreLoopRoom() {

        $PostData = $this->input->post();

        if (isset($PostData) && count($PostData) > 0) {

            //generate random strin to identify the create div.
            $RandStrig = generateRandomString(6);

            //assign string to data.
            $Data['div_id'] = $RandStrig;

            $response = $this->load->view("venues/add_more_loop", $Data, TRUE);
            //do anything you would need to with the response.
            echo json_encode($response);
        }
        exit;
    }

    //#################################################################
    // Name : GetZipCodeData
    // Purpose : To get state and city data from zipcode
    // In Params : void
    // Out params : category view data
    //################################################################# 
    public function GetZipCodeData() {

        //get post data.
        $PostData = $this->input->post();

        if (isset($PostData) && count($PostData['data']) > 0) {

            //remove space from post data.
            $Params = array_map('trim', $PostData['data']);

            $Response = $this->venues_model->GetZipCodeDetails($Params);

            echo json_encode($Response);
            exit;
        }
        exit;
    }

    //#################################################################
    // Name : SaveVenue
    // Purpose : To save venue data
    // In Params : all venue data
    // Out params : success message
    //################################################################# 

    public function SaveVenue() {

        //get post data
        $Params = $this->input->post();
      
        if (isset($Params) && count($Params) > 0) {

            //create venue first, and use its id for category,images and looped rooms.
            $CreateVenue = $this->venues_model->CreateVenue($Params);

            //if venue is created succesfully then insert category,images,looped rooms.
            if (isset($CreateVenue) && $CreateVenue['status'] == '1') {

                //get create venue id.
                $CreatedVenueId = $CreateVenue['venue_id'];

                //########### Add categories for venue start ######################
                if (isset($Params['hdcategory']) && $Params['hdcategory'] != '') {
                    //explode data
                    $CategoryArray = explode(',', $Params['hdcategory']);

                    //loop through array and create string for batch insert.
                    if (isset($CategoryArray) && count($CategoryArray) > 0) {
                        foreach ($CategoryArray as $CatKey => $CatVal) {
                            if ($CatVal != '') {
                                //prepare array for batch insert
                                $Category[] = "('" . $CreatedVenueId . "','" . $CatVal . "')";
                            }
                        }

                        //extact category array and create string.
                        if (isset($Category) && count($Category) > 0) {
                            $CategoryInsert = implode(',', $Category);

                            //call function to insert category.
                            $CreateVenueCategory = $this->venues_model->CreateVenueCategory($CategoryInsert);
                        }
                    }
                }
                //########### Add categories for venue end ######################
                //########### Add loop rooms for venue start ######################
                if (isset($Params['loop_room']) && count($Params['loop_room']) > 0) {
                    //loop through array and create string for batch insert.

                    foreach ($Params['loop_room'] as $LoopKey => $LoopVal) {
                        if ($LoopVal != '') {
                            //prepare array for batch insert.
                            $LoopRooms[] = "('" . $CreatedVenueId . "','" . $LoopVal . "')";
                        }
                    }

                    //extact category array and create string.
                    if (isset($LoopRooms) && count($LoopRooms) > 0) {

                        $LoopRoomInsert = implode(',', $LoopRooms);
                        //call function to insert category.
                        $CreateLoopRooms = $this->venues_model->CreateLoopRooms($LoopRoomInsert);
                    }
                }

                //########### Add loop rooms for venue end ######################
                //########### Add images for venue start ######################
                //define folder path.
                $TargetFolder = UPLOADS . 'venues/' . $CreatedVenueId;
                $TargetFolderThumb = UPLOADS . 'venues/' . $CreatedVenueId . '/thumb/';
                $TempImagePath = UPLOADS . 'temp/';
                $TempImagePathThumb = UPLOADS . 'temp/thumb/';

                //process to move files
                //create directory if not exist.
                if (is_dir($TargetFolder) === false) {
                    mkdir($TargetFolder);
                    mkdir($TargetFolderThumb);
                }
              //  mprd($Params['hdimages']);
                if ($Params['hdimages'] != '') {
                    $ImageData = explode(",", $_POST['hdimages']);

                    //set cover image
                    $CoverImage = $ImageData[0];

                    if (isset($ImageData) && count($ImageData) > 0) {
                        //move file to album name
                        foreach ($ImageData as $ImageVal) {

                            //move image from temp to album folder
                            if (file_exists($TempImagePath . $ImageVal)) {
                                //rename($TempImagePath . $ImageVal, $TargetFolder . '/' . $ImageVal);

                                $this->load->library('image_lib');
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $TempImagePath . $ImageVal;
                                $config['create_thumb'] = FALSE;
                                $config['new_image'] = $TargetFolder . '/' . $ImageVal;
                                $config['maintain_ratio'] = FALSE;
                                $config['master_dim'] = 'width';
                                $config['quality'] = 60;
                                $config['width'] = 2560;
                                $config['height'] = 1440;
                                $imageSize = $this->image_lib->get_image_properties($config['source_image'], TRUE);
                                $config['x_axis'] = (($imageSize['width'] / 2) - ($config['width'] / 2));
                                $config['y_axis'] = (($imageSize['height'] / 2) - ($config['height'] / 2));

                                //load image library
                                $this->image_lib->initialize($config);
                                $this->image_lib->crop();
                                $this->image_lib->clear();

                                $config_thumb['image_library'] = 'gd2';
                                $config_thumb['source_image'] = $TargetFolder . '/' . $ImageVal;
                                $config_thumb['create_thumb'] = TRUE;
                                $config_thumb['new_image'] = $TargetFolderThumb . $ImageVal;
                                $config_thumb['maintain_ratio'] = TRUE;
                                $config_thumb['thumb_marker'] = "";
                                $config_thumb['quality'] = 80;
                                $config_thumb['width'] = 800;
                                $config_thumb['height'] = 600;
                                $this->image_lib->initialize($config_thumb);
                                $this->image_lib->resize();
                                $this->image_lib->clear();


                                ## process for image resizing.
                                ##there are two device type 1.Ios and 2.Android
                                ##in those two folder there are three seprate folder like
                                ## @1x, @2x, @3x and thumb.
                                ##process start from here.

                                $TargetFolderVenues = UPLOADS . 'venues/' . $CreatedVenueId;
                                $results = scandir($TargetFolderVenues);
                               

                                foreach ($results as $result) {

                                    if (!is_dir($result)) {
                                        $imageSourceFolder = $result;
                                    }
                                    if ($result === '.' or $result === '..')
                                        continue;
                                    if (is_dir($TargetFolderVenues . '/' . $result)) {

                                        $newIosDir = $TargetFolderVenues . '/ios';

                                        $dirFirstx = $newIosDir . '/@1x';
                                        $dirSecondx = $newIosDir . '/@2x';
                                        $dirThirdx = $newIosDir . '/@3x';
                                        $dirThumb = $newIosDir . '/thumb';
                                        $allowedExtension = array("jpeg", "png");

                                        if (is_dir($newIosDir) == FALSE) {

                                            $getFileName = scandir($TargetFolderVenues);
                                            mkdir($newIosDir);
                                            $resizeFolderTemp = $newIosDir . '/resize';
                                        }

                                        if (is_dir($dirThirdx) == FALSE) {

                                            mkdir($dirThirdx);
                                            mkdir($newIosDir . '/resize');
                                        }

                                        foreach ($getFileName as $result1) {

                                            if (is_dir($result1) == false) {

                                                if (get_mime_by_extension($result1)) {

                                                    ##Resize for 2560*1440.
                                                   // $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $TargetFolderVenues . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['maintain_ratio'] = TRUE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 1200;
                                                    $config['height'] = 675;
                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();

                                                    ##Crop for 900*675.
                                                 //   $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $dirThirdx . '/' . $result1;
                                                    $config['maintain_ratio'] = FALSE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 900;
                                                    $config['height'] = 675;
                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();
                                                }
                                            }
                                        }
                                        // mprd($getFileName);
                                        // }
                                        ##second Directory.
                                        if (is_dir($dirSecondx) === FALSE) {

                                            mkdir($dirSecondx);
                                        }

                                        foreach ($getFileName as $result1) {

                                            if (is_dir($result1) == false) {
                                                if (get_mime_by_extension($result1)) {

                                                   // $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $dirSecondx . '/' . $result1;
                                                    $config['maintain_ratio'] = FALSE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 600;
                                                    $config['height'] = 450;

                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();
                                                }
                                            }
                                        }
                                        // }
                                        ##Third directory.
                                        if (is_dir($dirFirstx) === FALSE) {
                                            mkdir($dirFirstx);
                                        }

                                        foreach ($getFileName as $result1) {

                                            if (is_dir($result1) == false) {
                                                if (get_mime_by_extension($result1)) {

                                                   // $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $dirFirstx . '/' . $result1;
                                                    $config['maintain_ratio'] = FALSE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 300;
                                                    $config['height'] = 225;
                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();
                                                }
                                            }
                                        }
                                        //  }
                                        ## Create thumb folder.
                                        if (is_dir($dirThumb) === FALSE) {
                                            mkdir($dirThumb);
                                        }

                                        foreach ($getFileName as $result1) {

                                            if (is_dir($result1) == false) {
                                                if (get_mime_by_extension($result1)) {

                                                  //  $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    delete_files($dirThumb . '/' . $result1, TRUE);
                                                    $config['new_image'] = $dirThumb . '/' . 'thumb_' . $result1;
                                                    $config['maintain_ratio'] = FALSE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 240;
                                                    $config['height'] = 135;

                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();
                                                }
                                            }
                                        }
                                        //  }
                                        //  }
                                    }
                                    delete_files($resizeFolderTemp, TRUE);
                                }

                                ## Android.
                                //  $results1 = scandir($TargetFolderVenues);
                                //  $results = scandir($TargetFolderVenues);

                                foreach ($results as $result2) {


                                    if (!is_dir($result2)) {
                                        $imageSourceFolder = $result2;
                                    }
                                    if ($result2 === '.' or $result2 === '..')
                                        continue;
                                    if (is_dir($TargetFolderVenues . '/' . $result2)) {

                                        $newAndroidDir = $TargetFolderVenues . '/android';
                                        $dirFirstx = $newAndroidDir . '/@1x';
                                        $dirSecondx = $newAndroidDir . '/@2x';
                                        $dirThirdx = $newAndroidDir . '/@3x';
                                        $dirThumb = $newAndroidDir . '/thumb';
                                        $allowedExtension = array("jpeg", "png");

                                        if (is_dir($newAndroidDir) === FALSE) {
                                            $getFileName = scandir($TargetFolderVenues);
                                            mkdir($newAndroidDir);
                                            $resizeFolderTemp = $newAndroidDir . '/resize';
                                        }

                                        if (is_dir($dirThirdx) === false) {

                                            mkdir($dirThirdx);
                                            mkdir($newAndroidDir . '/resize');
                                        }

                                        foreach ($getFileName as $result1) {

                                            if (is_dir($result1) == false) {
                                                if (get_mime_by_extension($result1)) {

                                                    ##Resize for 2560*1440.
                                                 //   $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $TargetFolderVenues . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['maintain_ratio'] = TRUE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 1116;
                                                    $config['height'] = 628;
                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();

                                                    ##Crop for 900*675.
                                                  //  $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $dirThirdx . '/' . $result1;
                                                    $config['maintain_ratio'] = FALSE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 1015;
                                                    $config['height'] = 628;
                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();
                                                }
                                            }
                                        }
                                        //   }
                                        ##second Directory.
                                        if (is_dir($dirSecondx) === FALSE) {

                                            mkdir($dirSecondx);
                                        }

                                        foreach ($getFileName as $result1) {

                                            if (is_dir($result1) == false) {

                                                if (get_mime_by_extension($result1)) {

                                                  //  $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $dirSecondx . '/' . $result1;
                                                    $config['maintain_ratio'] = FALSE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 677;
                                                    $config['height'] = 419;
                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();
                                                }
                                            }
                                        }
                                        // }
                                        ##Third directory.
                                        if (is_dir($dirFirstx) === FALSE) {
                                            mkdir($dirFirstx);
                                        }

                                        foreach ($getFileName as $result1) {

                                            if (is_dir($result1) == false) {
                                                if (get_mime_by_extension($result1)) {

                                                  //  $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $dirFirstx . '/' . $result1;
                                                    $config['maintain_ratio'] = FALSE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 508;
                                                    $config['height'] = 314;

                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();
                                                }
                                            }
                                        }
                                        //  }
                                        ## Create thumb folder.
                                        if (is_dir($dirThumb) === FALSE) {
                                            mkdir($dirThumb);
                                        }
                                        foreach ($getFileName as $result1) {

                                            if (is_dir($result1) == false) {

                                                if (get_mime_by_extension($result1)) {

                                                    //$this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $dirThumb . '/' . $result1;
                                                    $config['maintain_ratio'] = FALSE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 355;
                                                    $config['height'] = 200;
                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();
                                                }
                                            }
                                        }
                                    }
                                    @unlink($resizeFolderTemp);
                                }
                            }
                            @unlink($TempImagePath . $ImageVal);

                            $ImageArray[] = "('" . $CreatedVenueId . "','" . $ImageVal . "')";
                        }

                        if (isset($ImageArray) && count($ImageArray) > 0) {
                            $ImageString = implode(",", $ImageArray);
                            //save image to db
                            $SaveImages = $this->venues_model->SaveVenueImages($ImageString);
                        }
                    }

                    //udpate cover image for album
                    $UpdateCover = $this->venues_model->UpdateCoverImage($CreatedVenueId, $CoverImage);
                }
                //########### Add images for venue end ######################
                //set data in sesion to send push notification when listing page load                
                if (isset($CreatedVenueId) && $CreatedVenueId != '') {
                    $this->session->set_userdata(array('venue_name' => $Params['venue_name'], 'venue_type' => $Params['hdvenuetype'], 'venue_id' => $CreatedVenueId, 'latitude' => $Params['hdlatitude'], 'longitude' => $Params['hdlongitude'], 'action' => 'notification'));
                }
                $this->session->set_flashdata('success', $this->lang->line('SUCCESS_VENUE_CREATE'));
                redirect('venues/');
            } else {
                $this->session->set_flashdata('error', $this->lang->line('GENERAL_ERROR'));
                redirect('venues/Create');
            }
        }
    }

    //###########################################################
    //function : SendNotification
    //To send notification
    //Input : customer data,venue name,venue type,venue id
    //Output : void
    //###########################################################
    public function SendNotification() {

        ## Load notification settings
        $this->load->library('apn');
        $this->apn->payloadMethod = 'enhance'; // you can turn on this method for debuggin purpose
        $this->apn->connectToPush();

        ## Notification time settings, ## Generate UTC data
        $Timezone = "UTC";
        date_default_timezone_set($Timezone);
        $NotificationDateTime = gmdate("Y-m-d h:i:s");

        $PostData = $this->input->post();

        if (isset($PostData) && count($PostData['data']) > 0) {

            //remove space from post data
            $Params = array_map('trim', $PostData['data']);

            ############ Send push notification to near by client ############
            ## Get all near by user with settings data
            $GetNearByUsers = $this->customer_model->GetSettingsForNearby($Params['hdlatitude'], $Params['hdlongitude']);
            $VenueType = $Params['venue_type'];
            $VenueName = $Params['venue_name'];
            $VenueId = $Params['venue_id'];

            if (isset($GetNearByUsers) && $GetNearByUsers['status'] == '1') {

                ## call notification function
                $CustomerData = $GetNearByUsers['customer_data'];
                //$this->SendNotification($GetNearByUsers['customer_data'], $Params['venue_name'], $Params['hdvenuetype'], $CreatedVenueId);
            }
            ############ Send push notification end  ############

            foreach ($CustomerData as $NotiKey => $NotiVal) {
                ##check for device token, if device token not blank then only send notification
                if (isset($NotiVal['device_token']) && $NotiVal['device_token'] != '') {

                    ## Define Data
                    $NotificatioMessage = (isset($VenueType) && $VenueType == '1') ? $this->lang->line('NEW_REGISTERED_VENUE') : $this->lang->line('NEW_REQUESTED_VENUE');
                    $NotificationFlag = (isset($VenueType) && $VenueType == '1') ? $NotiVal['notify_new_loop_added'] : $NotiVal['notify_new_loop_requested'];
                    $NotificationDistance = (isset($VenueType) && $VenueType == '1') ? $NotiVal['notify_new_loop_added_distance'] : $NotiVal['notify_new_loop_requested_distance'];
                    $NotificationType = (isset($VenueType) && $VenueType == '1') ? "2" : "3";

                    ## set notification if user distance is within range
                    if ($NotiVal['distance'] <= $NotificationDistance) {

                        ## set venue id in key
                        $this->apn->setData(array('venue_id' => $VenueId));

                        ## set message
                        $NotificationMessageToSend = str_replace("{VENUE_NAME}", $VenueName, $NotificatioMessage);

                        ## send notification
                        $this->apn->sendMessage($NotiVal['device_token'], $NotificationMessageToSend, 1, 'default');
                        ## print error if notification is not sent
                        if (isset($send_result) && !$send_result) {
                            $ResponseData['success'] = "0";
                            $ResponseData['message'] = $this->apn->error;
                        }
                        ## prepare array to insert data in notification table
                        $Notification[] = "('" . $NotiVal['customer_id'] . "','" . $VenueId . "','" . $NotificationMessageToSend . "','" . $NotificationDateTime . "','0','" . $NotificationType . "')";
                    }
                }

                $NotificatioMessage = "";
                $NotificationFlag = "";
                $NotificationDistance = "";
            }

            ## process notification insert
            if (isset($Notification) && count($Notification) > 0) {
                $NotificationStr = implode(',', $Notification);

                ## insert notification data
                if (isset($NotificationStr) && $NotificationStr != '') {
                    $AddNotification = $this->customer_model->AddNotification($NotificationStr);
                }
            }
        }

        ## unset all sesstion variable
        $this->session->unset_userdata('venue_id');
        $this->session->unset_userdata('venue_type');
        $this->session->unset_userdata('venue_name');
        $this->session->unset_userdata('action');
        $this->session->unset_userdata('latitude');
        $this->session->unset_userdata('longitude');

        echo "success";
    }

    //###########################################################
    //function : RequestedToRegisterVenue
    //To udpate venue from requested to register
    //Input : venue data
    //Output : void
    //###########################################################
    public function RequestedToRegisterVenue() {

        //get post data
        $Params = $this->input->post();

        if (isset($Params) && count($Params) > 0) {
            //remove space from post data
            //create venue first, and use its id for category,images and looped rooms
            $CreateVenue = $this->venues_model->UpdateVenue($Params);

            //if venue is created succesfully then insert category,images,looped rooms
            if (isset($CreateVenue) && $CreateVenue['status'] == '1') {

                //get create venue id 
                $UpdateVenueId = $Params['venue_id'];

                //########### Add categories for venue start ######################
                if (isset($Params['hdcategory']) && $Params['hdcategory'] != '') {

                    //delete all venue category for venue
                    $DeleteVenueCategory = $this->venues_model->DeleteVenueCategory($UpdateVenueId);

                    //explode data
                    $CategoryArray = explode(',', $Params['hdcategory']);

                    //loop through array and create string for batch insert
                    if (isset($CategoryArray) && count($CategoryArray) > 0) {
                        foreach ($CategoryArray as $CatKey => $CatVal) {
                            if ($CatVal != '') {
                                //prepare array for batch insert
                                $Category[] = "('" . $UpdateVenueId . "','" . $CatVal . "')";
                            }
                        }

                        //extact category array and create string
                        if (isset($Category) && count($Category) > 0) {
                            $CategoryInsert = implode(',', $Category);

                            //call function to insert category
                            $CreateVenueCategory = $this->venues_model->CreateVenueCategory($CategoryInsert);
                        }
                    }
                }
                //########### Add categories for venue end ######################
                //########### Add loop rooms for venue start ######################
                if (isset($Params['loop_room']) && count($Params['loop_room']) > 0) {
                    //loop through array and create string for batch insert
                    //delete all venue rooms for venue
                    //$DeleteVenueRooms = $this->venues_model->DeleteLoopRoomsVenue($UpdateVenueId);

                    foreach ($Params['loop_room'] as $LoopKey => $LoopVal) {
                        if ($LoopVal != '') {
                            //prepare array for batch insert
                            $LoopRooms[] = "('" . $UpdateVenueId . "','" . $LoopVal . "')";
                        }
                    }

                    //extact category array and create string
                    if (isset($LoopRooms) && count($LoopRooms) > 0) {

                        $LoopRoomInsert = implode(',', $LoopRooms);

                        //call function to insert category
                        $CreateLoopRooms = $this->venues_model->CreateLoopRooms($LoopRoomInsert);
                    }
                }

                //########### Add loop rooms for venue end ######################
                //########### Add images for venue start ######################
                //define folder path
                $TargetFolder = UPLOADS . 'venues/' . $UpdateVenueId;
                $TargetFolderThumb = UPLOADS . 'venues/' . $UpdateVenueId . '/thumb/';
                $TempImagePath = UPLOADS . 'temp/';
                $TempImagePathThumb = UPLOADS . 'temp/thumb/';

                //process to move files
                //create directory if not exist
                if (is_dir($TargetFolder) === false) {
                    mkdir($TargetFolder);
                    mkdir($TargetFolderThumb);
                }

                if ($Params['hdimages'] != '') {
                    $ImageData = explode(",", $_POST['hdimages']);
                    //mprd($ImageData);
                    //set cover image
                    $CoverImage = $ImageData[0];

                    if (isset($ImageData) && count($ImageData) > 0) {
                        //move file to album name
                        foreach ($ImageData as $ImageVal) {

                            //move iamge from temp to album folder
                            if (file_exists($TempImagePath . $ImageVal)) {
                                //rename($TempImagePath . $ImageVal, $TargetFolder . '/' . $ImageVal);

                                $this->load->library('image_lib');
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $TempImagePath . $ImageVal;
                                $config['create_thumb'] = FALSE;
                                $config['new_image'] = $TargetFolder . '/' . $ImageVal;
                                $config['maintain_ratio'] = FALSE;
                                $config['master_dim'] = 'width';
                                $config['quality'] = 60;
                                $config['width'] = 2560;
                                $config['height'] = 1440;
                                $imageSize = $this->image_lib->get_image_properties($config['source_image'], TRUE);
                                $config['x_axis'] = (($imageSize['width'] / 2) - ($config['width'] / 2));
                                $config['y_axis'] = (($imageSize['height'] / 2) - ($config['height'] / 2));

                                //load image library
                                $this->image_lib->initialize($config);
                                $this->image_lib->crop();
                                $this->image_lib->clear();

                                $config_thumb['image_library'] = 'gd2';
                                $config_thumb['source_image'] = $TargetFolder . '/' . $ImageVal;
                                $config_thumb['create_thumb'] = TRUE;
                                $config_thumb['new_image'] = $TargetFolderThumb . $ImageVal;
                                $config_thumb['maintain_ratio'] = TRUE;
                                $config_thumb['thumb_marker'] = "";
                                $config_thumb['quality'] = 80;
                                $config_thumb['width'] = 800;
                                $config_thumb['height'] = 600;
                                $this->image_lib->initialize($config_thumb);
                                $this->image_lib->resize();
                                $this->image_lib->clear();
                                
                                ## process for image resizing.
                                ##there are two device type 1.Ios and 2.Android
                                ##in those two folder there are three seprate folder like
                                ## @1x, @2x, @3x and thumb.
                                ##process start from here.
                                
                                $TargetFolderVenues = UPLOADS . 'venues/' . $UpdateVenueId;
                                $results = scandir($TargetFolderVenues);
                             

                                foreach ($results as $result) {

                                    if (!is_dir($result)) {
                                        $imageSourceFolder = $result;
                                    }
                                    if ($result === '.' or $result === '..')
                                        continue;
                                    if (is_dir($TargetFolderVenues . '/' . $result)) {

                                        $newIosDir = $TargetFolderVenues . '/ios';

                                        $dirFirstx = $newIosDir . '/@1x';
                                        $dirSecondx = $newIosDir . '/@2x';
                                        $dirThirdx = $newIosDir . '/@3x';
                                        $dirThumb = $newIosDir . '/thumb';
                                        $allowedExtension = array("jpeg", "png");

                                        if (is_dir($newIosDir) == FALSE) {

                                            $getFileName = scandir($TargetFolderVenues);
                                            mkdir($newIosDir);
                                            $resizeFolderTemp = $newIosDir . '/resize';
                                        }

                                        if (is_dir($dirThirdx) == FALSE) {

                                            mkdir($dirThirdx);
                                            mkdir($newIosDir . '/resize');
                                        }

                                        foreach ($getFileName as $result1) {

                                            if (is_dir($result1) == false) {

                                                if (get_mime_by_extension($result1)) {

                                                    ##Resize for 2560*1440.
                                                    $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $TargetFolderVenues . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['maintain_ratio'] = TRUE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 1200;
                                                    $config['height'] = 675;
                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();

                                                    ##Crop for 900*675.
                                                    $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $dirThirdx . '/' . $result1;
                                                    $config['maintain_ratio'] = FALSE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 900;
                                                    $config['height'] = 675;
                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();
                                                }
                                            }
                                        }
                                        // mprd($getFileName);
                                        // }
                                        ##second Directory.
                                        if (is_dir($dirSecondx) === FALSE) {

                                            mkdir($dirSecondx);
                                        }

                                        foreach ($getFileName as $result1) {

                                            if (is_dir($result1) == false) {
                                                if (get_mime_by_extension($result1)) {

                                                    $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $dirSecondx . '/' . $result1;
                                                    $config['maintain_ratio'] = FALSE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 600;
                                                    $config['height'] = 450;

                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();
                                                }
                                            }
                                        }
                                        // }
                                        ##Third directory.
                                        if (is_dir($dirFirstx) === FALSE) {
                                            mkdir($dirFirstx);
                                        }

                                        foreach ($getFileName as $result1) {

                                            if (is_dir($result1) == false) {
                                                if (get_mime_by_extension($result1)) {

                                                    $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $dirFirstx . '/' . $result1;
                                                    $config['maintain_ratio'] = FALSE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 300;
                                                    $config['height'] = 225;
                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();
                                                }
                                            }
                                        }
                                        //  }
                                        ## Create thumb folder.
                                        if (is_dir($dirThumb) === FALSE) {
                                            mkdir($dirThumb);
                                        }

                                        foreach ($getFileName as $result1) {

                                            if (is_dir($result1) == false) {
                                                if (get_mime_by_extension($result1)) {

                                                    $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    delete_files($dirThumb . '/' . $result1, TRUE);
                                                    $config['new_image'] = $dirThumb . '/' . 'thumb_' . $result1;
                                                    $config['maintain_ratio'] = FALSE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 240;
                                                    $config['height'] = 135;

                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();
                                                }
                                            }
                                        }
                                        //  }
                                        //  }
                                    }
                                    delete_files($resizeFolderTemp, TRUE);
                                }

                                ## Android.
                                //  $results1 = scandir($TargetFolderVenues);
                                //  $results = scandir($TargetFolderVenues);

                                foreach ($results as $result2) {


                                    if (!is_dir($result2)) {
                                        $imageSourceFolder = $result2;
                                    }
                                    if ($result2 === '.' or $result2 === '..')
                                        continue;
                                    if (is_dir($TargetFolderVenues . '/' . $result2)) {

                                        $newAndroidDir = $TargetFolderVenues . '/android';
                                        $dirFirstx = $newAndroidDir . '/@1x';
                                        $dirSecondx = $newAndroidDir . '/@2x';
                                        $dirThirdx = $newAndroidDir . '/@3x';
                                        $dirThumb = $newAndroidDir . '/thumb';
                                        $allowedExtension = array("jpeg", "png");

                                        if (is_dir($newAndroidDir) === FALSE) {
                                            $getFileName = scandir($TargetFolderVenues);
                                            mkdir($newAndroidDir);
                                            $resizeFolderTemp = $newAndroidDir . '/resize';
                                        }

                                        if (is_dir($dirThirdx) === false) {

                                            mkdir($dirThirdx);
                                            mkdir($newAndroidDir . '/resize');
                                        }

                                        foreach ($getFileName as $result1) {

                                            if (is_dir($result1) == false) {
                                                if (get_mime_by_extension($result1)) {

                                                    ##Resize for 2560*1440.
                                                    $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $TargetFolderVenues . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['maintain_ratio'] = TRUE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 1116;
                                                    $config['height'] = 628;
                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();

                                                    ##Crop for 900*675.
                                                    $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $dirThirdx . '/' . $result1;
                                                    $config['maintain_ratio'] = FALSE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 1015;
                                                    $config['height'] = 628;
                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();
                                                }
                                            }
                                        }
                                        //   }
                                        ##second Directory.
                                        if (is_dir($dirSecondx) === FALSE) {

                                            mkdir($dirSecondx);
                                        }

                                        foreach ($getFileName as $result1) {

                                            if (is_dir($result1) == false) {

                                                if (get_mime_by_extension($result1)) {

                                                    $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $dirSecondx . '/' . $result1;
                                                    $config['maintain_ratio'] = FALSE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 677;
                                                    $config['height'] = 419;
                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();
                                                }
                                            }
                                        }
                                        // }
                                        ##Third directory.
                                        if (is_dir($dirFirstx) === FALSE) {
                                            mkdir($dirFirstx);
                                        }

                                        foreach ($getFileName as $result1) {

                                            if (is_dir($result1) == false) {
                                                if (get_mime_by_extension($result1)) {

                                                    $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $dirFirstx . '/' . $result1;
                                                    $config['maintain_ratio'] = FALSE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 508;
                                                    $config['height'] = 314;

                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();
                                                }
                                            }
                                        }
                                        //  }
                                        ## Create thumb folder.
                                        if (is_dir($dirThumb) === FALSE) {
                                            mkdir($dirThumb);
                                        }
                                        foreach ($getFileName as $result1) {

                                            if (is_dir($result1) == false) {

                                                if (get_mime_by_extension($result1)) {

                                                    $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $dirThumb . '/' . $result1;
                                                    $config['maintain_ratio'] = FALSE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 355;
                                                    $config['height'] = 200;
                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();
                                                }
                                            }
                                        }
                                    }
                                    @unlink($resizeFolderTemp);
                                } 
                            }
                            @unlink($TempImagePath . $ImageVal);
                            $ImageArray[] = "('" . $UpdateVenueId . "','" . $ImageVal . "')";
                        }

                        if (isset($ImageArray) && count($ImageArray) > 0) {
                            $ImageString = implode(",", $ImageArray);
                            //save image to db
                            $SaveImages = $this->venues_model->SaveVenueImages($ImageString);
                        }
                    }

                    //udpate cover image for album
                    $UpdateCover = $this->venues_model->UpdateCoverImage($UpdateVenueId, $CoverImage);
                }
                //########### Add images for venue end ######################
                ## set venue data to session to send push notification when listing page load
                $this->session->set_userdata(array('venue_name' => $Params['venue_name'], 'venue_type' => $Params['hdvenuetype'], 'venue_id' => $Params['venue_id'], 'latitude' => $Params['hdlatitude'], 'longitude' => $Params['hdlongitude'], 'action' => 'notification'));

                $this->session->set_flashdata('success', $this->lang->line('SUCCESS_VENUE_UPDATE'));
                redirect('venues/');
            } else {
                $this->session->set_flashdata('error', $this->lang->line('GENERAL_ERROR'));
                redirect('venues/Create');
            }
        }
    }

    //###########################################################
    //function : MasterUpload
    //To upload album images
    //Input : image
    //Output : void
    //###########################################################
    public function UpdateRegisterVenue() {

        //get post data
        $Params = $this->input->post();
       
        if (isset($Params) && count($Params) > 0) {
            //remove space from post data
            //create venue first, and use its id for category,images and looped rooms
            $CreateVenue = $this->venues_model->UpdateVenue($Params);

            //if venue is created succesfully then insert category,images,looped rooms
            if (isset($CreateVenue) && $CreateVenue['status'] == '1') {

                //get create venue id 
                $UpdateVenueId = $Params['venue_id'];

                //########### Add categories for venue start ######################
                if (isset($Params['hdcategory']) && $Params['hdcategory'] != '') {

                    //delete all venue category for venue
                    $DeleteVenueCategory = $this->venues_model->DeleteVenueCategory($UpdateVenueId);

                    //explode data
                    $CategoryArray = explode(',', $Params['hdcategory']);

                    //loop through array and create string for batch insert
                    if (isset($CategoryArray) && count($CategoryArray) > 0) {
                        foreach ($CategoryArray as $CatKey => $CatVal) {
                            if ($CatVal != '') {
                                //prepare array for batch insert
                                $Category[] = "('" . $UpdateVenueId . "','" . $CatVal . "')";
                            }
                        }

                        //extact category array and create string
                        if (isset($Category) && count($Category) > 0) {
                            $CategoryInsert = implode(',', $Category);

                            //call function to insert category
                            $CreateVenueCategory = $this->venues_model->CreateVenueCategory($CategoryInsert);
                        }
                    }
                }
                //########### Add categories for venue end ######################
                //########### Add loop rooms for venue start ######################
                if (isset($Params['loop_room']) && count($Params['loop_room']) > 0) {
                    //loop through array and create string for batch insert
                    //delete all venue rooms for venue
                    //$DeleteVenueRooms = $this->venues_model->DeleteLoopRoomsVenue($UpdateVenueId);

                    foreach ($Params['loop_room'] as $LoopKey => $LoopVal) {
                        if ($LoopVal != '') {
                            //prepare array for batch insert
                            $LoopRooms[] = "('" . $UpdateVenueId . "','" . $LoopVal . "')";
                        }
                    }

                    //extact category array and create string
                    if (isset($LoopRooms) && count($LoopRooms) > 0) {

                        $LoopRoomInsert = implode(',', $LoopRooms);

                        //call function to insert category
                        $CreateLoopRooms = $this->venues_model->CreateLoopRooms($LoopRoomInsert);
                    }
                }

                //########### Add loop rooms for venue end ######################
                //########### Add images for venue start ######################
                //define folder path
                $TargetFolder = UPLOADS . 'venues/' . $UpdateVenueId;
                $TargetFolderThumb = UPLOADS . 'venues/' . $UpdateVenueId . '/thumb/';
                $TempImagePath = UPLOADS . 'temp/';
                $TempImagePathThumb = UPLOADS . 'temp/thumb/';

                //process to move files
                //create directory if not exist
                if (is_dir($TargetFolder) === false) {
                    mkdir($TargetFolder);
                    mkdir($TargetFolderThumb);
                }
                
                if ($_POST['hdimages'] != '') {
                    $ImageData = explode(",", $_POST['hdimages']);
                   
                    //set cover image
                    $CoverImage = $ImageData[0];

                    if (isset($ImageData) && count($ImageData) > 0) {
                        //move file to album name
                        foreach ($ImageData as $ImageVal) {

                            //move iamge from temp to album folder
                            if (file_exists($TempImagePath . $ImageVal)) {
                                //rename($TempImagePath . $ImageVal, $TargetFolder . '/' . $ImageVal);

                                $this->load->library('image_lib');
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $TempImagePath . $ImageVal;
                                $config['create_thumb'] = FALSE;
                                $config['new_image'] = $TargetFolder . '/' . $ImageVal;
                                $config['maintain_ratio'] = FALSE;
                                $config['master_dim'] = 'width';
                                $config['quality'] = 60;
                                $config['width'] = 2560;
                                $config['height'] = 1440;
                                $imageSize = $this->image_lib->get_image_properties($config['source_image'], TRUE);
                                $config['x_axis'] = (($imageSize['width'] / 2) - ($config['width'] / 2));
                                $config['y_axis'] = (($imageSize['height'] / 2) - ($config['height'] / 2));

                                //load image library
                                $this->image_lib->initialize($config);
                                $this->image_lib->crop();
                                $this->image_lib->clear();

                                $config_thumb['image_library'] = 'gd2';
                                $config_thumb['source_image'] = $TargetFolder . '/' . $ImageVal;
                                $config_thumb['create_thumb'] = TRUE;
                                $config_thumb['new_image'] = $TargetFolderThumb . $ImageVal;
                                $config_thumb['maintain_ratio'] = TRUE;
                                $config_thumb['thumb_marker'] = "";
                                $config_thumb['quality'] = 80;
                                $config_thumb['width'] = 800;
                                $config_thumb['height'] = 600;
                                $this->image_lib->initialize($config_thumb);
                                $this->image_lib->resize();
                                $this->image_lib->clear();
                                
                                ## process for image resizing.
                                ##there are two device type 1.Ios and 2.Android
                                ##in those two folder there are three seprate folder like
                                ## @1x, @2x, @3x and thumb.
                                ##process start from here.
                                
                                $TargetFolderVenues = UPLOADS . 'venues/' . $UpdateVenueId;
                                $results = scandir($TargetFolderVenues);
                                $resizeFolderTemp ='';

                                foreach ($results as $result) {

                                    if (!is_dir($result)) {
                                        $imageSourceFolder = $result;
                                    }
                                    if ($result === '.' or $result === '..')
                                        continue;
                                    if (is_dir($TargetFolderVenues . '/' . $result)) {

                                        $newIosDir = $TargetFolderVenues . '/ios';

                                        $dirFirstx = $newIosDir . '/@1x';
                                        $dirSecondx = $newIosDir . '/@2x';
                                        $dirThirdx = $newIosDir . '/@3x';
                                        $dirThumb = $newIosDir . '/thumb';
                                        $allowedExtension = array("jpeg", "png");

                                        if (is_dir($newIosDir) == FALSE) {

                                            $getFileName = scandir($TargetFolderVenues);
                                            mkdir($newIosDir);
                                            $resizeFolderTemp = $newIosDir . '/resize';
                                        }

                                        if (is_dir($dirThirdx) == FALSE) {

                                            mkdir($dirThirdx);
                                            mkdir($newIosDir . '/resize');
                                        }

                                        foreach ($getFileName as $result1) {

                                            if (is_dir($result1) == false) {

                                                if (get_mime_by_extension($result1)) {

                                                    ##Resize for 2560*1440.
                                                    $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $TargetFolderVenues . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['maintain_ratio'] = TRUE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 1200;
                                                    $config['height'] = 675;
                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();

                                                    ##Crop for 900*675.
                                                    $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $dirThirdx . '/' . $result1;
                                                    $config['maintain_ratio'] = FALSE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 900;
                                                    $config['height'] = 675;
                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();
                                                }
                                            }
                                        }
                                        // mprd($getFileName);
                                        // }
                                        ##second Directory.
                                        if (is_dir($dirSecondx) === FALSE) {

                                            mkdir($dirSecondx);
                                        }

                                        foreach ($getFileName as $result1) {

                                            if (is_dir($result1) == false) {
                                                if (get_mime_by_extension($result1)) {

                                                    $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $dirSecondx . '/' . $result1;
                                                    $config['maintain_ratio'] = FALSE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 600;
                                                    $config['height'] = 450;

                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();
                                                }
                                            }
                                        }
                                        // }
                                        ##Third directory.
                                        if (is_dir($dirFirstx) === FALSE) {
                                            mkdir($dirFirstx);
                                        }

                                        foreach ($getFileName as $result1) {

                                            if (is_dir($result1) == false) {
                                                if (get_mime_by_extension($result1)) {

                                                    $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $dirFirstx . '/' . $result1;
                                                    $config['maintain_ratio'] = FALSE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 300;
                                                    $config['height'] = 225;
                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();
                                                }
                                            }
                                        }
                                        //  }
                                        ## Create thumb folder.
                                        if (is_dir($dirThumb) === FALSE) {
                                            mkdir($dirThumb);
                                        }

                                        foreach ($getFileName as $result1) {

                                            if (is_dir($result1) == false) {
                                                if (get_mime_by_extension($result1)) {

                                                    $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    delete_files($dirThumb . '/' . $result1, TRUE);
                                                    $config['new_image'] = $dirThumb . '/' . 'thumb_' . $result1;
                                                    $config['maintain_ratio'] = FALSE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 240;
                                                    $config['height'] = 135;

                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();
                                                }
                                            }
                                        }
                                        //  }
                                        //  }
                                    }
                                    delete_files($resizeFolderTemp, TRUE);
                                }

                                ## Android.
                                //  $results1 = scandir($TargetFolderVenues);
                                //  $results = scandir($TargetFolderVenues);

                                foreach ($results as $result2) {


                                    if (!is_dir($result2)) {
                                        $imageSourceFolder = $result2;
                                    }
                                    if ($result2 === '.' or $result2 === '..')
                                        continue;
                                    if (is_dir($TargetFolderVenues . '/' . $result2)) {

                                        $newAndroidDir = $TargetFolderVenues . '/android';
                                        $dirFirstx = $newAndroidDir . '/@1x';
                                        $dirSecondx = $newAndroidDir . '/@2x';
                                        $dirThirdx = $newAndroidDir . '/@3x';
                                        $dirThumb = $newAndroidDir . '/thumb';
                                        $allowedExtension = array("jpeg", "png");

                                        if (is_dir($newAndroidDir) === FALSE) {
                                            $getFileName = scandir($TargetFolderVenues);
                                            mkdir($newAndroidDir);
                                            $resizeFolderTemp = $newAndroidDir . '/resize';
                                        }

                                        if (is_dir($dirThirdx) === false) {

                                            mkdir($dirThirdx);
                                            mkdir($newAndroidDir . '/resize');
                                        }

                                        foreach ($getFileName as $result1) {

                                            if (is_dir($result1) == false) {
                                                if (get_mime_by_extension($result1)) {

                                                    ##Resize for 2560*1440.
                                                    $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $TargetFolderVenues . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['maintain_ratio'] = TRUE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 1116;
                                                    $config['height'] = 628;
                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();

                                                    ##Crop for 900*675.
                                                    $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $dirThirdx . '/' . $result1;
                                                    $config['maintain_ratio'] = FALSE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 1015;
                                                    $config['height'] = 628;
                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();
                                                }
                                            }
                                        }
                                        //   }
                                        ##second Directory.
                                        if (is_dir($dirSecondx) === FALSE) {

                                            mkdir($dirSecondx);
                                        }

                                        foreach ($getFileName as $result1) {

                                            if (is_dir($result1) == false) {

                                                if (get_mime_by_extension($result1)) {

                                                    $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $dirSecondx . '/' . $result1;
                                                    $config['maintain_ratio'] = FALSE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 677;
                                                    $config['height'] = 419;
                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();
                                                }
                                            }
                                        }
                                        // }
                                        ##Third directory.
                                        if (is_dir($dirFirstx) === FALSE) {
                                            mkdir($dirFirstx);
                                        }

                                        foreach ($getFileName as $result1) {

                                            if (is_dir($result1) == false) {
                                                if (get_mime_by_extension($result1)) {

                                                    $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $dirFirstx . '/' . $result1;
                                                    $config['maintain_ratio'] = FALSE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 508;
                                                    $config['height'] = 314;

                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();
                                                }
                                            }
                                        }
                                        //  }
                                        ## Create thumb folder.
                                        if (is_dir($dirThumb) === FALSE) {
                                            mkdir($dirThumb);
                                        }
                                        foreach ($getFileName as $result1) {

                                            if (is_dir($result1) == false) {

                                                if (get_mime_by_extension($result1)) {

                                                    $this->load->library('image_lib');
                                                    $config['image_library'] = 'gd2';
                                                    $config['source_image'] = $resizeFolderTemp . '/' . $result1;
                                                    $config['create_thumb'] = FALSE;
                                                    $config['new_image'] = $dirThumb . '/' . $result1;
                                                    $config['maintain_ratio'] = FALSE;
                                                    $config['master_dim'] = 'width';
                                                    $config['quality'] = 60;
                                                    $config['width'] = 355;
                                                    $config['height'] = 200;
                                                    $this->image_lib->initialize($config);
                                                    if (!$this->image_lib->resize()) {
                                                        echo $this->image_lib->display_errors();
                                                    }
                                                    $this->image_lib->clear();
                                                }
                                            }
                                        }
                                    }
                                    @unlink($resizeFolderTemp);
                                }
                            }
                            @unlink($TempImagePath . $ImageVal);

//                            if (file_exists($TempImagePathThumb . $ImageVal)) {
//                                rename($TempImagePathThumb . $ImageVal, $TargetFolderThumb . '/' . $ImageVal);
//                            }

                            $ImageArray[] = "('" . $UpdateVenueId . "','" . $ImageVal . "')";
                        }

                        if (isset($ImageArray) && count($ImageArray) > 0) {
                            $ImageString = implode(",", $ImageArray);
                            //save image to db
                            $SaveImages = $this->venues_model->SaveVenueImages($ImageString);
                        }
                    }
                    ## if cover image is not blank then update
                    if (isset($CoverImage) && $CoverImage != '') {
                        //udpate cover image for album
                        $UpdateCover = $this->venues_model->UpdateCoverImage($UpdateVenueId, $CoverImage);
                    }
                }
                //########### Add images for venue end ######################

                $this->session->set_flashdata('success', $this->lang->line('SUCCESS_VENUE_UPDATE'));
                redirect('venues/');
            } else {
                $this->session->set_flashdata('error', $this->lang->line('GENERAL_ERROR'));
                redirect('venues/Create');
            }
        }
    }

    //###########################################################
    //function : MasterUpload
    //To upload album images
    //Input : image
    //Output : void
    //###########################################################
    public function MasterUpload() {
        
        //define image paths
        $TargetPath = UPLOADS . 'temp/';
        $TargetPathThumb = UPLOADS . 'temp/thumb/';
        $UserSessionData = array();
        $NewFileName = "";
       
        //upload file to temp folder
        //process image data and upload image
        if (isset($_FILES) && $_FILES['file']['name'] != '') {
            //get file upload data
            $field = array_keys($_FILES);

            ##########################  UPLOAD IMAGE CONFIG ############################            
            $config['upload_path'] = $TargetPath;
            $config['allowed_types'] = 'gif|jpg|png|jpeg|PNG|JPEG|JPG';
            //$config['max_size'] = 1024 * 5;
            $config['encrypt_name'] = TRUE;
            $config['overwrite'] = false;

            //load upload library
            $this->load->library('upload', $config);

            //initialize config
            $this->upload->initialize($config);

            if ($this->upload->do_upload($field[0])) {

                //extract data to get file name and make thumb
                extract($this->upload->data());

                //get file name
                $OriginalFileName = $_FILES['file']['name']; //get file name from extract
                //add file name to param array
                $NewFileName = $file_name;
            } else {
                $this->upload->display_errors();
                $NewFileName = '';
            }
        }
        echo $NewFileName;
        exit;
    }

//    public function MasterUpload() {
//        //define image paths
//        $TargetPath = UPLOADS . 'temp/';
//        $TargetPathThumb = UPLOADS . 'temp/thumb/';
//        $UserSessionData = array();
//        $NewFileName = "";
//        //mprd($TargetPath);
//        //upload file to temp folder
//        //process image data and upload image
//        if (isset($_FILES) && $_FILES['file']['name'] != '') {
//
//            $img = $_FILES['file']['tmp_name'];
//            //list($width, $height, $type, $attr) = getimagesize($img);
//            $this->load->library('image_lib');
//            $Temp = explode(".", strrev($_FILES['file']['name']));
//            $Extention = strrev($Temp[0]);
//            //resize image logic
//            $NewFileName = random_string() . "_" . time() . "." . $Extention;
//            $Params['image'] = $NewFileName;
//            $config['image_library'] = 'gd2';
//            $config['source_image'] = $_FILES['file']['tmp_name'];
//            $config['create_thumb'] = FALSE;
//            $config['new_image'] = $TargetPath . $NewFileName;
//            $config['maintain_ratio'] = FALSE;
//            $config['master_dim'] = 'width';
//            $config['quality'] = 60;
//            $config['width'] = 2560;
//            $config['height'] = 1440;
//            $imageSize = $this->image_lib->get_image_properties($config['source_image'], TRUE);
//            $config['x_axis'] = (($imageSize['width'] / 2) - ($config['width'] / 2));
//            $config['y_axis'] = (($imageSize['height'] / 2) - ($config['height'] / 2));
//
//            //load image library
//            $this->image_lib->initialize($config);
//            $this->image_lib->crop();
//            $this->image_lib->clear();
//
//            if (!$this->image_lib->crop()) {
//                mprd($this->image_lib->display_errors());
//            }
//            //create thumb for the image
//            $config_thumb['image_library'] = 'gd2';
//            $config_thumb['source_image'] = $config['new_image'];
//            $config_thumb['create_thumb'] = TRUE;
//            $config_thumb['new_image'] = $TargetPathThumb . $NewFileName;
//            $config_thumb['maintain_ratio'] = FALSE;
//            $config_thumb['thumb_marker'] = "";
//            $config_thumb['quality'] = 60;
//            $config_thumb['width'] = 800;
//            $config_thumb['height'] = 600;
//            $this->image_lib->initialize($config_thumb);
//            $this->image_lib->resize();
//            $this->image_lib->clear();
//
//            if (!$this->image_lib->resize()) {
//                mprd($this->image_lib->display_errors());
//            }
//        } else {
//            $NewFileName = "";
//        }
//
//        //return new file name, it will store in hidden
//        echo $NewFileName;
//        exit;
//    }
    //###########################################################
    //function : RemoveImageTemp
    //To remove image from temp folder
    //Input : image
    //Output : void
    //###########################################################
    public function RemoveImageTemp() {

        //define image paths

        $TargetPath = UPLOADS . 'temp/';
        $TargetPathThumb = UPLOADS . 'temp/thumb/';

        //get post data
        $PostData = $this->input->post();

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from post data
            $Params = array_map('trim', $PostData);

            //unlink image from temp folder
            if (file_exists($TargetPath . $Params['image'])) {
                unlink($TargetPath . $Params['image']);
            }
            if (file_exists($TargetPathThumb . $Params['image'])) {
                unlink($TargetPathThumb . $Params['image']);
            }

            echo 'success';
        }
        exit;
    }

    //###########################################################
    //function : GetCheckins
    //To get user checking data
    //Input : Venue id
    //Output : message
    //###########################################################
    public function GetCheckins() {
        $GetData = $this->input->post();

        //process data
        if (isset($GetData) && !empty($GetData)) {
            $Params = array_map('trim', $GetData);

            //fetch checking data for customer
            //get customer id
            $VenueId = $Params['venue_id'];

            //query to select checkin data

            $this->datatables->select('DATE_FORMAT(c.checkin_date,"%d %b %Y %h:%i %p") as checkin_date,CONCAT(v.first_name," ",v.last_name," ",v.id) as customer_name,v.city as customer_city,v.state as customer_state,if(v.zipcode != "",LPAD(v.zipcode, 5, "0"),v.zipcode) as customer_zip', false);
            $this->datatables->from("checkins as c ");
            $this->datatables->join('customers as v', 'c.customer_id = v.id', 'left');
            $this->datatables->where("c.venue_id", $VenueId);

            echo $this->datatables->generate();
        }
    }

    //###########################################################
    //function : GetFeedbacks
    //To get user feedback data
    //Input : Venue id
    //Output : message
    //###########################################################
    public function GetFeedbacks() {
        //get data from input params
        $GetData = $this->input->post();

        //process data
        if (isset($GetData) && !empty($GetData)) {
            //remove space from params
            $Params = array_map('trim', $GetData);

            //fetch checking data for customer
            //get customer id
            $VenueId = $Params['venue_id'];

            //query to select checkin data

            $this->datatables->select("IF(c.user_type = '2',CONCAT(c.first_name,'_',c.id),CONCAT(c.first_name,' ',c.last_name,'_',c.id)) as customer_name,c.email as customer_email,DATE_FORMAT(f.feedback_date,'%d %b %Y %h:%i %p') as feedback_date,vr.name as loop_room_name,CASE f.loop_working WHEN '0' THEN 'No' WHEN '1' THEN 'Yes' WHEN '2' THEN 'Other' END as satisfy,CASE f.loop_not_working_reason WHEN '1' THEN 'No Sound' WHEN '2' THEN 'Poor Sound Quaility' WHEN '3' THEN 'Headset Broken'  WHEN '4' THEN 'Others' ELSE '' END as reason,f.comment", false);
            $this->datatables->from("feedback as f ");
            $this->datatables->join('customers as c', 'f.customer_id = c.id', 'left');
            $this->datatables->join('venue_rooms as vr', 'f.loop_room_id = vr.id', 'left');
            $this->datatables->where("f.venue_id", $VenueId);

            ## feedback room filter
            if (isset($_POST['venue_room_filter']) && $_POST['venue_room_filter'] != '') {
                $this->datatables->where('f.loop_room_id', trim($_POST['venue_room_filter']));
            }

            echo $this->datatables->generate();
        }
    }

    //###########################################################
    //function : GetVotes
    //To get user votes data
    //Input : User id
    //Output : message
    //###########################################################
    public function GetVotes() {
        $GetData = $this->input->post();

        //process data
        if (isset($GetData) && !empty($GetData)) {
            $Params = array_map('trim', $GetData);

            //fetch checking data for customer
            //get customer id
            $VenueId = $Params['venue_id'];

            //query to select checkin data

            $this->datatables->select('DATE_FORMAT(v.vote_date,"%d %b %Y %h:%i %p") as vote_date,CONCAT(c.first_name," ",c.last_name," ",c.id) as customer_name,c.city as customer_city,c.state as customer_state,c.zipcode as customer_zip', false);
            $this->datatables->from("votes as v ");
            $this->datatables->join('customers as c', 'v.customer_id = c.id', 'left');
            $this->datatables->where("v.venue_id", $VenueId);

            echo $this->datatables->generate();
        }
    }

    //###########################################################
    //function : DeleteVenue
    //To delete venue
    //Input : Venue id
    //Output : message
    //###########################################################
    public function DeleteVenue() {

        $PostData = $this->input->post();

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from post data
            $Params = array_map('trim', $PostData['data']);

            //process customer data
            extract($Params);

            //call function to change status
            $DeleteVenue = $this->venues_model->DeleteVenue($venue_id);

            //delete venue folder from upload
            //set target folder for venue
            $VenueImagePath = UPLOADS . 'venues/' . $venue_id . '/';

            //call function to remove directory and sub directory
            $this->RemoveVenueDir($VenueImagePath);
            //print response
            echo json_encode($DeleteVenue);
        }
        exit;
    }

    //###########################################################
    //function : DeleteVenueRooms
    //To delete venue llop room
    //Input : Room id
    //Output : message
    //###########################################################
    public function DeleteVenueRooms() {

        $PostData = $this->input->post();

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from post data
            $Params = array_map('trim', $PostData['data']);

            //process customer data
            extract($Params);

            //call function to change status
            $DeleteImage = $this->venues_model->DeleteVenueRoom($room_id);

            //print response
            echo json_encode($DeleteImage);
        }
        exit;
    }

    //###########################################################
    //function : DeleteVenueImage
    //To delete venue image
    //Input : image id
    //Output : message
    //###########################################################
    public function DeleteVenueImage() {
        $PostData = $this->input->post();

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from post data
            $Params = array_map('trim', $PostData['data']);

            //process customer data
            extract($Params);

            //call function to change status
            $DeleteRoom = $this->venues_model->DeleteVenueImage($image_id);

            //unlink image from folder
            //define image paths

            $TargetPath = UPLOADS . 'venues/' . $venue_id . '/' . $image;
            $TargetPathThumb = UPLOADS . 'venues/' . $venue_id . '/thumb/' . $image;

            //remove space from post data
            $Params = array_map('trim', $PostData);

            //unlink image from temp folder
            if (file_exists($TargetPath)) {
                unlink($TargetPath);
            }
            if (file_exists($TargetPathThumb)) {
                unlink($TargetPathThumb);
            }

            //print response
            echo json_encode($DeleteRoom);
        }
        exit;
    }

    //#################################################################
    // Name : RegisterVenue
    // Purpose : To register venue
    // In Params : venue id
    // Out params : success message
    //#################################################################
    public function RegisterVenue() {

        $PostData = $this->input->post();

        if (isset($PostData) && count($PostData['data']) > 0) {

            //remove space from post data
            $Params = array_map('trim', $PostData['data']);

            //extract params
            extract($Params);

            $UpdateResult = $this->venues_model->UpdateVenueToRegister($venue_id);
            echo json_encode($UpdateResult);
            exit;
        }
    }

    //###########################################################
    //function : GetRegisterVenues
    //To get all register venue
    //Input : void
    //Output : details of all venue
    //###########################################################
    public function GetRegisterVenues() {
        //get data from input params
        $GetData = $this->input->post();

        //process data
        if (isset($GetData) && !empty($GetData)) {
            //query to select checkin data

            $this->datatables->select("v.name as venue_name,GROUP_CONCAT(distinct c.name SEPARATOR ', ') as category_name,v.city as venue_city,v.state as venue_state,if(v.zipcode != '',LPAD(v.zipcode, 5, '0'),v.zipcode)as zipcode,count(distinct ch.id) as checkins,count(distinct f.id) as feedbacks,v.id as detail", false);
            $this->datatables->from("venues as v ");
            $this->datatables->join('venue_categories as vc', 'vc.venue_id = v.id', 'left');
            $this->datatables->join('categories as c', 'vc.category_id = c.id', 'left');
            $this->datatables->join('checkins as ch', 'ch.venue_id = v.id', 'left');
            $this->datatables->join('feedback as f', 'f.venue_id = v.id', 'left');
            $this->datatables->where('v.venue_type', '1');
            $this->datatables->group_by('v.id');
            ## Venue name
            if (isset($_POST['venue_name']) && $_POST['venue_name'] != '') {
                $this->datatables->like('v.name', trim($_POST['venue_name']), '%');
            }

            ## city condition
            if (isset($_POST['city']) && $_POST['city'] != '') {
                $this->datatables->like('v.city', trim($_POST['city']), '%');
            }

            ## state condition
            if (isset($_POST['state']) && $_POST['state'] != '') {
                $this->datatables->like('v.state', trim($_POST['state']), '%');
            }
            ## zipcode condition
            if (isset($_POST['zipcode']) && $_POST['zipcode'] != '') {
                $this->datatables->where('zipcode like "' . trim($_POST['zipcode']) . '%"');
            }
            ## category filter
            if (isset($_POST['filter_category']) && $_POST['filter_category'] != '') {
                $this->datatables->where('vc.category_id', $_POST['filter_category']);
            }

            echo $this->datatables->generate();
        }
    }

    //###########################################################
    //function : GetRequestVenues
    //To get all request venue
    //Input : void
    //Output : details of all venue
    //###########################################################
    public function GetRequestVenues() {
        //get data from input params
        $GetData = $this->input->post();

        //process data
        if (isset($GetData) && !empty($GetData)) {
            //query to select checkin data

            $this->datatables->select("v.name as venue_name,GROUP_CONCAT(distinct c.name SEPARATOR ', ') as category_name,v.city as venue_city,v.state as venue_state,if(v.zipcode != '',LPAD(v.zipcode, 5, '0'),v.zipcode)as zipcode,count(distinct vo.id) as votes,v.id as detail", false);
            $this->datatables->from("venues as v ");
            $this->datatables->join('venue_categories as vc', 'vc.venue_id = v.id', 'left');
            $this->datatables->join('categories as c', 'vc.category_id = c.id', 'left');
            $this->datatables->join('votes as vo', 'vo.venue_id = v.id', 'left');
            $this->datatables->where('v.venue_type', '2');
            $this->datatables->group_by('v.id');

            ## Venue name
            if (isset($_POST['venue_name']) && $_POST['venue_name'] != '') {
                $this->datatables->like('v.name', trim($_POST['venue_name']), '%');
            }

            ## city condition
            if (isset($_POST['city']) && $_POST['city'] != '') {
                $this->datatables->like('v.city', trim($_POST['city']), '%');
            }

            ## state condition
            if (isset($_POST['state']) && $_POST['state'] != '') {
                $this->datatables->like('v.state', trim($_POST['state']), '%');
            }
            ## zipcode condition
            if (isset($_POST['zipcode']) && $_POST['zipcode'] != '') {
                $this->datatables->where('zipcode like "' . trim($_POST['zipcode']) . '%"');
            }
            ## category filter
            if (isset($_POST['filter_category']) && $_POST['filter_category'] != '') {
                $this->datatables->where('vc.category_id', $_POST['filter_category']);
            }
            echo $this->datatables->generate();
        }
    }

    //###########################################################
    //function : GetAllVenues
    //To get all venue
    //Input : void
    //Output : details of all venue
    //###########################################################
    public function GetAllVenues() {
        //get data from input params
        $GetData = $this->input->post();

        //process data
        if (isset($GetData) && !empty($GetData)) {
            //query to select checkin data

            $this->datatables->select("v.name as venue_name,GROUP_CONCAT(distinct c.name SEPARATOR ', ') as category_name,v.city as venue_city,v.state as venue_state,if(v.zipcode != '',LPAD(v.zipcode, 5, '0'),v.zipcode)as zipcode,v.id as detail", false);
            $this->datatables->from("venues as v ");
            $this->datatables->join('venue_categories as vc', 'vc.venue_id = v.id', 'left');
            $this->datatables->join('categories as c', 'vc.category_id = c.id', 'left');
            $this->datatables->where("v.venue_type != '3'");
            $this->datatables->group_by('v.id');

            ## Venue name
            if (isset($_POST['venue_name']) && $_POST['venue_name'] != '') {
                $this->datatables->like('v.name', trim($_POST['venue_name']), '%');
            }

            ## city condition
            if (isset($_POST['city']) && $_POST['city'] != '') {
                $this->datatables->like('v.city', trim($_POST['city']), '%');
            }

            ## state condition
            if (isset($_POST['state']) && $_POST['state'] != '') {
                $this->datatables->like('v.state', trim($_POST['state']), '%');
            }
            ## zipcode condition
            if (isset($_POST['zipcode']) && $_POST['zipcode'] != '') {
                $this->datatables->where('zipcode like "' . trim($_POST['zipcode']) . '%"');
            }
            ## category filter
            if (isset($_POST['filter_category']) && $_POST['filter_category'] != '') {
                $this->datatables->where('vc.category_id', $_POST['filter_category']);
            }
            ## venue type filter
            if (isset($_POST['venue_type']) && $_POST['venue_type'] != '') {
                $this->datatables->where('v.venue_type', $_POST['venue_type']);
            }
            //mprd($this->db->last_query());
            echo $this->datatables->generate();
        }
    }

    //###########################################################
    //function : UpdateLoopedRooms
    //To update loop rooms
    //Input : room id , room value
    //Output : message
    //###########################################################
    public function UpdateLoopedRooms() {
        $PostData = $this->input->post();

        if (isset($PostData) && count($PostData['data']) > 0) {

            //remove space from post data
            $Params = array_map('trim', $PostData['data']);

            //extract params
            extract($Params);

            $UpdateResult = $this->venues_model->UpdateLoopedRoomsVenue($room_id, $room_name);
            echo json_encode($UpdateResult);
            exit;
        }
    }

    //###########################################################
    //function : RemoveVenueDir
    //To delete venue images from upload
    //Input : venue file path
    //Output : message
    //###########################################################
    public function RemoveVenueDir($dirname) {
        // recursive function to delete 
        // all subdirectories and contents: 
        if (is_dir($dirname))
            $dir_handle = opendir($dirname);
        while ($file = readdir($dir_handle)) {
            if ($file != "." && $file != "..") {
                if (!is_dir($dirname . "/" . $file)) {
                    unlink($dirname . "/" . $file);
                } else {
                    $this->RemoveVenueDir($dirname . "/" . $file);
                }
            }
        }
        closedir($dir_handle);
        rmdir($dirname);
        return true;
    }

}
