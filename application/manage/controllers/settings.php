<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings extends CI_Controller {

    var $viewData = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');
        
        $this->load->helper(array('form', 'url'));
        if (!$this->session->userdata('M_ADMINLOGIN'))
            redirect('login');
    }

    //#################################################################
    // Name : index
    // Purpose : To fetch and display account settings/quote settings data
    // In Params : merchant id
    // Out params : load account setting view
    //#################################################################
    public function index() {
        //get merchant id from session

        $UserId = $this->session->userdata('M_ADMINID');

        //############## Account Settings view Start ###################
        //fetch data using merchant model
        $Information = $this->user_model->GetAccountSettings($UserId);

        //extract data
        if (isset($Information) && !empty($Information)) {
            //if status is error(0), then print error
            if ($Information['status'] == 0) {
                //response error
                $AccountInfo['account_settings_data'] = $Information['message'];
            } else {

                //load data to view, data will be used in view as well as edit section
                //just change of mode will decide which view to load
                $AccountInfo['account_settings_data'] = $Information['data'];
            }
             $ContentInfo = $this->user_model->getContentInfo();
             $AccountInfo['contentdata']= $ContentInfo['content_data']; 
        }
        
        //############## Quote settings modifier view end ###################
        //load view
        $this->load->view('settings/index', $AccountInfo);
    }

    //#################################################################
    // Name : updateaccount
    // Purpose : To fetch and display account settings/quote settings data
    // In Params : merchant id
    // Out params : load account setting view
    //#################################################################
    function updateaccount() {

        //pass this to model for update with merchant id
        if (isset($_POST['data']) && !empty($_POST['data'])) {
            //mprd($_POST);
            $UpdateSettings = $this->user_model->UpdateAccountSettings($_POST['data']);
        }
        echo json_encode(array('status'=>'1','message'=>SETTING_SUCCESS_UPDATE));
    }
    //#################################################################
    // Name : savecontent
    // Purpose : To fetch and display account settings/quote settings data
    // In Params : merchant id
    // Out params : load account setting view
    //#################################################################
    function savecontent() {
        $PostData  = $this->input->post();
        
         if (isset($PostData) && count($PostData) > 0) {
             //remove space from post data
            $Params = array_map('trim', $PostData);
            
            $ret = $this->general_model->updateContent($Params);
            
         }
         //$this->load->view('settings/index');
         redirect('settings/index', 'refresh');
        
        
    }
}
