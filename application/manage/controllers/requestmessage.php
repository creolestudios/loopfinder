<?php

/*
 * @category   Categories
 * @package    Add/Update/Delete Categories
 * @author     Bhargav Bhanderi <bhargav@creolestudios.com>
 * @author     Another Author <another@example.com>
 * @copyright  2014 Creole Studios
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    Release: 1.0 
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require APPPATH . '/libraries/php-export-data.class.php';

class Requestmessage extends CI_Controller {

    var $viewData = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('message_model');
        $this->load->library('Datatables');
        $this->load->helper(array('form', 'url'));
        if (!$this->session->userdata('M_ADMINLOGIN'))
            redirect('login');
    }

    //#################################################################
    // Name : index
    // Purpose : To fetch and display account settings/quote settings data
    // In Params : merchant id
    // Out params : load account setting view
    //#################################################################
    public function index() {

        //get count data for tab
        $GetCount = $this->message_model->GetCount();

        //load view
        $this->load->view('requestmessages/list_view', $GetCount);
    }

    //#################################################################
    // Name : ExportRequests
    // Purpose : To export request data
    // In Params : void
    // Out params : export request data to csv
    //#################################################################
    public function ExportRequests() {
        // 'browser' tells the library to stream the data directly to the browser.
        // other options are 'file' or 'string'
        // 'test.xls' is the filename that the browser will use when attempting to 
        // save the download        
        //get users data
        $GetRequests = $this->message_model->GetAllRequest();
        
        //if user data found
        if (isset($GetRequests) && $GetRequests['status'] == '1') {
            //call export csv data
            $exporter = new ExportDataCSV('browser', 'Requests_' . date("d_M_Y") . '.csv');

            $exporter->initialize(); // starts streaming data to web browser
            // pass addRow() an array and it converts it to Excel XML format and sends 
            // it to the browser
            //add header row for titles
            $exporter->addRow(array("User Name", "Email", "Request Type","Venue Name","City","State", "Venue Provide Headset","Request Date","Comment"));

            //looped through array
            foreach ($GetRequests['request_data'] as $RequestKey => $RequestVal) {
                $exporter->addRow(array($RequestVal['customer_name'],$RequestVal['email'],$RequestVal['request_type'],$RequestVal['venue_name'],$RequestVal['city'],$RequestVal['state'],$RequestVal['venue_provide_headset'],$RequestVal['request_date'],$RequestVal['comment']));
            }

            $exporter->finalize(); // writes the footer, flushes remaining data to browser.
        }
        exit(); // all done
    }

    //#################################################################
    // Name : details
    // Purpose : To show user's details
    // In Params : user id
    // Out params : load user details view
    //#################################################################    

    public function Details() {

        //initialize
        $ViewData = array();

        //get category id to fetch data
        $MessageId = base64_decode($this->uri->segment(3));

        //get action from url
        $Action = $this->uri->segment(5);

        //if action is archive then dont update message status
        if (isset($Action) && $Action != 'archive') {
            //update message status to read when details oepn
            $Status = "1";
            $UpdateMessage = $this->message_model->UpdateMessageStatus($MessageId, $Status);
        }

        //get category details based on category id
        $MessageDetails = $this->message_model->GetMessageDetails($MessageId);

        if (isset($MessageDetails) && $MessageDetails['status'] == '1') {
            $ViewData['request'] = $MessageDetails['request_data'];
        }

        //Load udpate category view
        $this->load->view('requestmessages/details_view', $ViewData);
    }

//###########################################################
    //function : DeleteUser
    //To delete User
    //Input : User id
    //Output : message
    //###########################################################
    public function DeleteRequest() {

        $PostData = $this->input->post();

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from post data
            $Params = array_map('trim', $PostData['data']);

            //process customer data
            extract($Params);

            //call function to change status
            $DeleteRequest = $this->message_model->DeleteRequest($request_id);

            //print response
            echo json_encode($DeleteRequest);
        }
        exit;
    }

    //###########################################################
    //function : AddArchive
    //To archive message
    //Input : message id
    //Output : message
    //###########################################################
    public function AddArchive() {
        $PostData = $this->input->post();

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from post data
            $Params = array_map('trim', $PostData['data']);

            //process customer data
            extract($Params);

            //call function to change status
            $UpdateMessage = $this->message_model->UpdateMessageStatus($request_id, $status);

            //print response
            echo json_encode($UpdateMessage);
        }
        exit;
    }

    //#################################################################
    // Name : UnreadMessages
    // Purpose : To get all unread messages
    // In Params : void
    // Out params : load all unread message
    //#################################################################    

    public function UnreadMessages() {
        //get data from input params
        $GetData = $this->input->post();

        //process data
        if (isset($GetData) && !empty($GetData)) {
            //remove space from params
            $Params = array_map('trim', $GetData);

            //query to select checkin data

            $this->datatables->select("r.venue_name as venue_name,DATE_FORMAT(r.request_date,'%d %b %Y %h:%i %p') as request_date,r.city as venue_city,r.state as venue_state,r.id as detail", false);
            $this->datatables->from("request as r ");
            $this->datatables->where("r.status", '0');

            ## Email condition
            if (isset($_POST['email']) && $_POST['email'] != '') {
                $this->datatables->like('c.email', trim($_POST['email']), '%');
            }

            ## name condition
            if (isset($_POST['name']) && $_POST['name'] != '') {
                $this->datatables->like('r.venue_name', trim($_POST['name']), '%');
            }

            ## city condition
            if (isset($_POST['city']) && $_POST['city'] != '') {
                $this->datatables->like('r.city', trim($_POST['city']), '%');
            }

            ## state condition
            if (isset($_POST['state']) && $_POST['state'] != '') {
                $this->datatables->like('r.state', trim($_POST['state']), '%');
            }

            ## date condition
            if ($_POST['from_date'] != '' && $_POST['to_date'] != '') {

                //convert date to mysql format
                $DateTemp = explode('/', $_POST['from_date']);
                $DateEndTemp = explode('/', $_POST['to_date']);

                ## Condition for the date comparisn
                $RequestStartDate = date('Y-m-d', strtotime($DateTemp[2] . '-' . $DateTemp[0] . '-' . $DateTemp[1]));
                $RequestEndDate = date('Y-m-d', strtotime($DateEndTemp[2] . '-' . $DateEndTemp[0] . '-' . $DateEndTemp[1]));

                $this->datatables->where("DATE(r.request_date) >=  '" . $RequestStartDate . "'");
                $this->datatables->where("DATE(r.request_date) <=  '" . $RequestEndDate . "'");

                //$this->datatables->where("DATE(r.request_date) <= ");
            }
            if ($_POST['from_date'] != '' && $_POST['to_date'] == '') {
                //convert date to mysql format
                $DateTemp = explode('/', $_POST['from_date']);

                ## Condition for the date comparisn
                $RequestStartDate = date('Y-m-d', strtotime($DateTemp[2] . '-' . $DateTemp[0] . '-' . $DateTemp[1]));

                $this->datatables->where("DATE(r.request_date) >=  '" . $RequestStartDate . "'");
            }
            if ($_POST['from_date'] == '' && $_POST['to_date'] != '') {
                //convert date to mysql format
                $DateEndTemp = explode('/', $_POST['to_date']);

                ## Condition for the date comparisn
                $RequestEndDate = date('Y-m-d', strtotime($DateEndTemp[2] . '-' . $DateEndTemp[0] . '-' . $DateEndTemp[1]));

                $this->datatables->where("DATE(r.request_date) <=  '" . $RequestEndDate . "'");
            }
            echo $this->datatables->generate();
        }
    }

    //#################################################################
    // Name : ArchiveMessages
    // Purpose : To all the messages
    // In Params : void
    // Out params : load all the messages
    //#################################################################
    public function ArchiveMessages() {
        //get data from input params
        $GetData = $this->input->post();

        //process data
        if (isset($GetData) && !empty($GetData)) {
            //remove space from params
            $Params = array_map('trim', $GetData);

            //query to select checkin data

            $this->datatables->select("r.venue_name as venue_name,DATE_FORMAT(r.request_date,'%d %b %Y %h:%i %p') as request_date,r.city as venue_city,r.state as venue_state,r.id as detail", false);
            $this->datatables->from("request as r ");
            $this->datatables->where("r.status", '2');

            ## Email condition
            if (isset($_POST['email']) && $_POST['email'] != '') {
                $this->datatables->like('c.email', trim($_POST['email']), '%');
            }

            ## name condition
            if (isset($_POST['name']) && $_POST['name'] != '') {
                $this->datatables->like('r.venue_name', trim($_POST['name']), '%');
            }

            ## city condition
            if (isset($_POST['city']) && $_POST['city'] != '') {
                $this->datatables->like('r.city', trim($_POST['city']), '%');
            }

            ## state condition
            if (isset($_POST['state']) && $_POST['state'] != '') {
                $this->datatables->like('r.state', trim($_POST['state']), '%');
            }

            ## date condition
            if ($_POST['from_arch_date'] != '' && $_POST['to_arch_date'] != '') {

                //convert date to mysql format
                $DateTemp = explode('/', $_POST['from_arch_date']);
                $DateEndTemp = explode('/', $_POST['to_arch_date']);

                ## Condition for the date comparisn
                $RequestStartDate = date('Y-m-d', strtotime($DateTemp[2] . '-' . $DateTemp[0] . '-' . $DateTemp[1]));
                $RequestEndDate = date('Y-m-d', strtotime($DateEndTemp[2] . '-' . $DateEndTemp[0] . '-' . $DateEndTemp[1]));

                $this->datatables->where("DATE(r.request_date) >=  '" . $RequestStartDate . "'");
                $this->datatables->where("DATE(r.request_date) <=  '" . $RequestEndDate . "'");

                //$this->datatables->where("DATE(r.request_date) <= ");
            }
            if ($_POST['from_arch_date'] != '' && $_POST['to_arch_date'] == '') {
                //convert date to mysql format
                $DateTemp = explode('/', $_POST['from_arch_date']);

                ## Condition for the date comparisn
                $RequestStartDate = date('Y-m-d', strtotime($DateTemp[2] . '-' . $DateTemp[0] . '-' . $DateTemp[1]));

                $this->datatables->where("DATE(r.request_date) >=  '" . $RequestStartDate . "'");
            }
            if ($_POST['from_arch_date'] == '' && $_POST['to_arch_date'] != '') {
                //convert date to mysql format
                $DateEndTemp = explode('/', $_POST['to_arch_date']);

                ## Condition for the date comparisn
                $RequestEndDate = date('Y-m-d', strtotime($DateEndTemp[2] . '-' . $DateEndTemp[0] . '-' . $DateEndTemp[1]));

                $this->datatables->where("DATE(r.request_date) <=  '" . $RequestEndDate . "'");
            }
            echo $this->datatables->generate();
        }
    }

    //#################################################################
    // Name : AllMessages
    // Purpose : To all the messages
    // In Params : void
    // Out params : load all the messages
    //#################################################################    

    public function AllMessages() {
        //get data from input params
        $GetData = $this->input->post();

        //process data
        if (isset($GetData) && !empty($GetData)) {
            //remove space from params
            $Params = array_map('trim', $GetData);

            //query to select checkin data

            $this->datatables->select("r.venue_name  as venue_name,DATE_FORMAT(r.request_date,'%d %b %Y %h:%i %p') as request_date,r.city as venue_city,r.state as venue_state,CONCAT(r.id,'_',r.status) as detail", false);
            $this->datatables->from("request as r ");
            $this->datatables->where("r.status != '2'");

            ## Email condition
            if (isset($_POST['email']) && $_POST['email'] != '') {
                $this->datatables->like('c.email', trim($_POST['email']), '%');
            }

            ## name condition
            if (isset($_POST['name']) && $_POST['name'] != '') {
                $this->datatables->like('r.venue_name', trim($_POST['name']), '%');
            }

            ## city condition
            if (isset($_POST['city']) && $_POST['city'] != '') {
                $this->datatables->like('r.city', trim($_POST['city']), '%');
            }
            ## state condition
            if (isset($_POST['state']) && $_POST ['state'] != '') {
                $this->datatables->like('r.state', trim($_POST['state']), '%');
            }

            ## date condition
            if ($_POST['from_all_date'] != '' && $_POST['to_all_date'] != '') {

                //convert date to mysql format
                $DateTemp = explode('/', $_POST['from_all_date']);
                $DateEndTemp = explode('/', $_POST['to_all_date']);

                ## Condition for the date comparisn
                $RequestStartDate = date('Y-m-d', strtotime($DateTemp[2] . '-' . $DateTemp[0] . '-' . $DateTemp[1]));
                $RequestEndDate = date('Y-m-d', strtotime($DateEndTemp[2] . '-' . $DateEndTemp[0] . '-' . $DateEndTemp[1]));

                $this->datatables->where("DATE(r.request_date) >=  '" . $RequestStartDate . "'");
                $this->datatables->where("DATE(r.request_date) <=  '" . $RequestEndDate . "'");

                //$this->datatables->where("DATE(r.request_date) <= ");
            }
            if ($_POST['from_all_date'] != '' && $_POST['to_all_date'] == '') {
                //convert date to mysql format
                $DateTemp = explode('/', $_POST['from_all_date']);

                ## Condition for the date comparisn
                $RequestStartDate = date('Y-m-d', strtotime($DateTemp[2] . '-' . $DateTemp[0] . '-' . $DateTemp[1]));

                $this->datatables->where("DATE(r.request_date) >=  '" . $RequestStartDate . "'");
            }
            if ($_POST['from_all_date'] == '' && $_POST['to_all_date'] != '') {
                //convert date to mysql format
                $DateEndTemp = explode('/', $_POST['to_all_date']);

                ## Condition for the date comparisn
                $RequestEndDate = date('Y-m-d', strtotime($DateEndTemp[2] . '-' . $DateEndTemp[0] . '-' . $DateEndTemp[1]));

                $this->datatables->where("DATE(r.request_date) <=  '" . $RequestEndDate . "'");
            }

            echo $this->datatables->generate();
        }
    }

}
