<?php

/*
 * @category   Hlaa Chapters
 * @package    Add/Update/Delete Categories
 * @author     Jignesh Virani  <jignesh@creolestudios.com>
 * @author     Another Author <another@example.com>
 * @copyright  2014 Creole Studios
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    Release: 1.0 
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require APPPATH . '/libraries/php-export-data.class.php';

class Hlaachapters extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('hlaachapters_model');
        $this->load->library('Datatables');
        $this->load->helper(array('form', 'url'));
        if (!$this->session->userdata('M_ADMINLOGIN'))
            redirect('login');
    }

    //#################################################################
    // Name : index
    // Purpose : To fetch and display account settings/quote settings data
    // In Params : merchant id
    // Out params : load account setting view
    //#################################################################
    public function index() {
        //get merchant id from session
        //load view
        $CreateChapter = $this->hlaachapters_model->CountChapter();
        $CountData['count'] = $CreateChapter['count'];

        $this->load->view('hlaachapters/list_view', $CountData);
    }

    //#################################################################
    // Name : GetHlaaChapters
    // Purpose : To show Hlaa chapters list
    // In Params : void
    // Out params : load Hlaa chapters data
    //#################################################################   
    public function GetHlaaChapters() {

        $GetData = $this->input->post();

        if (isset($GetData) && !empty($GetData)) {

            $this->datatables->select("h.name,if(h.zipcode != '',LPAD(h.zipcode, 5, '0'),h.zipcode)as zipcode, h.city, DATE_FORMAT(h.meetupdate,' %d %b %Y %h:%i %p')as meetupdate, h.state, h.id as detail", false)->from("venues as h")->where("h.venue_type","3");
            //$this->datatables->from("hlaa_chapters as h");
            ## Venue name
            if (isset($_POST['chapter_name']) && $_POST['chapter_name'] != '') {
                $this->datatables->like('h.name', trim($_POST['chapter_name']), '%');
            }

            ## city condition
            if (isset($_POST['city']) && $_POST['city'] != '') {
                $this->datatables->like('h.city', trim($_POST['city']), '%');
            }

            ## state condition
            if (isset($_POST['state']) && $_POST['state'] != '') {
                $this->datatables->like('h.state', trim($_POST['state']), '%');
            }
            ## zipcode condition
            if (isset($_POST['zipcode']) && $_POST['zipcode'] != '') {
                $this->datatables->where('h.zipcode like "' . trim($_POST['zipcode']) . '%"');
            }

            echo $this->datatables->generate();
        }
    }

    //#################################################################
    // Name : Create
    // Purpose : To show Hlaa chapters list
    // In Params : void
    // Out params : load Hlaa chapters data
    //#################################################################
    public function Create() {
        $this->load->view('hlaachapters/create_view');
    }

    //#################################################################
    // Name : Create
    // Purpose : To show Hlaa chapters list
    // In Params : void
    // Out params : load Hlaa chapters data
    //#################################################################

    public function Savehlaachapters() {
        $Params = $this->input->post();
        if (isset($Params) && count($Params) > 0) {
            
            //create chapters first, and use its id for category,images
            $CreateChapter = $this->hlaachapters_model->CreateChapter($Params);
            
            //if hlaa chapters is created succesfully then insert images
            if (isset($CreateChapter) && $CreateChapter['status'] == '1') {

                //get create chapter id 
                $CreatedChapterId = $CreateChapter['chapter_id'];
                
                //########### Add categories for venue start ######################
                if (isset($Params['hdcategory']) && $Params['hdcategory'] != '') {
                    //call function to insert category
                    
                    $CreateVenueCategory = $this->hlaachapters_model->CreateChaptersCategory($CreatedChapterId);  
                }

                //########### Add images for venue start ######################
                //define folder path
                $TargetFolder = UPLOADS . 'venues/' . $CreatedChapterId;
                $TargetFolderThumb = UPLOADS . 'venues/' . $CreatedChapterId . '/thumb/';
                $TempImagePath = UPLOADS . 'temp/';
                $TempImagePathThumb = UPLOADS . 'temp/thumb/';

                //process to move files
                //create directory if not exist
                if (is_dir($TargetFolder) === false) {
                    mkdir($TargetFolder);
                    mkdir($TargetFolderThumb);
                }
                if ($Params['hdimages'] != '') {
                    $ImageData = explode(",", $_POST['hdimages']);
                    //mprd($ImageData);
                    //set cover image
                    $CoverImage = $ImageData[0];

                    if (isset($ImageData) && count($ImageData) > 0) {
                        //move file to album name
                        foreach ($ImageData as $ImageVal) {

                            //move iamge from temp to album folder
                            if (file_exists($TempImagePath . $ImageVal)) {

                                $this->load->library('image_lib');
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $TempImagePath . $ImageVal;
                                $config['create_thumb'] = FALSE;
                                $config['new_image'] = $TargetFolder . '/' . $ImageVal;
                                $config['maintain_ratio'] = FALSE;
                                $config['master_dim'] = 'width';
                                $config['quality'] = 60;
                                $config['width'] = 2560;
                                $config['height'] = 1440;
                                $imageSize = $this->image_lib->get_image_properties($config['source_image'], TRUE);
                                $config['x_axis'] = (($imageSize['width'] / 2) - ($config['width'] / 2));
                                $config['y_axis'] = (($imageSize['height'] / 2) - ($config['height'] / 2));

                                //load image library
                                $this->image_lib->initialize($config);
                                $this->image_lib->crop();
                                $this->image_lib->clear();

                                $config_thumb['image_library'] = 'gd2';
                                $config_thumb['source_image'] = $TargetFolder . '/' . $ImageVal;
                                $config_thumb['create_thumb'] = TRUE;
                                $config_thumb['new_image'] = $TargetFolderThumb . $ImageVal;
                                $config_thumb['maintain_ratio'] = TRUE;
                                $config_thumb['thumb_marker'] = "";
                                $config_thumb['quality'] = 80;
                                $config_thumb['width'] = 800;
                                $config_thumb['height'] = 600;
                                $this->image_lib->initialize($config_thumb);
                                $this->image_lib->resize();
                                $this->image_lib->clear();
                            }
                            @unlink($TempImagePath . $ImageVal);

                            $ImageArray[] = "('" . $CreatedChapterId . "','" . $ImageVal . "')";
                        }

                        if (isset($ImageArray) && count($ImageArray) > 0) {
                            $ImageString = implode(",", $ImageArray);
                            //save image to db
                            $SaveImages = $this->hlaachapters_model->SaveChaptersImages($ImageString);
                        }
                    }

                    //udpate cover image for album
                    $UpdateCover = $this->hlaachapters_model->UpdateCoverImage($CreatedChapterId, $CoverImage);
                }
                //########### Add images for venue end ######################
                //set data in sesion to send push notification when listing page load                
                if (isset($CreatedChapterId) && $CreatedChapterId != '') {
                    // $this->session->set_userdata(array('venue_name' => $Params['chapter_name'], 'venue_type' => $Params['hdvenuetype'], 'venue_id' => $CreatedChapterId, 'latitude' => $Params['hdlatitude'], 'longitude' => $Params['hdlongitude'], 'action' => 'notification'));
                }
                $this->session->set_flashdata('success', $this->lang->line('SUCCESS_CHAPTERS_CREATE'),'hlaa chapters');
                redirect('hlaachapters/');
            } else {
                $this->session->set_flashdata('error', $this->lang->line('GENERAL_ERROR'),'hlaa chapters');
                redirect('hlaachapters/Create');
            }
        }
    }

    //#################################################################
    // Name : details
    // Purpose : To show venue details
    // In Params : venue id
    // Out params : load venue view as per register OR requested
    //#################################################################    

    public function Details() {

        //get venue id from URL
        $ChapterId = base64_decode($this->uri->segment(3));
        //global declaration 
        $ChapterData = array();
        //get venue details
        $ChapterDetails = $this->hlaachapters_model->GetChaptersDetails($ChapterId);

        $ChapterData['chapter_details'] = $ChapterDetails['chapter_data'];
        $ChapterData['chapter_images'] = $ChapterDetails['chapter_images'];

        if (isset($ChapterData) && !empty($ChapterData)) {

            $this->load->view('hlaachapters/details_view', $ChapterData);
        } else {
            $this->load->view('hlaachapters/list_view');
        }
    }

    //###########################################################
    //function : DeleteChapter
    //To delete Chapter
    //Input : Chapter id
    //Output : message
    //###########################################################
    public function DeleteChapter() {

        $PostData = $this->input->post();

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from post data
            $Params = array_map('trim', $PostData['data']);

            //process customer data
            extract($Params);

            //call function to change status
            $DeleteChapter = $this->hlaachapters_model->DeleteChapter($chapter_id);

            //delete venue folder from upload
            //set target folder for venue
            $ChapterImagePath = UPLOADS . 'venues/' . $chapter_id . '/';

            //call function to remove directory and sub directory
            $temp = $this->RemoveChapterDir($ChapterImagePath);
            //print response
            echo json_encode($DeleteChapter);
        }
        exit;
    }

    //#################################################################
    // Name : EditHlaaChapters
    // Purpose : To edit chapters
    // In Params : chapters id
    // Out params : Load data for chapter edit.
    //#################################################################
    public function EditHlaaChapters() {

        //get venue id from URL
        $ChapterId = base64_decode($this->uri->segment(3));
        //global declaration
        $CahpterData = array();

        $ChapterDetails = $this->hlaachapters_model->GetChaptersDetails($ChapterId);

        $ChapterData['chapter_details'] = $ChapterDetails['chapter_data'];
        $ChapterData['chapter_images'] = $ChapterDetails['chapter_images'];

        if (isset($ChapterData) && !empty($ChapterData)) {

            $this->load->view('hlaachapters/edit_view', $ChapterData);
        } else {
            $this->load->view('hlaachapters/list_view');
        }
    }

    //###########################################################
    //function : MasterUpload
    //To upload album images
    //Input : image
    //Output : void
    //###########################################################
    public function UpdateChapter() {

        //get post data
        $Params = $this->input->post();

        if (isset($Params) && count($Params) > 0) {

            ##update chapter data.

            $CreateChapter = $this->hlaachapters_model->UpdateChapter($Params);

            //if venue is created succesfully then insert category,images,looped rooms
            if (isset($CreateChapter) && $CreateChapter['status'] == '1') {

                //get create venue id 
                $UpdateChapterId = $Params['chapter_id'];
                //########### Add loop rooms for venue end ######################
                //########### Add images for venue start ######################
                //define folder path
                $TargetFolder = UPLOADS . 'venues/' . $UpdateChapterId;
                $TargetFolderThumb = UPLOADS . 'venues/' . $UpdateChapterId . '/thumb/';
                $TempImagePath = UPLOADS . 'temp/';
                $TempImagePathThumb = UPLOADS . 'temp/thumb/';

                //process to move files
                //create directory if not exist
                if (is_dir($TargetFolder) === false) {
                    mkdir($TargetFolder);
                    mkdir($TargetFolderThumb);
                }

                if ($Params['hdimages'] != '') {
                    $ImageData = explode(",", $_POST['hdimages']);
                    //set cover image
                    $CoverImage = $ImageData[0];

                    if (isset($ImageData) && count($ImageData) > 0) {
                        //move file to album name
                        foreach ($ImageData as $ImageVal) {

                            //move iamge from temp to album folder
                            if (file_exists($TempImagePath . $ImageVal)) {
                                //rename($TempImagePath . $ImageVal, $TargetFolder . '/' . $ImageVal);

                                $this->load->library('image_lib');
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $TempImagePath . $ImageVal;
                                $config['create_thumb'] = FALSE;
                                $config['new_image'] = $TargetFolder . '/' . $ImageVal;
                                $config['maintain_ratio'] = FALSE;
                                $config['master_dim'] = 'width';
                                $config['quality'] = 60;
                                $config['width'] = 2560;
                                $config['height'] = 1440;
                                $imageSize = $this->image_lib->get_image_properties($config['source_image'], TRUE);
                                $config['x_axis'] = (($imageSize['width'] / 2) - ($config['width'] / 2));
                                $config['y_axis'] = (($imageSize['height'] / 2) - ($config['height'] / 2));

                                //load image library
                                $this->image_lib->initialize($config);
                                $this->image_lib->crop();
                                $this->image_lib->clear();

                                $config_thumb['image_library'] = 'gd2';
                                $config_thumb['source_image'] = $TargetFolder . '/' . $ImageVal;
                                $config_thumb['create_thumb'] = TRUE;
                                $config_thumb['new_image'] = $TargetFolderThumb . $ImageVal;
                                $config_thumb['maintain_ratio'] = TRUE;
                                $config_thumb['thumb_marker'] = "";
                                $config_thumb['quality'] = 80;
                                $config_thumb['width'] = 800;
                                $config_thumb['height'] = 600;
                                $this->image_lib->initialize($config_thumb);
                                $this->image_lib->resize();
                                $this->image_lib->clear();
                            }
                            @unlink($TempImagePath . $ImageVal);

//                            if (file_exists($TempImagePathThumb . $ImageVal)) {
//                                rename($TempImagePathThumb . $ImageVal, $TargetFolderThumb . '/' . $ImageVal);
//                            }

                            $ImageArray[] = "('" . $UpdateChapterId . "','" . $ImageVal . "')";
                        }

                        if (isset($ImageArray) && count($ImageArray) > 0) {
                            $ImageString = implode(",", $ImageArray);
                            //save image to db
                            $SaveImages = $this->hlaachapters_model->SaveChaptersImages($ImageString);
                        }
                    }
                    ## if cover image is not blank then update
                    if (isset($CoverImage) && $CoverImage != '') {
                        //udpate cover image for album
                        $UpdateCover = $this->hlaachapters_model->UpdateCoverImage($UpdateChapterId, $CoverImage);
                    }
                }
                //########### Add images for venue end ######################
                $this->session->set_flashdata('success', $this->lang->line('SUCCESS_CHAPTER_UPDATE'));
                redirect('hlaachapters/');
            } else {
                $this->session->set_flashdata('error', $this->lang->line('GENERAL_ERROR'));
                redirect('hlaachapters/Create');
            }
        }
    }

    //#################################################################
    // Name : SetDefaultImage
    // Purpose : To set default image for venue
    // In Params : venue id,image
    // Out params : success message
    //#################################################################
    public function SetDefaultImage() {
        $PostData = $this->input->post();

        if (isset($PostData) && count($PostData['data']) > 0) {

            //remove space from post data
            $Params = array_map('trim', $PostData['data']);
            //extract params
            extract($Params);
            $VerifyResult = $this->hlaachapters_model->SetVenueDefaultImage($chapter_id, $image);
            echo json_encode($VerifyResult);
            exit;
        }
    }

    //###########################################################
    //function : DeleteChapterImage
    //To delete chapter image
    //Input : image id
    //Output : message
    //###########################################################
    public function DeleteChapterImage() {
        $PostData = $this->input->post();

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from post data
            $Params = array_map('trim', $PostData['data']);

            //process customer data
            extract($Params);

            //call function to change status
            $DeleteImage = $this->hlaachapters_model->DeleteChapterImage($image_id);

            //unlink image from folder
            //define image paths

            $TargetPath = UPLOADS . 'venues/' . $chapter_id . '/' . $image;
            $TargetPathThumb = UPLOADS . 'venues/' . $chapter_id . '/thumb/' . $image;

            //remove space from post data
            $Params = array_map('trim', $PostData);

            //unlink image from temp folder
            if (file_exists($TargetPath)) {
                unlink($TargetPath);
            }
            if (file_exists($TargetPathThumb)) {
                unlink($TargetPathThumb);
            }

            //print response
            echo json_encode($DeleteImage);
        }
        exit;
    }
    //###########################################################
    //function : isNameAvail
    //To  check avaibility of chapter name.
    //Input : chapter name
    //Output : message
    //###########################################################
    
    public function isNameAvail(){
        
         $PostData = $this->input->post();

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from post data
            $Params = array_map('trim', $PostData);

            $ret = $this->hlaachapters_model->CheckChapterExist($Params);
            echo json_encode($ret);
            exit;
        }
        
    }

    //###########################################################
    //function : RemoveVenueDir
    //To delete venue images from upload
    //Input : venue file path
    //Output : message
    //###########################################################
    public function RemoveChapterDir($dirname) {
        // recursive function to delete 
        // all subdirectories and contents: 

        if (is_dir($dirname))
            $dir_handle = opendir($dirname);
        while ($file = readdir($dir_handle)) {
            if ($file != "." && $file != "..") {
                if (!is_dir($dirname . "/" . $file)) {
                    unlink($dirname . "/" . $file);
                } else {
                    $this->RemoveChapterDir($dirname . "/" . $file);
                }
            }
        }
        closedir($dir_handle);
        rmdir($dirname);
        return true;
    }

}
