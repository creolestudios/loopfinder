<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ajax extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');
    }

    public function index() {
        
    }

    /**
     * [forgotpassword ADMIN]
     * @return [type] [description]
     */
    function forgotpassword() {
        if (isset($_POST) && count($_POST) > 0) {
            $res = $this->user_model->check_merchant_email_valid();
            extract($res);
            // mprd($res);
            if ($status == 1) {
                ##############################################################################################
                //SEND MAIL
                ##############################################################################################
                /* echo $email;
                  echo "<br>";
                  echo $subject;
                  echo "<br>";
                  echo $message;
                  echo "<br>";
                  echo $html;exit; */


                $this->load->library('email');
                $config['charset'] = 'iso-8859-1';
                $config['wordwrap'] = TRUE;
                $config['mailtype'] = "html";
                $this->email->initialize($config);
                $this->email->set_newline("\r\n");
                $this->email->from(ADMIN_EMAIL);
                $this->email->to($email);
                $this->email->subject($subject);
                $this->email->message($html);

                if ($this->email->send()) {
                    echo json_encode($message);
                } else {
                    $err = '<span>' . MAIL_SENT_FAIL . '</span>';
                    echo json_encode($err);
                }
            } else {
                echo json_encode($message);
                exit;
            }
        } else {
            $this->load->view('login/login_view');
        }
    }

    /**
     * [checkOld_passWord ADMIN]
     * @return [type] [description]
     */
    function checkOld_passWord() {
        $vOldPassword = base64_decode($this->input->post('data'));
        $checkPassword = $this->user_model->checkOldPassword($vOldPassword);
        extract($checkPassword);
        echo json_encode($message);
    }

    /**
     * [changeAdmin_passWord ADMIN]
     * @return [type] [description]
     */
    function changeAdmin_passWord() {
        $updatePassword = $this->user_model->changePassword();
        extract($updatePassword);
        echo json_encode($message);
        exit;
    }

    function changeAdminPass() {
        $updatePassword = $this->user_model->resetPass();
        extract($updatePassword);
        echo json_encode($status);
        exit;
    }

    /**
     * [settings ADMIN]
     * @return [type] [description]
     */
    function settings() {
        $editData = $this->user_model->edit_admin_setting();
        extract($editData);
        echo json_encode($message);
        exit;
    }

    /*
      | -------------------------------------------------------------------
      |  DLETE IMAGES AND DIRECTORY
      | -------------------------------------------------------------------
     */

    function deleteAll($dir) {
        foreach (scandir($dir) as $file) {
            if ('.' === $file || '..' === $file)
                continue;
            if (is_dir("$dir/$file")) {
                $this->deleteAll("$dir/$file");
            } else {
                unlink("$dir/$file");
            }
        }
        rmdir($dir);
        return true;
    }

}

/* End of file ajax.php */
/* Location: ./application/admin/controllers/ajax.php */