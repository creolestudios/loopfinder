<?php

class Forgot_pass extends CI_Controller {

    var $viewData = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');
    }

    function index($check = '') {
        //$this->lang->switch_to('en');
        $data = array(
            'title' => 'Forgot Password'
        );

        if (count($_POST) > 0) {
            $resU = $this->user_model->check_admin_email_valid();
        } else {
            $this->load->model('general_model');
            if (isset($check) && $check == '1') {
                $succ = array('0' => PASSWORD_SENT);
                $this->session->set_userdata('SUCCESS', $succ);
            }
            $this->load->view('login/login_view', $data);
        }
    }

    /*
      | -------------------------------------------------------------------
      |  ACTIVATE EMAIL ACCOUNT
      | -------------------------------------------------------------------
     */

    function activate() {
        $id = $this->uri->segment(3);
        $token = $this->uri->segment(4);
        if ($id != "" && $token != '') {
            // $varify_link=true;
            $varify_link = $this->user_model->getadminToken(base64_decode($id), $token);
            /**
             * [$resetTokne Rest token of admin, this link is not reusable]
             * @var [type]
             */
            $resetToken = $this->user_model->updateTokenAdmin(base64_decode($id), "");
            // CHECK IF URL IS NOT WRONG
            if ($varify_link) {
                $data = array(
                    'id' => $id,
                    'title' => 'Reset Password',
                    'message' => 'Please Enter New password',
                    'flg' => "MFqGaJngen76IyoY8rXoCf6" . $id . "SEklL4eHRx9i"
                );
            } else {
                $data = array(
                    'id' => $id,
                    'title' => 'Reset Password',
                    'message' => 'Link has been expired please try again!',
                    'flg' => "HemFUAxOKnxSEklL4eHRx9i"
                );
            }
            $this->load->view('password/reset_pass_view', $data);
        } else {
            // Not valid Link
            $this->load->view('messages/wanna_404');
        }
    }

    /*
      | -------------------------------------------------------------------
      |  RESET PASSWORD
      | -------------------------------------------------------------------
     */

    function reset_pass() {
        if (count($_POST) > 0) {
            $this->user_model->reset_admin_pass($this->input->post('admin_pwd'));
        }
    }

    /*
      | -------------------------------------------------------------------
      |  END OF CLASS FILE
      | -------------------------------------------------------------------
     */
}
