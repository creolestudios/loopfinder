<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Venue_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->customer = 'customers';
        $this->category = 'categories';
        $this->venues = 'venues';
        $this->venue_images = 'venue_images';
        $this->venue_categories = 'venue_categories';
        $this->venue_rooms = 'venue_rooms';
        $this->venue_checkins = 'checkins';
        $this->venue_votes = 'votes';
        $this->venue_request = 'request';
        $this->venue_feedback = 'feedback';
    }

    //#################################################################
    // Name : CheckinUser
    // Purpose : To checkin for place
    // In Params : customer id,venue id,checkin date
    // Out params : success/error response
    //#################################################################
    public function CheckinUser($Params) {

        //global initilization
        $ReturnData = array();

        //extract params
        extract($Params);

        //prepare array for insert
        $InsertArray = array(
            'venue_id' => $venue_id,
            'customer_id' => $customer_id,
            'checkin_date' => $checkin_date
        );

        //add checkin for venue
        $AddVenueCheckin = $this->db->insert($this->venue_checkins, $InsertArray);

        //check it insertion in success or not
        if ($this->db->affected_rows() > 0) {
            $ReturnData['status'] = '1';
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : CheckVoteExist
    // Purpose : To check is user already vote or not
    // In Params : customer id,venue id
    // Out params : success/error response
    //#################################################################
    public function CheckVoteExist($Params) {

        //global declaration
        $ReturnData = array();

        //extract params
        extract($Params);

        //check for email
        $this->db->select("count(vo.id) as vote_by_user", false);
        $this->db->from("$this->venue_votes as vo");
        $this->db->where("vo.venue_id", $venue_id);
        $this->db->where("vo.customer_id", $customer_id);

        //get data of query
        $GetVotesData = $this->db->get();

        //if email found then return 1 else return 0
        if ($GetVotesData->num_rows() > 0) {

            //fetch data of query
            $ExistVoteDetails = $GetVotesData->result_array();

            if (isset($ExistVoteDetails) && $ExistVoteDetails[0]['vote_by_user'] > 0) {
                //process return data
                $ReturnData['status'] = 1;
            } else {
                //process return data
                $ReturnData['status'] = 0;
            }
        } else {

            $ReturnData['status'] = 0;
        }

        //return data
        return $ReturnData;
    }

    //#################################################################
    // Name : VotesVenue
    // Purpose : To votes for requested venue
    // In Params : customer id,venue id,votes date
    // Out params : success/error response
    //#################################################################
    public function VotesVenue($Params) {

        //global initilization
        $ReturnData = array();

        //extract params
        extract($Params);

        //prepare array for insert
        $InsertArray = array(
            'venue_id' => $venue_id,
            'customer_id' => $customer_id,
            'vote_type' => $vote_type,
            'vote_date' => $vote_date
        );

        //add checkin for venue
        $AddVenueCheckin = $this->db->insert($this->venue_votes, $InsertArray);

        //check it insertion in success or not
        if ($this->db->affected_rows() > 0) {
            $ReturnData['status'] = '1';
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //###########################################################
    //Function : UpdateVote
    //purpose : To update vote up or down
    //input : customer id,venue id
    //outpur : success/error
    //###########################################################

    public function UpdateVote($Params) {

        //global declaration
        $ReturnData = array();

        //extract params
        extract($Params);

        //prepare update array
        $updateData = array(
            "vote_type" => $vote_type
        );

        //query to update
        $UpdateVote = $this->db->update($this->venue_votes, $updateData, array("venue_id " => $venue_id, "customer_id " => $customer_id));

        if ($this->db->affected_rows() > 0) {
            $ReturnData['status'] = '1';
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : RequestNewVenue
    // Purpose : To request new venue
    // In Params : customer id,name,venue type(already loop/need new),state,city,comment
    // Out params : success/error response
    //#################################################################
    public function RequestNewVenue($Params) {

        //global initilization
        $ReturnData = array();

        //extract params
        extract($Params);

        //prepare array for insert
        $InsertArray = array(
            'customer_id' => $customer_id,
            'venue_name' => $venue_name,
            'request_type' => $request_type,
            'venue_provide_headset' => $venue_provide_headset,
            'state' => $state,
            'city' => $city,
            'comment' => $comment,
            'request_date' => $request_date,
            'status' => '0'
        );

        //add checkin for venue
        $AddNewRequest = $this->db->insert($this->venue_request, $InsertArray);

        //check it insertion in success or not
        if ($this->db->affected_rows() > 0) {
            $ReturnData['status'] = '1';
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : RequestNewVenue
    // Purpose : To request new venue
    // In Params : customer id,name,venue type(already loop/need new),state,city,comment
    // Out params : success/error response
    //#################################################################
    public function VenueFeedback($Params) {

        //global initilization
        $ReturnData = array();

        //extract params
        extract($Params);

        //prepare array for insert
        $InsertArray = array(
            'venue_id' => $venue_id,
            'customer_id' => $customer_id,
            'loop_room_id' => $loop_room_id,
            'loop_working' => $loop_working,
            'loop_not_working_reason' => $loop_not_working_reason,
            'comment' => $comment,
            'feedback_date' => $feedback_date
        );

        //add checkin for venue
        $AddNewFeedback = $this->db->insert($this->venue_feedback, $InsertArray);

        //check it insertion in success or not
        if ($this->db->affected_rows() > 0) {

            if (isset($loop_working) && $loop_working == '1') {
                //update the last verified date for looped room
                $timezone = "UTC";
                date_default_timezone_set($timezone);

                $VerifyDateTime = gmdate("Y-m-d h:i:s");

                //prepare update array
                $updateData = array(
                    "last_verified" => $VerifyDateTime
                );

                //query to update
                $UpdateLastVerified = $this->db->update($this->venue_rooms, $updateData, array("id " => $loop_room_id, "venue_id " => $venue_id));
            }

            $ReturnData['status'] = '1';
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : VenueDetail
    // Purpose : To select venue detail
    // In Params : customer id, venue id
    // Out params : venue detail
    //#################################################################    

    public function GetAllVenues($Params) {

        // global declaration.
        $ReturnData = array();
        //extract params.
        extract($Params);

        //set the limit
        $Limit = VENUE_PAGE_LIMIT;

        //set venue image path
        $venueImagePath = UPLOADS_URL . 'venues/';
        $DefaultVenueImage = UPLOADS_URL . 'default_venue.png';

        //define categories path
        $CategoryPinImagePath = UPLOADS_URL . 'category_icons/';
        $DefaultPinImage = UPLOADS_URL . 'map-pin-default.png';
        $CategoriesImage = UPLOADS_URL . 'category_image/';

        // select venue details in venues table
        $this->db->select(" SQL_CALC_FOUND_ROWS v.id as venue_id,CONCAT('" . $CategoriesImage . "',c.image) as category_image, GROUP_CONCAT(distinct c.name SEPARATOR ', ') as category_name,v.name as venue_name,v.address,v.city,v.state,v.zipcode,IF(v.venue_image = '','',CONCAT('" . $venueImagePath . "',v.id,'/', v.venue_image)) as cover_image,v.is_certified,IF(v.certified_date = '0000-00-00', '' , v.certified_date) as certified_date,v.venue_type,v.latitude,v.longitude,TRUNCATE(( 3959 * acos ( cos ( radians('" . $latitude . "') ) * cos( radians( v.latitude ) ) * cos( radians( v.longitude ) - radians('" . $longitude . "') ) + sin ( radians('" . $latitude . "') ) * sin( radians( v.latitude ) ) ) ),2) AS `distance`,(SELECT IF(vote_type IS NULL,'',vote_type) from votes where venue_id = v.id and customer_id = '" . $customer_id . "') as vote_by_user,IF(v.venue_type = '1',c.pin,c.not_looped_pin)category_pin,IF(v.venue_type = '1',(SELECT count(id) AS c from feedback where venue_id = v.id)+(SELECT count(id) AS c from checkins  where venue_id = v.id),(SELECT count(id) from votes where venue_id = v.id AND vote_type = '1')) as popular", false);
        $this->db->from('venues as v');
        $this->db->join("venue_categories as vc", " v.id = vc.venue_id", "left");
        $this->db->join("categories as c", "c.id = vc.category_id", "left");

        //apply filter to the venue list
        ## Categories filter
        if (isset($category_id) && $category_id != '') {
            $this->db->where_in('vc.category_id', $category_id);
        } else {
            $this->db->where('vc.category_id != 9');
        }

        ## Venue name filter
        if (isset($venue_name) && $venue_name != '') {
            //$this->db->like('v.name', $venue_name, 'both');
            $this->db->where("REPLACE(v.name,'''','') like  '%{$venue_name}%'");
            $this->db->or_where("REPLACE(v.name,'/','') like  '%{$venue_name}%'");
            $this->db->or_where("REPLACE(v.name,'-','') like  '%{$venue_name}%'");
        }

        ## Location(city) filter for venue
        if (isset($location) && $location != '') {

            $locationTemp = explode(', ', $location);
            $location = $locationTemp[0];
            //$this->db->like('v.city', $location, 'both');
            $this->db->where("REPLACE(v.city,'''','') like  '{$location}%'");
            $this->db->where("REPLACE(v.city,'/','') like  '{$location}%'");
            $this->db->where("REPLACE(v.city,'-','') like  '{$location}%'");
        }

        ## Venue type filter
        if (isset($venue_type) && $venue_type != '0') {

            $this->db->where('v.venue_type', $venue_type);
        }
        ## pagination settings
        if (isset($offset) && $offset == '-1') {
            //$this->db->where("v.id < $offset");
            $offset = 0;
        }

        ## apply distance filter
        if (isset($distance) && $distance != '') {
            $this->db->having("TRUNCATE(( 3959 * acos ( cos ( radians('" . $latitude . "') ) * cos( radians( v.latitude ) ) * cos( radians( v.longitude ) - radians('" . $longitude . "') ) + sin ( radians('" . $latitude . "') ) * sin( radians( v.latitude ) ) ) ), 2) <= ", $distance);
        }

        $this->db->group_by("v.id");
        if (isset($sort_by) && $sort_by == '1') {
            $this->db->order_by('distance', 'asc');
        } else {
            $this->db->order_by('popular', 'desc');
        }

        $this->db->limit($Limit, $offset);
        $getVenuesList = $this->db->get();

        ## get total row count
        $TotalRows = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;

        if ($getVenuesList->num_rows() > 0) {
            //fetch the data
            $VenuesData = $getVenuesList->result_array();

            //prepare for response
            $ReturnData['status'] = '1';

            //check for total records,if its less then limit then set offset to -1
//            if (isset($VenuesData) && count($VenuesData) < $Limit) {
//                $ReturnData['offset'] = "-1";
//            } else {
//                $OffsetNew = $VenuesData[$Limit - 1]['venue_id'];
//
//                if (isset($OffsetNew) && $OffsetNew == '') {
//                    $OffsetNew = "-1";
//                }
//                $ReturnData['offset'] = $OffsetNew;
//            }
            $ReturnData['total_venues'] = $TotalRows;
            $ReturnData['venues'] = $VenuesData;
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : VenueDetails
    // Purpose : To select venue detail
    // In Params : customer id, venue id
    // Out params : venue detail
    //#################################################################

    public function VenueDetails($Params) {

        //extract params.
        extract($Params);

        // global declaration.
        $ReturnData = array();
        $VenuesImages = array();
        $VenueDetails = array();
        $LoopedRooms = array();

        $venueImagePath = UPLOADS_URL . 'venues/';

        $CategoryPinImagePath = UPLOADS_URL . 'category_icons/';
        $DefaultPinImage = UPLOADS_URL . 'map-pin-default.png';
        $CategoriesImage = UPLOADS_URL . 'category_image/';

        ## Get Venue data
        $this->db->select("v.id as venue_id,v.name as venue_name,GROUP_CONCAT(distinct c.name SEPARATOR ', ') as category_name,v.venue_type,IF(v.venue_image = '','',CONCAT('" . $venueImagePath . "','" . $venue_id . "','/', v.venue_image)) as venue_image,v.address,v.city,v.state,v.zipcode,v.latitude,v.longitude,v.phone_number,v.email,v.phone_number,v.website,v.is_certified,IF(v.certified_date = '0000-00-00' ,'',v.certified_date)as certified_date,v.public_comment as additional_information,(SELECT IF(vote_type IS NULL or vote_type = '','',vote_type) from votes where venue_id = '" . $venue_id . "' and customer_id = '" . $customer_id . "') as vote_by_user,IF(v.venue_type = '1',c.pin,c.not_looped_pin)category_pin,CONCAT('" . $CategoriesImage . "',c.image) as category_image", false);
        $this->db->from('venues v');
        $this->db->join("venue_categories vc", " v.id = vc.venue_id and vc.venue_id = '" . $venue_id . "'", "left");
        $this->db->join("categories  c", "c.id = vc.category_id", "left");
        $this->db->where("v.id", $venue_id);
        $this->db->group_by("v.id");
        $getVenuesDetails = $this->db->get();


        ## venue details is found then fetch looped rooms and venue images
        if ($getVenuesDetails->num_rows > 0) {

            ##Fetch vennue data
            $VenueDetails = $getVenuesDetails->result_array();

            ##Fetch venue images
            $this->db->select(" (CONCAT('" . $venueImagePath . "',venue_images.venue_id,'/', image)) as image", false);
            $this->db->from('venue_images');
            $this->db->where_in('venue_images.venue_id', $venue_id);
            $getVenuesImages = $this->db->get();

            //process image data
            if ($getVenuesImages->num_rows > 0) {
                //fetch the data
                $VenuesImages = $getVenuesImages->result_array();
            }

            ## fetch looped room data
            $this->db->select("id as looped_room_id,name,IF(ISNULL(venue_rooms.last_verified),'',venue_rooms.last_verified)as last_verified", false);
            $this->db->from("venue_rooms");
            $this->db->where_in('venue_rooms.venue_id', $venue_id);
            $VenuesLoopedRooms = $this->db->get();

            //process looped room data
            if ($VenuesLoopedRooms->num_rows > 0) {
                $LoopedRooms = $VenuesLoopedRooms->result_array();
            }

            ## generate output
            $ReturnData['status'] = '1';
            $ReturnData['venue_details'] = $VenueDetails[0];
            ##Image data
            $ReturnData['venue_images'] = $VenuesImages;
            ## looped room data
            $ReturnData['looped_rooms'] = $LoopedRooms;
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : GetCheckInDetails
    // Purpose : To get check-in details of user
    // In Params : customer id, venue id
    // Out params : venue detail
    //#################################################################
    public function GetCheckInDetails($Params) {

        //global declaration
        $ReturnData = array();

        //extract params
        extract($Params);

        //check for email
        $this->db->select("c.id,c.checkin_date", false);
        $this->db->from("$this->venue_checkins as c");
        $this->db->where("c.venue_id", $venue_id);
        $this->db->where("c.customer_id", $customer_id);
        $this->db->order_by("c.id", "desc");
        $this->db->limit("1");

        //get data of query
        $GetCheckInData = $this->db->get();

        //if email found then return 1 else return 0
        if ($GetCheckInData->num_rows() > 0) {

            //fetch data of query
            $ExistCheckInDetails = $GetCheckInData->result_array();

            $ReturnData['status'] = 1;
            $ReturnData['checkin_details'] = $ExistCheckInDetails[0];
        } else {

            $ReturnData['status'] = 0;
        }
        //return data
        return $ReturnData;
    }

    //#################################################################
    // Name : VenueCity
    // Purpose : To get cities list
    // In Params : 
    // Out params : venues city list
    //#################################################################

    public function VenueCity($Params) {

        //global declaration
        $ReturnData = array();

        //extract params
        extract($Params);

        $this->db->select('CONCAT(city,",",state) as city', false);
        $this->db->from("venues");
        $this->db->where("REPLACE(city,'''','') like  '{$city}%'");
        // $this->db->like('v.city', $location, 'after');
        $this->db->group_by("city");
        $query = $this->db->get();
        //echo $this->db->last_query();exit;  
        if ($query->num_rows > 0) {
            //fetch data of query
            $CityNameList = $query->result_array();
            $ReturnData['status'] = 1;
            $ReturnData['city_name'] = $CityNameList;
        } else {
            $ReturnData['status'] = 0;
        }

        //return data
        return $ReturnData;
    }

    public function GetVenueName() {

        //global declaration
        $ReturnData = array();

        //check for email
        $this->db->select("v.name", false);
        $this->db->from("$this->venues as v");
        $this->db->group_by('v.name');
        $this->db->order_by("v.name", "asc");
        
        //get data of query
        $GetVenues = $this->db->get();

        //if email found then return 1 else return 0
        if ($GetVenues->num_rows() > 0) {

            //fetch data of query
            $Venues = $GetVenues->result_array();

            $ReturnData['status'] = 1;
            $ReturnData['venues'] = $Venues;
        } else {

            $ReturnData['status'] = 0;
        }
        //return data
        return $ReturnData;
    }

    //#################################################################
    // Name : VenueCity
    // Purpose : To get cities list
    // In Params : 
    // Out params : venues city list
    //#################################################################

    public function NearByVenues($Params) {


        // global declaration.
        $ReturnData = array();
        $LoopedRooms = array();
        $VenueDetails = array();

        //extract params
        extract($Params);

        $distance = 25;

        $venueImagePath = UPLOADS_URL . 'venues/';
        $VenueImage = UPLOADS_URL . 'default_venue.png';
        $CategoriesImage = UPLOADS_URL . 'category_image/';

        if (isset($Params) && !empty($Params)) {

            $venue_lat = $latitude;
            $venue_long = $longitude;

            $latDistance = "TRUNCATE(( 3959 * acos( cos ( radians('" . $venue_lat . "') ) *  cos( radians( venues.latitude ) ) *  cos( radians( venues.longitude ) - radians('" . $venue_long . "') ) + sin ( radians('" . $venue_lat . "') ) * sin( radians( venues.latitude ) ) ) ),2) AS `distance`";
        }
        $this->db->select("   venues.id,
                              venues.name as name,
                              GROUP_CONCAT( DISTINCT vr.id) as looped_rooms_id,
                              GROUP_CONCAT( DISTINCT vr.`name`) as looped_rooms,
                              CONCAT('" . $CategoriesImage . "',c.image) as category_image,
                              IF(venues.venue_image = '', '',CONCAT('" . $venueImagePath . "',venues.id,'/thumb/', venues.venue_image)) as venue_image,
                              $latDistance,
                              GROUP_CONCAT( DISTINCT c.`name` SEPARATOR ', ') AS category_name", FALSE);

        $this->db->from('venues');
        $this->db->join('venue_categories', 'venues.id = venue_categories.venue_id', 'inner');
        $this->db->join('categories c', 'c.id = venue_categories.category_id', 'inner');
        $this->db->join('venue_rooms vr', 'vr.venue_id = venues.id', 'inner');
        $this->db->where('venue_categories.category_id != 9');
        $this->db->group_by('venues.id');

        if ($venue_lat != '' && $venue_long != '') {
            $this->db->having("distance <= ", $distance);
        }
        $this->db->order_by("distance", "asc");
        $getVenuesDetails = $this->db->get();
        // echo $this->db->last_query();
        ## venue details is found then fetch looped rooms and venue images
        if ($getVenuesDetails->num_rows > 0) {
            ##Fetch vennue data
            $VenueDetails = $getVenuesDetails->result_array();
            ## generate output
            $ReturnData['status'] = '1';
            $ReturnData['venue_details'] = $VenueDetails;
        } else {
            $ReturnData['status'] = '0';
        }
        return $ReturnData;
    }

}

/* End of file customer_model.php */
/* Location: ./application/api/models/user_model.php */