<?php

/*
 * @category   Categories Model
 * @package    Database activity for category
 * @author     Bhargav Bhanderi <bhargav@creolestudios.com>
 * @author     Another Author <another@example.com>
 * @copyright  2014 Creole Studios
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    Release: 1.0 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class feedback_model extends CI_Model {

    var $table;

    function __construct() {
        parent::__construct();
        $this->load->helper('cookie');
        $this->request = 'request';
        $this->feedback = 'feedback';
        $this->customers = 'customers';
    }

    //#################################################################
    // Name : GetAllFeedback
    // Purpose : To get all the feedback for export
    // In Params : void
    // Out params : all feedback data
    //#################################################################
    public function GetAllFeedback() {

        //initialize return data
        $ReturnData = array();

        $this->db->select("CONCAT(c.first_name,' ',c.last_name) as customer_name,c.email,v.name as venue_name,v.city,v.state,vr.name as loop_room_name,DATE_FORMAT(f.feedback_date,'%d/%c/%Y %h:%i %p') as feedback_date,CASE f.loop_working WHEN '0' THEN 'No' WHEN '1' THEN 'Yes' WHEN '2' THEN 'Other' END as satisfy,CASE f.loop_not_working_reason WHEN '1' THEN 'No Sound' WHEN '2' THEN 'Poor Sound Quaility' WHEN '3' THEN 'Headset Broken'  WHEN '4' THEN 'Others' ELSE '' END as reason,f.comment", false);
        $this->db->from("$this->feedback as f");
        $this->db->join('customers as c', 'f.customer_id = c.id', 'left');
        $this->db->join('venues as v', 'f.venue_id = v.id', 'left');
        $this->db->join('venue_rooms as vr', 'f.loop_room_id = vr.id', 'left');
        $this->db->order_by("f.id","DESC");

        $GetFeedbackDetails = $this->db->get();

        if ($GetFeedbackDetails->num_rows() > 0) {

            //fetch the data
            $FeedbackData = $GetFeedbackDetails->result_array();

            $ReturnData['status'] = '1';
            $ReturnData['feedback_data'] = $FeedbackData;
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : GetCount
    // Purpose : To get count of read and total message
    // In Params : void
    // Out params : count details
    //#################################################################

    public function GetCount() {

        //global initilization
        $ReturnData = array();

        //query to fetch count
        $GetCount = $this->db->query("SELECT (SELECT count(id) from feedback where loop_working = '0' and flag != '2') as total_unread_count,(SELECT count(id) from feedback where flag = '2') as total_archive_count,count(id) as total_rows from feedback ");
        $CountData = $GetCount->result_array();

        if (isset($CountData) && !empty($CountData)) {
            $ReturnData['total_unread_count'] = $CountData[0]['total_unread_count'];
            $ReturnData['total_archive_count'] = $CountData[0]['total_archive_count'];
            $ReturnData['total_rows'] = $CountData[0]['total_rows'];
        } else {
            $ReturnData['total_unread_count'] = 0;
            $ReturnData['total_archive_count'] = 0;
            $ReturnData['total_rows'] = 0;
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : GetFeedbackDetails
    // Purpose : To get feedback details
    // In Params : feedback id
    // Out params : feedback details
    //#################################################################
    public function GetFeedbackDetails($FeedbackId) {

        //initialize return data
        $ReturnData = array();

        $this->db->select("f.id as feedback_id,c.id as customer_id,CONCAT(c.first_name,' ',c.last_name) as customer_name,c.email,v.id as venue_id,v.name as venue_name,v.city,v.state,vr.name as loop_room_name,f.feedback_date,CASE f.loop_working WHEN '0' THEN 'No' WHEN '1' THEN 'Yes' WHEN '2' THEN 'Other' ELSE '--NA--' END as satisfy,CASE f.loop_not_working_reason WHEN '1' THEN 'No Sound' WHEN '2' THEN 'Poor Sound Quaility' WHEN '3' THEN 'Headset Broken'  WHEN '4' THEN 'Others' ELSE '--NA--' END as reason,f.comment,f.flag as status", false);
        $this->db->from("$this->feedback as f");
        $this->db->join('customers as c', 'f.customer_id = c.id', 'left');
        $this->db->join('venues as v', 'f.venue_id = v.id', 'left');
        $this->db->join('venue_rooms as vr', 'f.loop_room_id = vr.id', 'left');
        $this->db->where("f.id", $FeedbackId);

        $GetFeedbackDetails = $this->db->get();

        if ($GetFeedbackDetails->num_rows() > 0) {

            //fetch the data
            $FeedbackData = $GetFeedbackDetails->result_array();

            $ReturnData['status'] = '1';
            $ReturnData['feedback_data'] = $FeedbackData[0];
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //###########################################################
    //function : DeleteFeedback
    //To delete feedback
    //Input : feedback id
    //Output : success/error response
    //###########################################################   
    public function DeleteFeedback($FeedbackId) {

        $this->db->delete($this->feedback, array('id' => $FeedbackId));

        $ReturnData['status'] = "1";
        $ReturnData['message'] = $this->lang->line('SUCCESS_FEEDBACK_DELETE');

        return $ReturnData;
    }

    //###########################################################
    //function : UpdateFeedbackStatus
    //To update feedback status to read
    //Input : feedback id
    //Output : success/error response
    //###########################################################  
    public function UpdateFeedbackStatus($FeedbackId, $Status) {

        //prepare array to insert
        $UpdateArray = array(
            'flag' => $Status
        );

        //update the customer
        $UpdateMessage = $this->db->update($this->feedback, $UpdateArray, array("id " => $FeedbackId));

        $ReturnData['status'] = "1";
        if ($Status == '2') {
            $ReturnData['message'] = $this->lang->line('SUCCESS_FEEDBACK_ARCHIV');
        } else {
            $ReturnData['message'] = $this->lang->line('SUCCESS_FEEDBACK_READ');
        }

        return $ReturnData;
    }

}
