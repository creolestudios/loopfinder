<?php

/*
 * @category   Categories Model
 * @package    Database activity for category
 * @author     Bhargav Bhanderi <bhargav@creolestudios.com>
 * @author     Another Author <another@example.com>
 * @copyright  2014 Creole Studios
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    Release: 1.0 
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class categories_model extends CI_Model {

    var $table;

    function __construct() {
        parent::__construct();
        $this->load->helper('cookie');
        $this->category = 'categories';
        $this->categories_pins = 'categories_pins';
    }

    //#################################################################
    // Name : CheckCategoryExist
    // Purpose : To check it categorys is exist or not
    // In Params : categoy name and id at edit time
    // Out params : load account setting view
    //#################################################################
    public function CheckCategoryExist($Params) {

        //extract data
        extract($Params);

        $this->db->select('name');
        $this->db->from($this->category);
        $this->db->where("name", $category_name);

        if (isset($category_id) && $category_id != '') {
            $this->db->where("id != $category_id");
        }

        $GetCategoryQuery = $this->db->get();
        if ($GetCategoryQuery->num_rows() > 0) {
            return false;
        } else {
            return true;
        }
    }

    //#################################################################
    // Name : CheckPriorityExist
    // Purpose : To check if priority already assign then swap.
    // In Params : priority number
    // Out params : success message
    //#################################################################
    public function CheckPriorityExist($Params) {
        //extract data
        extract($Params);

     
            $this->db->select('c.priority');
            $this->db->from('categories c');
            $this->db->where("c.priority = $category_priority");
            if (isset($category_id) && $category_id != '') {
                
                $this->db->where("id != $category_id");
            }
            $priorityResult = $this->db->get();
            if ($priorityResult->num_rows > 0) {
                return false;
            } else {
                return true;
            }
        
    }

    //#################################################################
    // Name : GetCategoryPins
    // Purpose : To get all categories pin
    // In Params : void
    // Out params : all data of category pin
    //#################################################################
    public function GetCategoryPins() {

        //initilize return data
        $ReturnData = array();

        $this->db->select('name,image_name');
        $this->db->from($this->categories_pins);
        $this->db->order_by('name', 'ASC');
        $GetCategoryPins = $this->db->get();


        if ($GetCategoryPins->num_rows() > 0) {
            //fetch the data
            $CategoryPinData = $GetCategoryPins->result_array();

            $ReturnData['status'] = '1';
            $ReturnData['category_data'] = $CategoryPinData;
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //#################################################################
    // Name : CreateCategory
    // Purpose : To create category
    // In Params : category name and image
    // Out params : success message and status
    //#################################################################
    public function CreateCategory($Params) {

        //initilize return data
        $ReturnData = '';

        //extract post data
        extract($Params);
       
        //prepare array for insertion
        $InsertArray = array(
            'name' => $category_name,
            'priority' => $category_priority,
            'image' => $image,
            'pin' => $category_pin,
            'not_looped_pin' => $category_pin2
        );

        //insert customer data
        $AddCategory = $this->db->insert($this->category, $InsertArray);

        //check it insertion in success or not
        if ($this->db->affected_rows() > 0) {
            $ReturnData['status'] = '1';
        } else {
            $ReturnData['status'] = '0';
        }

        //return data
        return $ReturnData;
    }

    //#################################################################
    // Name : UpdateCategory
    // Purpose : To update category
    // In Params : category name and image
    // Out params : success message and status
    //#################################################################
    public function UpdateCategory($Params) {
        extract($Params);
        //prepare array to insert
        $UpdateArray = array(
            'name' => $category_name,
            'priority' => $category_priority
        );


        if ($image != '') {
            $UpdateArray = array_merge($UpdateArray, array('image' => $image));
        }

        if ($category_pin != '') {
            $UpdateArray = array_merge($UpdateArray, array('pin' => $category_pin));
        }

        if ($category_pin2 != '') {
            $UpdateArray = array_merge($UpdateArray, array('not_looped_pin' => $category_pin2));
        }

        //update the customer
        $UpdateCategory = $this->db->update($this->category, $UpdateArray, array("id " => $category_id));

        $ReturnData['status'] = '1';

        //return data
        return $ReturnData;
    }

    //#################################################################
    // Name : GetCategoryDetails
    // Purpose : To get category details
    // In Params : category id
    // Out params : category details
    //#################################################################
    public function GetCategoryDetails($CategoryId) {

        //initialize return data
        $ReturnData = array();

        //set image path
        $CategoryImagePath = UPLOADS_URL . 'category_image/';
        $DefaultImage = UPLOADS_URL . 'default_album.png';

        $this->db->select("c.id as category_id,c.name as category_name,c.priority as category_priority,IF(c.image = '','" . $DefaultImage . "',CONCAT('" . $CategoryImagePath . "',c.image)) as category_image,pin as category_pin, not_looped_pin as category_pin2", false);
        $this->db->from("$this->category as c");
        $this->db->where("id", $CategoryId);

        $GetCategoryDetails = $this->db->get();

        if ($GetCategoryDetails->num_rows() > 0) {

            //fetch the data
            $CategoryData = $GetCategoryDetails->result_array();

            $ReturnData['status'] = '1';
            $ReturnData['category_data'] = $CategoryData[0];
        } else {
            $ReturnData['status'] = '0';
        }

        return $ReturnData;
    }

    //###########################################################
    //function : DeleteCategory
    //To delete category
    //Input : category id
    //Output : success/error response
    //###########################################################   
    public function DeleteCategory($CategoryId) {

        $this->db->delete($this->category, array('id' => $CategoryId));

        $ReturnData['status'] = "1";
        $ReturnData['message'] = $this->lang->line('SUCCESS_CATEGORY_DELETE');

        return $ReturnData;
    }

    //###########################################################
    //function : GetAllCategories
    //To get all the categories
    //Input : void()
    //Output : success/error response
    //###########################################################   
    public function GetAllCategories() {

        //initialize return data
        $ReturnData = array();

        $this->db->select("c.id as category_id,c.priority,c.name as category_name", false);
        $this->db->from("$this->category as c");
        $this->db->order_by('c.priority', 'asc');
        $GetCategoryDetails = $this->db->get();

        if ($GetCategoryDetails->num_rows() > 0) {

            //fetch the data
            $CategoryData = $GetCategoryDetails->result_array();

            $ReturnData['status'] = '1';
            $ReturnData['category_data'] = $CategoryData;
        } else {
            $ReturnData['status'] = '1';
        }

        return $ReturnData;
    }

}

/* End of file venue_model.php */
/* Location: ./application/otojoy/models/categories_model.php */
?>