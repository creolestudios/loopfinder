<?php

/*
 * @category   Categories
 * @package    Add/Update/Delete Categories
 * @author     Bhargav Bhanderi <bhargav@creolestudios.com>
 * @author     Another Author <another@example.com>
 * @copyright  2014 Creole Studios
 * @license    http://www.php.net/license/3_0.txt  PHP License 3.0
 * @version    Release: 1.0 
 */

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require APPPATH . '/libraries/php-export-data.class.php';

class Users extends CI_Controller {

    var $viewData = array();

    public function __construct() {
        parent::__construct();
        $this->load->model('customer_model');
        $this->load->library('Datatables');
        $this->load->helper(array('form', 'url'));
        if (!$this->session->userdata('M_ADMINLOGIN'))
            redirect('login');
    }

    //#################################################################
    // Name : index
    // Purpose : To fetch and display account settings/quote settings data
    // In Params : merchant id
    // Out params : load account setting view
    //#################################################################
    public function index() {
        //get merchant id from session

        $UserId = $this->session->userdata('M_ADMINID');

        //############## Account Settings view Start ###################
        //fetch data using merchant model
        //  $Information = $this->user_model->GetAccountSettings($UserId);
        //extract data
        if (isset($Information) && !empty($Information)) {
            //if status is error(0), then print error
            if ($Information['status'] == 0) {
                //response error
                $AccountInfo['account_settings_data'] = $Information['message'];
            } else {

                //load data to view, data will be used in view as well as edit section
                //just change of mode will decide which view to load
                $AccountInfo['account_settings_data'] = $Information['data'];
            }
        }


        //############## Quote settings modifier view end ###################
        //load view
        $this->load->view('users/list_view');
    }

    //#################################################################
    // Name : ExportUsers
    // Purpose : To fetch and display account settings/quote settings data
    // In Params : merchant id
    // Out params : load account setting view
    //#################################################################
    public function ExportUsers() {
        // 'browser' tells the library to stream the data directly to the browser.
        // other options are 'file' or 'string'
        // 'test.xls' is the filename that the browser will use when attempting to 
        // save the download        
        //get users data
        $GetUsers = $this->customer_model->GetAllUsers();

        //if user data found
        if (isset($GetUsers) && $GetUsers['status'] == '1') {
            //call export csv data
            $exporter = new ExportDataCSV('browser', 'Users_' . date("d_M_Y") . '.csv');

            $exporter->initialize(); // starts streaming data to web browser
            // pass addRow() an array and it converts it to Excel XML format and sends 
            // it to the browser
            //add header row for titles
            $exporter->addRow(array("Name", "Email", "Age", "Gender", "Hearing Loss", "Hearing Aid", "City", "State", "ZIP", "Requests", "Votes", "Feedbacks", "Check-Ins", "Register Date"));

            //looped through array
            foreach ($GetUsers['customer_data'] as $UserKey => $UserVal) {
                $exporter->addRow(array($UserVal['name'], $UserVal['email'], $UserVal['age'], $UserVal['gender'], $UserVal['hearing_level'], $UserVal['hearing_ad'], $UserVal['city'], $UserVal['state'], $UserVal['zipcode'], $UserVal['requests'], $UserVal['votes'], $UserVal['feedbacks'], $UserVal['checkins'], $UserVal['register_date']));
            }

            $exporter->finalize(); // writes the footer, flushes remaining data to browser.
        }
        exit(); // all done
    }

    //#################################################################
    // Name : GetUsers
    // Purpose : To show user's list
    // In Params : void
    // Out params : load user data
    //#################################################################   
    public function GetUsers() {


        $rec_start = $this->input->post('start');

        $this->datatables->select("CONCAT(c.first_name,' ',c.last_name) as name,c.email as email,c.zipcode as zipcode,c.city as city,c.state as state,count(distinct r.id) as requests,count(distinct v.id) as votes,count(distinct f.id) as feedbacks,count(distinct ch.id) as checkins,c.id as detail", false)->from(" customers as c");
        $this->datatables->join('request as r', 'r.customer_id = c.id', 'left');
        $this->datatables->join('votes as v', 'v.customer_id = c.id', 'left');
        $this->datatables->join('feedback as f', 'f.customer_id = c.id', 'left');
        $this->datatables->join('checkins as ch', 'ch.customer_id = c.id', 'left');
        $this->datatables->group_by('c.id');

        //condition for filters
        if (isset($_POST['name']) && $_POST['name'] != '') {
            $Name = $_POST['name'];
            //explode name by space
            $NameArray = explode(" ", $Name);
            foreach ($NameArray as $NameVal) {
                $this->datatables->where("(c.first_name LIKE '%{$NameVal}%' OR  c.last_name LIKE '%{$NameVal}%') AND  (c.first_name LIKE '%{$NameVal}%' OR  c.last_name LIKE '%{$NameVal}%')");
            }
        }
        ## Email condition
        if (isset($_POST['email']) && $_POST['email'] != '') {
            $this->datatables->where('c.email like "%' . trim($_POST['email']) . '%"');
        }
        ## zipcode condition
        if (isset($_POST['zipcode']) && $_POST['zipcode'] != '') {
            $this->datatables->where('c.zipcode like "' . trim($_POST['zipcode']) . '%"');
        }
        ## city condition
        if (isset($_POST['city']) && $_POST['city'] != '') {
            $this->datatables->where('c.city like "%' . trim($_POST['city']) . '%"');
        }
        ## city condition
        if (isset($_POST['state']) && $_POST['state'] != '') {
            $this->datatables->where('c.state like "%' . trim($_POST['state']) . '%"');
        }


        echo $this->datatables->generate();
    }

    //#################################################################
    // Name : details
    // Purpose : To show user's details
    // In Params : user id
    // Out params : load user details view
    //#################################################################    

    public function Details() {

        //initialize
        $ViewData = array();

        //get category id to fetch data
        $CustomerId = base64_decode($this->uri->segment(3));

        //get user details
        //get category details based on category id
        $CustomerDetails = $this->customer_model->GetUserDetails($CustomerId);
        
        if (isset($CustomerDetails) && $CustomerDetails['status'] == '1') {
            $ViewData['customer_details'] = $CustomerDetails['customer_data'];
        }
        //get total number of checkins and votes
        $TotalActivities = $this->customer_model->GetActivityTotal($CustomerId);
        //mprd($TotalActivities);
        //assign activities
        if (isset($TotalActivities) && !empty($TotalActivities)) {
            $ViewData['checkins'] = $TotalActivities['checkins'];
            $ViewData['votes'] = $TotalActivities['votes'];
        }

        $this->load->view('users/details_view', $ViewData);
    }

    //#################################################################
    // Name : Update
    // Purpose : To update user's details
    // In Params : user id
    // Out params : load user details view
    //#################################################################  
    public function Update() {
        //initialize
        $ViewData = array();

        //get category id to fetch data
        $CustomerId = base64_decode($this->uri->segment(3));

        //get user details
        //get category details based on category id
        $CustomerDetails = $this->customer_model->GetUserDetails($CustomerId);

        if (isset($CustomerDetails) && $CustomerDetails['status'] == '1') {
            $ViewData['customer_details'] = $CustomerDetails['customer_data'];
        }

        $this->load->view('users/update_view', $ViewData);
    }

    //#################################################################
    // Name : SaveUser
    // Purpose : To update user's details
    // In Params : user id and user details
    // Out params : load user details view
    //#################################################################  
    public function SaveUser() {
        //initialize
        $ViewData = array();

        $PostData = $this->input->post();
        
        //process post data
        if (isset($PostData) && !empty($PostData)) {
            //remove space from all the data
            $Params = array_map('trim', $PostData);

            $UpdateUser = $this->customer_model->UpdateUserDetails($Params);

            //check if category added successfully or not
            if (isset($UpdateUser) && $UpdateUser['status'] == '1') {
                $this->session->set_flashdata('success', $this->lang->line('SUCCESS_USER_UPDATE'));
                redirect('users/Details/' . base64_encode($Params['customer_id']));
            } else {
                $this->session->set_flashdata('error', $this->lang->line('NO_CHANGES_MADE'));
                redirect('users/Details/' . base64_encode($Params['customer_id']));
            }
        }
    }

    //###########################################################
    //function : DeleteUser
    //To delete User
    //Input : User id
    //Output : message
    //###########################################################
    public function DeleteUser() {

        $PostData = $this->input->post();

        if (isset($PostData) && count($PostData) > 0) {

            //remove space from post data
            $Params = array_map('trim', $PostData['data']);

            //process customer data
            extract($Params);

            //call function to change status
            $DeleteCustomer = $this->customer_model->DeleteCustomer($customer_id);

            //print response
            echo json_encode($DeleteCustomer);
        }
        exit;
    }

    //###########################################################
    //function : GetVotes
    //To get user votes data
    //Input : User id
    //Output : message
    //###########################################################
    public function GetVotes() {
        $GetData = $this->input->post();

        //process data
        if (isset($GetData) && !empty($GetData)) {
            $Params = array_map('trim', $GetData);

            //fetch checking data for customer
            //get customer id
            $CustomerId = $Params['customer_id'];

            //query to select checkin data

            $this->datatables->select('DATE_FORMAT(c.vote_date,"%d %b %Y %h:%i %p") as vote_date,CONCAT(v.name,"_",v.id) as venue_name,v.city as venue_city,v.state as venue_state', false);
            $this->datatables->from("votes as c ");
            $this->datatables->join('venues as v', 'c.venue_id = v.id', 'left');
            $this->datatables->where("c.customer_id", $CustomerId);

            echo $this->datatables->generate();
        }
    }

    //###########################################################
    //function : GetCheckins
    //To get user checking data
    //Input : User id
    //Output : message
    //###########################################################
    public function GetCheckins() {
        $GetData = $this->input->post();

        //process data
        if (isset($GetData) && !empty($GetData)) {
            $Params = array_map('trim', $GetData);

            //fetch checking data for customer
            //get customer id
            $CustomerId = $Params['customer_id'];

            //query to select checkin data

            $this->datatables->select('DATE_FORMAT(c.checkin_date,"%d %b %Y %h:%i %p") as checkin_date,CONCAT(v.name,"_",v.id) as venue_name,v.city as venue_city,v.state as venue_state', false);
            $this->datatables->from("checkins as c ");
            $this->datatables->join('venues as v', 'c.venue_id = v.id', 'left');
            $this->datatables->where("c.customer_id", $CustomerId);

            echo $this->datatables->generate();
        }
    }

    //###########################################################
    //function : GetActivities
    //To get user activity data
    //Input : User id
    //Output : message
    //###########################################################
    public function GetActivities() {
        $GetData = $this->input->post();

        //process data
        if (isset($GetData) && !empty($GetData)) {
            $Params = array_map('trim', $GetData);

            //fetch checking data for customer
            //get customer id
            $CustomerId = $Params['customer_id'];

            //query to select checkin data

            $this->datatables->select('CONCAT(v.name,"_",v.id) as venue_name,IF(v.venue_type = 1,DATE_FORMAT(c.checkin_date,"%d %b %Y %h:%i %p"),DATE_FORMAT(vo.vote_date,"%d %b %Y %h:%i %p")) as checkin_date,v.city as venue_city,v.state as venue_state,IF(v.venue_type = 1,"Check-in","Votes") as activity_type', false);
            $this->datatables->from("venues as v ");
            $this->datatables->join('checkins as c', 'c.venue_id = v.id AND c.customer_id = "' . $CustomerId . '"', 'left');
            $this->datatables->join('votes as vo', 'vo.venue_id = v.id AND vo.customer_id = "' . $CustomerId . '"', 'left');
            $this->datatables->where('checkin_date is not null');

            echo $this->datatables->generate();
            //echo $this->db->last_query();
        }
    }

    //###########################################################
    //function : GetFeedbacks
    //To get user feedback data
    //Input : User id
    //Output : message
    //###########################################################
    public function GetFeedbacks() {
        //get data from input params
        $GetData = $this->input->post();

        //process data
        if (isset($GetData) && !empty($GetData)) {
            //remove space from params
            $Params = array_map('trim', $GetData);

            //fetch checking data for customer
            //get customer id
            $CustomerId = $Params['customer_id'];

            //query to select checkin data

            $this->datatables->select("DATE_FORMAT(f.feedback_date,'%d %b %Y %h:%i %p') as feedback_date,CONCAT(v.name,'_',v.id) as venue_name,vr.name as loop_room_name,v.city as venue_city,v.state as venue_state,CASE f.loop_working WHEN '0' THEN 'No' WHEN '1' THEN 'Yes' WHEN '2' THEN 'Other' END as satisfy,CASE f.loop_not_working_reason WHEN '1' THEN 'No Sound' WHEN '2' THEN 'Poor Sound Quaility' WHEN '3' THEN 'Headset Broken' WHEN '4' THEN 'Others' ELSE '' END as reason,f.comment", false);
            $this->datatables->from("venues as v ");
            $this->datatables->join('feedback as f', 'f.venue_id = v.id', 'left');
            $this->datatables->join('venue_rooms as vr', 'f.loop_room_id = vr.id', 'left');
            $this->datatables->where("f.customer_id", $CustomerId);

            echo $this->datatables->generate();
        }
    }

    //###########################################################
    //function : GetRequests
    //To get user request data
    //Input : User id
    //Output : message
    //###########################################################
    public function GetRequests() {

        //get data from input params
        $GetData = $this->input->post();

        //process data
        if (isset($GetData) && !empty($GetData)) {
            //remove space from params
            $Params = array_map('trim', $GetData);

            //fetch checking data for customer
            //get customer id
            $CustomerId = $Params['customer_id'];

            //query to select checkin data

            $this->datatables->select("DATE_FORMAT(r.request_date,'%d %b %Y %h:%i %p') as request_date,r.venue_name,r.city as venue_city,r.state as venue_state,r.comment", false);
            $this->datatables->from("request as r ");
            $this->datatables->where("r.customer_id", $CustomerId);

            echo $this->datatables->generate();
        }
    }

}
