<?php
/**
 * Author : Jignesh Virani
 * Description : Demo of google clusturing using MySql.
 */
define('UPLOADS_URL', "http://localhost/otojoy/uploads/venues/");

define('DB_NAME', 'otojoy');

define('DB_USER', 'root');

define('DB_PASSWORD', '');

define('DB_HOST', '192.168.1.204');

$con = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

// Fetching locations 
$sql = "
                         SELECT DISTINCT
                            venues.`name`,
                            venues.id,
                            venues.address,
                            venues.latitude,
                            venues.longitude,
                            venues.venue_image,
                            GROUP_CONCAT(c.`name`) AS cname,
                            c.pin AS cpin

                    FROM
                     venues 
                    INNER JOIN venue_categories ON venues.id = venue_categories.venue_id

                    INNER JOIN categories c  ON c.id = venue_categories.category_id
                    GROUP BY venues.id";

$result = mysqli_query($con, $sql);
$locations = array();
if (mysqli_num_rows($result) > 0) {
    while ($location = mysqli_fetch_assoc($result)) {
        foreach ($location as $key => $val) {
            $key = utf8_encode($key);
            $location[$key] = utf8_encode($val);
        }
        $locations[] = $location;
    }
    echo $locations_js = json_encode($locations);
    exit;
} else {
    $locations = 0;
}
mysqli_free_result($result);
?>

<html>
    <head>


        <style>
            .photo-map {
                background-color : #222222;
                height : 100%;
                width : 100%;
            }

        </style>



        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3&sensor=false"></script>
        <script src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclusterer/src/markerclusterer_compiled.js" type="text/javascript"></script>
        <script>
            var marker;
            var gm_map;
            var markerArray = [];
            var address = 'CA';
            //var infowindow;
            var geocoder = new google.maps.Geocoder();
            window.locations = '<?php echo $locations_js; ?>';

            if (window.locations != 0) {
                window.locations = JSON.parse(window.locations);
                console.log(window.locations);
            } else {
                window.locations = false;
                console.log(window.locations);
            }

            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    gm_map.setCenter(results[0].geometry.location);
                    gm_map.fitBounds(results[0].geometry.bounds);
                } else {
                    alert("Unable to complete the geological setting due to the following error:\n\n" + status);
                }
            });
            function getCluserMarkers(locations) {
                var i = "";
                var lat = "";
                var long = "";
                var clusterMarkers = [];
                var marker = "";
                var ico_me;
                var name;
                var address;
                var category;
                var cpin;
                var id;
                var image;
                var content;
                var infowindow;



                var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
                var imageIcon = '<?php echo UPLOADS_URL; ?>'
                for (i in locations) {
                    console.log(locations[i]);
                    if (locations[i].cname == 'Theater')             // icon 1
                    {
                        ico_me = iconBase + 'schools_maps.png';
                    }
                    else if (locations[i].cname == 'Library')          // icon 2
                    {
                        ico_me = iconBase + 'library_maps.png';
                    }
                    else if (locations[i].cname == 'Goverment')           // icon 3
                    {
                        ico_me = iconBase + 'parking_lot_maps.png';
                    }
                    else if (locations[i].cname == 'House Worship')        // icon 4
                    {
                        ico_me = iconBase + 'info-i_maps.png';
                    }
                    else if (locations[i].cname == 'test')               // icon 5
                    {
                        ico_me = iconBase + 'factory_maps.png';
                    }
                    else
                    {
                        ico_me = "";
                    }
                    //add name in marker
                    name = locations[i].name;
                    address = locations[i].address;
                    category = locations[i].cname;
//                     for(var i=0; i< category.length;i++)
//                     {alert(category[i]);}

                    image = locations[i].venue_image;
                    cpin = locations[i].cpin;
                    id = locations[i].id;

                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(locations[i].latitude, locations[i].longitude),
                        map: gm_map,
                        icon: ico_me,
                        // content: content
                    });
                    //display click event
                    content = "<img width='100' height='100' src=" + imageIcon + id + '/' + image + '><br>' +
                            "<a href=''>" + name + '</a><br>' +
                            "<b>address</b>: " + address + '<br>' +
                            "<b>category</b>:" + category + '<br>' +
                            "<button name='view more' id='view-more' class='view-more'>View More</button>";
                    var infowindow = new google.maps.InfoWindow({content: content, maxWidth: 1000, maxHeight: 800})
                    google.maps.event.addListener(marker, 'mouseover', (function (marker, content, infowindow) {
                        return function () {
                            infowindow.setContent(content);
                            infowindow.open(gm_map, marker);
                        }
                    })(marker, content, infowindow));
                    //click event end
                    clusterMarkers.push(marker);
                }
                return clusterMarkers;
            }


            function initialize() {

                var marker, i;
                var clusterMarkers = "";

                if (window.locations) {
                    clusterMarkers = getCluserMarkers(window.locations);
                }

                var options_googlemaps = {
                    minZoom: 0,
                    maxZoom: 100,
                    zoom: 0,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    streetViewControl: false
                }

                gm_map = new google.maps.Map(document.getElementById('google-maps'), options_googlemaps);

                if (clusterMarkers.length > 0) {

                    var options_markerclusterer = {
                        gridSize: 20,
                        maxZoom: 18,
                        zoomOnClick: true, // using style
                    };
                    var markerCluster = new MarkerClusterer(gm_map, clusterMarkers, options_markerclusterer);
                    google.maps.event.addListener(markerCluster, 'clusterclick', function (cluster) {
                        alert('You have clicked on cluster.');

                    });
                }
            }

            $(document).ready(function () {
                initialize();
            });
        </script>
    </head>
    <body>
        <div class="photo-map" id="google-maps"></div>
    </body>
</html>