var Login = function () {
    var $SITEPATH = BASEURL;
    var handleLogin = function () {
        $('.login-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true
                },
                remember: {
                    required: false
                }
            },
            messages: {
                email: {
                    required: "Email is required."
                },
                password: {
                    required: "Password is required."
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit   
                $('.alert-error', $('.login-form')).show();
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },
            submitHandler: function (form) {

                var login_email = $("#login_email").val();

                var login_pwd = $("#login_password").val();

                var remember = $("#remember").val();

                var dataString = {
                    email: login_email,
                    password: login_pwd,
                    remember: remember
                }

                //ajax link
                var path = $SITEPATH + 'login';

                //call ajax
                $.post(path, {
                    data: dataString
                },
                function (res) {
                    //response handler
                    if ($.trim(res) == '') {
                        alert('Something went wrong!');
                        return false;
                    } else {
                        if (res == true) {
                            window.location = BASEURL + "dashboard";
                        } else {
                            //alert-danger
                            $("#login_msg").addClass("alert-danger");
                            $("#login_msg").removeClass("hide");
                            $("#login_msg").html(res).slideDown(800).delay(3000).slideUp(800);
                        }
                        return false;
                    }
                }, 'json');
                return false;
            }
        });

        $('.login-form input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.login-form').validate().form()) {
                    var login_email = $("#login_email").val();

                    var login_pwd = $("#login_password").val();

                    var remember = $("#remember").val();

                    var dataString = {
                        email: login_email,
                        password: login_pwd,
                        remember: remember
                    }

                    //ajax link
                    var path = $SITEPATH + 'login';

                    //call ajax
                    $.post(path, {
                        data: dataString
                    },
                    function (res) {
                        //response handler
                        if ($.trim(res) == '') {
                            alert('Something went wrong!');
                            return false;
                        } else {
                            if (res == true) {
                                window.location = "dashboard";
                            } else {
                                //alert-danger
                                $("#login_msg").addClass("alert-danger");
                                $("#login_msg").removeClass("hide");
                                $("#login_msg").html(res).slideDown(800).delay(3000).slideUp(800);
                            }
                            return false;
                        }
                    }, 'json');
                    return false;
                    //$('.login-form').submit();
                }
                return false;
            }
        });
    }

    var handleForgetPassword = function () {
        $('.forget-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                forgot_pass_email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                forgot_pass_email: {
                    required: "Email is required."
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit   

            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },
            submitHandler: function (form) {

                //get email address
                var email = $("#forgot_pass_email").val();
                //define path
                var path = $SITEPATH + 'ajax/forgotpassword/';

                //show loader
                $("#forgot_pass_email").addClass("spinner");

                //call ajax
                $.post(path, {
                    data: email
                },
                function (res) {
                    //response handler
                    if ($.trim(res) == '') {
                        alert('Something went wrong!');
                        return false;
                    } else {
                        $("#forgot_pass_email").removeClass("spinner");
                        $("#forgot_pass_email").val('');
                        $("#forgotPass_msg").html(res).slideDown(800).delay(3000).slideUp(800);
                        return false;
                    }
                }, 'json');

                return false;
            }
        });

        $('.forget-form input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.forget-form').validate().form()) {

                    //get email address
                    var email = $("#forgot_pass_email").val();
                    //define path
                    var path = $SITEPATH + 'ajax/forgotpassword/';

                    //show loader
                    $("#forgot_pass_email").addClass("spinner");

                    //call ajax
                    $.post(path, {
                        data: email
                    },
                    function (res) {
                        //response handler
                        if ($.trim(res) == '') {
                            alert('Something went wrong!');
                            return false;
                        } else {
                            $("#forgot_pass_email").val('');
                            $("#forgot_pass_email").removeClass("spinner");
                            $("#forgot_pass_email").val('');
                            $("#forgotPass_msg").html(res).slideDown(800).delay(3000).slideUp(800);
                            return false;
                        }
                    }, 'json');

                    return false;
                }
                return false;
            }
        });

        jQuery('#forget-password').click(function () {
            jQuery('.login-form').hide();
            jQuery('.forget-form').show();
        });

        jQuery('#back-btn').click(function () {
            jQuery('.login-form').show();
            jQuery('.forget-form').hide();
        });

    }

    //################# Reset password handler #################################

    var handleResetPassword = function () {
        $('.reset-pass-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                new_password: {
                    required: true
                },
                confirm_password: {
                    required: true,
                    equalTo: "#new_password"
                }
            },
            messages: {
                new_password: {
                    required: "Password is required."
                },
                confirm_password: {
                    required: "Confirm password is required."
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit   

            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },
            submitHandler: function (form) {

                //geather params
                var admin_password = $("#new_password").val();
                var flag = $("#flg").val();
                var admin_id = $("#merchant_id").val();

                var dataString = {
                    new_password: admin_password,
                    old_token: flag,
                    id: admin_id
                }
                //define path
                var path = $SITEPATH + 'ajax/changeAdminPass';

                //ajax call
                $.post(path, {
                    data: dataString
                },
                function (res) {
                    //response handler
                    if ($.trim(res) == 0) {
                        $("#reset_pass_msg").removeClass("hide");
                        $("#reset_pass_msg").addClass("alert-danger");
                        $("#reset_pass_msg").html('<span>Link has been expired! Please try again.</span>').slideDown(900).delay(3000).slideUp(900);
                        return false;
                    } else {
                        $("#reset_pass_msg").removeClass("hide");
                        $("#reset_pass_msg").addClass("alert-success");
                        $("#reset_pass_msg").html('<span>Password has been changed successfully!</span>').slideDown(900).delay(3000).slideUp(900);
                        window.setTimeout(function () {
                            location.href = BASEURL + "dashboard";
                        }, 3000);
                        return false;
                    }
                }, 'json');
                return false;
            }
        });

        $('.reset-pass-form input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.forget-form').validate().form()) {

                    //geather params
                    var admin_password = $("#new_password").val();
                    var flag = $("#flg").val();
                    var admin_id = $("#merchant_id").val();

                    var dataString = {
                        new_password: admin_password,
                        old_token: flag,
                        id: admin_id
                    }
                    //define path
                    var path = $SITEPATH + 'ajax/changeAdminPass';

                    //ajax call
                    $.post(path, {
                        data: dataString
                    },
                    function (res) {
                        //response handler
                        if ($.trim(res) == 0) {
                            $("#reset_pass_msg").removeClass("hide");
                            $("#reset_pass_msg").addClass("alert-danger");
                            $("#reset_pass_msg").html('<span>Link has been expired! Please try again.</span>').slideDown(900).delay(3000).slideUp(900);
                            return false;
                        } else {
                            $("#reset_pass_msg").removeClass("hide");
                            $("#reset_pass_msg").addClass("alert-success");
                            $("#reset_pass_msg").html('<span>Password has been changed successfully!</span>').slideDown(900).delay(3000).slideUp(900);
                            window.setTimeout(function () {
                                location = BASEURL + "dashboard";
                            }, 3000);
                            return false;
                        }
                    }, 'json');
                    return false;
                }
                return false;
            }
        });


    }

    //#########################################################################

    return {
        //main function to initiate the module
        init: function () {

            handleLogin();
            handleForgetPassword();
            handleResetPassword();
            //handleRegister();        

        }

    };

}();