var FormValidation = function () {

    //################### validatiaon for change password functoinality ##############
    var $SITEPATH = BASEURL;
    var handleChanePassword = function () {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

        var form1 = $('#change_password');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);

        form1.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                old_password: {
                    required: true,
                },
                new_password: {
                    required: true,
                    minlength: 6,
                    maxlength: 20
                },
                confirm_password: {
                    required: true,
                    minlength: 6,
                    maxlength: 20,
                    equalTo: "#new_password"
                },
            },
            messages: {// custom messages for radio buttons and checkboxes
                old_password: {
                    required: "Old Password is required"
                },
                new_password: {
                    required: "New Password is required",
                    minlength: jQuery.validator.format("Password should be at least {0} characters")
                },
                confirm_password: {
                    required: "Confirm Password is required",
                    equalTo: "Password does not match."
                },
            },
            invalidHandler: function (event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                //Metronic.scrollTo(error1, -200);
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {

                var old_password = $("#old_password").val();

                var new_password = $("#new_password").val();

                var confirm_password = $("#confirm_password").val();

                var dataString = {
                    old_password: old_password,
                    new_password: new_password,
                    confirm_password: confirm_password
                }

                //ajax link
                var path = $SITEPATH + 'changepassword';

                //call ajax
                $.post(path, {
                    data: dataString
                },
                function (res) {

                    //response handler
                    if ($.trim(res) == '') {
                        alert('Something went wrong!');
                        return false;
                    } else {
                        if (res.status == true) {
                            //print success message
                            $('#change_password')[0].reset();
                            toastr.success(res.message, 'Change Password');
                        } else {
                            //print error message
                            toastr.error(res.message, 'Change Password');
                        }
                        return false;
                    }
                }, 'json');
                return false;
            }
        });

    }

    //############### Account information update ##################################################
    var handleAccountSettings = function () {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

        var form1 = $('#account_info');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);

        form1.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                admin_fname: {
                    required: true,
                },
                admin_lname: {
                    required: true
                },
                admin_email: {
                    required: true,
                    email: true
                }
            },
            messages: {// custom messages for radio buttons and checkboxes
                admin_fname: {
                    required: "First name is required."
                },
                admin_lname: {
                    required: "Last name is required."
                },
                admin_email: {
                    required: "Email is required.",
                    email: "Please provide correct email."
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                Metronic.scrollTo(error1, -200);
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                        .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {

                var admin_fname = $("#admin_fname").val();

                var admin_lname = $("#admin_lname").val();

                var admin_email = $("#admin_email").val();

                var mobile_number = $("#mobile_number").val();

                var admin_id = $("#admin_id").val();
                var dataString = {
                    admin_fname: admin_fname,
                    admin_lname: admin_lname,
                    admin_email: admin_email,
                    mobile_number: mobile_number,
                    admin_id: admin_id
                }

                //ajax link
                var path = $SITEPATH + 'settings/updateaccount';

                //call ajax
                $.post(path, {
                    data: dataString
                },
                function (res) {

                    //response handler
                    if ($.trim(res) == '') {
                        alert('Something went wrong!');
                        return false;
                    } else {
                        if (res.status == true) {
                            toastr.success(res.message, 'My Account');

                            window.setTimeout(function () {
                                location = BASEURL + "settings";
                            }, 4000);
                        }
                        return false;
                    }
                }, 'json');
                return false;
            }
        });

    }

    //############### Account information update ##################################################
    var handleCategory = function () {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

        var form1 = $('#category_form');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);
        
        form1.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            onkeyup: false,
            ignore: "",
            rules: {
                category_name: {
                    required: true,
                    
                    remote: {
                        url: $SITEPATH + 'categories/isNameAvail',
                        type: "post",
                        data: {
                            category_name: function () {
                                return $("#category_name").val();
                            },
                            category_id: function () {
                                if (typeof ($("#category_id").val()) != 'undefined')
                                {
                                    return $("#category_id").val();
                                } else {
                                    return '';
                                }

                            },
                        }

                    }

                },
                category_priority: {
                    //required: true,
                    digits: true,
                    remote: {
                        url: $SITEPATH + 'categories/categoryCheck',
                        type: "post",
                        data: {
                            category_priority: function () {
                                return $("#category_priority").val();
                            },
                            category_id: function () {
                                if (typeof ($("#category_id").val()) != 'undefined')
                                {
                                    return $("#category_id").val();
                                } else {
                                    return '';
                                }

                            }
                        }

                    }
                    
                },
                category_pin: {
                    required: true,
                },
                category_pin2: {
                    required: true,
                },
                category_file: {
                    required: true,
                }
            },
            messages: {
                category_name: {
                    required: "Category name is required.",
                    remote: "Category with same name exists, Please use another name."
                },
                category_priority:{
                   // required: "Category Priority is required.",
                    digits:"Please enter only digit",
                    remote: "Category with same priority exists, Please enter another priority."
                },
                category_pin: {
                    required: "Looped Pin is required."
                },
                category_pin2: {
                    required: "Not Looped Pin is required."
                },
                category_file: {
                    required: "Category image is required",
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                Metronic.scrollTo(error1, -200);
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {

                form.submit();
            }
        });

    }

    //############### Account information update ##################################################
    var handleImportCSV = function () {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

        var form1 = $('#import_csv');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);

        form1.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            onkeyup: false,
            ignore: "",
            rules: {
                file_venue: {
                    required: true,
                }
            },
            messages: {
                file_venue: {
                    required: "Please provide csv file."
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                Metronic.scrollTo(error1, -200);
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {

                form.submit();
            }
        });

    }

    //############### Handle venue form ##################################################
    var handleVenue = function () {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

        var VenueForm = $('#AddVenue');
        var error1 = $('.alert-danger', VenueForm);
        var success1 = $('.alert-success', VenueForm);

        VenueForm.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            onkeyup: false,
            ignore: "",
            rules: {
                venue_name: {
                    required: true,
                    remote: {
                        url: $SITEPATH + 'venues/isNameAvail',
                        type: "post",
                        data: {
                            venue_name: function () {
                                return $("#venue_name").val();
                            },
                            venue_id: function () {
                                var venueId = $("#venue_id").val();

                                if (typeof ($("#venue_id").val()) != 'undefined')
                                {
                                    return $("#venue_id").val();
                                } else {
                                    return '';
                                }

                            },
                        }

                    }

                },
                category: {
                    required: true,
                },
                certified_date: {
                    required: true,
                },
                zipcode: {
                    required: true,
                },
                address: {
                    required: true,
                },
                city: {
                    required: true,
                },
                state: {
                    required: true,
                }
//                phone_number: {
//                    required: true,
//                },
//                email: {
//                    required: true,
//                    email: true
//                },
//                website: {
//                    required: true
//                }
            },
            messages: {
                venue_name: {
                    required: "Venue name is required.",
                    remote: "Venue with same name exists, Please use another name."
                    
                },
                category: {
                    required: "Category is required."
                },
                certified_date: {
                    required: "Certification Date is required."
                },
                zipcode: {
                    required: "ZipCode is required."
                }, address: {
                    required: "Address is required."
                },
                city: {
                    required: "City is required."
                },
                state: {
                    required: "State is required."
                }
//                phone_number: {
//                    required: "Phone is required."
//                },
//                email: {
//                    required: "Email is required.",
//                    email: "Please enter a valid email address.",
//                },
//                website: {
//                    required: "Website is required."
//                }
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                Metronic.scrollTo(error1, -200);
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {
                form.submit();
            }


        });
    }
    
    
    //############### Handle HLAA Chapter form ##################################################
    var handleChapter = function () {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

        var HlaaForm = $('#Addhlaa');
        var error1 = $('.alert-danger', HlaaForm);
        var success1 = $('.alert-success', HlaaForm);

        HlaaForm.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            onkeyup: false,
            ignore: "",
            rules: {
                chapter_name: {
                    required: true,
                    remote: {
                        url: $SITEPATH + 'hlaachapters/isNameAvail',
                        type: "post",
                        data: {
                            chapter_name: function () {
                                return $("#chapter_name").val();
                            },
                            chapter_id: function () {
                                var venueId = $("#chapter_id").val();

                                if (typeof ($("#chapter_id").val()) != 'undefined')
                                {
                                    return $("#chapter_id").val();
                                } else {
                                    return '';
                                }

                            },
                        }

                    }
                },
                meetup_date: {
                    required: true,
                },
                zipcode: {
                    required: true,
                },
                address: {
                    required: true,
                },
                city: {
                    required: true,
                },
                state: {
                    required: true,
                }
//                phone_number: {
//                    required: true,
//                },
//                email: {
//                    required: true,
//                    email: true
//                },
//                website: {
//                    required: true
//                }
            },
            messages: {
                chapter_name: {
                    required: "Chapter name is required.",
                    remote: "Chapter with same name exists, Please use another name."
                },
                
                meetup_date: {
                    required: "Meetup date is required."
                },
                zipcode: {
                    required: "ZipCode is required."
                }, address: {
                    required: "Address is required."
                },
                city: {
                    required: "City is required."
                },
                state: {
                    required: "State is required."
                }
//                phone_number: {
//                    required: "Phone is required."
//                },
//                email: {
//                    required: "Email is required.",
//                    email: "Please enter a valid email address.",
//                },
//                website: {
//                    required: "Website is required."
//                }
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                Metronic.scrollTo(error1, -200);
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {
                form.submit();
            }


        });
    }
    
    //############### Handle HLAA Chapter edit form ##################################################
    var handleChapterEdit = function () {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

        var HlaaEditForm = $('#Edithlaa');
        var error1 = $('.alert-danger', HlaaEditForm);
        var success1 = $('.alert-success', HlaaEditForm);

        HlaaEditForm.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            onkeyup: false,
            ignore: "",
            rules: {
                chapter_name: {
                    required: true,
                },
                
                meetup_date: {
                    required: true,
                },
                zipcode: {
                    required: true,
                },
                address: {
                    required: true,
                },
                city: {
                    required: true,
                },
                state: {
                    required: true,
                },
                phone_number: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true
                },
                website: {
                    required: true
                }
            },
            messages: {
                chapter_name: {
                    required: "Chapter name is required.",
//                    remote: "Venue with same name exists, Please use another name."
                },
                
                meetup_date: {
                    required: "Meetup Date is required."
                },
                zipcode: {
                    required: "ZipCode is required."
                }, address: {
                    required: "Address is required."
                },
                city: {
                    required: "City is required."
                },
                state: {
                    required: "State is required."
                },
                phone_number: {
                    required: "Phone is required."
                },
                email: {
                    required: "Email is required.",
                    email: "Please enter a valid email address.",
                },
                website: {
                    required: "Website is required."
                }
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                Metronic.scrollTo(error1, -200);
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {
                form.submit();
            }


        });
    }

    //############### Handle User form ##################################################
    var handleUser = function () {
        // for more info visit the official plugin documentation: 
        // http://docs.jquery.com/Plugins/Validation

        var UserForm = $('#UserForm');
        var error1 = $('.alert-danger', UserForm);
        var success1 = $('.alert-success', UserForm);

        UserForm.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            onkeyup: false,
            ignore: "",
            rules: {
                first_name: {
                    required: true,
                },
                last_name: {
                    required: true,
                },
//                gender: {
//                    required: true,
//                },
                age: {
                    required: true,
                },
//                hearing_level: {
//                    required: true,
//                },
//                hearing_ad: {
//                    required: true,
//                },
                city: {
                    required: true,
                },
                state: {
                    required: true,
                },
                zipcode: {
                    required: true,
                }
            },
            messages: {
                first_name: {
                    required: "Fitst Name is required."
                },
                last_name: {
                    required: "Last Name is required."
                },
//                gender: {
//                    required: "Gender is required."
//                },
                age: {
                    required: "Age is required."
                },
//                hearing_level: {
//                    required: "Hearing Level is required."
//                },
//                hearing_ad: {
//                    required: "Hearing Aid is required."
//                },
                city: {
                    required: "City is required."
                },
                state: {
                    required: "State is required."
                },
                zipcode: {
                    required: "Zipcode is required."
                }
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                if (element.parent(".input-group").size() > 0) {
                    error.insertAfter(element.parent(".input-group"));
                } else if (element.attr("data-error-container")) {
                    error.appendTo(element.attr("data-error-container"));
                } else if (element.parents('.radio-list').size() > 0) {
                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                } else if (element.parents('.radio-inline').size() > 0) {
                    error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                } else if (element.parents('.checkbox-list').size() > 0) {
                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                } else if (element.parents('.checkbox-inline').size() > 0) {
                    error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                } else {
                    error.insertAfter(element); // for other inputs, just perform default behavior
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit              
                success1.hide();
                error1.show();
                Metronic.scrollTo(error1, -200);
            },
            highlight: function (element) { // hightlight error inputs
                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {

                form.submit();
            }


        });



    }
    return {
        //main function to initiate the module
        init: function () {
            handleChanePassword();
            handleAccountSettings();
            handleCategory();
            handleVenue();
            handleChapter();
            handleChapterEdit();
            handleUser();
            handleImportCSV();
        }

    };

}();