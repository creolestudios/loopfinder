$('#feedback-from').validate({
    rules: {
        comment: {
            required: true,
            minlength: 10},
        feed_name: {
            required: true,
            minlength: 4},
        email: {
            required: true,
            email: true
        }
    },
    messages:
            {
                comment: {
                    required: "Please Enter some comment.",
                    minlength: "Please enter atleast 10 words."

                },
                name: {
                    required: "Please enter name.",
                    minlength: "Please enter atleast 4 words.",
                },
                email: {
                    required: "Please Enter valid email.",
                    email: "Invalid email"

                }

            },
    submitHandler: function (form) {
        form.submit();
    }

});
