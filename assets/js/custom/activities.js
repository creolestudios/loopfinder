$(document).ready(function () {

    //generate data tables for checkins
    var checkin_table = $('#checkins_table').dataTable({
        "bLengthChange": true,
        "bProcessing": true,
        "bServerSide": true,
        "bSortCellsTop": true,
        "bAutoWidth": false,
        "aaSorting": [],
        "loadingMessage": 'Loading...',
        "sPaginationType": "full_numbers",
        "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ Check-In(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No Check-In(s) found.",
            "oPaginate": {
                "sFirst": "<<",
                "sLast": ">>",
                "sNext": ">",
                "sPrevious": "<",
            },
        },
        "sAjaxSource": BASEURL + "users/GetCheckins",
        "sServerMethod": "POST",
        "fnServerParams": function (aoData) {
            aoData.push({"name": "customer_id", "value": $("#customer_id").val()});
        },
        "aoColumns": [
            {"data": "checkin_date", "bSortable": true, "bSearchable": true, },
            {"data": "venue_name", "bSortable": true, "bSearchable": true, "mRender": function (data, type, full) {

                    //split user name to get id
                    var Temp = data.split("_");

                    return '<a href="' + BASEURL + 'venues/Details/' + base64_encode(Temp[1]) + '" class="" style="color:#0095D7">' + Temp[0] + " " + Temp[1] + '</a>';
                }},
            {"data": "venue_city", "bSortable": true, "bSearchable": true, },
            {"data": "venue_state", "bSortable": true, "bSearchable": true},
        ],
    });

    //generate data tables for votes up
    var votes_table = $('#votes_table').dataTable({
        "bLengthChange": true,
        "bProcessing": true,
        "bServerSide": true,
        "bSortCellsTop": true,
        "bAutoWidth": false,
        "aaSorting": [],
        "loadingMessage": 'Loading...',
        "sPaginationType": "full_numbers",
        "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ Vote(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No Vote(s) found.",
            "oPaginate": {
                "sFirst": "<<",
                "sLast": ">>",
                "sNext": ">",
                "sPrevious": "<",
            },
        },
        "sAjaxSource": BASEURL + "users/GetVotes",
        "sServerMethod": "POST",
        "fnServerParams": function (aoData) {
            aoData.push({"name": "customer_id", "value": $("#customer_id").val()});
        },
        "aoColumns": [
            {"data": "vote_date", "bSortable": true, "bSearchable": false},
            {"data": "venue_name", "bSortable": true, "bSearchable": true, "mRender": function (data, type, full) {

                    //split user name to get id
                    var Temp = data.split("_");

                    return '<a href="' + BASEURL + 'venues/Details/' + base64_encode(Temp[1]) + '" class="" style="color:#0095D7">' + Temp[0] + " " + Temp[1] + '</a>';
                }},
            {"data": "venue_city", "bSortable": true, "bSearchable": false},
            {"data": "venue_state", "bSortable": true, "bSearchable": false},
        ],
    });

    //generate data tables for activities up
    var votes_table = $('#activity_table').dataTable({
        "bLengthChange": true,
        "bProcessing": true,
        "bServerSide": true,
        "bSortCellsTop": true,
        "aaSorting": [],
        "bAutoWidth": false,
        "loadingMessage": 'Loading...',
        "sPaginationType": "full_numbers",
        "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ Activity(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No Activity(s) found.",
            "oPaginate": {
                "sFirst": "<<",
                "sLast": ">>",
                "sNext": ">",
                "sPrevious": "<",
            },
        },
        "sAjaxSource": BASEURL + "users/GetActivities",
        "sServerMethod": "POST",
        "fnServerParams": function (aoData) {
            aoData.push({"name": "customer_id", "value": $("#customer_id").val()});
        },
        "aoColumns": [
            {"data": "checkin_date", "bSortable": true, "bSearchable": true},
            {"data": "venue_name", "bSortable": true, "bSearchable": true, "mRender": function (data, type, full) {

                    //split user name to get id
                    var Temp = data.split("_");

                    return '<a href="' + BASEURL + 'venues/Details/' + base64_encode(Temp[1]) + '" class="" style="color:#0095D7">' + Temp[0] + " " + Temp[1] + '</a>';
                }},
            {"data": "venue_city", "bSortable": true, "bSearchable": true},
            {"data": "venue_state", "bSortable": true, "bSearchable": true},
            {"data": "activity_type", "bSortable": true, "bSearchable": true},
        ],
    });

    //generate data tables for feedback up
    var Feedback_table = $('#feedback_table').dataTable({
        "bLengthChange": true,
        "bProcessing": true,
        "bServerSide": true,
        "bSortCellsTop": true,
        "aaSorting": [],
        "bAutoWidth": false,
        "loadingMessage": 'Loading...',
        "sPaginationType": "full_numbers",
        "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ Feedback(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No Feedback(s) found.",
            "oPaginate": {
                "sFirst": "<<",
                "sLast": ">>",
                "sNext": ">",
                "sPrevious": "<",
            },
        },
        "sAjaxSource": BASEURL + "users/GetFeedbacks",
        "sServerMethod": "POST",
        "fnServerParams": function (aoData) {
            aoData.push({"name": "customer_id", "value": $("#customer_id").val()});
        },
        "aoColumns": [
            {"data": "feedback_date", "bSortable": true, "bSearchable": true},
            {"data": "venue_name", "bSortable": true, "bSearchable": true, "mRender": function (data, type, full) {

                    //split user name to get id
                    var Temp = data.split("_");

                    return '<a href="' + BASEURL + 'venues/Details/' + base64_encode(Temp[1]) + '" class="" style="color:#0095D7">' + Temp[0] + " " + Temp[1] + '</a>';
                }},
            {"data": "loop_room_name", "bSortable": true, "bSearchable": true},
            {"data": "venue_city", "bSortable": true, "bSearchable": true},
            {"data": "venue_state", "bSortable": true, "bSearchable": true},
            {"data": "satisfy", "bSortable": true, "bSearchable": true},
            {"data": "reason", "bSortable": false, "bSearchable": true},
            {"data": "comment", "bSortable": false, "bSearchable": true},
        ],
    });

    //generate data tables for feedback up
    var Request_table = $('#request_table').dataTable({
        "bLengthChange": true,
        "bProcessing": true,
        "bServerSide": true,
        "bSortCellsTop": true,
        "aaSorting": [],
        "bAutoWidth": false,
        "loadingMessage": 'Loading...',
        "sPaginationType": "full_numbers",
        "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ Request(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No Request(s) found.",
            "oPaginate": {
                "sFirst": "<<",
                "sLast": ">>",
                "sNext": ">",
                "sPrevious": "<",
            },
        },
        "sAjaxSource": BASEURL + "users/GetRequests",
        "sServerMethod": "POST",
        "fnServerParams": function (aoData) {
            aoData.push({"name": "customer_id", "value": $("#customer_id").val()});
        },
        "aoColumns": [
            {"data": "request_date", "bSortable": true, "bSearchable": true},
            {"data": "venue_name", "bSortable": true, "bSearchable": true},
            {"data": "venue_city", "bSortable": true, "bSearchable": true},
            {"data": "venue_state", "bSortable": true, "bSearchable": true},
            {"data": "comment", "bSortable": false, "bSearchable": true},
        ],
    });
    $('.dataTables_filter').css("display", "none");
});
