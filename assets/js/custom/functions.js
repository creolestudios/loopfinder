//funciton to change modifier status and UI
ChangeCustomerStatus = function (path, parsed_data, status, customer_id, row_id) {

    //call to ajax function
    $.post(path, {
        data: parsed_data
    },
    function (res) {
        handleResponseChangeCustomerStatus(res, status, customer_id, row_id);
    }, 'json');
};

//funciton to delete modifier
DeleteCategory = function (path, parsed_data, category_id, Obj) {

    //call to ajax function
    $.post(path, {
        data: parsed_data
    },
    function (res) {
        handleResponseDeleteCategory(res, category_id, Obj);
    }, 'json');
};

DeleteVenueRoom = function (path, parsed_data, room_id, VenueId, Obj) {

    //call to ajax function
    $.post(path, {
        data: parsed_data
    },
    function (res) {
        handleResponseDeleteVenueRoom(res, room_id, VenueId, Obj);
    }, 'json');
};

DeleteVenueImage = function (path, parsed_data, image_id, Obj) {

    //call to ajax function
    $.post(path, {
        data: parsed_data
    },
    function (res) {
        handleResponseDeleteVenueImage(res, image_id, Obj);
    }, 'json');
};

DeleteChapterImage = function (path, parsed_data, image_id, Obj) {

    //call to ajax function
    $.post(path, {
        data: parsed_data
    },
    function (res) {
        handleResponseDeleteChapterImage(res, image_id, Obj);
    }, 'json');
};

//function to add more category
AddMoreLoopRooms = function (path, parsed_data) {
    //call to ajax function
    $.post(path, {
        data: parsed_data
    },
    function (res) {
        handleResponseAddMoreLoopRooms(res);
    }, 'json');
};

GetZipCodeDetails = function (path, parsed_data) {
    //call to ajax function
    $.post(path, {
        data: parsed_data
    },
    function (res) {
        handleZipCodeDetails(res);
    }, 'json');
};
//funciton to delete customer
DeleteCustomer = function (path, parsed_data, customer_id, Obj) {

    //call to ajax function
    $.post(path, {
        data: parsed_data
    },
    function (res) {
        handleResponseDeleteCustomer(res, customer_id, Obj);
    }, 'json');
};
//funciton to delete venue
DeleteVenue = function (path, parsed_data, venue_id, Obj) {

    //call to ajax function
    $.post(path, {
        data: parsed_data
    },
    function (res) {
        handleResponseDeleteVenue(res, venue_id, Obj);
    }, 'json');
};
//funciton to delete chapter
DeleteChapter = function (path, parsed_data, chapter_id, Obj) {
    
    //call to ajax function
    $.post(path, {
        data: parsed_data
    },
    function (res) {
        handleResponseDeleteChapter(res, chapter_id, Obj); 
    }, 'json');
};
//funciton to delete request for 
DeleteRequest = function (path, parsed_data, request_id, Obj) {

    //call to ajax function
    $.post(path, {
        data: parsed_data
    },
    function (res) { 
        handleResponseDeleteRequest(res, request_id, Obj);
    }, 'json');
};

DeleteFeedback = function (path, parsed_data, feedback_id, Obj) {

    //call to ajax function
    $.post(path, {
        data: parsed_data
    },
    function (res) {
        handleResponseDeleteFeedback(res, feedback_id, Obj);
    }, 'json');
};

AddArchive = function (path, parsed_data, request_id, Obj, Status) {

    //call to ajax function
    $.post(path, {
        data: parsed_data
    },
    function (res) {
        handleResponseAddArchive(res, request_id, Obj, Status);
    }, 'json');
};

AddArchiveFeedback = function (path, parsed_data, feedback_id, Obj, Status) {

    //call to ajax function
    $.post(path, {
        data: parsed_data
    },
    function (res) {
        handleResponseAddArchiveFeedback(res, feedback_id, Obj, Status);
    }, 'json');
};
//function to verify venue room
VerifyVenueRoom = function (path, parsed_data, room_id, Obj) {

    //call to ajax function
    $.post(path, {
        data: parsed_data
    },
    function (res) {
        handleResponseVerifyRoom(res, room_id, Obj);
    }, 'json');
};
//function to handle venue conversion from requested to register
RegisterVenue = function (path, parsed_data, venue_id, Obj) {

    //call to ajax function
    $.post(path, {
        data: parsed_data
    },
    function (res) {
        handleResponseRegisterVenue(res, venue_id, Obj);
    }, 'json');
};
//function to handle loop room updates
UpdateLoopedRooms = function (path, parsed_data, RoomId, RoomName, Obj) {
    $("#" + RoomId).addClass('spinner');
    //call to ajax function
    $.post(path, {
        data: parsed_data
    },
    function (res) {
        handleResponseUpdateLoopedRooms(res, RoomId, RoomName, Obj);
    }, 'json');
};
//set default image
SetDefauleImage = function (path, parsed_data, venue_id, Obj) {

    //call to ajax function
    $.post(path, {
        data: parsed_data
    },
    function (res) {
        handleResponseSetDefault(res, venue_id, Obj);
    }, 'json');
};

//set default image2
SetDefauleImage2 = function (path, parsed_data, chapter_id, Obj) {

    //call to ajax function
    $.post(path, {
        data: parsed_data
    },
    function (res) { 
        handleResponseSetDefault2(res, chapter_id, Obj);
    }, 'json');
};

SendNotificationResponse = function (path, parsed_data) {
    //call to ajax function
    $.post(path, {
        data: parsed_data
    },
    function (res) {
        //  handleResponseUpdateLoopedRooms(res, RoomId, RoomName, Obj);
    }, 'json');
};
/*********************************************************
 HENDLER FUNCTIONS
 **********************************************************/
/**
 * [base64_encode ENCRYPTION]
 * @param  {[STR]} data [description]
 * @return {[STR]}      [description]
 */
function base64_encode(data) {
    var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
            ac = 0,
            enc = "",
            tmp_arr = [];

    if (!data) {
        return data;
    }

    do { // pack three octets into four hexets
        o1 = data.charCodeAt(i++);
        o2 = data.charCodeAt(i++);
        o3 = data.charCodeAt(i++);

        bits = o1 << 16 | o2 << 8 | o3;

        h1 = bits >> 18 & 0x3f;
        h2 = bits >> 12 & 0x3f;
        h3 = bits >> 6 & 0x3f;
        h4 = bits & 0x3f;

        // use hexets to index into b64, and append result to encoded string
        tmp_arr[ac++] = b64.charAt(h1) + b64.charAt(h2) + b64.charAt(h3) + b64.charAt(h4);
    } while (i < data.length);

    enc = tmp_arr.join('');

    var r = data.length % 3;

    return (r ? enc.slice(0, r - 3) : enc) + '==='.slice(r || 3);

}

/**
 * [handleResponseChangeModifierStatus change statuss]
 * @param  {[type]} res [edit view]
 * @return {[type]}     [description]
 */
function handleResponseChangeCustomerStatus(res, status, customer_id, row_id) {

    //assign changes

    if ($.trim(res) != '') {
        //show message
        if (status == "1")
        {
            $(".alert-success").removeClass("hide").html(res.message).slideDown(800).delay(5000).slideUp(800);
            $("#" + row_id).removeClass("yellow").addClass("green");
            $("#" + row_id).text('Active');
            $("#" + row_id).attr('data-attr-change', '0');
            $("#" + row_id).attr('title', 'Click to Inactive');
            //
        } else {
            $(".alert-success").removeClass("hide").html(res.message).slideDown(800).delay(3500).slideUp(800);
            $("#" + row_id).removeClass("green").addClass("yellow");
            $("#" + row_id).text('Inactive');
            $("#" + row_id).attr('data-attr-change', '1');
            $("#" + row_id).attr('title', 'Click to Active');
        }
    }
}

/**
 * [handleResponseDeleteModifier Load edit mode]
 * @param  {[type]} res [edit view]
 * @return {[type]}     [description]
 */
function handleResponseDeleteCategory(res, customer_id, Obj) {

    if ($.trim(res) == '') {
        alert('Something went wrong!');
        return false;
    } else {
        toastr.success(res.message, 'Categories');
        Obj.closest('tr').fadeOut('slow');

    }
}

function handleResponseDeleteVenueRoom(res, room_id, VenueId, Obj) {

    if ($.trim(res) == '') {
        toastr.error('Something went wrong!', 'Venues');
        return false;
    } else {
        toastr.success(res.message, 'Venues');
        $("#main_loop_div_" + room_id).fadeOut('slow');
        window.setTimeout(function () {
            window.location.href = BASEURL + 'venues/EditVenue/' + base64_encode(VenueId);
        }, 1500);

    }
}

function handleResponseDeleteVenueImage(res, image_id, Obj) {

    if ($.trim(res) == '') {
        toastr.error('Something went wrong!', 'Venues');
        return false;
    } else {
        toastr.success(res.message, 'Venues');
        $("#main_image_div_" + image_id).fadeOut('slow');

    }
}
function handleResponseDeleteChapterImage(res, image_id, Obj) {

    if ($.trim(res) == '') {
        toastr.error('Something went wrong!', 'Venues');
        return false;
    } else {
        toastr.success(res.message, 'hlaachapters');
        $("#main_image_div_" + image_id).fadeOut('slow');

    }
}
/**
 * [handleResponseDeleteModifier Load edit mode]
 * @param  {[type]} res [edit view]
 * @return {[type]}     [description]
 */
function handleResponseDeleteCustomer(res, customer_id, Obj) {

    if ($.trim(res) == '') {
        toastr.error('Something went wrong!', 'Users');
        return false;
    } else {
        toastr.success(res.message, 'Users');
        window.setTimeout(function () {
            window.location.href = BASEURL + 'users/';
        }, 2500);

    }
}
/**
 * [handleResponseDeleteModifier Load edit mode]
 * @param  {[type]} res [edit view]
 * @return {[type]}     [description]
 */
function handleResponseDeleteVenue(res, venue_id, Obj) {

    if ($.trim(res) == '') {
        alert('Something went wrong!');
        return false;
    } else {
        toastr.success(res.message, 'Venues');
        window.setTimeout(function () {
            window.location.href = BASEURL + 'venues/';
        }, 2500);

    }
}


function handleResponseDeleteChapter(res, chapter_id, Obj) {
    
    if ($.trim(res) == '') {
        alert('Something went wrong!');
        return false;
    } else {
        toastr.success(res.message, 'Hlaachapters');
        window.setTimeout(function () {
            window.location.href = BASEURL + 'hlaachapters/';
        }, 2500);

    }
}
/**
 * [handleResponseDeleteModifier Load edit mode]
 * @param  {[type]} res [edit view]
 * @return {[type]}     [description]
 */
function handleResponseDeleteRequest(res, request_id, Obj) {

    if ($.trim(res) == '') {
        toastr.error('Something went wrong!', 'Request');
        return false;
    } else {
        toastr.success(res.message, 'Request');
        window.setTimeout(function () {
            window.location.href = BASEURL + 'requestmessage/';
        }, 2500);

    }
}

/**
 * [handleResponseDeleteModifier Load edit mode]
 * @param  {[type]} res [edit view]
 * @return {[type]}     [description]
 */
function handleResponseDeleteFeedback(res, feedback_id, Obj) {

    if ($.trim(res) == '') {
        toastr.error('Something went wrong!', 'Feedback');
        return false;
    } else {
        toastr.success(res.message, 'Request');
        window.setTimeout(function () {
            window.location.href = BASEURL + 'feedback/';
        }, 2500);

    }
}

function handleResponseAddArchive(res, request_id, Obj, Status) {
    if ($.trim(res) == '') {
        toastr.error('Something went wrong!', 'Request');
        return false;
    } else {
        toastr.success(res.message, 'Request');
        if (Status == '2')
        {
            window.setTimeout(function () {
                window.location.href = BASEURL + 'requestmessage/Details/' + base64_encode(request_id) + '/action/archive';
            }, 1000);
        } else {
            window.setTimeout(function () {
                window.location.href = BASEURL + 'requestmessage/Details/' + base64_encode(request_id);
            }, 1000);
        }

    }
}

function handleResponseAddArchiveFeedback(res, feedback_id, Obj, Status) {
    if ($.trim(res) == '') {
        toastr.error('Something went wrong!', 'Feedback');
        return false;
    } else {
        toastr.success(res.message, 'Feedback');

        if (Status == '2')
        {
            window.setTimeout(function () {
                window.location.href = BASEURL + 'feedback/Details/' + base64_encode(feedback_id) + '/action/archive';
            }, 1000);
        } else {
            window.setTimeout(function () {
                window.location.href = BASEURL + 'feedback/Details/' + base64_encode(feedback_id);
            }, 1000);
        }
    }
}


/**
 * [handleResponseDeleteModifier Load edit mode]
 * @param  {[type]} res [edit view]
 * @return {[type]}     [description]
 */
function handleResponseVerifyRoom(res, room_id, Obj) {

    if ($.trim(res) == '') {
        toastr.error('Something went wrong!', 'Venue');
        return false;
    } else {
        if (res.status == '1')
        {
            toastr.success(res.message, 'Venue');
            //update text in span
            $("#span_room_" + room_id).html('Last Verified(' + res.date_verified + ')');
        } else {
            toastr.warning(res.message, 'Venue');
        }
    }
}
/**
 * [handleResponseDeleteModifier Load edit mode]
 * @param  {[type]} res [edit view]
 * @return {[type]}     [description]
 */
function handleResponseRegisterVenue(res, venue_id, Obj) {
    if ($.trim(res) == '') {
        toastr.error('Something went wrong!', 'Venue');
        return false;
    } else {
        if (res.status == '1')
        {
            toastr.success(res.message, 'Venue');
            window.setTimeout(function () {
                window.location.href = BASEURL + 'venues/Details/' + base64_encode(venue_id);
            }, 2500);
        } else {
            toastr.warning(res.message, 'Venue');
        }
    }
}

function handleResponseUpdateLoopedRooms(res, room_id, Obj)
{
    $("#" + room_id).removeClass('spinner');
    if ($.trim(res) == '') {
        toastr.error('Something went wrong!', 'Venue');
        return false;
    } else {
        if (res.status == '1')
        {
            toastr.success(res.message, 'Venue');
        } else {
            toastr.warning(res.message, 'Venue');
        }
    }
}
function handleResponseSetDefault(res, venue_id, Obj) {
    if ($.trim(res) == '') {
        toastr.error('Something went wrong!', 'Venue');
        return false;
    } else {
        if (res.status == '1')
        {
            toastr.success(res.message, 'Venue');
            window.setTimeout(function () {
                window.location.href = BASEURL + 'venues/Details/' + base64_encode(venue_id);
            }, 2500);

        } else {
            toastr.warning(res.message, 'Venue');
        }
    }
}


function handleResponseSetDefault2(res, chapter_id, Obj) {
   
    if ($.trim(res) == '') {
        toastr.error('Something went wrong!', 'HLAA Chapters');
        return false;
    } else {
        if (res.status == '1')
        {
            toastr.success(res.message, 'HLAA Chapters');
            window.setTimeout(function () {
                window.location.href = BASEURL + 'hlaachapters/Details/' + base64_encode(chapter_id);
            }, 2500);

        } else {
            toastr.warning(res.message, 'HLAA Chapters');
        }
    }
}
/**
 * [handleResponseAddMoreCategory Load edit mode]
 * @param  {[type]} res [edit view]
 * @return {[type]}     [description]
 */
function handleResponseAddMoreLoopRooms(res) {
    if ($.trim(res) == '') {
        alert('Something went wrong!');
        return false;
    } else {
        $("#div_append_looped_rooms").append(res);
    }
}
/**
 * [handleResponseAddMoreCategory Load edit mode]
 * @param  {[type]} res [edit view]
 * @return {[type]}     [description]
 */
function handleZipCodeDetails(res) {
    if ($.trim(res) == '') {
        alert('Something went wrong!');
        return false;
    } else {
        console.log(res);
      
        //if data found then assign to state and city
        if (res.status)
        {
            //fetch zipcode data and assign to state and city text field
            $("#city").val(res.zipcode[0].city);
            $("#state").val(res.zipcode[0].state_code);
        } else {
            //$("#city").val('');
           // $("#state").val('');
        }
        //$("#div_append_looped_rooms").append(res);
    }
}