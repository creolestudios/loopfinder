$(document).ready(function(){
   //Hlaa Chapters table
    //############# Total request message tables
    var Register_Venue_Table = $('#hlaa_chapters').dataTable({
        "bLengthChange": true,
        "bProcessing": true,
        "bServerSide": true,
        "bSortCellsTop": true,
        "aaSorting": [],
        "loadingMessage": 'Loading...',
        "sPaginationType": "full_numbers",
        "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ Registered Venue(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No Venues(s) found.",
            "oPaginate": {
                "sFirst": "<<",
                "sLast": ">>",
                "sNext": ">",
                "sPrevious": "<",
            },
        },
        "sAjaxSource": BASEURL + "venues/GetRegisterVenues",
        "sServerMethod": "POST",
        "fnServerParams": function (aoData) {
            aoData.push(
                    {"name": "filter_category", "value": $('#filter_category').val()},
            {"name": "venue_name", "value": $('#venue_name').val()},
            {"name": "city", "value": $('#search_city').val()},
            {"name": "state", "value": $('#state').val()},
            {"name": "zipcode", "value": $('#zipcode').val()}

            );
        },
        "aoColumns": [
            {"data": "venue_name", "bSortable": true, "bSearchable": true},
            {"data": "category_name", "bSortable": true, "bSearchable": true},
            {"data": "venue_city", "bSortable": true, "bSearchable": true},
            {"data": "venue_state", "bSortable": true, "bSearchable": true},
            {"data": "zipcode", "bSortable": true, "bSearchable": true},
            {"data": "checkins", "bSortable": true, "bSearchable": true},
            {"data": "feedbacks", "bSortable": true, "bSearchable": true},
            {"data": "detail", "bSearchable": false, "bSortable": false, "mRender": function (data, type, full) {

                    return '<a href="' + BASEURL + 'venues/Details/' + base64_encode(data) + '" class="btn btn-circle blue"><i class="fa fa-eye"></i> View </a>';
                }}
        ],
    });

    $('#filter_category').change(function () {
        Register_Venue_Table.fnFilter()
    });
    $('#venue_name').change(function () {
        Register_Venue_Table.fnFilter()
    });
    $('#search_city').change(function () {
        Register_Venue_Table.fnFilter()
    });

    $('#state').change(function (event) {
        Register_Venue_Table.fnFilter()
    });
    $('#zipcode').change(function (event) {
        Register_Venue_Table.fnFilter()
    }); 
});