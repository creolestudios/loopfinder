$(document).ready(function () {

    //generate data tables for checkins
    var checkin_table = $('#checkins_table').dataTable({
        "bLengthChange": true,
        "bProcessing": true,
        "bServerSide": true,
        "bSortCellsTop": true,
        "aaSorting": [],
        "bAutoWidth": false,
        "loadingMessage": 'Loading...',
        "sPaginationType": "full_numbers",
        "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ Check-in(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No Check-in(s) found.",
            "oPaginate": {
                "sFirst": "<<",
                "sLast": ">>",
                "sNext": ">",
                "sPrevious": "<",
            },
        },
        "sAjaxSource": BASEURL + "venues/GetCheckins",
        "sServerMethod": "POST",
        "fnServerParams": function (aoData) {
            aoData.push({"name": "venue_id", "value": $("#hdvenueid").val()});
        },
        "aoColumns": [
            {"data": "checkin_date", "bSortable": true, "bSearchable": true},
            {"data": "customer_name", "bSortable": true, "bSearchable": false, "mRender": function (data, type, full) {

                    //split user name to get id
                    var Temp = data.split(" ");

                    return '<a href="' + BASEURL + 'users/Details/' + base64_encode(Temp[2]) + '" class="" style="color:#0095D7">' + Temp[0] + " " + Temp[1] + '</a>';
                }},
            {"data": "customer_city", "bSortable": true, "bSearchable": true},
            {"data": "customer_state", "bSortable": true, "bSearchable": true},
            {"data": "customer_zip", "bSortable": true, "bSearchable": true},
        ],
    });



    //generate data tables for feedback up
    var Feedback_table = $('#feedback_table').dataTable({
        "bLengthChange": true,
        "bProcessing": true,
        "bServerSide": true,
        "bSortCellsTop": true,
        "aaSorting": [],
        "bAutoWidth": false,
        "loadingMessage": 'Loading...',
        "sPaginationType": "full_numbers",
        "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ Feedback(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No Feedback(s) found.",
            "oPaginate": {
                "sFirst": "<<",
                "sLast": ">>",
                "sNext": ">",
                "sPrevious": "<",
            },
        },
        "sAjaxSource": BASEURL + "venues/GetFeedbacks",
        "sServerMethod": "POST",
        "fnServerParams": function (aoData) {
            aoData.push({"name": "venue_id", "value": $("#hdvenueid").val()});
            aoData.push({"name": "venue_room_filter", "value": $("#venue_room_filter").val()});
        },
        "aoColumns": [
            {"data": "feedback_date", "bSortable": true, "bSearchable": true},
            {"data": "customer_name", "bSortable": true, "bSearchable": false, "mRender": function (data, type, full) {

                    //split user name to get id
                    var Temp = data.split("_");
                    return '<a href="' + BASEURL + 'users/Details/' + base64_encode(Temp[1]) + '" class="" style="color:#0095D7">' + Temp[0] + '</a>';
                }},
            {"data": "customer_email", "bSortable": true, "bSearchable": true},
            {"data": "loop_room_name", "bSortable": true, "bSearchable": true},
            {"data": "satisfy", "bSortable": true, "bSearchable": true},
            {"data": "reason", "bSortable": false, "bSearchable": true},
            {"data": "comment", "bSortable": false, "bSearchable": true},
        ],
    });
    $('#venue_room_filter').change(function () {
        Feedback_table.fnFilter()
    });

    //votes table
    //generate data tables for votes up
    var votes_table = $('#votes_table').dataTable({
        "bLengthChange": true,
        "bProcessing": true,
        "bServerSide": true,
        "bSortCellsTop": true,
        "aaSorting": [],
        "bAutoWidth": false,
        "loadingMessage": 'Loading...',
        "sPaginationType": "full_numbers",
        "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ Vote(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No Vote(s) found.",
            "oPaginate": {
                "sFirst": "<<",
                "sLast": ">>",
                "sNext": ">",
                "sPrevious": "<",
            },
        },
        "sAjaxSource": BASEURL + "venues/GetVotes",
        "sServerMethod": "POST",
        "fnServerParams": function (aoData) {
            aoData.push({"name": "venue_id", "value": $("#hdvenueid").val()});
        },
        "aoColumns": [
            {"data": "vote_date", "bSortable": true, "bSearchable": false},
            {"data": "customer_name", "bSortable": true, "bSearchable": false, "mRender": function (data, type, full) {

                    //split user name to get id
                    var Temp = data.split(" ");

                    return '<a href="' + BASEURL + 'users/Details/' + base64_encode(Temp[2]) + '" class="" style="color:#0095D7">' + Temp[0] + " " + Temp[1] + '</a>';
                }},
            {"data": "customer_city", "bSortable": true, "bSearchable": false},
            {"data": "customer_state", "bSortable": true, "bSearchable": false},
            {"data": "customer_zip", "bSortable": true, "bSearchable": false},
        ],
    });

    //register venue table
    //############# Total request message tables
    var Register_Venue_Table = $('#register_venue_table').dataTable({
        "bLengthChange": true,
        "bProcessing": true,
        "bServerSide": true,
        "bSortCellsTop": true,
        "aaSorting": [],
        "loadingMessage": 'Loading...',
        "sPaginationType": "full_numbers",
        "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ Registered Venue(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No Venues(s) found.",
            "oPaginate": {
                "sFirst": "<<",
                "sLast": ">>",
                "sNext": ">",
                "sPrevious": "<",
            },
        },
        "sAjaxSource": BASEURL + "venues/GetRegisterVenues",
        "sServerMethod": "POST",
        "fnServerParams": function (aoData) {
            aoData.push(
                    {"name": "filter_category", "value": $('#filter_category').val()},
            {"name": "venue_name", "value": $('#venue_name').val()},
            {"name": "city", "value": $('#search_city').val()},
            {"name": "state", "value": $('#state').val()},
            {"name": "zipcode", "value": $('#zipcode').val()}

            );
        },
        "aoColumns": [
            {"data": "venue_name", "bSortable": true, "bSearchable": true},
            {"data": "category_name", "bSortable": true, "bSearchable": true},
            {"data": "venue_city", "bSortable": true, "bSearchable": true},
            {"data": "venue_state", "bSortable": true, "bSearchable": true},
            {"data": "zipcode", "bSortable": true, "bSearchable": true},
            {"data": "checkins", "bSortable": true, "bSearchable": true},
            {"data": "feedbacks", "bSortable": true, "bSearchable": true},
            {"data": "detail", "bSearchable": false, "bSortable": false, "mRender": function (data, type, full) {

                    return '<a href="' + BASEURL + 'venues/Details/' + base64_encode(data) + '" class="btn btn-circle blue"><i class="fa fa-eye"></i> View </a>';
                }}
        ],
    });

    $('#filter_category').change(function () {
        Register_Venue_Table.fnFilter()
    });
    $('#venue_name').change(function () {
        Register_Venue_Table.fnFilter()
    });
    $('#search_city').change(function () {
        Register_Venue_Table.fnFilter()
    });

    $('#state').change(function (event) {
        Register_Venue_Table.fnFilter()
    });
    $('#zipcode').change(function (event) {
        Register_Venue_Table.fnFilter()
    });

    //request venue table
    //############# Total request message tables
    var Request_Venue_Table = $('#request_venue_table').dataTable({
        "bLengthChange": true,
        "bProcessing": true,
        "bServerSide": true,
        "bSortCellsTop": true,
        "aaSorting": [[5, 'desc']],
        "loadingMessage": 'Loading...',
        "sPaginationType": "full_numbers",
        "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ Requested Venue(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No Venue(s) found.",
            "oPaginate": {
                "sFirst": "<<",
                "sLast": ">>",
                "sNext": ">",
                "sPrevious": "<",
            },
        },
        "sAjaxSource": BASEURL + "venues/GetRequestVenues",
        "sServerMethod": "POST",
        "fnServerParams": function (aoData) {
            aoData.push(
                    {"name": "filter_category", "value": $('#req_filter_category').val()},
            {"name": "venue_name", "value": $('#req_venue_name').val()},
            {"name": "city", "value": $('#req_search_city').val()},
            {"name": "state", "value": $('#req_state').val()},
            {"name": "zipcode", "value": $('#req_zipcode').val()}

            );
        },
        "aoColumns": [
            {"data": "venue_name", "bSortable": true, "bSearchable": true},
            {"data": "category_name", "bSortable": true, "bSearchable": true},
            {"data": "venue_city", "bSortable": true, "bSearchable": true},
            {"data": "venue_state", "bSortable": true, "bSearchable": true},
            {"data": "zipcode", "bSortable": true, "bSearchable": true},
            {"data": "votes", "bSortable": true, "bSearchable": true},
            {"data": "detail", "bSearchable": false, "bSortable": false, "mRender": function (data, type, full) {

                    return '<a href="' + BASEURL + 'venues/Details/' + base64_encode(data) + '" class="btn btn-circle blue"><i class="fa fa-eye"></i> View </a>';
                }}
        ],
    });

    $('#req_filter_category').change(function () {
        Request_Venue_Table.fnFilter()
    });
    $('#req_venue_name').change(function () {
        Request_Venue_Table.fnFilter()
    });
    $('#req_search_city').change(function () {
        Request_Venue_Table.fnFilter()
    });
    $('#req_state').change(function (event) {
        Request_Venue_Table.fnFilter()
    });
    $('#req_zipcode').change(function (event) {
        Request_Venue_Table.fnFilter()
    });


    //all venue table
    //############# Total request message tables
    var All_Venue_Table = $('#all_venue_table').dataTable({
        "bLengthChange": true,
        "bProcessing": true,
        "bServerSide": true,
        "bSortCellsTop": true,
        "aaSorting": [],
        "loadingMessage": 'Loading...',
        "sPaginationType": "full_numbers",
        "oLanguage": {"sInfo": "Showing _START_ to _END_ of _TOTAL_ Venue(s)", "sSearch": "", "sInfoEmpty": "", "sProcessing": "Please Wait...", "sZeroRecords": "No venue(s) found.",
            "oPaginate": {
                "sFirst": "<<",
                "sLast": ">>",
                "sNext": ">",
                "sPrevious": "<",
            },
        },
        "sAjaxSource": BASEURL + "venues/GetAllVenues",
        "sServerMethod": "POST",
        "fnServerParams": function (aoData) {
            aoData.push(
                    {"name": "filter_category", "value": $('#all_filter_category').val()},
            {"name": "venue_name", "value": $('#all_venue_name').val()},
            {"name": "venue_type", "value": $('#all_filter_venue_type').val()},
            {"name": "city", "value": $('#all_search_city').val()},
            {"name": "state", "value": $('#all_state').val()},
            {"name": "zipcode", "value": $('#all_zipcode').val()}

            );
        },
        "aoColumns": [
            {"data": "venue_name", "bSortable": true, "bSearchable": true},
            {"data": "category_name", "bSortable": true, "bSearchable": true},
            {"data": "venue_city", "bSortable": true, "bSearchable": true},
            {"data": "venue_state", "bSortable": true, "bSearchable": true},
            {"data": "zipcode", "bSortable": true, "bSearchable": true},
            {"data": "detail", "bSearchable": false, "bSortable": false, "mRender": function (data, type, full) {

                    return '<a href="' + BASEURL + 'venues/Details/' + base64_encode(data) + '" class="btn btn-circle blue"><i class="fa fa-eye"></i> View </a>';
                }}
        ],
    });

    $('#all_filter_category').change(function () {
        All_Venue_Table.fnFilter()
    });
    $('#all_venue_name').change(function () {
        All_Venue_Table.fnFilter()
    });
    $('#all_phone').change(function () {
        All_Venue_Table.fnFilter()
    });
    $('#all_search_city').change(function () {
        All_Venue_Table.fnFilter()
    });
    $('#all_state').change(function (event) {
        All_Venue_Table.fnFilter()
    });
    $('#all_zipcode').change(function (event) {
        All_Venue_Table.fnFilter()
    });
    $('#all_filter_venue_type').change(function (event) {
        All_Venue_Table.fnFilter()
    });


    $('.dataTables_filter').css("display", "none");
});
