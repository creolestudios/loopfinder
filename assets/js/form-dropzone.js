var minImageWidth = 2560,
        minImageHeight = 1440;

var FormDropzone = function () {
  
    return {
        //main function to initiate the module
        init: function () {

            Dropzone.options.myDropzone = {
                autoDiscover: false,
                maxFilesize: 500,
                url: BASEURL + 'venues/MasterUpload',
                acceptedFiles: 'image/*',
                init: function () {
                    this.on("addedfile", function (file) {
                        //get value of total uploaded file
                        var TotalImages = parseInt($("#image_count").val());
                        
                        // Create the remove button
                        var removeButton = Dropzone.createElement("<button class='btn btn-sm btn-block'>Remove file</button>");

                        // Capture the Dropzone instance as closure.
                        var _this = this;

                        // Listen to the click event
                        removeButton.addEventListener("click", function (e) {
                            // Make sure the button click doesn't submit the form:
                            e.preventDefault();
                            e.stopPropagation();
                            var ServerResponse = file.xhr;

                            if (typeof (ServerResponse) != 'undefined')
                            {
                                //call ajax to remove image from temp folder
                                var path = BASEURL + 'venues/RemoveImageTemp';

                                //call ajax
                                $.post(path, {
                                    image: file.xhr.response
                                },
                                function (res) {
                                    //response handler

                                }, 'json');

                                //remove values from hidden field
                                var list = $('#hdimages').val();
                                var values = list.split(',');
                                var value = file.xhr.response;
                                for (var i = 0; i < values.length; i++) {
                                    if (values[i] == value) {
                                        values.splice(i, 1);
                                        //
                                        $('#hdimages').val(values.join(','));
                                    }
                                }
                            }

                            // Remove the file preview.
                            _this.removeFile(file);
                            // If you want to the delete the file on the server as well,
                            // you can do the AJAX request here.
                        });

                        // Add the button to the file preview element.
                        file.previewElement.appendChild(removeButton);
                    });

                    this.on("success", function (file, response) {
                        //append all the value to the hidden field, those value will be used for insertion
                       
                        if (response != "")
                        {
                            $('#hdimages').val(function (i, val) {
                                return val + (!val ? '' : ',') + response;
                            });
                        }
                    });


                    this.on("thumbnail", function (file) {
                        if (file.width < minImageWidth || file.height < minImageHeight) {
                            file.rejectDimensions()
                        }
                        else {
                            file.acceptDimensions();
                        }
                    });

                },
                accept: function (file, done) {
                    file.acceptDimensions = done;
                    file.rejectDimensions = function () {
                        done("Image size is  too small.");
                    };
                }
            }
        }
    };
}();